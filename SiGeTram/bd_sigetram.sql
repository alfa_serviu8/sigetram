USE [BD_Tramites]
GO
/****** Object:  User [usuario_interno]    Script Date: 05-04-2021 20:31:45 ******/
CREATE USER [usuario_interno] FOR LOGIN [usuario_interno] WITH DEFAULT_SCHEMA=[dbo]
GO
sys.sp_addrolemember @rolename = N'db_owner', @membername = N'usuario_interno'
GO
sys.sp_addrolemember @rolename = N'db_datareader', @membername = N'usuario_interno'
GO
sys.sp_addrolemember @rolename = N'db_datawriter', @membername = N'usuario_interno'
GO
/****** Object:  Table [dbo].[Documento]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Documento](
	[Id_Documento] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_Documento] [varchar](255) NULL,
	[Tipo_Contenido] [varchar](50) NULL,
	[Contenido] [varbinary](max) NULL,
	[Tamacno] [int] NULL,
 CONSTRAINT [PK_Documento] PRIMARY KEY CLUSTERED 
(
	[Id_Documento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estado_Expediente]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estado_Expediente](
	[Id_Estado_Expediente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_Estado_Expediente] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Estado_Expediente] PRIMARY KEY CLUSTERED 
(
	[Id_Estado_Expediente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estado_Proyecto]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estado_Proyecto](
	[Id_Estado_Proyecto] [int] NOT NULL,
	[Nombre_Estado_Proyecto] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Estado_Proyecto] PRIMARY KEY CLUSTERED 
(
	[Id_Estado_Proyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Expediente]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expediente](
	[Id_Expediente] [int] IDENTITY(1,1) NOT NULL,
	[Id_Tipo_Expediente] [int] NULL,
	[Descripcion] [varchar](250) NULL,
	[Id_Proyecto] [int] NULL,
	[Id_Estado_Expediente] [int] NOT NULL,
	[Id_Proceso] [int] NULL,
	[Id_Instancia_Proceso] [int] NULL,
	[Rut_Revisor] [int] NULL,
 CONSTRAINT [PK_Expediente] PRIMARY KEY CLUSTERED 
(
	[Id_Expediente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Expediente_Documento]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expediente_Documento](
	[Id_Expediente_Documento] [int] IDENTITY(1,1) NOT NULL,
	[Id_Expediente] [int] NOT NULL,
	[Id_Documento] [int] NOT NULL,
 CONSTRAINT [PK_Expediente_Documento] PRIMARY KEY CLUSTERED 
(
	[Id_Expediente_Documento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Expediente_Tarea]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expediente_Tarea](
	[Id_Expediente_Tarea] [int] IDENTITY(1,1) NOT NULL,
	[Id_Expediente] [int] NOT NULL,
	[Id_Tarea] [int] NOT NULL,
	[Id_Estado_Expediente] [int] NOT NULL,
	[Observaciones] [varchar](255) NULL,
 CONSTRAINT [PK_Tarea_Expediente] PRIMARY KEY CLUSTERED 
(
	[Id_Expediente_Tarea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Proyecto]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proyecto](
	[Id_Proyecto] [int] IDENTITY(1,1) NOT NULL,
	[Codigo_Proyecto] [varchar](20) NOT NULL,
	[Nombre_Proyecto] [varchar](250) NOT NULL,
	[Rut_Empresa] [int] NOT NULL,
	[Rut_Revisor] [int] NULL,
	[Id_Estado_Proyecto] [int] NOT NULL,
	[Id_Tipo_Subsidio] [int] NOT NULL,
	[Id_Tipologia] [int] NOT NULL,
	[Cantidad_Familias] [int] NOT NULL,
	[Id_Region] [int] NOT NULL,
	[Id_Provincia] [int] NOT NULL,
	[Id_Comuna] [int] NOT NULL,
	[Id_Tipo_Tramite] [int] NULL,
	[Id_Proceso] [int] NULL,
 CONSTRAINT [PK_Proyecto] PRIMARY KEY CLUSTERED 
(
	[Id_Proyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tipo_Expediente]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipo_Expediente](
	[Id_Tipo_Expediente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_Tipo_Expediente] [varchar](50) NOT NULL,
	[Vigente] [int] NOT NULL,
	[Id_Proceso] [int] NULL,
	[Id_Rol] [int] NULL,
 CONSTRAINT [PK_Tipo_Expediente] PRIMARY KEY CLUSTERED 
(
	[Id_Tipo_Expediente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tipo_Subsidio]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipo_Subsidio](
	[Id_Tipo_Subsidio] [int] IDENTITY(1,1) NOT NULL,
	[Codigo_Tipo_Subsidio] [varchar](20) NOT NULL,
	[Nombre_Tipo_Subsidio] [varchar](100) NOT NULL,
	[Vigente] [int] NOT NULL,
 CONSTRAINT [PK_Tipo_Subsidio] PRIMARY KEY CLUSTERED 
(
	[Id_Tipo_Subsidio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tipologia]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipologia](
	[Id_Tipologia] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_Tipologia] [varchar](50) NOT NULL,
	[Id_Tipo_Subsidio] [int] NOT NULL,
 CONSTRAINT [PK_Tipologia] PRIMARY KEY CLUSTERED 
(
	[Id_Tipologia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Expediente] ADD  CONSTRAINT [DF_Expediente_Descripcion]  DEFAULT ('') FOR [Descripcion]
GO
ALTER TABLE [dbo].[Tipo_Subsidio] ADD  CONSTRAINT [DF_Tipo_Subsidio_Vigente]  DEFAULT ((1)) FOR [Vigente]
GO
ALTER TABLE [dbo].[Expediente]  WITH CHECK ADD  CONSTRAINT [FK_Expediente_Estado_Expediente] FOREIGN KEY([Id_Estado_Expediente])
REFERENCES [dbo].[Estado_Expediente] ([Id_Estado_Expediente])
GO
ALTER TABLE [dbo].[Expediente] CHECK CONSTRAINT [FK_Expediente_Estado_Expediente]
GO
ALTER TABLE [dbo].[Expediente]  WITH CHECK ADD  CONSTRAINT [FK_Expediente_Proyecto] FOREIGN KEY([Id_Proyecto])
REFERENCES [dbo].[Proyecto] ([Id_Proyecto])
GO
ALTER TABLE [dbo].[Expediente] CHECK CONSTRAINT [FK_Expediente_Proyecto]
GO
ALTER TABLE [dbo].[Expediente]  WITH CHECK ADD  CONSTRAINT [FK_Expediente_Tipo_Expediente] FOREIGN KEY([Id_Tipo_Expediente])
REFERENCES [dbo].[Tipo_Expediente] ([Id_Tipo_Expediente])
GO
ALTER TABLE [dbo].[Expediente] CHECK CONSTRAINT [FK_Expediente_Tipo_Expediente]
GO
ALTER TABLE [dbo].[Expediente_Documento]  WITH CHECK ADD  CONSTRAINT [FK_Expediente_Documento_Documento] FOREIGN KEY([Id_Documento])
REFERENCES [dbo].[Documento] ([Id_Documento])
GO
ALTER TABLE [dbo].[Expediente_Documento] CHECK CONSTRAINT [FK_Expediente_Documento_Documento]
GO
ALTER TABLE [dbo].[Expediente_Documento]  WITH CHECK ADD  CONSTRAINT [FK_Expediente_Documento_Expediente] FOREIGN KEY([Id_Expediente])
REFERENCES [dbo].[Expediente] ([Id_Expediente])
GO
ALTER TABLE [dbo].[Expediente_Documento] CHECK CONSTRAINT [FK_Expediente_Documento_Expediente]
GO
ALTER TABLE [dbo].[Expediente_Tarea]  WITH CHECK ADD  CONSTRAINT [FK_Expediente_Tarea_Expediente] FOREIGN KEY([Id_Expediente])
REFERENCES [dbo].[Expediente] ([Id_Expediente])
GO
ALTER TABLE [dbo].[Expediente_Tarea] CHECK CONSTRAINT [FK_Expediente_Tarea_Expediente]
GO
ALTER TABLE [dbo].[Proyecto]  WITH CHECK ADD  CONSTRAINT [FK_Proyecto_Estado_Proyecto] FOREIGN KEY([Id_Estado_Proyecto])
REFERENCES [dbo].[Estado_Proyecto] ([Id_Estado_Proyecto])
GO
ALTER TABLE [dbo].[Proyecto] CHECK CONSTRAINT [FK_Proyecto_Estado_Proyecto]
GO
ALTER TABLE [dbo].[Proyecto]  WITH CHECK ADD  CONSTRAINT [FK_Proyecto_Tipo_Subsidio] FOREIGN KEY([Id_Tipo_Subsidio])
REFERENCES [dbo].[Tipo_Subsidio] ([Id_Tipo_Subsidio])
GO
ALTER TABLE [dbo].[Proyecto] CHECK CONSTRAINT [FK_Proyecto_Tipo_Subsidio]
GO
ALTER TABLE [dbo].[Proyecto]  WITH CHECK ADD  CONSTRAINT [FK_Proyecto_Tipologia] FOREIGN KEY([Id_Tipologia])
REFERENCES [dbo].[Tipologia] ([Id_Tipologia])
GO
ALTER TABLE [dbo].[Proyecto] CHECK CONSTRAINT [FK_Proyecto_Tipologia]
GO
/****** Object:  StoredProcedure [dbo].[Documento_Agregar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Documento_Agregar] 
	-- Add the parameters for the stored procedure here
	@Nombre_Documento  varchar(50),
	@Tipo_Contenido varchar(50),
	@Tamacno int,
	@Contenido varbinary(max),
	@id_expediente int,
	@id_documento int out

	


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	

	Insert into Documento(Nombre_Documento,Tipo_Contenido,Tamacno,Contenido)
	values(@Nombre_Documento,@Tipo_Contenido,@Tamacno,@Contenido)

	select @Id_documento = @@IDENTITY

	execute dbo.Expediente_Documento_Agregar @id_expediente,@id_documento
END
GO
/****** Object:  StoredProcedure [dbo].[Documento_Eliminar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Documento_Eliminar] 
	-- Add the parameters for the stored procedure here
	@Id_Documento int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Delete Expediente_Documento 
	where  Id_Documento = @id_documento


	Delete Documento
	where Id_Documento=@id_documento
    -- Insert statements for procedure here
	

End

GO
/****** Object:  StoredProcedure [dbo].[Documento_Listado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Documento_Listado] 
	-- Add the parameters for the stored procedure here
	@Id_Expediente int, 
	@pagina_solicitada int,
	@tamacno_pagina  int,
	@total_registros int out
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 Select @total_Registros = count(*)
FROM        Documento AS d inner join Expediente_Documento as ed on (d.id_documento=ed.Id_Documento)
				                           
                   
where 

(@id_expediente = -1 or ed.id_expediente=@id_expediente) 


DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Bd_Comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


 SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY d.Id_Documento desc) as RowNum,
	d.Id_Documento,
	d.Nombre_Documento,
	d.Tipo_Contenido,
	d.Tamacno,
	ed.id_Expediente,
	e.Id_Estado_Expediente
	
	
	
	
	FROM Documento AS d inner join Expediente_Documento as ed on (d.id_documento=ed.Id_Documento) inner join Expediente as e on (ed.Id_Expediente=e.Id_Expediente)
				
	Where 
		(@id_expediente = -1 or ed.id_expediente=@id_expediente) 

	
		
		
		) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final

	
	
END



GO
/****** Object:  StoredProcedure [dbo].[Documento_PorId]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Documento_PorId] 
	-- Add the parameters for the stored procedure here
	@Id_Documento int
	as
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT d.Contenido,d.Tipo_Contenido
	From Documento as d
	Where d.id_documento=@Id_Documento
END

GO
/****** Object:  StoredProcedure [dbo].[Empresa_Actualizar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Empresa_Actualizar]
	@Rut_Empresa int,
	@Es_Empresa_Patrocinante int,
	@Razon_Social varchar(250),
	@Nombre_Fantasia varchar(250),
	@Email_Particular varchar(50),
	@Pass_Sistemas varchar(25),
	@Fono_Fijo int,
	@Celular int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	update bd_comun_sistemas.dbo.cliente 
	set
		Razon_Social=@Razon_Social,
		Nombre_Fantasia=@Nombre_Fantasia,
		Email_Particular=@Email_Particular,
		Pass_Sistemas=@Pass_Sistemas,
		Fono_Fijo=@Fono_Fijo,
		Celular=@Celular,
		Es_Empresa_Patrocinante=@Es_Empresa_Patrocinante

	where Rut=@Rut_Empresa
	
END

GO
/****** Object:  StoredProcedure [dbo].[Empresa_Agregar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Empresa_Agregar] 
	-- Add the parameters for the stored procedure here
	@Rut_Empresa int,
	@Dv  varchar(1),
	@Razon_Social varchar(250),
	@Nombre_Fantasia varchar(250),
	@Email_Particular varchar(50),
	@Fono_Fijo int,
	@Celular int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Ingresa docu,ento -1 significa pendiente de aprobar 
    -- Insert statements for procedure here
	
	insert bd_comun_sistemas.dbo.Cliente (Rut,Dv,Razon_Social,Nombre_Fantasia,Email_Particular,Fono_Fijo,Celular,Id_Comuna,Es_Empresa_Patrocinante)
	values(@Rut_Empresa,@Dv,@Razon_Social,@Nombre_Fantasia,@Email_Particular,@Fono_Fijo,@Celular,8101,1)
	end
	
GO
/****** Object:  StoredProcedure [dbo].[Empresa_Listado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Empresa_Listado] 
	-- Add the parameters for the stored procedure here
	@Rut_Empresa int, 
	@Es_Empresa_Patrocinante int, 
	@Nombre_Empresa varchar(500),
	@pagina_Solicitada int,
	@tamacno_pagina  int,
	@total_registros int out
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
Select @total_Registros = count(*)
FROM BD_COMUN_SISTEMAS.dbo.Cliente AS c 
                   
where 
 (@Es_Empresa_Patrocinante = -1 or c.Es_Empresa_Patrocinante=@Es_Empresa_Patrocinante) and 
(@Rut_Empresa = 0 or c.Rut=@Rut_Empresa) and 
((@Nombre_Empresa = '' or c.Razon_Social like '%' + @Nombre_Empresa + '%') or
(@Nombre_Empresa = '' or c.Nombre_Fantasia like '%' + @Nombre_Empresa + '%'))


DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Bd_Comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


 SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY c.Razon_Social asc) as RowNum,
	c.Rut as Rut_Empresa,
	c.Dv,
	isnull(c.Razon_Social,'') as Razon_Social,
	isnull(c.Nombre_Fantasia,'') as Nombre_Fantasia,
	isnull(c.Razon_Social + isnull('/' + c.Nombre_Fantasia, ''), isnull(c.Nombre_Fantasia, '')) as Nombre_Empresa,
	isnull(c.Empresa_Rol,'') as Rol,
	isnull(c.Empresa_Vigente_en_Registro_Tecnico,0) as Vigente,
	isnull(c.Email_Particular,'') as Email_Particular,
	isnull(c.Es_Empresa_Patrocinante,0) as Es_Empresa_Patrocinante,
	isnull(c.Pass_Sistemas,'') as Clave,
	isnull(c.Fono_Fijo,0) as Fono_Fijo,
	isNull(c.Celular,0) as Celular
	
	

	FROM BD_COMUN_SISTEMAS.dbo.Cliente AS c 
                   
where 
 (@Es_Empresa_Patrocinante = -1 or c.Es_Empresa_Patrocinante=@Es_Empresa_Patrocinante) and
(@Rut_Empresa = 0 or c.Rut=@Rut_Empresa) and 
((@Nombre_Empresa = '' or c.Razon_Social like '%' + @Nombre_Empresa + '%') or
(@Nombre_Empresa = '' or c.Nombre_Fantasia like '%' + @Nombre_Empresa + '%'))
	
		) as lista 
		
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
	
END
GO
/****** Object:  StoredProcedure [dbo].[Estado_Expediente_Listado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Estado_Expediente_Listado] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Id_Estado_Expediente,Nombre_Estado_Expediente
	from Estado_Expediente 
	order by Id_Estado_Expediente
	
END


GO
/****** Object:  StoredProcedure [dbo].[Estado_Proyecto_Listado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Estado_Proyecto_Listado] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Id_Estado_Proyecto,Nombre_Estado_Proyecto
	from Estado_Proyecto 
	order by Id_Estado_Proyecto
	
END

GO
/****** Object:  StoredProcedure [dbo].[Estado_Proyecto_Listado_Paginado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Estado_Proyecto_Listado_Paginado] 
	-- Add the parameters for the stored procedure here

	@Id_Estado_Proyecto		int,
	@Nombre_Estado_Proyecto   varchar(50),
	@Pagina_Solicitada  int,
	@Tamacno_Pagina     int,
	@total_Registros    int Output	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @fila_Inicial int
declare @fila_Final int   


select @total_Registros = count(*)
from   Estado_Proyecto
where	(@Id_Estado_Proyecto = 0 or Id_Estado_Proyecto = @Id_Estado_Proyecto) and
		(@Nombre_Estado_Proyecto = '' or Nombre_Estado_Proyecto like '%' + @Nombre_Estado_Proyecto + '%') 

Exec bd_comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY Id_Estado_Proyecto) as RowNum,
Id_Estado_Proyecto,Nombre_Estado_Proyecto
from   Estado_Proyecto
where	(@Id_Estado_Proyecto = 0 or Id_Estado_Proyecto = @Id_Estado_Proyecto) and
		(@Nombre_Estado_Proyecto = '' or Nombre_Estado_Proyecto like '%' + @Nombre_Estado_Proyecto + '%') 
) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
    
END

GO
/****** Object:  StoredProcedure [dbo].[Expediente_Actualizar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Expediente_Actualizar] 
	-- Add the parameters for the stored procedure here
	@Id_Expediente int,
	@Id_Tipo_Expediente int, 
	@Descripcion varchar(250),
	@Id_Estado_Expediente int,
	@Id_Proceso int,
	@Rut_Revisor int,
	@Id_Instancia_Proceso int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update Expediente 
	set
	Id_Tipo_Expediente=@Id_Tipo_Expediente,
	Descripcion=@Descripcion,
	Id_Estado_Expediente=@Id_Estado_Expediente,
	Id_Proceso=@Id_Proceso,
	Rut_Revisor=@Rut_Revisor,
	Id_Instancia_Proceso=@Id_Instancia_Proceso

	where 
	Id_Expediente=@Id_Expediente
END

GO
/****** Object:  StoredProcedure [dbo].[Expediente_Agregar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Expediente_Agregar]
	-- Add the parameters for the stored procedure here
	
	@Id_Tipo_Expediente int,
	@Descripcion varchar(250),
	@Id_Proyecto int,
	@Id_Proceso int,
	@Id_Expediente int out

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	insert into Expediente (Id_Tipo_Expediente,Descripcion,Id_Proyecto,Id_Estado_Expediente,Id_Proceso)
	values (@Id_Tipo_Expediente,@Descripcion,@Id_Proyecto,1,@Id_Proceso)
	
	select @Id_Expediente = @@IDENTITY

END

GO
/****** Object:  StoredProcedure [dbo].[Expediente_Documento_Agregar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Expediente_Documento_Agregar] 
	-- Add the parameters for the stored procedure here
	@Id_Expediente int ,
	@Id_Documento int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into Expediente_documento (Id_Expediente,Id_Documento)
	values (@id_Expediente,@Id_Documento)
END

GO
/****** Object:  StoredProcedure [dbo].[Expediente_Documento_Eliminar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Expediente_Documento_Eliminar] 
	-- Add the parameters for the stored procedure here
	@Id_Expediente_Documento int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Delete Expediente_Documento
	Where Id_Expediente_Documento = @Id_Expediente_Documento
END
GO
/****** Object:  StoredProcedure [dbo].[Expediente_Eliminar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Expediente_Eliminar] 
	-- Add the parameters for the stored procedure here
	@Id_Expediente int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Delete Expediente
	where id_expediente=@id_expediente
END
GO
/****** Object:  StoredProcedure [dbo].[Expediente_Listado_Paginado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Expediente_Listado_Paginado] 

	@Id_Expediente int,
	@Rut_Revisor int,
	@Id_Proyecto int,
	@Id_Tipo_Expediente int,
	@Id_Estado_Expediente int,
	@Id_Proceso int,
	@Rut_Empresa int,
	@Descripcion varchar(250),
	@pagina_Solicitada int,
	@tamacno_pagina  int,
	@total_registros int out
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 Select @total_Registros = count(*)

FROM Expediente as e 
LEFT JOIN Tipo_Expediente as te on (e.Id_Tipo_Expediente=te.Id_Tipo_Expediente) 
LEFT JOIN Estado_Expediente as ee on (e.Id_Estado_Expediente=ee.Id_Estado_Expediente) 
INNER JOIN Proyecto as p  ON (e.Id_Proyecto = p.Id_Proyecto) 

where 
 
(@Id_Expediente = 0 or Id_Expediente = @Id_Expediente) and
(@Id_Proyecto = 0 or e.Id_Proyecto = @Id_Proyecto) and
(@Id_Proceso = 0 or p.Id_Proceso = @Id_Proceso) and
(@Rut_Revisor = 0 or e.Rut_Revisor = @Rut_Revisor) and
(@Id_Tipo_Expediente = 0 or e.Id_Tipo_Expediente = @Id_Tipo_Expediente) and 
(@Id_Estado_Expediente = 0 or e.Id_Estado_Expediente = @Id_Estado_Expediente) and 
(@Rut_Empresa = 0 or p.Rut_Empresa = @Rut_Empresa) and 
((@Descripcion = '' or e.Descripcion like '%' + @Descripcion + '%') or
(@Descripcion = '' or p.Nombre_Proyecto like '%' + @Descripcion + '%') or
(@Descripcion = '' or p.Codigo_Proyecto like '%' + @Descripcion + '%'))

DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Bd_Comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY p.Nombre_proyecto,e.id_expediente,e.Id_Tipo_Expediente desc) as RowNum,
	e.Id_Expediente,
	isNull(e.Id_Tipo_Expediente,0) as Id_Tipo_Expediente,
	e.Id_Estado_Expediente,
	isNull(e.Descripcion,'') as Descripcion,
	e.Id_Proyecto,
	p.Id_Proceso,
	isnull(e.Id_Instancia_Proceso,0) as Id_Instancia_Proceso,
	isnull(ipr.Fecha_Inicio,'19000101') as Fecha_Envio,
	isNull(te.Nombre_Tipo_Expediente,'') as Nombre_Tipo_Expediente,
	ee.Nombre_Estado_Expediente,
	isnull(e.Rut_Revisor,0) as Rut_Revisor,
	isnull((rs.Nombres + ' ' + rs.Paterno + ' ' +  rs.Materno),'') as Nombre_Revisor,
	p.Codigo_Proyecto,
	p.Nombre_Proyecto,
	isnull(pr.Nombre_Proceso,'') as Nombre_Proceso,
	p.Rut_Empresa,
	isnull(emp.Razon_Social,'/') as Razon_Social_Empresa,
	isnull(Emp.Razon_Social + isnull('/' + Emp.Nombre_Fantasia, ''), isnull(Emp.Nombre_Fantasia, '')) as Nombre_Empresa,
	(select count(Id_Documento) from Expediente_Documento as ed where ed.Id_Expediente=e.Id_Expediente) as total_documentos
	
	
	
	

FROM Expediente as e 
LEFT JOIN Tipo_Expediente as te on (e.Id_Tipo_Expediente=te.Id_Tipo_Expediente) 
LEFT JOIN Estado_Expediente as ee on (e.Id_Estado_Expediente=ee.Id_Estado_Expediente) 
INNER JOIN Proyecto as p  ON (e.Id_Proyecto = p.Id_Proyecto)
left join BD_COMUN_SISTEMAS.dbo.Proceso as pr on (p.Id_Proceso=pr.Id_Proceso)
left join BD_COMUN_SISTEMAS.dbo.Instancia_Proceso as ipr on (e.Id_Instancia_Proceso=ipr.Id_Instancia_Proceso)
inner Join BD_COMUN_SISTEMAS.dbo.Cliente Emp ON (emp.Rut = p.Rut_Empresa)  
left JOIN BD_COMUN_SISTEMAS.dbo.Cliente rs ON e.Rut_Revisor = rs.Rut


where 

(@Id_Expediente = 0 or Id_Expediente = @Id_Expediente) and
(@Id_Proyecto = 0 or e.Id_Proyecto = @Id_Proyecto) and
(@Id_Proceso = 0 or p.Id_Proceso = @Id_Proceso) and
(@Rut_Revisor = 0 or e.Rut_Revisor = @Rut_Revisor) and
(@Id_Tipo_Expediente = 0 or e.Id_Tipo_Expediente = @Id_Tipo_Expediente) and 
(@Id_Estado_Expediente = 0 or e.Id_Estado_Expediente = @Id_Estado_Expediente) and 
(@Rut_Empresa = 0 or p.Rut_Empresa = @Rut_Empresa) and 
((@Descripcion = '' or e.Descripcion like '%' + @Descripcion + '%') or
(@Descripcion = '' or p.Nombre_Proyecto like '%' + @Descripcion + '%') or
(@Descripcion = '' or p.Codigo_Proyecto like '%' + @Descripcion + '%'))
	
		) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
	
END




GO
/****** Object:  StoredProcedure [dbo].[Expediente_Tarea_Actualizar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Expediente_Tarea_Actualizar]
	-- Add the parameters for the stored procedure here
	@Id_Expediente_Tarea int, 
	@Id_Estado_Expediente int,
	@Observaciones varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Expediente_Tarea set Observaciones=@Observaciones,
	Id_Estado_Expediente=@Id_Estado_Expediente
	Where Id_Expediente_Tarea=@Id_Expediente_Tarea
END
GO
/****** Object:  StoredProcedure [dbo].[Expediente_Tarea_Agregar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Expediente_Tarea_Agregar] 
	-- Add the parameters for the stored procedure here
	@Id_Expediente int, 
	@Id_Tarea int,
	@Id_Estado_Expediente int,
	@Observaciones varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into Expediente_Tarea(Id_Expediente,Id_Tarea,Id_Estado_Expediente,Observaciones)
	values(@Id_Expediente,@Id_Tarea,@Id_Estado_Expediente,@Observaciones)
END
GO
/****** Object:  StoredProcedure [dbo].[Expediente_Tarea_Listado_Paginado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Expediente_Tarea_Listado_Paginado] 
	-- Add the parameters for the stored procedure here
	@Id_Expediente int, 
	@Vigente int,
	@pagina_Solicitada int,
	@tamacno_pagina  int,
	@total_registros int out
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 Select @total_Registros = count(*)

FROM Expediente as e inner join Expediente_Tarea as et on (e.Id_Expediente=et.id_expediente)
inner join Estado_Expediente as ee on (e.Id_Estado_Expediente=ee.Id_Estado_Expediente) 
INNER JOIN Bd_Comun_Sistemas.dbo.Tarea as t  ON (et.Id_Tarea = t.Id_Tarea) 

where 

(@Id_Expediente = 0 or et.Id_Expediente = @Id_Expediente) and
(@Vigente = -1 or t.Vigente = @Vigente) 




DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Bd_Comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY e.Id_Expediente desc) as RowNum,
	e.Id_Expediente,
	e.Id_Tipo_Expediente,
	e.Descripcion,
	te.Nombre_Tipo_Expediente,
	et.Id_Expediente_Tarea,
	et.Id_Estado_Expediente,
	ee.Nombre_Estado_Expediente,
	et.Observaciones,
	t.Id_Tarea,
	t.Id_Tipo_Tarea,
	tt.Nombre_Simbolo as Nombre_Tipo_Tarea,
	isnull(t.Rut_Asignado,0) as Rut_Asignado,
	isnull(e.Rut_Revisor,0) as Rut_Revisor,
	t.Fecha_Inicio,
	isnull(t.Fecha_Termino,'19000101') as Fecha_Termino,
	t.Vigente,
	p.Id_Tipo_Tramite


FROM Expediente as e inner join Expediente_Tarea as et on (e.Id_Expediente=et.id_expediente)
inner join Proyecto as p on (p.Id_Proyecto= e.Id_Proyecto)
inner join Estado_Expediente as ee on (e.Id_Estado_Expediente=ee.Id_Estado_Expediente) 
inner join Tipo_Expediente as te on (e.Id_Tipo_Expediente=te.Id_Tipo_Expediente)
INNER JOIN Bd_Comun_Sistemas.dbo.Tarea as t  ON (et.Id_Tarea = t.Id_Tarea) 
inner join Bd_Comun_Sistemas.dbo.Simbolo  as  tt on (t.Id_Tipo_Tarea=tt.Id_Simbolo)

where 

(@Id_Expediente = 0 or et.Id_Expediente = @Id_Expediente) and
(@Vigente = -1 or t.Vigente = @Vigente)) 
 as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
	
END



GO
/****** Object:  StoredProcedure [dbo].[Proyecto_Actualizar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Proyecto_Actualizar] 
	-- Add the parameters for the stored procedure here

	@Id_Proyecto int,
	@Codigo_Proyecto varchar(20),
	@Nombre_Proyecto varchar(250), 
	@Rut_Empresa int,
	@Id_Estado_Proyecto int,
	@Id_Proceso int,
	@Id_Tipo_Subsidio int,
	@Id_Tipologia int,
	@Cantidad_Familias int,
	@Id_Region int,
	@Id_Provincia int,	
	@Id_Comuna int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update Proyecto 
	set
	Codigo_Proyecto=@Codigo_Proyecto,
	Nombre_Proyecto=@Nombre_Proyecto,
	Rut_Empresa=@Rut_Empresa,
	Id_Estado_Proyecto=@Id_Estado_Proyecto,
	Id_Proceso=@Id_Proceso,
	Id_Tipo_Subsidio=@Id_Tipo_Subsidio,
	Id_Tipologia=@Id_Tipologia,
	Cantidad_Familias=@Cantidad_Familias, 
	Id_Region=@Id_Region,
	Id_Provincia=@Id_Provincia,
	Id_Comuna=@Id_Comuna
	where Id_Proyecto=@Id_Proyecto
	
END

GO
/****** Object:  StoredProcedure [dbo].[Proyecto_Agregar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Proyecto_Agregar] 
	-- Add the parameters for the stored procedure here
	
	@Codigo_Proyecto varchar(20),
	@Nombre_Proyecto varchar(250), 
	@Rut_Empresa int,
	@Id_Estado_Proyecto int,
	@Id_Proceso int,
	@Id_Tipo_Subsidio int,
	@Id_Tipologia int,
	@Cantidad_Familias int,
	@Id_Region int,
	@Id_Provincia int,
	@Id_Comuna int,
	@Id_proyecto int out
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	
    -- Insert statements for procedure here
    
	insert into Proyecto (Codigo_Proyecto,Nombre_Proyecto,Rut_Empresa,Id_Estado_Proyecto,Id_Tipo_Subsidio,Id_Tipologia,Cantidad_Familias,Id_Region,Id_Provincia,Id_Comuna,Id_Proceso)
	values(@Codigo_Proyecto,@Nombre_Proyecto,@Rut_Empresa,@Id_Estado_Proyecto,@Id_Tipo_Subsidio,@Id_Tipologia,@Cantidad_Familias,@Id_Region,@Id_Provincia,@Id_Comuna,@Id_Proceso)
	
	select @Id_Proyecto = @@IDENTITY
END


GO
/****** Object:  StoredProcedure [dbo].[Proyecto_Listado_Paginado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Proyecto_Listado_Paginado] 
	
	@Id_Proyecto int,
	@Codigo_Proyecto varchar(20),
	@Nombre_Proyecto varchar(250),
	@Rut_Empresa int,
	@Nombre_Empresa varchar(500),
	@Id_Estado_Proyecto int,
	@Id_Tipo_Subsidio int,
	@Id_Tipologia int,
	@Id_Proceso int,
	@Id_Comuna int,
	@pagina_Solicitada int,
	@tamacno_pagina  int,
	@total_registros int out
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Select @total_Registros = count(*)

FROM Proyecto p
INNER JOIN Estado_Proyecto ep on p.Id_Estado_Proyecto = ep.Id_Estado_Proyecto 
INNER JOIN Tipo_Subsidio ts on p.Id_Tipo_Subsidio = ts.Id_Tipo_Subsidio 
LEFT OUTER JOIN BD_COMUN_SISTEMAS.dbo.Cliente Empresa ON empresa.Rut = p.Rut_Empresa 
LEFT OUTER JOIN BD_COMUN_SISTEMAS.dbo.Comuna ON p.Id_Comuna = BD_COMUN_SISTEMAS.dbo.Comuna.Id_Comuna 
LEFT OUTER JOIN BD_COMUN_SISTEMAS.dbo.Provincia ON BD_COMUN_SISTEMAS.dbo.Comuna.Id_Provincia = Provincia.Id_Provincia 
LEFT OUTER JOIN BD_COMUN_SISTEMAS.dbo.Region ON Provincia.Id_Region = BD_COMUN_SISTEMAS.dbo.Region.Id_Region


where	(@Id_Proyecto = 0 or p.Id_Proyecto=@Id_Proyecto) and
	    (@Rut_Empresa = 0 or Empresa.Rut=@Rut_Empresa) and
		(@Id_Estado_Proyecto = 0 or p.Id_Estado_Proyecto=@Id_Estado_Proyecto) and 
		(@Id_Tipo_Subsidio = 0 or p.Id_Tipo_Subsidio=@Id_Tipo_Subsidio) and 		
		(@Id_Tipologia = 0 or p.Id_Tipologia=@Id_Tipologia) and 
		(@Id_Proceso = 0 or p.Id_Proceso=@Id_Proceso) and 	
		(@Id_Comuna = 0 or p.Id_Comuna=@Id_Comuna) and
		((@Nombre_Empresa = '' or Empresa.Razon_Social like '%' + @Nombre_Empresa + '%') or
		(@Nombre_Empresa = '' or p.Codigo_Proyecto like '%' + @Nombre_Empresa + '%') or 
		(@Nombre_Empresa = '' or p.Nombre_Proyecto like '%' + @Nombre_Empresa + '%') or
	    (@Nombre_Empresa = '' or Empresa.Nombre_Fantasia like '%' + @Nombre_Empresa + '%')) and
		(@Codigo_Proyecto = '' or p.Codigo_Proyecto  = @Codigo_Proyecto) and
		(@Nombre_Proyecto = '' or p.Nombre_Proyecto like '%' + @Nombre_Proyecto + '%')
		
		
		

DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Bd_Comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


 SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY p.Rut_Empresa,p.codigo_proyecto desc) as RowNum,
	
p.Id_Proyecto
,p.Codigo_Proyecto
,p.Nombre_Proyecto
,isnull(Empresa.Rut,0) as Rut_Empresa
,isnull(Empresa.Razon_Social + isnull('/' + Empresa.Nombre_Fantasia, ''), isnull(Empresa.Nombre_Fantasia, '')) as Nombre_Empresa
,ep.Id_Estado_Proyecto 
,Nombre_Estado_Proyecto
,P.Id_Tipo_Subsidio 
,ts.Nombre_Tipo_Subsidio
,P.Id_Tipologia 
,t.Nombre_Tipologia
,p.Id_Comuna
,Comuna.Nombre_Comuna
,Provincia.Id_Provincia 
,Provincia.Nombre_Provincia 
,Region.Id_Region  
,Region.Nombre_Region
,p.Cantidad_Familias
,p.Id_Proceso
,pr.Nombre_Proceso




FROM Proyecto p
INNER JOIN Estado_Proyecto ep on p.Id_Estado_Proyecto = ep.Id_Estado_Proyecto 
INNER JOIN Tipo_Subsidio ts on p.Id_Tipo_Subsidio = ts.Id_Tipo_Subsidio 
INNER JOIN Tipologia t on p.Id_Tipologia = t.Id_Tipologia 
Inner join BD_COMUN_SISTEMAS.dbo.Proceso as pr ON pr.Id_Proceso = p.Id_Proceso 
LEFT OUTER JOIN BD_COMUN_SISTEMAS.dbo.Cliente Empresa ON empresa.Rut = p.Rut_Empresa 
LEFT OUTER JOIN BD_COMUN_SISTEMAS.dbo.Comuna ON p.Id_Comuna = BD_COMUN_SISTEMAS.dbo.Comuna.Id_Comuna 
LEFT OUTER JOIN BD_COMUN_SISTEMAS.dbo.Provincia ON BD_COMUN_SISTEMAS.dbo.Comuna.Id_Provincia = Provincia.Id_Provincia 
LEFT OUTER JOIN BD_COMUN_SISTEMAS.dbo.Region ON Provincia.Id_Region = BD_COMUN_SISTEMAS.dbo.Region.Id_Region

 

where	(@Id_Proyecto = 0 or p.Id_Proyecto=@Id_Proyecto) and

		(@Rut_Empresa = 0 or Empresa.Rut=@Rut_Empresa) and
		(@Id_Estado_Proyecto = 0 or p.Id_Estado_Proyecto=@Id_Estado_Proyecto) and 
		(@Id_Tipo_Subsidio = 0 or p.Id_Tipo_Subsidio=@Id_Tipo_Subsidio) and 		
		(@Id_Tipologia = 0 or p.Id_Tipologia=@Id_Tipologia) and 
		(@Id_Proceso = 0 or p.Id_Proceso=@Id_Proceso) and 	
		(@Id_Comuna = 0 or p.Id_Comuna=@Id_Comuna) and
		((@Nombre_Empresa = '' or Empresa.Razon_Social like '%' + @Nombre_Empresa + '%') or
		(@Nombre_Empresa = '' or p.Codigo_Proyecto like '%' + @Nombre_Empresa + '%') or 
		(@Nombre_Empresa = '' or p.Nombre_Proyecto like '%' + @Nombre_Empresa + '%') or
		(@Nombre_Empresa = '' or Empresa.Nombre_Fantasia like '%' + @Nombre_Empresa + '%')) and
		(@Codigo_Proyecto = '' or p.Codigo_Proyecto  = @Codigo_Proyecto)
		and
		(@Nombre_Proyecto = '' or p.Nombre_Proyecto like '%' + @Nombre_Proyecto + '%') 	
	
		) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
	
END

GO
/****** Object:  StoredProcedure [dbo].[Rol_Actualizar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Actualizar] 
	-- Add the parameters for the stored procedure here

	@Id_Rol int,
	@Nombre_Rol varchar(100),
	@Habilitado int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	update BD_COMUN_SISTEMAS.dbo.Rol
	set Nombre_Rol=@Nombre_Rol,Habilitado=@Habilitado
	where Id_Rol=@Id_Rol
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Agregar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Agregar] 
	-- Add the parameters for the stored procedure here

	@Nombre_Rol varchar(100),
	@Habilitado int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into BD_COMUN_SISTEMAS.dbo.rol (Nombre_Rol,Habilitado)
	values (@Nombre_Rol,@Habilitado)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Listado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Listado]
@id_sistema int,
@id_servicio int,
@pagina_Solicitada int,
@tamacno_pagina  int,
@total_registros int out
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
Select @total_Registros = count(*)
FROM BD_COMUN_SISTEMAS.dbo.Cliente AS c 

Select	@total_Registros = count(*)
	From Bd_Comun_sistemas.dbo.Rol_Sistema as r 
	Where (@id_sistema = 0 or r.id_sistema=@id_sistema) and 
		  (@id_servicio = 0 or r.id_servicio=@id_servicio) 



DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Bd_Comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


 SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY r.Nombre_Rol asc) as RowNum,
	rs.Id_Rol,
	rs.Id_Rol_Sistema,
	rs.Id_Servicio,
	r.Nombre_Rol
		
	FROM Bd_Comun_sistemas.dbo.rol_sistema as rs inner join
	Bd_Comun_sistemas.dbo.rol as r on (rs.id_rol = r.Id_Rol) 
	
					
	Where 
	
	
		(@id_sistema = 0 or rs.id_sistema=@id_sistema) and 
		(@id_servicio = 0 or rs.id_servicio=@id_servicio)  
	
                   
		) as lista 
		
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Usuario_Agregar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Usuario_Agregar] 
	-- Add the parameters for the stored procedure here

	@Id_Rol int, 
	@Id_Sistema int,
	@Id_Servicio int,
	@Rut_Usuario int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	insert BD_COMUN_SISTEMAS.dbo.Rol_Usuario (Id_Rol,id_Sistema,id_servicio,Rut_Usuario)
	values (@Id_Rol,@Id_sistema,@id_servicio,@Rut_Usuario)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Usuario_Eliminar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Usuario_Eliminar] 
	-- Add the parameters for the stored procedure here
	@Id_Rol_Usuario int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	delete BD_COMUN_SISTEMAS.dbo.Rol_Usuario
	where Id_Rol_Usuario=@Id_Rol_Usuario
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Usuario_Listado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Usuario_Listado] 
	-- Add the parameters for the stored procedure here
@id_sistema int,
@id_servicio int,
@id_rol  int,
@rut_usuario  int,
@pagina_Solicitada int,
@tamacno_pagina  int,
@total_registros int out
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
Select @total_Registros = count(*)
FROM BD_COMUN_SISTEMAS.dbo.Cliente AS c 

Select	@total_Registros = count(*)
	From BD_COMUN_SISTEMAS.dbo.Rol_Usuario as ru 
	Where (@id_sistema = 0 or ru.id_sistema=@id_sistema) and 
		 (@id_servicio = 0 or ru.id_servicio=@id_servicio)  and 
		 (@id_rol = 0 or ru.id_rol=@id_rol)  and
		 (@rut_usuario = 0 or ru.Rut_Usuario=@rut_usuario)
                   



DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Bd_Comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


 SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY cl.Paterno) as RowNum,
	ru.Id_Rol,
	ru.Id_Rol_Usuario,
	ru.Id_Servicio,
	ru.Id_Sistema,
	ru.Rut_Usuario,
	(cl.Nombres + ' ' + cl.Paterno + ' ' +  cl.Materno) as Nombre_Completo	,
	f.Email,
	r.Nombre_Rol,
	cl.Id_Dependencia
	
	FROM Bd_Comun_sistemas.dbo.rol_Usuario as ru left join 
		Bd_Comun_sistemas.dbo.rol as r  on(ru.Id_Rol=r.Id_Rol) inner join
		 BD_COMUN_SISTEMAS.dbo.Cliente cl ON ru.Rut_Usuario = cl.Rut inner join
		BD_COMUN_SISTEMAS.dbo.Funcionario f ON f.Rut = cl.Rut 		
	Where 
		
		 (@id_sistema = 0 or ru.id_sistema=@id_sistema) and 
		 (@id_servicio = 0 or ru.id_servicio=@id_servicio)  and 
		 (@id_rol = 0 or ru.id_rol=@id_rol)  and
		 (@rut_usuario = 0 or ru.Rut_Usuario=@rut_usuario)
                   
		) as lista 
		
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
	
END
GO
/****** Object:  StoredProcedure [dbo].[Tipo_Expediente_Actualizar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipo_Expediente_Actualizar] 
	-- Add the parameters for the stored procedure here
	
	@Id_Tipo_Expediente int,
	@Nombre_Tipo_Expediente varchar(50),
	@Vigente int,
	@Id_Proceso int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	update Tipo_Expediente 
	set
	Nombre_Tipo_Expediente=@Nombre_Tipo_Expediente,
	Vigente=@Vigente,
	Id_Proceso=@Id_Proceso
	where Id_Tipo_Expediente=@Id_Tipo_Expediente
	
END

GO
/****** Object:  StoredProcedure [dbo].[Tipo_Expediente_Agregar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipo_Expediente_Agregar] 
	-- Add the parameters for the stored procedure here
	
	@Nombre_Tipo_Expediente varchar(50),
	@Vigente int,
	@Id_Proceso int

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--declare @Id_Tipo_Subsidio int
	
    -- Insert statements for procedure here
    
	insert into Tipo_Expediente (Nombre_Tipo_Expediente,Vigente,Id_Proceso)
	values(@Nombre_Tipo_Expediente,@Vigente,@Id_Proceso)
	

END

GO
/****** Object:  StoredProcedure [dbo].[Tipo_Expediente_Eliminar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipo_Expediente_Eliminar] 
	-- Add the parameters for the stored procedure here
	
	@Id_Tipo_Expediente int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	delete from Tipo_Expediente where Id_Tipo_Expediente=@Id_Tipo_Expediente
	
END

GO
/****** Object:  StoredProcedure [dbo].[Tipo_Expediente_Listado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipo_Expediente_Listado] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Id_Tipo_Expediente,Nombre_Tipo_Expediente
	from Tipo_Expediente 
	order by Id_Tipo_Expediente
	
END
GO
/****** Object:  StoredProcedure [dbo].[Tipo_Expediente_Listado_Paginado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipo_Expediente_Listado_Paginado] 
	-- Add the parameters for the stored procedure here

	@Id_Tipo_Expediente		int,
	@Nombre_Tipo_Expediente varchar(50),
	@Vigente int,
	@Id_Proceso int,
	@Pagina_Solicitada int,
	@Tamacno_Pagina int,
	@total_Registros int Output	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @fila_Inicial int
declare @fila_Final int   


select @total_Registros = count(*)
from   Tipo_Expediente 
where	(@Id_Tipo_Expediente = 0 or Id_Tipo_Expediente = @Id_Tipo_Expediente) and
		(@Id_Proceso = 0 or Id_Proceso = @Id_Proceso) and
		(@Nombre_Tipo_Expediente = '' or Nombre_Tipo_Expediente like '%' + @Nombre_Tipo_Expediente + '%') and
		(@Vigente = -1 or Vigente = @Vigente)


Exec bd_comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY te.Id_Tipo_Expediente) as RowNum,
te.Id_Tipo_Expediente,
te.Nombre_Tipo_Expediente,
te.Vigente,
te.Id_Proceso,
p.Nombre_Proceso
from   Tipo_Expediente  as te inner join BD_COMUN_SISTEMAS.dbo.Proceso  as p on (te.Id_Proceso=p.Id_Proceso)
where	(@Id_Tipo_Expediente = 0 or te.Id_Tipo_Expediente = @Id_Tipo_Expediente) and
		(@Id_Proceso = 0 or te.Id_Proceso = @Id_Proceso) and
	    (@Nombre_Tipo_Expediente = '' or te.Nombre_Tipo_Expediente like '%' + @Nombre_Tipo_Expediente + '%') and
		(@Vigente = -1 or te.Vigente = @Vigente)
) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
    
END

GO
/****** Object:  StoredProcedure [dbo].[Tipo_Subsidio_Actualizar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipo_Subsidio_Actualizar] 
	-- Add the parameters for the stored procedure here
	
	@Id_Tipo_Subsidio int,
	@Codigo_Tipo_Subsidio varchar(10),
	@Nombre_Tipo_Subsidio varchar(100), 
	@Vigente int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	update Tipo_Subsidio 
	set
	Codigo_Tipo_Subsidio=@Codigo_Tipo_Subsidio,
	Nombre_Tipo_Subsidio=@Nombre_Tipo_Subsidio,
	Vigente=@Vigente
	where Id_Tipo_Subsidio=@Id_Tipo_Subsidio
	
END

GO
/****** Object:  StoredProcedure [dbo].[Tipo_Subsidio_Agregar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipo_Subsidio_Agregar] 
	-- Add the parameters for the stored procedure here
	
	@Codigo_Tipo_Subsidio varchar(10),
	@Nombre_Tipo_Subsidio varchar(100), 
	@Vigente int	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--declare @Id_Tipo_Subsidio int
	
    -- Insert statements for procedure here
    
	insert into Tipo_Subsidio (Codigo_Tipo_Subsidio,Nombre_Tipo_Subsidio,Vigente)
	values(@Codigo_Tipo_Subsidio,@Nombre_Tipo_Subsidio,@Vigente)
	

END
GO
/****** Object:  StoredProcedure [dbo].[Tipo_Subsidio_Listado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipo_Subsidio_Listado] 
	-- Add the parameters for the stored procedure here
	
	@Codigo_Tipo_Subsidio varchar(10),
	@Nombre_Tipo_Subsidio varchar(100),
	@Vigente int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

select Id_Tipo_Subsidio,Codigo_Tipo_Subsidio,Nombre_Tipo_Subsidio,Vigente
from Tipo_Subsidio 
where	(@Codigo_Tipo_Subsidio = '' or Codigo_Tipo_Subsidio like '%' + @Codigo_Tipo_Subsidio + '%') and
		(@Nombre_Tipo_Subsidio = '' or Nombre_Tipo_Subsidio like '%' + @Nombre_Tipo_Subsidio + '%')	and
		(@Vigente = 0 or @Vigente = @Vigente) 
order by Id_Tipo_Subsidio
	
END

GO
/****** Object:  StoredProcedure [dbo].[Tipo_Subsidio_Listado_Paginado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipo_Subsidio_Listado_Paginado] 
	-- Add the parameters for the stored procedure here

	@Id_Tipo_Subsidio		int,
	@Codigo_Tipo_Subsidio varchar(10),
	@Nombre_Tipo_Subsidio varchar(100),
	@Vigente int,
	@Pagina_Solicitada int,
	@Tamacno_Pagina int,
	@total_Registros int Output	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @fila_Inicial int
declare @fila_Final int   


select @total_Registros = count(*)
from   Tipo_Subsidio
where	(@Id_Tipo_Subsidio = 0 or Id_Tipo_Subsidio = @Id_Tipo_Subsidio) and
		(@Codigo_Tipo_Subsidio = '' or Codigo_Tipo_Subsidio like '%' + @Codigo_Tipo_Subsidio + '%') and
		(@Nombre_Tipo_Subsidio = '' or Nombre_Tipo_Subsidio like '%' + @Nombre_Tipo_Subsidio + '%')	and
		(@Vigente = -1 or Vigente = @Vigente) 


Exec bd_comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY Id_Tipo_Subsidio) as RowNum,
Id_Tipo_Subsidio,Codigo_Tipo_Subsidio,Nombre_Tipo_Subsidio,Vigente
from   Tipo_Subsidio
where	(@Id_Tipo_Subsidio = 0 or Id_Tipo_Subsidio = @Id_Tipo_Subsidio) and
		(@Codigo_Tipo_Subsidio = '' or Codigo_Tipo_Subsidio like '%' + @Codigo_Tipo_Subsidio + '%') and
		(@Nombre_Tipo_Subsidio = '' or Nombre_Tipo_Subsidio like '%' + @Nombre_Tipo_Subsidio + '%')	and
		(@Vigente = -1 or Vigente = @Vigente)  
) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
    
END

GO
/****** Object:  StoredProcedure [dbo].[Tipologia_Actualizar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipologia_Actualizar] 
	-- Add the parameters for the stored procedure here
	
	@Id_Tipologia int,
	@Nombre_Tipologia varchar(50), 
	@Id_Tipo_Subsidio int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	update Tipologia 
	set
	Nombre_Tipologia=@Nombre_Tipologia,
	Id_Tipo_Subsidio=@Id_Tipo_Subsidio
	where Id_Tipologia=@Id_Tipologia
	
END

GO
/****** Object:  StoredProcedure [dbo].[Tipologia_Agregar]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipologia_Agregar] 
	-- Add the parameters for the stored procedure here
	
	@Nombre_Tipologia varchar(50), 
	@Id_Tipo_Subsidio int	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	insert into Tipologia (Nombre_Tipologia,Id_Tipo_Subsidio)
	values(@Nombre_Tipologia,@Id_Tipo_Subsidio)

END
GO
/****** Object:  StoredProcedure [dbo].[Tipologia_Listado_Paginado]    Script Date: 05-04-2021 20:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipologia_Listado_Paginado] 
	-- Add the parameters for the stored procedure here

	@Id_Tipologia		int,
	@Nombre_Tipologia   varchar(50),
	@Id_Tipo_Subsidio	int,
	@Pagina_Solicitada  int,
	@Tamacno_Pagina     int,
	@total_Registros    int Output	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @fila_Inicial int
declare @fila_Final int   


select @total_Registros = count(*)
from   Tipologia t
INNER JOIN Tipo_Subsidio ts on t.Id_Tipo_Subsidio = ts.Id_Tipo_Subsidio
where	(@Id_Tipologia = 0 or t.Id_Tipologia = @Id_Tipologia) and
		(@Nombre_Tipologia = '' or t.Nombre_Tipologia like '%' + @Nombre_Tipologia + '%') and
		(@Id_Tipo_Subsidio = 0 or t.Id_Tipo_Subsidio = @Id_Tipo_Subsidio)

Exec bd_comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY t.Id_Tipologia) as RowNum,
t.Id_Tipologia,t.Nombre_Tipologia,t.Id_Tipo_Subsidio,ts.Nombre_Tipo_Subsidio
from   Tipologia t
INNER JOIN Tipo_Subsidio ts on t.Id_Tipo_Subsidio = ts.Id_Tipo_Subsidio
where	(@Id_Tipologia = 0 or t.Id_Tipologia = @Id_Tipologia) and
		(@Nombre_Tipologia = '' or t.Nombre_Tipologia like '%' + @Nombre_Tipologia + '%') and
		(@Id_Tipo_Subsidio = 0 or t.Id_Tipo_Subsidio = @Id_Tipo_Subsidio) 
		
) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
    
END

GO
