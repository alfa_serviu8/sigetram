﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTram.Controllers
{
    public class ProyectosController : ApiController
    {
        ProyectoBo prBo = new ProyectoBoImp();

        [HttpPost]
        [Route("api/proyectos/listado")]
        public IHttpActionResult Listado(ProyectoBuscado pb)
        {
                    
            return Ok(new { pagina = prBo.Listado(pb)});
                    }

        [HttpPost]
        [Route("api/proyectos/")]
        public void Post(Proyecto pr)
        {
            int i=0;
            prBo.Agregar(pr);
        }

        [HttpPut]
        [Route("api/proyectos/")]
        public void Put(Proyecto pr)
        {
            prBo.Actualizar(pr);
        }

        //[HttpGet]
        //[Route("api/proyectos/snat/{idLlamado}")]
        //public IHttpActionResult Get(int idLlamado)
        //{
        //    ProyectoDao salida  = new ProyectoDaoImp();

        //    return Ok(new  { salida = salida.Banco(idLlamado), llamado = idLlamado });
            
        //}
    }
}
