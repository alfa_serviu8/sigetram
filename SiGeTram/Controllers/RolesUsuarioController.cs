﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using SiGeTram.Models.Entidades;
using SiGeTram.Models.Bo;

namespace SiGeTram.Controllers
{
    public class RolesUsuarioController : ApiController
    {
        RolUsuarioBo ruBo = new RolUsuarioBoImp();

        [HttpPost]
        [Route("api/rolesUsuario/listado")]
        public IHttpActionResult Listado(RolUsuarioBuscado rub)
        {
                    
            return Ok(new { pagina = ruBo.Listado(rub)});
                    }

        [HttpPost]
        [Route("api/rolesUsuario/")]
        public void Post(RolUsuario ru)
        {
            ruBo.Agregar(ru);
        }

        [HttpDelete]
        [Route("api/rolesUsuario/{id}")]
        public void Delete(int id)
        {
            ruBo.Eliminar(id);
        }

        
    }
}
