﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using SiGeTram.Models.Entidades;
using SiGeTram.Models.Bo;

namespace SiGeTram.Controllers
{
    public class RolesController : ApiController
    {
        RolBo rBo = new RolBoImp();

        [HttpPost]
        [Route("api/roles/listado")]
        public IHttpActionResult Listado(RolBuscado rb)
        {
                    
            return Ok(new { pagina = rBo.Listado(rb)});
                    }

        [HttpPost]
        [Route("api/roles/")]
        public void Post(Rol r)
        {
            rBo.Agregar(r);
        }

        [HttpPut]
        [Route("api/roles/")]
        public void Put(Rol r)
        {
            rBo.Actualizar(r);
        }

        
    }
}
