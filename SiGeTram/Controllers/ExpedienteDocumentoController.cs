﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Controllers
{
    public class ExpedienteDocumentoController : ApiController
    {
        ExpedienteDocumentoBo eBo = new ExpedienteDocumentoBoImp();

        [HttpPost]
        [Route("api/expedienteDocumento/listado")]
        public IHttpActionResult Listado(ExpedienteBuscado eb)
        {
         
           return Ok(new { pagina = eBo.Listado(eb) });
        }

       

        


    }
}
