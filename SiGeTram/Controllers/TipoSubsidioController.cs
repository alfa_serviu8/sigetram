﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTram.Controllers
{
    public class TipoSubsidioController : ApiController
    {
        TipoSubsidioBo tsBo = new TipoSubsidioBoImp();

        [HttpPost]
        [Route("api/tipoSubsidios/listado")]
        public IHttpActionResult Listado(TipoSubsidioBuscado tsb)
        {
        
            return Ok(new { pagina = tsBo.Listado(tsb) });
        }

        [HttpPost]
        [Route("api/tipoSubsidios/")]
        public void Post(TipoSubsidio ts)
        {
            tsBo.Agregar(ts);
        }
      
        [HttpPut]
        [Route("api/tipoSubsidios/")]
        public void Put(TipoSubsidio ts)
        {
            tsBo.Actualizar(ts);
        }
    }
}
