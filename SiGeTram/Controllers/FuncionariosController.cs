﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeFunDB.Entidades;
using SiGeFunDB.Dao;
using System.Windows;


namespace SiGeTram.Controllers
{
    public class FuncionariosController : ApiController
    {
        private FuncionarioDao funcionario_DAO = new FuncionarioDaoImp();
     



        [HttpGet]
        [Route("api/funcionarios/{Id_Servicio}/{Id_Dependencia}/{Id_Calidad_Juridica}/{Id_Estamento}/{Nombre_Completo}/{Habilitado}/{Id_Genero}/{Id_Estado_Civil}/{Es_Persona_Natural}/{Es_Empresa_Provedor}/{Es_Funcionario_Publico}/{Es_Usuario_Interno}/{Pagina_Solicitada}/{tamacno_Pagina}")]
        public IHttpActionResult Listado_Paginado(int Id_Servicio, int Id_Dependencia, int Id_Calidad_Juridica, int Id_Estamento, string Nombre_Completo, int Habilitado, int Id_Genero, int Id_Estado_Civil, int Es_Persona_Natural, int Es_Empresa_Provedor, int Es_Funcionario_Publico, int Es_Usuario_Interno,int Pagina_Solicitada,int Tamacno_Pagina)
        {
            List<Funcionario> lista = new List<Funcionario>();
          int  Total_Registros;


            lista = funcionario_DAO.Listado_Paginado(Id_Servicio, Id_Dependencia, Id_Calidad_Juridica, Id_Estamento, Nombre_Completo, Habilitado, Id_Genero, Id_Estado_Civil, Es_Persona_Natural, Es_Empresa_Provedor, Es_Funcionario_Publico, Es_Usuario_Interno,Pagina_Solicitada,Tamacno_Pagina,out Total_Registros);

            if (lista.Count > 0)
            {

                return Ok(new { Total_Registros = Total_Registros, listado = lista });

            }
            else
            {

                return Ok(new { Total_Registros = Total_Registros });

            }
        }


        


        [HttpGet]
        [Route("api/funcionarios/servicios/{id_servicio}")]
        public List<Funcionario> Listado(int id_servicio)
        {
            return funcionario_DAO.Listado(id_servicio);

           
        }

       
        [HttpGet]
        [Route("api/funcionarios/{Rut}")]
        public IHttpActionResult PorRut (int Rut)
        {
            List<Funcionario> lista = new List<Funcionario>();

            lista = funcionario_DAO.PorRut(Rut);

            if (lista.Count > 0)
            {

                return Ok(new { exito = true, funcionario = lista[0] });

            }
            else
            {

                return Ok(new { exito = false });

            }
                                    
        }


        [HttpGet]
        [Route("api/funcionarios/servicio/{Rut_Con_Dv}")]
        public IHttpActionResult EstableceDatos(string Rut_Con_Dv)
        {

            List<Funcionario> lista = new List<Funcionario>();

            string mensajeError = "";

            lista =  funcionario_DAO.Establecer_datos(Rut_Con_Dv, out mensajeError);

            if (mensajeError == "")
            {

                if (lista.Count > 0)
                {
                    return Ok(new { exito = true, funcionario=lista[0] });
                }
                else

                {
                    return Ok(new{exito= false, mensaje = "No existen datos en el Registro Civil"});

                }
            }
            else
            {
                return Ok(new { exito = false, mensaje = mensajeError });

            }
            
           
        }


        [HttpPost]
        [Route("api/funcionarios/")]
        public IHttpActionResult Post(Funcionario funcionario)
        {
            funcionario_DAO.Agregar(funcionario);

            return Ok();
        }


        [HttpPut]
        [Route("api/funcionarios/")]
        public IHttpActionResult Put(Funcionario funcionario)
        {
            funcionario_DAO.Actualizar(funcionario);

            return Ok();
        }

    }
}
