﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;
using SiGeProc.Dao;
using SiGeProc.Entidades;

namespace SiGeTram.Controllers
{
    public class TareasController : ApiController
    {
        [HttpPost]
        [Route("api/tareas/listado")]
        public IHttpActionResult Listado(TareaDaoImp dao)
        {
            //List<Tarea> lista = dao.Listado();

            //return Ok(new { total_registros = dao.TotalRegistros(), listado = lista });
            return Ok();
            
        }

        [HttpPost]
        [Route("api/tareas/")]
        public void Post(Tarea tarea)
        {
            //tarea.Agregar();
        }

        [HttpPut]
        [Route("api/tareas/")]
        public void Put(Tarea tarea)
        {
            //tarea.Actualizar();
        }
    }
}