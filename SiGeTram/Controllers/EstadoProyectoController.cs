﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Controllers
{
    public class EstadoProyectoController : ApiController
    {
        EstadoProyectoBo epBo = new EstadoProyectoBoImp();

        [HttpPost]
        [Route("api/estadoProyectos/listado")]
        public IHttpActionResult Listado(EstadoProyectoBuscado epb)
        {
            return Ok(new { pagina = epBo.Listado(epb) });
        }
    }
}
