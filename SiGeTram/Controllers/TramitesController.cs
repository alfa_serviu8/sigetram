﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTram.Controllers
{
    public class TramitesController : ApiController
    {
        TramiteBo tBo = new TramiteBoImp();

        [HttpPost]
        [Route("api/tramites/listado")]
        public IHttpActionResult Listado(TramiteBuscado tb)
        {
            

            return Ok(new { pagina = tBo.Listado(tb)});
        }

        [HttpPost]
        [Route("api/tramites/")]
        public IHttpActionResult Post(Tramite t)
        {
           int id_tramite= tBo.Agregar(t);
          
           return Ok(new { id_tramite = id_tramite });

        }
      
       
    }
}
