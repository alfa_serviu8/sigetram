﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTram.Controllers
{
    public class ElementoReferenciaController : ApiController
    {
        ElementoReferenciaBo erBo = new ElementoReferenciaBoImp();

        [HttpGet]
        [Route("api/elementoReferencia/{idElemento}")]
        public IHttpActionResult Listado(int idElemento)
        {
        
            return Ok(new {listado= erBo.Listado(idElemento) });
        }

       
    }
}
