﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTram.Controllers
{
    
    public class EstadoExpedienteController : ApiController
    {
        EstadoExpedienteBo eeBo = new EstadoExpedienteBoImp();

        [HttpPost]
        [Route("api/estadoExpedientes/listado")]
        public IHttpActionResult Listado(EstadoExpedienteBuscado eeb)
        {
            return Ok(new { pagina = eeBo.Listado(eeb) });

         
        }

        
    }
}
