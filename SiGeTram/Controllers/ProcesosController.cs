﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using SiGeProc.Entidades;
using SiGeProc.Bo;
using SiGeTramDB.Dao;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTram.Controllers
{
    public class ProcesosController : ApiController
    {
        ProcesoBo pBo = new ProcesoBoImp();

        [HttpPost]
        [Route("api/procesos/listado")]
        public IHttpActionResult Listado(ProcesoBuscado pb)
        {
           return Ok(new { pagina=pBo.Listado(pb) });
        
            
        }

        [HttpPost]
        [Route("api/procesos/")]
        public void Post(Proceso p)
        {
            //tarea.Agregar();
        }

        [HttpPut]
        [Route("api/procesos/")]
        public void Put(Proceso p)
        {
            //tarea.Actualizar();
        }

        [HttpPost]
        [Route("api/procesos/respaldo")]
        public IHttpActionResult Respaldar(Proceso p)
        {
          int i = 0;


            List<Expediente> list = new TipoTramiteBoImp().Respaldar(p.Id_Proceso);

            return Ok(list);
        }
    }
}