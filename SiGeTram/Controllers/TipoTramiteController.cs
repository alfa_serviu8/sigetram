﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTram.Controllers
{
    public class TipoTramiteController : ApiController
    {
        TipoTramiteBo ttBo = new TipoTramiteBoImp();

        [HttpPost]
        [Route("api/tipoTramite/listado")]
        public IHttpActionResult Listado(TipoTramiteBuscado ttb)
        {
        
            return Ok(new { pagina = ttBo.Listado(ttb) });
        }

        [HttpGet]
        [Route("api/tipoTramite")]
        public IHttpActionResult Basico()
        {
            //ttBo.AgregarBasico(new TipoTramite(0, "Prueba Basico", "Prueba Basico", 0));


            return Ok();
        }

        //[HttpGet]
        //[Route("api/tipoTramite/Respaldo/{idTramite}")]
        //public IHttpActionResult Respaldo(int idTramite)
        //{
        //    //ttBo.AgregarBasico(new TipoTramite(0, "Prueba Basico", "Prueba Basico", 0));

        //    List<Expediente> list = ttBo.Respaldar(idTramite);


        //    return Ok(new {listado= list });
        
        //}

        [HttpPost]
        [Route("api/tipoTramite/respaldo")]
        public IHttpActionResult Respaldar(TipoTramite tt)
        {
            List<Expediente> list = ttBo.Respaldar(tt.Id_Tipo_Tramite);

            return Ok();
        }


        [HttpPost]
        [Route("api/tipoTramite")]
        public IHttpActionResult Agregar(TipoTramite tt)
        {
            ttBo.AgregarBasico(tt);


            return Ok();
        }

        [HttpPut]
        [Route("api/tipoTramite")]
        public IHttpActionResult Actualizar(TipoTramite tt)
        {
            ttBo.Actualizar(tt);


            return Ok();
        }



    }
}
