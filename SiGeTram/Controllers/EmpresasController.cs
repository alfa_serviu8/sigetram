﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Entidades;
using SiGeTramDB.Dao;
using SiGeTramDB.Bo;

using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace SiGeTram.Controllers
{
    public class EmpresasController : ApiController
    {
         EmpresaBo eBo= new EmpresaBoImp(new EmpresaDaoImp());

        [HttpGet]
        [Route("api/empresas")]
        public IHttpActionResult Listado()
        {
            

            return Ok(new { pagina = "" });

        }

        [HttpPost]
        [Route("api/empresas/listado")]
        public IHttpActionResult Listado(EmpresaBuscada eb)
        {
            int i = 0;
            PaginaEmpresa pe = eBo.Listado(eb);


            return Ok(new { pagina = pe });

        }



        [HttpPost]
        [Route("api/empresas")]
        public IHttpActionResult Agregar(Empresa e)
        {
            eBo.Agregar(e);

            return Ok();

        }


        [HttpPut]
        [Route("api/empresas")]
        public IHttpActionResult Actualizar(Empresa e)
        {
            eBo.Actualizar(e);

            return Ok() ;

        }

        [HttpPut]
        [Route("api/empresas/resetearClave")]
        public IHttpActionResult ResetearClave(Empresa e)
        {
            eBo.ResetearClave(e);

            return Ok();
        }


    }
}
