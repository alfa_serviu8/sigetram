﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;
using SiGeProc.Bo;
using SiGeProc.Entidades;

namespace SiGeTram.Controllers
{
    public class FlujoController : ApiController
    {
        [HttpGet]
        [Route("api/flujo/{idSimbolo}/{condicion}")]
        public IHttpActionResult Listado(int idSimbolo,int condicion)
        {
            List<Simbolo> lista = new FlujoBoImp().Siguientes(idSimbolo,condicion,2);

            return Ok(new { listado = lista });
           
            
        }

        [HttpPost]
        [Route("api/tareas/")]
        public void Post(Tarea tarea)
        {
            //tarea.Agregar();
        }

        [HttpPut]
        [Route("api/tareas/")]
        public void Put(Tarea tarea)
        {
            //tarea.Actualizar();
        }
    }
}