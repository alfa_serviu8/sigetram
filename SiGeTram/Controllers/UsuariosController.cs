﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTram.Models.Entidades;
using SiGeTram.Models.Dao;
using SiGeTram.Models.Bo;


namespace SiGeTram.Controllers
{
    public class UsuariosController : ApiController
    {
        private UsuarioDao usuarioDao = new UsuarioDaoImp();
        private UsuarioBO usuarioBo = new UsuarioBOImp();

        [HttpGet]
        [Route("api/usuarios/{rut}")]
        public List<Usuario> PorRut(int rut)
        {
            return usuarioDao.PorRut(rut);
        }
               

        [HttpGet]
        [Route("api/usuarios/login")]
        public Usuario PorLogin()
        {
           string login = User.Identity.Name;

            login = login.Substring(9);

            //login = "cloyolav";
            return usuarioBo.PorLogin(login);

            
        }
         
        
      


    }
}
