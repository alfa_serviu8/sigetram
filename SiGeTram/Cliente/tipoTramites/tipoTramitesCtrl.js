﻿app.controller("tipoTramitesCtrl", ["$scope", "tipoTramitesSrv", "usuariosSrv", "$location", "$routeParams", "$localStorage", "rolUsuarioSrv",  "sistemaSrv", TipoTramitesCtrl]);

function TipoTramitesCtrl($scope, tipoTramitesSrv, usuariosSrv, $location, $routeParams, $localStorage, rolUsuarioSrv, sistemaSrv) {

    $scope.tipoTramites = [];

    
    

    //if (!$localStorage.tipoTramite) {

    //    $scope.tipoTramite = { "Id_Tipo_Tramite": 0, "Nombre_Tipo_Tramite": "" };

    //} else {

    //    $localStorage.tipoTramite = { "Id_Tipo_Tramite": 0, "Nombre_Tipo_Tramite": "" };
    //    $scope.tipoTramite = { "Id_Tipo_Tramite": 0, "Nombre_Tipo_Tramite": "" };
    //}

    $scope.usuario = {

    };
   
    $scope.filtro = tipoTramitesSrv.filtro_actual();

 
  
    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();
    }
   
    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.usuario = response.data;

        $scope.filtro = angular.copy(tipoTramitesSrv.filtro_actual());

        $scope.listadoPaginado(tipoTramitesSrv.filtro_actual());

        $scope.maxSize = 5;

        $scope.currentPage = 1;

             
        
        //$localStorage.tipoTramite = { "Id_Tipo_Tramite": 0, "Nombre_Tipo_Tramite": "" };
        //$scope.tipoTramite = $localStorage.tipoTramite;

        //console.log("Este es eltramite");
        //console.log($localStorage.tipoTramite);    

      

   


    }) // fin usuario
  

    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }

    $scope.editar = function (idTipoTramite) {

        $location.path("/tipoTramites/" + idTipoTramite + "/editar")

    }

    $scope.respaldar = function (idTipoTramite) {

        $location.path("/tipoTramites/" + idTipoTramite + "/respaldar")

    }
 

    $scope.guardarTramite = function (tipoTramiteSeleccionado) {

        $localStorage.tipoTramite = tipoTramiteSeleccionado;

        tipoTramitesSrv.guardar_tipo_tramite(tipoTramiteSeleccionado);

        console.log("este es el tipo de tramite seleccionado");
        console.log($localStorage.tipoTramite);

        $scope.tipoTramite = $localStorage.tipoTramite;

        rolUsuarioSrv.porUsuario($scope.usuario.Rut).then(function (response) {

            $scope.rol = new Roles(response.data.pagina.rolesUsuario);


        });

        rolUsuarioSrv.porRol(19).then(function (response) {

            $scope.listado = [];

            $scope.listado = response.data.pagina.rolesUsuario;

            

        });


     
        $location.path("/expedientes/");
    }

    $scope.listadoPaginado = function (filtro_actual) {

        //var filtro = angular.copy(filtro_actual);
        var filtro = filtro_actual;

        tipoTramitesSrv.guardar_filtro(filtro);

        tipoTramitesSrv.listado(filtro).then(function (respuesta) {

            $scope.tipoTramites = respuesta.data.pagina.tiposTramite;

                    
            $scope.bigTotalItems = respuesta.data.pagina.total_registros;
        })
    }
}// fin controlador

app.controller("tipoTramiteAgregarCtrl", ["$scope", "tipoTramitesSrv", "$location", "$routeParams", TipoTramiteAgregarCtrl]);

function TipoTramiteAgregarCtrl($scope, tipoTramitesSrv, $location, $routeParams) {

    $scope.tipoTramite = {
        Vigente: "1"
    };

    $scope.guardando_tipo_tramite = false;

    $scope.guardar = function () {

        tipoTramitesSrv.agregar($scope.tipoTramites).then(function (respuesta) {

            console.log(respuesta);

            $location.path("/tipoTramites");

            //console.log(respuesta);
        })
    }
}


app.controller("tipoTramiteEditarCtrl", ["$scope", "tipoTramitesSrv", "$location", "$routeParams", TipoTramiteEditarCtrl]);

function TipoTramiteEditarCtrl($scope, tipoTramitesSrv, $location, $routeParams) {

    $scope.tipoTramite = {};

    $scope.guardando_tipo_tramite = false;

    $scope.filtro = angular.copy(tipoTramitesSrv.filtro_actual());

    $scope.filtro.tipoTramite.Id_Tipo_Tramite = $routeParams.id_tipo_tramite;



    tipoTramitesSrv.listado($scope.filtro).then(function (respuesta) {


        $scope.tipoTramite = respuesta.data.pagina.tiposTramite[0];

        $scope.tipoTramite.Vigente = $scope.tipoTramite.Vigente.toString();

    });



    $scope.guardar = function () {

        tipoTramitesSrv.actualizar($scope.tipoTramite).then(function (respuesta) {

            $location.path("/tipoTramites");
        });
    }
}

app.controller("tipoTramiteRespaldarCtrl", ["$scope", "tipoTramitesSrv", "$location", "$routeParams", TipoTramiteRespaldarCtrl]);

function TipoTramiteRespaldarCtrl($scope, tipoTramitesSrv, $location, $routeParams) {

    $scope.tipoTramite = {};

    $scope.respaldando_tipo_tramite = false;

    $scope.filtro = angular.copy(tipoTramitesSrv.filtro_actual());

    $scope.filtro.tipoTramite.Id_Tipo_Tramite = $routeParams.id_tipo_tramite;



    tipoTramitesSrv.listado($scope.filtro).then(function (respuesta) {


        $scope.tipoTramite = respuesta.data.pagina.tiposTramite[0];

        $scope.tipoTramite.Vigente = $scope.tipoTramite.Vigente.toString();

    });



    $scope.respaldar = function () {

        $scope.respaldando_tipo_tramite = true;
        alert("El respaldo tomará algunos minutos. Espere por favor....")
        tipoTramitesSrv.respaldar($scope.tipoTramite).then(function (respuesta) {

            //$location.path("/tipoTramites");
            $scope.respaldando_tipo_tramite = false;
            alert("El respaldo finalizó, por favor verifique los datos respaldados.")
        });
    }
}