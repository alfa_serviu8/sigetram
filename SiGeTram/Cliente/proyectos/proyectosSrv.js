﻿app.factory("proyectosSrv", ["$http", "urlApi", ProyectosSrv]);

function ProyectosSrv($http, urlApi) {

    var ruta = urlApi + "proyectos/";

    var filtro = {
        proyecto: {
            "Id_Proyecto": 0,
            "Codigo_Proyecto": "",
            "Nombre_Proyecto": "",
            "Rut_Empresa": 0,
            "Nombre_Empresa": "",
            "Rut_Revisor": 0,
            "Id_Tipo_Subsidio": "0",
            "Id_Estado_Proyecto": "0",
            "Id_Tipologia": "0",
            "Id_Region": "0",
            "Id_Provincia": "0",
            "Id_Comuna": "0",
            "Id_Proceso":0
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };

    var filtroAdm = {
        proyecto: {
            "Id_Proyecto": 0,
            "Codigo_Proyecto": "",
            "Nombre_Proyecto": "",
            "Rut_Empresa": 0,
            "Nombre_Empresa": "",
            "Rut_Revisor": 0,
            "Id_Tipo_Subsidio": "0",
            "Id_Estado_Proyecto": "0",
            "Id_Tipologia": "0",
            "Id_Region": "0",
            "Id_Provincia": "0",
            "Id_Comuna": "0",
            "Id_Proceso":0
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };

    var guardar_filtro_adm = function (nuevoFiltroAdm) {


        filtroAdm = nuevoFiltroAdm;
    };

    var filtro_actual_adm = function () {

        return filtroAdm;

    };


    var guardar_filtro = function (nuevoFiltro) {

       
        filtro = nuevoFiltro;
    };

    var filtro_actual = function () {
        return filtro;
    };

    var listado = function (filtro) {
        return $http.post(ruta + "listado", filtro);
    };

    var porId = function (idProyecto) {
        var filtro = {
            proyecto: {
                "Id_Proyecto": idProyecto,
                "Codigo_Proyecto": "",
                "Nombre_Proyecto": "",
                "Rut_Empresa": 0,
                "Nombre_Empresa": "",
                "Rut_Revisor": 0,
                "Rut_Operador": 0,
                "Id_Tipo_Subsidio": "0",
                "Id_Estado_Proyecto": "0",
                "Id_Tipologia": "0",
                "Id_Region": "0",
                "Id_Provincia": "0",
                "Id_Comuna": "0",
                "Id_Proceso":0
              
            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }
        };


        return listado(filtro);
    };

    var porEmpresa = function (rutEmpresa) {

        var filtro = {
            proyecto: {
                "Id_Proyecto": 0,
                "Codigo_Proyecto": "",
                "Nombre_Proyecto": "",
                "Rut_Empresa": rutEmpresa,
                "Nombre_Empresa": "",
                "Rut_Revisor": 0,
                "Rut_Operador": 0,
                "Id_Tipo_Subsidio": "0",
                "Id_Estado_Proyecto": "0",
                "Id_Tipologia": "0",
                "Id_Region": "0",
                "Id_Provincia": "0",
                "Id_Comuna": "0",
                "Id_Proceso": 0
            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }
        };


        return listado(filtro);



    };

    var porProceso = function (idProceso) {

        var filtro = {
            proyecto: {
                "Id_Proyecto": 0,
                "Codigo_Proyecto": "",
                "Nombre_Proyecto": "",
                "Rut_Empresa": rutEmpresa,
                "Nombre_Empresa": "",
                "Rut_Revisor": 0,
                "Rut_Operador": 0,
                "Id_Tipo_Subsidio": "0",
                "Id_Estado_Proyecto": "0",
                "Id_Tipologia": "0",
                "Id_Region": "0",
                "Id_Provincia": "0",
                "Id_Comuna": "0",
                "Id_Proceso": idProceso
            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }
        };


        return listado(filtro);



    };


    var porCodigo = function (codigo) {

        var filtro = {
            proyecto: {
                "Id_Proyecto": 0,
                "Codigo_Proyecto": codigo,
                "Nombre_Proyecto": "",
                "Rut_Empresa": 0,
                "Nombre_Empresa": "",
                "Rut_Revisor": 0,
                "Rut_Operador": 0,
                "Id_Tipo_Subsidio": "0",
                "Id_Estado_Proyecto": "0",
                "Id_Tipologia": "0",
                "Id_Region": "0",
                "Id_Provincia": "0",
                "Id_Comuna": "0",
                "Id_Proceso": 0
            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }
        };


        return listado(filtro);



    };
    var agregar = function (proyecto) {

       
        return $http.post(ruta, proyecto);
    };

    var actualizar = function (proyecto) {
        return $http.put(ruta, proyecto);
    };

    var eliminar = function (Id_Proyecto) {

        return $http.delete(ruta, Id_Proyecto);
    };

    return {
        listado: listado,
        porId: porId,
        porProceso: porProceso,
        porEmpresa: porEmpresa,
        porCodigo: porCodigo,
        guardar_filtro: guardar_filtro,
        guardar_filtro_adm: guardar_filtro_adm,
        filtro_actual: filtro_actual,
        filtro_actual_adm: filtro_actual_adm,
        agregar: agregar,
        actualizar: actualizar,
        eliminar: eliminar
    };
}
