﻿app.controller("proyectosCtrl", ["$scope", "proyectosSrv",  "tipoSubsidiosSrv", "usuariosSrv", "estadoProyectosSrv", "tipologiasSrv", "$location", "$routeParams", "rolUsuarioSrv", "sistemaSrv",ProyectosCtrl]);

function ProyectosCtrl($scope, proyectosSrv,  tipoSubsidiosSrv, usuariosSrv, estadoProyectosSrv, tipologiasSrv, $location, $routeParams, rolUsuarioSrv, sistemaSrv) {

    
    $scope.proyectos = [];
  
    $scope.subsidios = [];

    tipoSubsidiosSrv.listadoGeneral().then(function (respuesta) {

        $scope.subsidios = respuesta.data.pagina.tiposSubsidio;
    });

    $scope.estados = [];

    estadoProyectosSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosProyecto;
    });


    var filtroTipologia = {
        tipologia: {
            "Id_Tipologia": "0",
            "Nombre_Tipologia": "",
            "Id_Tipo_Subsidio": "0"
        },
        "numero_pagina": "1",
        "tamacno_pagina": "10"

    };

  

    $scope.lista_de_tipologias = function () {


        if ($scope.filtro.proyecto.Id_Tipo_Subsidio == "0") {
            filtroTipologia.tipologia.Id_Tipo_Subsidio = "-1";

            tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {
                $scope.tipologias = respuesta.data.pagina.tipologias;
            });
        }
        else {
            filtroTipologia.tipologia.Id_Tipo_Subsidio = $scope.filtro.proyecto.Id_Tipo_Subsidio;
            tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {
                $scope.tipologias = respuesta.data.pagina.tipologias;
            });
        }
        $scope.filtro.proyecto.Id_Tipologia = "0";
    };

   

    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();
    };


    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.filtro = angular.copy(proyectosSrv.filtro_actual());

        $scope.filtro.proyecto.Id_Tipologia = "0";

       
        rolUsuarioSrv.porUsuario(usuariosSrv.usuarioActual().Rut).then(function (response) {

            $scope.rol = new Roles(response.data.pagina.rolesUsuario);

            $scope.filtro.proyecto.Rut_Revisor = usuariosSrv.usuarioActual().Rut;


            if ($routeParams.Rut_Empresa) {

                $scope.filtro.proyecto.Rut_Empresa = $routeParams.Rut_Empresa;

            }

            $scope.listadoPaginado($scope.filtro);

            $scope.maxSize = 5;

            $scope.currentPage = 1;
        });
    });


    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    };


    $scope.listadoPaginado = function (filtro_actual) {

        var filtro = filtro_actual;

        console.log("Este es el filtro");
        console.log(filtro);
        proyectosSrv.guardar_filtro(filtro);

        proyectosSrv.listado(filtro).then(function (respuesta) {

            $scope.proyectos = respuesta.data.pagina.proyectos;

            $scope.bigTotalItems = respuesta.data.pagina.totalRegistros;
        });
    }
}

app.controller("proyectosAdmCtrl", ["$scope", "proyectosSrv", "tipoSubsidiosSrv", "procesosSrv", "usuariosSrv", "estadoProyectosSrv", "tipologiasSrv", "$location", "$routeParams", "rolUsuarioSrv", "tipoExpedientesSrv", ProyectosAdmCtrl]);

function ProyectosAdmCtrl($scope, proyectosSrv, tipoSubsidiosSrv, procesosSrv, usuariosSrv, estadoProyectosSrv, tipologiasSrv, $location, $routeParams, rolUsuarioSrv, tipoExpedientesSrv) {

   
    $scope.proyectos = [];

    $scope.subsidios = [];

    tipoSubsidiosSrv.listadoGeneral().then(function (respuesta) {

        $scope.subsidios = respuesta.data.pagina.tiposSubsidio;
    });




    $scope.procesos = [];

    procesosSrv.listadoGeneral().then(function (respuesta) {

        $scope.procesos= respuesta.data.pagina.procesos;

        console.log("estos son los tramites");

        console.log($scope.procesos);


    })






    $scope.tipoExpedientes = [];


    $scope.agregar = function () {

        if ($scope.filtro.proyecto.Id_Proceso == "0") {
            alert("Para agregar debe seleccionar un tipo de tramite");
        } else {

            tipoExpedientesSrv.porProceso($scope.filtro.proyecto.Id_Proceso).then(function (respuesta) {

                $scope.tipoExpedientes = respuesta.data.pagina.tiposExpediente;

                if ($scope.tipoExpedientes.length == 0) {

                    alert("No existen tipos de expedientes definidos para el trámite seleccionado. Por favor agregue los tipos de expedientes que se usarán y luego agruegue los proyectos");


                } else {

                    $location.path("/proyectos/" + $scope.filtro.proyecto.Id_Proceso + "/agregar");

                }


            })







            
        }


    }


    $scope.estados = [];

    estadoProyectosSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosProyecto;
    });


    var filtroTipologia = {
        tipologia: {
            "Id_Tipologia": "0",
            "Nombre_Tipologia": "",
            "Id_Tipo_Subsidio": "0"
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        } 

    };

   

    $scope.lista_de_tipologias = function () {

        if ($scope.filtro.proyecto.Id_Tipo_Subsidio == "0") {
            filtroTipologia.tipologia.Id_Tipo_Subsidio = "-1";

            tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {
                $scope.tipologias = respuesta.data.pagina.tipologias;

                console.log("ests son las topologias");
                console.log($scope.tipologias);
            });
        }
        else {
            filtroTipologia.tipologia.Id_Tipo_Subsidio = $scope.filtro.proyecto.Id_Tipo_Subsidio;

            tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {
                $scope.tipologias = respuesta.data.pagina.tipologias;
            });
        }
        $scope.filtro.proyecto.Id_Tipologia = "0";
    };

   

    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();
    };




    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.filtro = angular.copy(proyectosSrv.filtro_actual_adm());


        

        if ($routeParams.Rut_Empresa) {

            $scope.filtro.proyecto.Rut_Empresa = $routeParams.Rut_Empresa;
            $scope.filtro.proyecto.Nombre_Empresa = "";

        }


        if ($routeParams.Id_Proceso) {

            $scope.filtro.proyecto.Id_Proceso = $routeParams.Id_Proceso;

        } else {

            $scope.filtro.proyecto.Id_Proceso = "0";
        }

        $scope.filtro.proyecto.Id_Tipologia = "0";



        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;


    });


    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    };


    $scope.listadoPaginado = function (filtro_actual) {

        var filtro = filtro_actual;

        proyectosSrv.guardar_filtro_adm(filtro);


        console.log("este es el filtro");
        console.log(filtro);

        proyectosSrv.listado(filtro).then(function (respuesta) {

            $scope.proyectos = respuesta.data.pagina.proyectos;

            $scope.bigTotalItems = respuesta.data.pagina.totalRegistros;

            console.log("Los proyectos");
            console.log(respuesta);
        });
    };

    $scope.volver = function () {


        $location.path("/empresas");
    }

}

app.controller("proyectoAgregarCtrl", ["$scope", "proyectosSrv", "tipoSubsidiosSrv", "procesosSrv", "usuariosSrv", "estadoProyectosSrv", "empresasSrv", "regionSrv", "provinciaSrv", "comunaSrv", "tipologiasSrv", "$location", "$routeParams", "rolUsuarioSrv","tipoExpedientesSrv", ProyectoAgregarCtrl]);

function ProyectoAgregarCtrl($scope, proyectosSrv, tipoSubsidiosSrv, procesosSrv, usuariosSrv, estadoProyectosSrv, empresasSrv, regionSrv, provinciaSrv, comunaSrv, tipologiasSrv, $location, $routeParams, rolUsuarioSrv, tipoExpedientesSrv) {

    $scope.proyecto = {
        Id_Proyecto: 0,
        Codigo_Proyecto: "",
        Nombre_Proyecto: "",
        Rut_Empresa: "",
        Rut_Revisor: "",
        Id_Estado_Proyecto: "",
        Id_Tipo_Subsidio: "",
        Id_Tipologia: "",
        Cantidad_Familias: "",
        Id_Region: "",
        Id_Provincia: "",
        Id_Comuna: "",
        Id_Proceso: $routeParams.Id_Proceso
    };

    var filtro = tipoSubsidiosSrv.filtro_actual();

    $scope.subsidios = [];

    tipoSubsidiosSrv.listado(filtro).then(function (respuesta) {

        $scope.subsidios = respuesta.data.pagina.tiposSubsidio;


    })

    $scope.tipoExpedientes = [];

    $scope.validarTipoExpediente = function () {

        tipoExpedientesSrv.porProceso($scope.proyecto.Id_Proceso).then(function (respuesta) {

            $scope.tipoExpedientes = respuesta.data.pagina.tiposExpediente;

            if ($scope.tipoExpedientes.length == 0) {

                alert("No existen tipos de expedientes definidos para el trámite seleccionado. Por favor agregue los tipos de expedientes que se usarán y luego agruegue los proyectos");

                $scope.proyecto.Id_Proceso = "";
            }


        })


    }


    



    $scope.procesos = [];

    procesosSrv.listadoGeneral().then(function (respuesta) {

        $scope.procesos = respuesta.data.pagina.procesos;

       

    })


    $scope.estados = [];

    estadoProyectosSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosProyecto;
        $scope.proyecto.Id_Estado_Proyecto = "1";
    })

    $scope.empresas = [];

    empresasSrv.listadoGeneral().then(function (respuesta) {

        $scope.empresas = respuesta.data.pagina.empresas;
    });


    var filtroTipologia = {
        tipologia: {
            "Id_Tipologia": 0,
            "Nombre_Tipologia": "",
            "Id_Tipo_Subsidio": ""
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        } 
    };

    $scope.lista_de_tipologias = function () {

        console.log(filtroTipologia);

        if ($scope.proyecto.Id_Tipo_Subsidio == "") {
            filtroTipologia.tipologia.Id_Tipo_Subsidio = "-1";
            tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {
                $scope.tipologias = respuesta.data.pagina.tipologias;
            })
        }
        else {
            filtroTipologia.tipologia.Id_Tipo_Subsidio = $scope.proyecto.Id_Tipo_Subsidio;
            tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {
                $scope.tipologias = respuesta.data.pagina.tipologias;
            })
        }
    }


    $scope.revisores = [];
    $scope.regiones = [];
    $scope.provincias = [];
    $scope.comunas = [];

    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.proyecto.Id_Region = usuariosSrv.usuarioActual().Id_Region.toString();

        regionSrv.Listado().then(function (respuesta) {

            $scope.regiones = respuesta.data;

            $scope.lista_de_provincias();
        });


        // se debe hacer algo ya que estos revisores cambian de acuerdo al tramite

        rolUsuarioSrv.porRol(19).then(function (respuesta) {

            $scope.revisores = respuesta.data.pagina.rolesUsuario;
        });
    });


    $scope.lista_de_provincias = function () {

        provinciaSrv.PorRegion($scope.proyecto.Id_Region).then(function (respuesta) {
            $scope.provincias = respuesta.data;

            if ($scope.proyecto.Id_Region != usuariosSrv.usuarioActual().Id_Region) {

                $scope.proyecto.Id_Provincia = "";
            }
            else {

                $scope.proyecto.Id_Provincia = usuariosSrv.usuarioActual().Id_Provincia.toString();
            }

            $scope.lista_de_comunas();
        })
    }

    $scope.lista_de_comunas = function () {

        if ($scope.proyecto.Id_Provincia == "") {

            comunaSrv.Listado(0).then(function (respuesta) {
                $scope.comunas = respuesta.data;
            })
        }
        else {

            comunaSrv.Listado($scope.proyecto.Id_Provincia).then(function (respuesta) {
                $scope.comunas = respuesta.data;
            })
        }
    };

    $scope.proyectoExiste = false;

    $scope.verificarProyecto = function () {

        $scope.proyectoExiste = false;

        if ($scope.proyecto.Codigo_Proyecto !== "0" && $scope.proyecto.Codigo_Proyecto !== undefined) {

            proyectosSrv.porCodigo($scope.proyecto.Codigo_Proyecto).then(function (respuesta) {
                
                if (respuesta.data.pagina.proyectos.length > 0) {
                   
                        $scope.proyectoExiste = true;

                    $scope.proyecto.Codigo_Proyecto = undefined;
                }

            });
        }
        
    };

    $scope.guardando_proyecto = false;

    $scope.guardar = function () {

        console.log("este es el proyecto");
        console.log($scope.proyecto);


        proyectosSrv.agregar($scope.proyecto).then(function (respuesta) {

            $scope.volver();
        })
    };

    $scope.volver = function () {

        $location.path("/procesos/"+ $scope.proyecto.Id_Proceso+"/proyectos");

    }

    $scope.closeAlert2 = function () {

        $scope.proyectoExiste = false;

    };
}


app.controller("proyectoEditarCtrl", ["$scope", "proyectosSrv", "tipoSubsidiosSrv", "procesosSrv", "usuariosSrv", "estadoProyectosSrv", "empresasSrv", "regionSrv", "provinciaSrv", "comunaSrv", "tipologiasSrv", "$location", "$routeParams", "rolUsuarioSrv", ProyectoEditarCtrl]);

function ProyectoEditarCtrl($scope, proyectosSrv, tipoSubsidiosSrv, procesosSrv ,usuariosSrv, estadoProyectosSrv, empresasSrv, regionSrv, provinciaSrv, comunaSrv, tipologiasSrv, $location, $routeParams, rolUsuarioSrv) {

    $scope.proyecto = {};

    $scope.subsidios = [];
    $scope.estados = [];
    $scope.regiones = [];
    $scope.provincias = [];
    $scope.comunas = [];

   
    $scope.empresas = [];

    empresasSrv.listadoGeneral().then(function (respuesta) {

        $scope.empresas = respuesta.data.pagina.empresas;
    });


    $scope.guardando_proyecto = false;

    var filtro = tipoSubsidiosSrv.filtro_actual();

    tipoSubsidiosSrv.listado(filtro).then(function (respuesta) {

        $scope.subsidios = respuesta.data.pagina.tiposSubsidio;
    })

    $scope.procesos = [];

    procesosSrv.listadoGeneral().then(function (respuesta) {

        $scope.procesos = respuesta.data.pagina.procesos;

       
    })


    estadoProyectosSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosProyecto;
    })

    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        regionSrv.Listado().then(function (respuesta) {

            $scope.regiones = respuesta.data;
        })
        // ojo aqui depende el tramite

        rolUsuarioSrv.porRol(19).then(function (respuesta) {

            $scope.revisores = respuesta.data.pagina.rolesUsuario;
            console.log("estos son los revisores");
            console.log(respuesta);
        });
    })


    var filtroTipologia = {
        tipologia: {
            "Id_Tipologia": 0,
            "Nombre_Tipologia": "",
            "Id_Tipo_Subsidio": ""
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };


    $scope.lista_de_tipologias = function () {

        console.log(filtroTipologia);

        if ($scope.proyecto.Id_Tipo_Subsidio == "") {

            tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {

                $scope.tipologias = respuesta.data.pagina.tipologias;
            })
        }
        else {

            filtroTipologia.tipologia.Id_Tipo_Subsidio = $scope.proyecto.Id_Tipo_Subsidio;

            tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {

                $scope.tipologias = respuesta.data.pagina.tipologias;
            })
        }
    }


    $scope.lista_de_provincias = function () {

        provinciaSrv.PorRegion($scope.proyecto.Id_Region).then(function (respuesta) {
            $scope.provincias = respuesta.data;

            if ($scope.proyecto.Id_Region != usuariosSrv.usuarioActual().Id_Region) {

                $scope.proyecto.Id_Provincia = "";
            }
            else {

                $scope.proyecto.Id_Provincia = usuariosSrv.usuarioActual().Id_Provincia.toString();
            }

            $scope.lista_de_comunas();
        });
    };


    $scope.lista_de_comunas = function () {

        if ($scope.proyecto.Id_Provincia == "") {

            comunaSrv.Listado(0).then(function (respuesta) {
                $scope.comunas = respuesta.data;
            });
        }
        else {

            comunaSrv.Listado($scope.proyecto.Id_Provincia).then(function (respuesta) {
                $scope.comunas = respuesta.data;
            });
        }
    };
         

    proyectosSrv.porId($routeParams.Id_Proyecto).then(function (respuesta) {

        $scope.proyecto = respuesta.data.pagina.proyectos[0];

        console.log(respuesta);

        $scope.proyecto.Codigo_Proyecto = $scope.proyecto.Codigo_Proyecto.toString();
        $scope.proyecto.Nombre_Proyecto = $scope.proyecto.Nombre_Proyecto.toString();
        $scope.proyecto.Rut_Empresa = $scope.proyecto.Rut_Empresa.toString();
        $scope.proyecto.Rut_Revisor = $scope.proyecto.Rut_Revisor.toString();
        $scope.proyecto.Id_Estado_Proyecto = $scope.proyecto.Id_Estado_Proyecto.toString();
        $scope.proyecto.Id_Tipo_Subsidio = $scope.proyecto.Id_Tipo_Subsidio.toString();

        filtroTipologia.tipologia.Id_Tipo_Subsidio = $scope.proyecto.Id_Tipo_Subsidio;

        tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {

            $scope.tipologias = respuesta.data.pagina.tipologias;

        })

        $scope.proyecto.Id_Tipologia = $scope.proyecto.Id_Tipologia.toString();
        $scope.proyecto.Cantidad_Familias = $scope.proyecto.Cantidad_Familias;
        $scope.proyecto.Id_Region = $scope.proyecto.Id_Region.toString();

        provinciaSrv.PorRegion($scope.proyecto.Id_Region).then(function (respuesta) {
            $scope.provincias = respuesta.data;
        })
        $scope.proyecto.Id_Provincia = $scope.proyecto.Id_Provincia.toString();

        comunaSrv.Listado($scope.proyecto.Id_Provincia).then(function (respuesta) {
            $scope.comunas = respuesta.data;
        })

        $scope.proyecto.Id_Comuna = $scope.proyecto.Id_Comuna.toString();


        $scope.proyecto.Id_Tipo_Tramite = $scope.proyecto.Id_Tipo_Tramite.toString();
    });


    $scope.guardar = function () {

        console.log($scope.proyecto);

        proyectosSrv.actualizar($scope.proyecto).then(function (respuesta) {

            $location.path("/proyectosAdm");

    

        });
    };




}

app.controller("proyectoEditarAdmCtrl", ["$scope", "proyectosSrv", "tipoSubsidiosSrv", "procesosSrv", "usuariosSrv", "estadoProyectosSrv", "empresasSrv", "regionSrv", "provinciaSrv", "comunaSrv", "tipologiasSrv", "$location", "$routeParams", "rolUsuarioSrv","tipoExpedientesSrv", ProyectoEditarAdmCtrl]);

function ProyectoEditarAdmCtrl($scope, proyectosSrv, tipoSubsidiosSrv, procesosSrv, usuariosSrv, estadoProyectosSrv, empresasSrv, regionSrv, provinciaSrv, comunaSrv, tipologiasSrv, $location, $routeParams, rolUsuarioSrv, tipoExpedientesSrv) {

    $scope.proyecto = {};

    $scope.subsidios = [];
    $scope.estados = [];
    $scope.regiones = [];
    $scope.provincias = [];
    $scope.comunas = [];
    $scope.empresas = [];

    empresasSrv.listadoGeneral().then(function (respuesta) {

        $scope.empresas = respuesta.data.pagina.empresas;

        console.log("estos son los empresas");
        console.log($scope.empresas);
    });


    $scope.tipoExpedientes = [];

    $scope.validarTipoExpediente = function () {

        tipoExpedientesSrv.porProceso($scope.proyecto.Id_Proceso).then(function (respuesta) {

            $scope.tipoExpedientes = respuesta.data.pagina.tiposExpediente;

            if ($scope.tipoExpedientes.length == 0) {

                alert("No existen tipos de expedientes definidos para el trámite seleccionado. Por favor agregue los tipos de expedientes que se usarán y luego agruegue los proyectos");

                $scope.proyecto.Id_Proceso = "";
            }


        })


    }


    $scope.guardando_proyecto = false;

    var filtro = tipoSubsidiosSrv.filtro_actual();

    tipoSubsidiosSrv.listado(filtro).then(function (respuesta) {

        $scope.subsidios = respuesta.data.pagina.tiposSubsidio;
    })


    $scope.procesos = [];

    procesosSrv.listadoGeneral().then(function (respuesta) {

        $scope.procesos = respuesta.data.pagina.procesos;

        
    })


    estadoProyectosSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosProyecto;
    })

    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        regionSrv.Listado().then(function (respuesta) {

            $scope.regiones = respuesta.data;
        })

        rolUsuarioSrv.porRol(19).then(function (respuesta) {

            $scope.revisores = respuesta.data.pagina.rolesUsuario;

            console.log(respuesta);
        });
    })


    var filtroTipologia = {
        tipologia: {
            "Id_Tipologia": 0,
            "Nombre_Tipologia": "",
            "Id_Tipo_Subsidio": ""
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };


    $scope.lista_de_tipologias = function () {

        console.log(filtroTipologia);

        if ($scope.proyecto.Id_Tipo_Subsidio == "") {
            tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {
                $scope.tipologias = respuesta.data.pagina.tipologias;
            })
        }
        else {
            filtroTipologia.tipologia.Id_Tipo_Subsidio = $scope.proyecto.Id_Tipo_Subsidio;
            tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {
                $scope.tipologias = respuesta.data.pagina.tipologias;
            })
        }
    }


    $scope.lista_de_provincias = function () {

        provinciaSrv.PorRegion($scope.proyecto.Id_Region).then(function (respuesta) {
            $scope.provincias = respuesta.data;

            if ($scope.proyecto.Id_Region != usuariosSrv.usuarioActual().Id_Region) {

                $scope.proyecto.Id_Provincia = "";
            }
            else {

                $scope.proyecto.Id_Provincia = usuariosSrv.usuarioActual().Id_Provincia.toString();
            }

            $scope.lista_de_comunas();
        });
    };


    $scope.lista_de_comunas = function () {

        if ($scope.proyecto.Id_Provincia == "") {

            comunaSrv.Listado(0).then(function (respuesta) {
                $scope.comunas = respuesta.data;
            });
        }
        else {

            comunaSrv.Listado($scope.proyecto.Id_Provincia).then(function (respuesta) {
                $scope.comunas = respuesta.data;
            });
        }
    };


    proyectosSrv.porId($routeParams.Id_Proyecto).then(function (respuesta) {

        $scope.proyecto = respuesta.data.pagina.proyectos[0];

        console.log(respuesta);

        $scope.proyecto.Codigo_Proyecto = $scope.proyecto.Codigo_Proyecto.toString();
        $scope.proyecto.Nombre_Proyecto = $scope.proyecto.Nombre_Proyecto.toString();
        $scope.proyecto.Rut_Empresa = $scope.proyecto.Rut_Empresa.toString();
        $scope.proyecto.Rut_Revisor = $scope.proyecto.Rut_Revisor.toString();
        $scope.proyecto.Id_Estado_Proyecto = $scope.proyecto.Id_Estado_Proyecto.toString();
        $scope.proyecto.Id_Proceso = $scope.proyecto.Id_Proceso.toString();
        $scope.proyecto.Id_Tipo_Subsidio = $scope.proyecto.Id_Tipo_Subsidio.toString();

        filtroTipologia.tipologia.Id_Tipo_Subsidio = $scope.proyecto.Id_Tipo_Subsidio;
        tipologiasSrv.listado(filtroTipologia).then(function (respuesta) {
            $scope.tipologias = respuesta.data.pagina.tipologias;
        })

        $scope.proyecto.Id_Tipologia = $scope.proyecto.Id_Tipologia.toString();
        $scope.proyecto.Cantidad_Familias = $scope.proyecto.Cantidad_Familias;
        $scope.proyecto.Id_Region = $scope.proyecto.Id_Region.toString();

        provinciaSrv.PorRegion($scope.proyecto.Id_Region).then(function (respuesta) {
            $scope.provincias = respuesta.data;
        })
        $scope.proyecto.Id_Provincia = $scope.proyecto.Id_Provincia.toString();

        comunaSrv.Listado($scope.proyecto.Id_Provincia).then(function (respuesta) {
            $scope.comunas = respuesta.data;
        })

        $scope.proyecto.Id_Comuna = $scope.proyecto.Id_Comuna.toString();
    });


    $scope.guardar = function () {

       
        proyectosSrv.actualizar($scope.proyecto).then(function (respuesta) {

            $scope.volver();
        });
    };

    $scope.volver = function () {

        $location.path("/procesos/" + $scope.proyecto.Id_Proceso + "/proyectos");

    }


}
