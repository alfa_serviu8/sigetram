﻿app.controller("tipoExpedientesCtrl", ["$scope", "tipoExpedientesSrv", "procesosSrv", "usuariosSrv", "$location", "$routeParams", TipoExpedientesCtrl]);

function TipoExpedientesCtrl($scope, tipoExpedientesSrv, procesosSrv, usuariosSrv,$location,$routeParams) {

    $scope.tipoExpedientes = [];

   
    $scope.filtro = tipoExpedientesSrv.filtro_actual();
    if ($routeParams.Id_Proceso) {
        $scope.filtro.tipoExpediente.Id_Proceso = $routeParams.Id_Proceso
    } else {


        $scope.filtro.tipoExpediente.Id_Proceso = "0";

    };
  
    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.tipoExpedientesPorTramite = [];
    $scope.agregar = function () {

        if ($scope.filtro.tipoExpediente.Id_Proceso == "0") {
            alert("Para agregar debe seleccionar un tipo de tramite");
        } else {


            $location.path("/tipoExpedientes/" + $scope.filtro.tipoExpediente.Id_Proceso + "/agregar")

            //tipoExpedientesSrv.porProceso($scope.filtro.tipoExpediente.Id_Proceso).then(function (respuesta) {

            //    $scope.tipoExpedientesPorTramite = respuesta.data.pagina.tiposExpediente;

            //    if ($scope.tipoExpedientesPorTramite.length == 0) {

            //        alert("No existen tipos de expedientes definidos para el trámite seleccionado. Por favor agregue los tipos de expedientes que se usarán y luego agruegue los proyectos");


            //    } else {
            //        $location.path("/tipoExpedientes/" + $scope.filtro.tipoExpediente.Id_Proceso + "/agregar")
            //    }
            //})


        }
    }

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();
    }


    $scope.procesos = [];

    procesosSrv.listadoGeneral().then(function (respuesta) {

        $scope.procesos = respuesta.data.pagina.procesos;

        console.log("estos son los tramites");

        console.log($scope.procesos);

        filtro.tipoExpediente.Id_Proceso = "0";


    })




    usuariosSrv.porLogin().then(function (response) {

       



        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.filtro = angular.copy(tipoExpedientesSrv.filtro_actual());

        $scope.listadoPaginado(tipoExpedientesSrv.filtro_actual());

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }) // fin usuario
  

    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }


    $scope.listadoPaginado = function (filtro_actual) {

        //var filtro = angular.copy(filtro_actual);
        var filtro = filtro_actual;

        tipoExpedientesSrv.guardar_filtro(filtro);

        tipoExpedientesSrv.listado(filtro).then(function (respuesta) {

            $scope.tipoExpedientes = respuesta.data.pagina.tiposExpediente;

                    
            $scope.bigTotalItems = respuesta.data.pagina.total_registros;
        })
    }
}// fin controlador


app.controller("tipoExpedienteAgregarCtrl", ["$scope", "tipoExpedientesSrv", "procesosSrv", "$location", "$routeParams",TipoExpedienteAgregarCtrl]);

function TipoExpedienteAgregarCtrl($scope, tipoExpedientesSrv, procesosSrv,$location,$routeParams) {

    $scope.tipoExpediente = {
        Vigente: "1",
        Id_Proceso:"0"
    };

    $scope.procesos = [];

    procesosSrv.listadoGeneral().then(function (response) {

        $scope.procesos = response.data.pagina.procesos;

        $scope.tipoExpediente.Id_Proceso = $routeParams.Id_Proceso;

    })



    $scope.guardando_tipo_expediente = false;

    $scope.guardar = function () {

        tipoExpedientesSrv.agregar($scope.tipoExpediente).then(function (respuesta) {

            console.log(respuesta);

            $scope.volver();

            //console.log(respuesta);
        })
    }

    $scope.volver = function () {

        $location.path("/procesos/" + $scope.tipoExpediente.Id_Proceso + "/tiposExpediente");

    }
}


app.controller("tipoExpedienteEditarCtrl", ["$scope", "tipoExpedientesSrv","procesosSrv", "$location", "$routeParams", TipoExpedienteEditarCtrl]);

function TipoExpedienteEditarCtrl($scope, tipoExpedientesSrv, procesosSrv, $location, $routeParams) {

    $scope.procesos = [];

    procesosSrv.listadoGeneral().then(function (response) {

        $scope.procesos = response.data.pagina.procesos;

    })


    $scope.tipoExpediente = {};

    $scope.guardando_tipo_expediente = false;

    $scope.filtro = angular.copy(tipoExpedientesSrv.filtro_actual());

    $scope.filtro.tipoExpediente.Id_Tipo_Expediente = $routeParams.Id_Tipo_Expediente;

   

    tipoExpedientesSrv.listado($scope.filtro).then(function (respuesta) {

   
        $scope.tipoExpediente = respuesta.data.pagina.tiposExpediente[0];

        $scope.tipoExpediente.Vigente = $scope.tipoExpediente.Vigente.toString();

        $scope.tipoExpediente.Id_Proceso = $scope.tipoExpediente.Id_Proceso.toString();

    });



    $scope.guardar = function () {

        tipoExpedientesSrv.actualizar($scope.tipoExpediente).then(function (respuesta) {

            $scope.volver();
        });
    }

    $scope.volver = function () {

        $location.path("/procesos/" + $scope.tipoExpediente.Id_Proceso + "/tiposExpediente");

    }
}