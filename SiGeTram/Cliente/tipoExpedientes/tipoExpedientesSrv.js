﻿app.factory("tipoExpedientesSrv", ["$http", "urlApi", TipoExpedientesSrv]);

function TipoExpedientesSrv($http, urlApi) {

    var ruta = urlApi + "tipoExpedientes/";

    var filtro = {
        tipoExpediente: {
            "Id_Tipo_Expediente": 0,
            "Id_Proceso": 0,
            "Nombre_Tipo_Expediente": "",
            "Vigente": -1
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }

    };



    var guardar_filtro = function (filtro) {

                 filtro = filtro;
    };

    var filtro_actual = function () {
        return filtro;
    };

    var listado = function (filtro) {
        return $http.post(ruta + "listado", filtro);
    };

    var listadoGeneral = function () {
        var filtro = {
            tipoExpediente: {
                "Id_Tipo_Expediente": 0,
                "Id_Proceso": 0,
                "Nombre_Tipo_Expediente": "",
                "Vigente": -1
            },
            pagina: {
                "numero": "1",
                "tamacno": "50"
            }

        };

      
        return listado(filtro);
    };

    var porProceso = function (idProceso) {
        var filtro = {
            tipoExpediente: {
                "Id_Tipo_Expediente": 0,
                "Id_Proceso": idProceso,
                "Nombre_Tipo_Expediente": "",
                "Vigente": -1
            },
            pagina: {
                "numero": "1",
                "tamacno": "50"
            }

        };


        return listado(filtro);
    };

    var agregar = function (tipoexpediente) {
        return $http.post(ruta, tipoexpediente);
    };
  
    var actualizar = function (tipoexpediente) {
        return $http.put(ruta, tipoexpediente);
    };
       
    var eliminar = function (Id_Tipo_Expediente) {

        return $http.delete(ruta, Id_Tipo_Expediente);
    };



    return {
        listado: listado,
        listadoGeneral: listadoGeneral,
        porProceso: porProceso,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual,
        agregar: agregar,
        actualizar: actualizar,
        eliminar: eliminar
    };

}



