﻿
app.controller("expedientesCtrl", ["$scope", "expedientesSrv", "procesosSrv", "usuariosSrv", "$location", "$routeParams", "estadoExpedientesSrv", "tipoExpedientesSrv", "rolUsuarioSrv","sistemaSrv", "proyectosSrv",ExpedientesCtrl]);

function ExpedientesCtrl( $scope, expedientesSrv, procesosSrv,usuariosSrv, $location, $routeParams, estadoExpedientesSrv, tipoExpedientesSrv, rolUsuarioSrv, sistemaSrv, proyectosSrv) {

    $scope.expedientes = [];
    $scope.estados = [];
    $scope.tipos = [];
    $scope.usuarioActual = {};
    $scope.proyecto = {};
    $scope.filtro = {};
    $scope.procesos = [];

   
    $scope.filtro = angular.copy(expedientesSrv.filtro_actual());



    $scope.tipoExpedientesPorProceso = function () {
       

        tipoExpedientesSrv.porProceso($scope.filtro.expediente.Id_Proceso).then(function (respuesta) {

            $scope.tipos = respuesta.data.pagina.tiposExpediente;

            console.log("estos son los tipos de expedientes");
            console.log($scope.tipos);
        });


    };


    procesosSrv.listadoGeneral().then(function (respuesta) {

        $scope.procesos = respuesta.data.pagina.procesos;
        console.log("estos son los procesos");
        console.log($scope.procesos);
        $scope.filtro.expediente.Id_Proceso = "0";

        $scope.tipoExpedientesPorProceso();



    })

    

    $scope.pageChanged = function () {

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;
    };

    $scope.tamacno_cambio = function () {

        $scope.buscar();
    };

    usuariosSrv.porLogin().then(function (response) {
                
        $scope.usuarioActual = response.data;


        // trae lista de estados de expediente

        estadoExpedientesSrv.listado().then(function (respuesta) {

            $scope.estados = respuesta.data.pagina.estadosExpediente;
        });

        $scope.filtro.expediente.Rut_Revisor = usuariosSrv.usuarioActual().Rut;

        // trae lista  de tipos de expediente

       

        $scope.filtro.expediente.Id_Proceso = "0";

        //$scope.tipoExpedientesPorProceso();

        if ($routeParams.id_proyecto) {
            $scope.filtro.expediente.Id_Proyecto = $routeParams.id_proyecto;
        } else {
            $scope.filtro.expediente.Id_Proyecto = 0;
            //$scope.filtro.expediente.Rut_Revisor = $scope.usuarioActual.Rut;
        }

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;

   
        rolUsuarioSrv.PorRol($scope.usuario.Rut, new sistemaSrv().numero(), $scope.usuario.Id_Servicio).then(function (response) {

            $scope.rol = new Roles(response.data);

            console.log("estos son los roles");
            console.log($scope.rol);



        });
    }); // fin usuario




    $scope.buscar = function () {

        if ($scope.filtro.expediente.Descripcion !== undefined) {

            $scope.filtro.expediente.Id_Proyecto = 0;
        }       

        console.log("este es el filtro");
        console.log($scope.filtro);

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    };

    $scope.agregar = function () {

        $location.path("proyectos/" + $routeParams.Id_Proyecto + "/expediente/agregar");
    };

    $scope.revisar = function (idExpediente) {
               
                   $location.path("expedientes/" + idExpediente + "/revisar");
       
    };

    $scope.listadoPaginado = function (filtro_actual) {

        var filtro = filtro_actual;
        

        expedientesSrv.guardar_filtro(filtro);


        console.log("estos son los expedientes");
        



        expedientesSrv.listado(filtro).then(function (respuesta) {

            $scope.expedientes = respuesta.data.pagina.expedientes;

            console.log($scope.expedientes);

            $scope.bigTotalItems = respuesta.data.pagina.totalRegistros;
        });
    };
    
       
    $scope.volver = function () {

        $location.path("/proyectos");
    };
}// fin controlador

app.controller("expedientesConsultaCtrl", ["$scope", "expedientesSrv", "procesosSrv", "usuariosSrv", "$location", "$routeParams", "estadoExpedientesSrv", "tipoExpedientesSrv", "rolUsuarioSrv", "sistemaSrv", "proyectosSrv", ExpedientesConsultaCtrl]);

function ExpedientesConsultaCtrl($scope, expedientesSrv, procesosSrv,usuariosSrv, $location, $routeParams, estadoExpedientesSrv, tipoExpedientesSrv, rolUsuarioSrv, sistemaSrv, proyectosSrv) {

    $scope.expedientes = [];
    $scope.estados = [];
    $scope.tipos = [];
    $scope.usuarioActual = {};
    $scope.proyecto = {};
    $scope.filtro = {};
    $scope.procesos = [];


    procesosSrv.listadoGeneral().then(function (respuesta) {

        $scope.procesos = respuesta.data.pagina.procesos;

        
    })



    //$scope.tipoExpedientesPorTramite = function () {

    //    tipoExpedientesSrv.porTipoTramite($scope.filtro.expediente.Id_Tipo_Tramite).then(function (respuesta) {

    //        $scope.tipos = respuesta.data.pagina.tiposExpediente;
    //    });


    //};







    $scope.pageChanged = function () {

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;
    };

    $scope.tamacno_cambio = function () {

        $scope.buscar();
    };


    usuariosSrv.porLogin().then(function (response) {

        $scope.usuarioActual = response.data;

        // trae lista de estados de expediente

        estadoExpedientesSrv.listado().then(function (respuesta) {

            $scope.estados = respuesta.data.pagina.estadosExpediente;
        });

        tipoExpedientesSrv.listadoGeneral().then(function (respuesta) {

            $scope.tipos = respuesta.data.pagina.tiposExpediente;
        });

        // trae lista  de tipos de expediente

        $scope.filtro = angular.copy(expedientesSrv.filtro_actual());

        if ($routeParams.id_proyecto) {
            $scope.filtro.expediente.Id_Proyecto = $routeParams.id_proyecto;
        } else {
            $scope.filtro.expediente.Id_Proyecto = 0;
            $scope.filtro.expediente.Rut_Revisor = 0;
        }

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;

        rolUsuarioSrv.porUsuario($scope.usuario.Rut).then(function (response) {

            $scope.rol = new Roles(response.data.pagina.rolesUsuario);
        });

        //tipoTramitesSrv.listadoGeneral().then(function (respuesta) {

        //    $scope.tipoTramites = respuesta.data.pagina.tiposTramite;

        //    console.log("estos son los tramites");

        //    console.log($scope.tipoTramites);


        //})




    }); // fin usuario


    $scope.buscar = function () {

        if ($scope.filtro.expediente.Descripcion !== undefined) {

            $scope.filtro.expediente.Id_Proyecto = 0;
        }

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    };

    $scope.agregar = function () {

        $location.path("proyectos/" + $routeParams.Id_Proyecto + "/expediente/agregar");
    };


    $scope.listadoPaginado = function (filtro_actual) {

        var filtro = filtro_actual;

        expedientesSrv.guardar_filtro(filtro);

        expedientesSrv.listado(filtro).then(function (respuesta) {

            $scope.expedientes = respuesta.data.pagina.expedientes;

            console.log("Este es el filtro");
            console.log(filtro);

            $scope.bigTotalItems = respuesta.data.pagina.totalRegistros;
        });
    };


    

    $scope.volver = function () {

        $location.path("/proyectos");
    };
}// fin controlador

app.controller("expedientesAdmCtrl", ["$scope", "expedientesSrv", "procesosSrv", "usuariosSrv", "$location", "$routeParams", "estadoExpedientesSrv", "tipoExpedientesSrv", "rolUsuarioSrv", "sistemaSrv", "proyectosSrv", ExpedientesAdmCtrl]);

function ExpedientesAdmCtrl($scope, expedientesSrv, procesosSrv, usuariosSrv, $location, $routeParams, estadoExpedientesSrv, tipoExpedientesSrv, rolUsuarioSrv, sistemaSrv, proyectosSrv) {

    $scope.expedientes = [];
    $scope.estados = [];
    $scope.tipos = [];
    $scope.usuarioActual = {};
    $scope.proyecto = {};
    $scope.filtro = {};
    $scope.procesos = [];



    $scope.filtro = angular.copy(expedientesSrv.filtro_actual_adm());

    $scope.usuariosPorRol = [];


    rolUsuarioSrv.porRol(27).then(function (response) {

        $scope.usuariosPorRol = response.data.pagina.rolesUsuario;

    



    })




    $scope.tipoExpedientesPorProceso = function () {


        tipoExpedientesSrv.porProceso($scope.filtro.expediente.Id_Proceso).then(function (respuesta) {

            $scope.tipos = respuesta.data.pagina.tiposExpediente;

            console.log("estos son los tipos de expedientes");
            console.log($scope.tipos);
        });


    };


    procesosSrv.listadoGeneral().then(function (respuesta) {

        $scope.procesos = respuesta.data.pagina.procesos;
        console.log("estos son los procesos");
        console.log($scope.procesos);
        $scope.filtro.expediente.Id_Proceso = "0";

        $scope.tipoExpedientesPorProceso();



    })


   

  
    $scope.pageChanged = function () {

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;
    };

    $scope.tamacno_cambio = function () {

        $scope.buscar();
    };


    usuariosSrv.porLogin().then(function (response) {

        $scope.usuarioActual = response.data;

        // trae lista de estados de expediente

        estadoExpedientesSrv.listado().then(function (respuesta) {

            $scope.estados = respuesta.data.pagina.estadosExpediente;
        });

        tipoExpedientesSrv.listadoGeneral().then(function (respuesta) {

            $scope.tipos = respuesta.data.pagina.tiposExpediente;
        });

               
        
  

        if ($routeParams.Id_Proyecto) {

            $scope.filtro.expediente.Id_Proyecto = $routeParams.Id_Proyecto;
        }
         else {
            $scope.filtro.expediente.Id_Proyecto = 0;
        
        }

        $scope.listadoPaginado($scope.filtro);


        console.log("Este es el filtro");
        console.log($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;

        //rolUsuarioSrv.porUsuario($scope.usuario.Rut, new sistemaSrv().numero(), $scope.usuario.Id_Servicio).then(function (response) {

        //    $scope.rol = new Roles(response.data);
        //});


       

        //tipoTramitesSrv.listadoGeneral().then(function (respuesta) {

        //    $scope.tipoTramites = respuesta.data.pagina.tiposTramite;

        //    console.log("estos son los tramites");

        //    console.log($scope.tipoTramites);


        //})




    }); // fin usuario


    $scope.buscar = function () {

        if ($scope.filtro.expediente.Descripcion !== undefined) {

            $scope.filtro.expediente.Id_Proyecto = 0;
        }

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    };

    
    $scope.listadoPaginado = function (filtro_actual) {

        var filtro = filtro_actual;

        console.log("este es el filtro");
        console.log(filtro);


        expedientesSrv.guardar_filtro_adm(filtro);

        expedientesSrv.listado(filtro).then(function (respuesta) {

            $scope.expedientes = respuesta.data.pagina.expedientes;

            console.log($scope.expedientes);

            $scope.bigTotalItems = respuesta.data.pagina.totalRegistros;
        });
    };
          

    $scope.volver = function () {
       $location.path("/proyectosAdm");
    };
}// fin controlador

app.controller("expedienteDocumentoCtrl", ["$scope", "expedientesSrv","expedienteDocumentoSrv", "procesosSrv", "usuariosSrv", "$location", "$routeParams", "estadoExpedientesSrv", "tipoExpedientesSrv", "rolUsuarioSrv", "sistemaSrv", "proyectosSrv", ExpedienteDocumentoCtrl]);

function ExpedienteDocumentoCtrl($scope, expedientesSrv, expedienteDocumentoSrv, procesosSrv, usuariosSrv, $location, $routeParams, estadoExpedientesSrv, tipoExpedientesSrv, rolUsuarioSrv, sistemaSrv, proyectosSrv) {

    $scope.expedientes = [];
    $scope.estados = [];
    $scope.tipos = [];
    $scope.usuarioActual = {};
    $scope.proyecto = {};
    $scope.filtro = {};
    $scope.procesos = [];



    $scope.filtro = angular.copy(expedienteDocumentoSrv.filtro_actual());

    $scope.usuariosPorRol = [];


    rolUsuarioSrv.porRol(27).then(function (response) {

        $scope.usuariosPorRol = response.data.pagina.rolesUsuario;





    })




    $scope.tipoExpedientesPorProceso = function () {


        tipoExpedientesSrv.porProceso($scope.filtro.expedienteDocumento.Id_Proceso).then(function (respuesta) {

            $scope.tipos = respuesta.data.pagina.tiposExpediente;

            console.log("estos son los tipos de expedientes");
            console.log($scope.tipos);
        });


    };


    procesosSrv.listadoGeneral().then(function (respuesta) {

        $scope.procesos = respuesta.data.pagina.procesos;
        console.log("estos son los procesos");
        console.log($scope.procesos);
        $scope.filtro.expediente.Id_Proceso = "0";

        $scope.tipoExpedientesPorProceso();



    })
          
    $scope.pageChanged = function () {

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;
    };

    $scope.tamacno_cambio = function () {

        $scope.buscar();
    };


    usuariosSrv.porLogin().then(function (response) {

        $scope.usuarioActual = response.data;

        // trae lista de estados de expediente

        estadoExpedientesSrv.listado().then(function (respuesta) {

            $scope.estados = respuesta.data.pagina.estadosExpediente;
        });

        tipoExpedientesSrv.listadoGeneral().then(function (respuesta) {

            $scope.tipos = respuesta.data.pagina.tiposExpediente;
        });





        if ($routeParams.Id_Proyecto) {

            $scope.filtro.expediente.Id_Proyecto = $routeParams.Id_Proyecto;
        }
        else {
            $scope.filtro.expediente.Id_Proyecto = 0;

        }

        $scope.listadoPaginado($scope.filtro);


        

        $scope.maxSize = 5;

        $scope.currentPage = 1;

       



    }); // fin usuario


    $scope.buscar = function () {

        if ($scope.filtro.expediente.Descripcion !== undefined) {

            $scope.filtro.expediente.Id_Proyecto = 0;
        }

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    };


    $scope.listadoPaginado = function (filtro_actual) {

        var filtro = filtro_actual;

        console.log("este es el filtro");
        console.log(filtro);


        expedienteDocumentoSrv.guardar_filtro(filtro);

        expedienteDocumentoSrv.listado(filtro).then(function (respuesta) {

            $scope.expedientes = respuesta.data.pagina.expedientesDocumento;

            console.log($scope.expedientes);

            $scope.bigTotalItems = respuesta.data.pagina.totalRegistros;
        });
    };


    $scope.volver = function () {
        $location.path("/proyectosAdm");
    };
}// fin controlador



app.controller("expedienteEditarCtrl", ["$scope", "expedientesSrv", "$location", "$routeParams", "documentosSrv", "estadoExpedientesSrv", "tipoExpedientesSrv", "usuariosSrv", ExpedienteEditarCtrl]);

function ExpedienteEditarCtrl($scope, expedientesSrv, $location, $routeParams, documentosSrv, estadoExpedientesSrv, tipoExpedientesSrv,usuariosSrv) {

    $scope.expediente = {};
    $scope.titulo = "Modificar expediente";
    $scope.estados = [];
    $scope.tipos = [];
    $scope.totalDocumentos = 0;

    $scope.$on("totalDocumentos", function (event, total) {

        $scope.totalDocumentos = total;
    });
  
    $scope.estableceTab = function (nuevoTab) {

        $scope.tab = nuevoTab;

        
    };

    $scope.esTab = function (tab) {

        return $scope.tab === tab;
    };

    $scope.estableceTab('Descripcion');

    $scope.enviar = function () {

        $location.path("/proyectos/" + $scope.expediente.Id_Proyecto + "/expediente/" + $scope.expediente.Id_Expediente + "/enviar");
    };


    if ($routeParams.Id_Expediente) {
        $scope.expediente.Id_Expediente = $routeParams.Id_Expediente;
   
    } else {

        $scope.expediente.Id_Expediente = 0;
        $scope.expediente.Id_Proyecto = $routeParams.Id_Proyecto;
        $scope.expediente.Id_Estado_Expediente = "1";
        $scope.expediente.Id_Tipo_Expediente = "0";
        $scope.titulo = "Agregar expediente";
    }
     
            
    usuariosSrv.porLogin().then(function (response) {

        $scope.usuarioActual = response.data;

        // trae lista de estados de expediente

        estadoExpedientesSrv.listado().then(function (respuesta) {

            $scope.estados = respuesta.data.listado;
        });

        tipoExpedientesSrv.listadoGeneral().then(function (respuesta) {

            $scope.tipos = respuesta.data.listado;
        });
    }); // fin trae usuario logeado

   
    if ($scope.expediente.Id_Expediente > 0) {

        expedientesSrv.porId($scope.expediente.Id_Expediente).then(function (respuesta) {

            $scope.expediente = respuesta.data.pagina.expedientes[0];

            $scope.expediente.Id_Estado_Expediente = $scope.expediente.Id_Estado_Expediente.toString();

            $scope.expediente.Id_Tipo_Expediente = $scope.expediente.Id_Tipo_Expediente.toString();
        });
    }


    $scope.guardando_expediente = false;
    
    $scope.siguiente = function () {

        if ($scope.expediente.Id_Expediente > 0) {

            expedientesSrv.actualizar($scope.expediente).then(function (respuesta) {
            });

        } else {

            expedientesSrv.agregar($scope.expediente).then(function (respuesta) {

                $scope.expediente.Id_Expediente = respuesta.data.Id_Expediente;
            });
        }

        $scope.$broadcast("Expediente", $scope.expediente);

        establecerVista("Documentos");
    };

    $scope.anterior = function () {

        establecerVista("Descripcion");
    };


    $scope.volver = function () {

        $location.path("/proyectos/" + $routeParams.Id_Proyecto + "/expedientes");
    };
}



app.controller("expedienteVerCtrl", ["$scope", "expedientesSrv", "$location", "$routeParams", "documentosSrv", "estadoExpedientesSrv", "tipoExpedientesSrv", "usuariosSrv", ExpedienteVerCtrl]);

function ExpedienteVerCtrl($scope, expedientesSrv, $location, $routeParams, documentosSrv, estadoExpedientesSrv, tipoExpedientesSrv, usuariosSrv) {

    $scope.expediente = {};
    $scope.titulo = '';
    $scope.estados = [];
    $scope.tipos = [];
    $scope.totalDocumentos = 0;

    $scope.estableceTab = function (nuevoTab) {

        $scope.tab = nuevoTab;

        if (nuevoTab !== "Descripcion") {

            $scope.$broadcast("Expediente", $scope.expediente);

            console.log("Distinto a Descripcion");
            console.log($scope.expediente);
        }
    };

    $scope.esTab = function (tab) {

        return $scope.tab === tab;

    };

    $scope.estableceTab('Descripcion');

    estadoExpedientesSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosExpediente;
    });

  


    expedientesSrv.porId($routeParams.id_expediente).then(function (respuesta) {

        $scope.expediente = respuesta.data.pagina.expedientes[0];
      
        $scope.expediente.Id_Estado_Expediente = $scope.expediente.Id_Estado_Expediente.toString();
        $scope.expediente.Id_Tipo_Expediente = $scope.expediente.Id_Tipo_Expediente.toString();

        tipoExpedientesSrv.porTipoTramite($scope.expediente.Id_Tipo_Tramite).then(function (respuesta) {

            $scope.tipos = respuesta.data.pagina.tiposExpediente;

            console.log("estos son los tipos");
            console.log(respuesta.data);
        });



    });


    $scope.volver = function () {

        $location.path("/expedientes");
    };
}





app.controller("tareasHistorialCtrl", ["$scope", "expediente_tareaSrv", TareasHistorialCtrl]);

function TareasHistorialCtrl($scope, expediente_tareaSrv) {

    $scope.tareas = [];

    $scope.tarea = {};

    $scope.$on("Expediente", function (event, expediente) {
      

        expediente_tareaSrv.historial(expediente.Id_Expediente).then(function (respuesta) {

            $scope.tareas = respuesta.data.pagina.expedienteTareas;

            console.log(respuesta.data);

            $scope.bigTotalItems = respuesta.data.pagina.total_registros;

            console.log("Estas son las tareas");
            console.log($scope.tareas);

        });
    });


}


app.controller("expedienteAdmVerCtrl", ["$scope", "expedientesSrv", "$location", "$routeParams", "documentosSrv", "estadoExpedientesSrv", "tipoExpedientesSrv", "usuariosSrv", ExpedienteAdmVerCtrl]);

function ExpedienteAdmVerCtrl($scope, expedientesSrv, $location, $routeParams, documentosSrv, estadoExpedientesSrv, tipoExpedientesSrv, usuariosSrv) {

    $scope.expediente = {};
    $scope.titulo = '';
    $scope.estados = [];
    $scope.tipos = [];
    $scope.totalDocumentos = 0;

    $scope.estableceTab = function (nuevoTab) {

        $scope.tab = nuevoTab;

        if (nuevoTab === "Documentos") {

            $scope.$broadcast("Expediente", $scope.expediente);
        }
    };

    $scope.esTab = function (tab) {

        return $scope.tab === tab;

    };

    $scope.estableceTab('Descripcion');

    estadoExpedientesSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosExpediente;
    });

    tipoExpedientesSrv.listadoGeneral().then(function (respuesta) {

        $scope.tipos = respuesta.data.pagina.tiposExpediente;
    });


    expedientesSrv.porId($routeParams.id_expediente).then(function (respuesta) {

        $scope.expediente = respuesta.data.pagina.expedientes[0];

        $scope.expediente.Id_Estado_Expediente = $scope.expediente.Id_Estado_Expediente.toString();
        $scope.expediente.Id_Tipo_Expediente = $scope.expediente.Id_Tipo_Expediente.toString();
    });


    $scope.volver = function () {

        $location.path("/expedientesAdm");
    };
}

app.controller("expedienteAdmEliminarCtrl", ["$scope", "expedientesSrv", "$location", "$routeParams", "documentosSrv", "estadoExpedientesSrv", "tipoExpedientesSrv", "usuariosSrv", ExpedienteAdmEliminarCtrl]);






function ExpedienteAdmEliminarCtrl($scope, expedientesSrv, $location, $routeParams, documentosSrv, estadoExpedientesSrv, tipoExpedientesSrv, usuariosSrv) {

    $scope.expediente = {};
    $scope.titulo = '';
    $scope.estados = [];
    $scope.tipos = [];
    $scope.totalDocumentos = 0;
    $scope.completando_tarea = false;

    $scope.estableceTab = function (nuevoTab) {

        $scope.tab = nuevoTab;

        if (nuevoTab === "Documentos") {

            $scope.$broadcast("Expediente", $scope.expediente);
        }
    };

    $scope.esTab = function (tab) {

        return $scope.tab === tab;

    };

    $scope.estableceTab('Descripcion');

    estadoExpedientesSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosExpediente;
    });

    tipoExpedientesSrv.listadoGeneral().then(function (respuesta) {

        $scope.tipos = respuesta.data.pagina.tiposExpediente;
    });


    expedientesSrv.porId($routeParams.id_expediente).then(function (respuesta) {

        $scope.expediente = respuesta.data.pagina.expedientes[0];

        $scope.expediente.Id_Estado_Expediente = $scope.expediente.Id_Estado_Expediente.toString();

        $scope.expediente.Id_Tipo_Expediente = $scope.expediente.Id_Tipo_Expediente.toString();

    });


    $scope.volver = function () {

        $location.path("/expedientesAdm");
    };

   
    $scope.eliminar = function () {

        $scope.completando_tarea = true;

        $scope.expediente.Id_Estado_Expediente = 5; // eliminado


        expedientesSrv.actualizar($scope.expediente).then(function (respuesta) {

            $scope.completando_tarea = false;


                             
                $location.path("/expedientesAdm");
           

          
        });
    };
    
}


app.controller("expedienteAdmAsignarCtrl", ["$scope", "expedientesSrv", "$location", "$routeParams", "rolUsuarioSrv", "estadoExpedientesSrv", "tipoExpedientesSrv", "usuariosSrv", ExpedienteAdmAsignarCtrl]);

function ExpedienteAdmAsignarCtrl($scope, expedientesSrv, $location, $routeParams, rolUsuarioSrv, estadoExpedientesSrv, tipoExpedientesSrv, usuariosSrv) {

    $scope.expediente = {};
    $scope.titulo = '';
    $scope.estados = [];
    $scope.tipos = [];
    $scope.totalDocumentos = 0;

    $scope.revisores = [];

    $scope.estableceTab = function (nuevoTab) {

        $scope.tab = nuevoTab;

        if (nuevoTab === "Documentos") {

            $scope.$broadcast("Expediente", $scope.expediente);
        }
    };

    $scope.esTab = function (tab) {

        return $scope.tab === tab;

    };

    $scope.estableceTab('Descripcion');

    estadoExpedientesSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosExpediente;
    });

    tipoExpedientesSrv.listadoGeneral().then(function (respuesta) {

        $scope.tipos = respuesta.data.pagina.tiposExpediente;
    });


    expedientesSrv.porId($routeParams.id_expediente).then(function (respuesta) {

        $scope.expediente = respuesta.data.pagina.expedientes[0];

        $scope.expediente.Id_Estado_Expediente = $scope.expediente.Id_Estado_Expediente.toString();
        $scope.expediente.Id_Tipo_Expediente = $scope.expediente.Id_Tipo_Expediente.toString();
        $scope.expediente.Rut_Revisor = $scope.expediente.Rut_Revisor.toString();

        rolUsuarioSrv.porRol(27).then(function (respuesta) {

            $scope.revisores = respuesta.data.pagina.rolesUsuario;

            console.log(respuesta);
        });
    });

    $scope.guardando_expediente = false;

    $scope.guardar = function () {
     
        expedientesSrv.actualizar($scope.expediente).then(function (respuesta) {
            $scope.volver();

        });
    }


    $scope.volver = function () {

        $location.path("/expedientesAdm");
    };
}




app.controller("expedienteEnviarCtrl", ["$scope", "expedientesSrv","funcionarioSrv","usuariosSrv","procesosSrv", "$location", "$routeParams", ExpedienteEnviarCtrl]);

function ExpedienteEnviarCtrl($scope, expedientesSrv, funcionarioSrv, usuariosSrv, procesosSrv, $location, $routeParams) {

       

    var filtro = {

        expediente: {
            "Id_Expediente": $routeParams.Id_Expediente,
            "Id_Tipo_Expediente": "0",
            "Id_Estado_Expediente": "0",
            "Id_Proyecto": "0",
            "Descripcion": ""
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    
    };

    $scope.expediente = {};
    var expediente = {};
    var revisor = {};

    // pero aquí tenemos que ver como se va hacer con el operador

    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        expedientesSrv.listado(filtro).then(function (respuesta) {

            $scope.expediente = respuesta.data.pagina.expedientes[0];

            expediente = new Expediente(respuesta.data.listado[0]);

            funcionarioSrv.PorRut($scope.expediente.Rut_Revisor).then(function (respuesta) {

                revisor = respuesta.data.funcionario;
            });
        });
    });

    $scope.enviar = function () {
           

        

        $scope.enviandoExpediente = false;

      
            $scope.expediente.Id_Estado_Expediente = 2;

            expedientesSrv.actualizar($scope.expediente).then(function (respuesta) {

                $scope.enviandoExpediente = false;

                $location.path("/proyectos/" + $routeParams.Id_Proyecto + "/expedientes");
            });
       
    };

    $scope.volver = function () {

        $location.path("/proyectos/" + $routeParams.Id_Proyecto + "/expediente/" + $routeParams.Id_Expediente + "/editar");
    };

}

app.controller("expedienteRevisarCtrl", ["$scope", "expediente_tareaSrv", "usuariosSrv", "$routeParams", "$location", "empresasSrv", ExpedienteRevisarCtrl]);

function ExpedienteRevisarCtrl($scope, expediente_tareaSrv,  usuariosSrv, $routeParams, $location, empresasSrv) {


    $scope.recepcionando_expediente = false;
    $scope.observando_expediente = false;


    var expediente = {};

    var empresa = {};

    $scope.vista = '';


    $scope.$on("totalDocumentos", function (event, total) {

        $scope.totalDocumentos = total;

    });


    $scope.estableceTab = function (nuevoTab) {

        $scope.tab = nuevoTab;

        if (nuevoTab === "Documentos") {

            $scope.$broadcast("Expediente", $scope.expediente_tarea);
        }

        if (nuevoTab === "Revisar") {

            $scope.$broadcast("Expediente", $scope.expediente_tarea);
        }

    };

    $scope.esTab = function (tab) {

        return $scope.tab == tab;

    };

    $scope.estableceTab('Descripcion');


    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        expediente_tareaSrv.porId($routeParams.id_expediente).then(function (respuesta) {

        $scope.expediente_tarea = respuesta.data.pagina.expedienteTareas[0];

            $scope.$broadcast("Expediente", $scope.expediente_tarea);

               console.log("El expediente es");

               console.log($scope.expediente_tarea);

               

            });
    });

    $scope.iniciarObservar = function () {

        $scope.vista = 'observado';

    }


    $scope.iniciarRecepcionar = function () {

        $scope.vista = 'recepcionado';

    }

    $scope.recepcionar = function () {

        $scope.recepcionando_expediente = true;

        if ($scope.expediente_tarea.Observaciones === undefined) {

            $scope.expediente_tarea.Observaciones = "Sin Observaciones";

        }

        $scope.expediente_tarea.Id_Estado_Expediente = 4;

        $scope.expediente_tarea.Rut_Asignado = usuariosSrv.usuarioActual().Rut;

        expediente_tareaSrv.recepcionar($scope.expediente_tarea).then(function (respuesta) {
            console.log("esta es la respuesta al actualizar el expediente");
            console.log(respuesta);

            $location.path("/expedientes");
           
        });// fin actualizar expediente

    };

    $scope.observar = function () {
        $scope.observando_expediente = true;

        if ($scope.expediente_tarea.Observaciones === undefined) {

            $scope.expediente_tarea.Observaciones = "Sin Observaciones";

        }

        $scope.expediente_tarea.Id_Estado_Expediente = 3;

        $scope.expediente_tarea.Rut_Asignado = usuariosSrv.usuarioActual().Rut;

        expediente_tareaSrv.observar($scope.expediente_tarea).then(function (respuesta) {
            console.log("esta es la respuesta al actualizar el expediente");
            console.log(respuesta);
            $location.path("/expedientes");

        });// fin actualizar expediente


    };

    $scope.volver = function () {

        $scope.vista = '';
    };

}

function ValidaRut(cRut) {

        cRut = cRut.replace(/[\.-]/g, "");
        cRut = cRut.toUpperCase();
        var patt = /^\d{1,8}[0-9K]$/;
        var ok = patt.test(cRut);
        var cStr = cRut.slice(0, -1);
        var cDig = cRut.slice(-1);
        var nSum = 0;
        var nVal = 0;
        var cVal = "";

        if (ok) {
            for (nMul = 2; cStr != ""; nMul = (nMul == 7) ? 2 : nMul + 1) {
                nSum += Number(cStr.slice(-1)) * nMul;
                cStr = cStr.slice(0, -1);
            }
            nVal = 11 - (nSum % 11);
            switch (nVal) {
                case 11:
                    cVal = "0";
                    break;
                case 10:
                    cVal = "K";
                    break;
                default:
                    cVal = nVal.toString();
            }
            ok = cVal == cDig;
        }

        return ok;
    }

    