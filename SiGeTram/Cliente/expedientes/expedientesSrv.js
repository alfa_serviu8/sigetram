﻿app.factory("expedientesSrv", ["$http", "urlApi", "$localStorage", ExpedientesSrv]);

function ExpedientesSrv($http, urlApi) {

    var ruta = urlApi + "expedientes/";

    var filtro = {
        expediente: {
            "Id_Expediente": 0,
            "Id_Tipo_Expediente": "0",
            "Nombre_Tipo_Expediente": "",
            "Nombre_Estado_Expediente": "",
            "Descripcion": "",
            "Id_Proyecto": 0,
            "Id_Proceso": 0,
            "Id_Estado_Expediente": "0",
            "Rut_Revisor": "0",
            "Rut_Empresa": "0"
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };


    var filtroAdm = {
        expediente: {
            "Id_Expediente": 0,
            "Id_Tipo_Expediente": "0",
            "Nombre_Tipo_Expediente": "",
            "Nombre_Estado_Expediente": "",
            "Descripcion": "",
            "Id_Proyecto": 0,
            "Id_Proceso": 0,
            "Id_Estado_Expediente": "0",
            "Rut_Revisor": "0",
            "Rut_Empresa": "0"
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };
 

    var guardar_filtro_adm = function (nuevoFiltroAdm) {
        filtroAdm = nuevoFiltroAdm;
    };

    var guardar_filtro = function (nuevoFiltro) {
        filtro = nuevoFiltro;
    };

    var filtro_actual = function () {
        return filtro;
    };

    var filtro_actual_adm = function () {
        return filtroAdm;
    };

    var listado = function (filtro) {

        
        return $http.post(ruta + "listado", filtro);
    };

    var porId = function (Id_Expediente) {

        var filtro = {

            expediente: {

                "Id_Expediente": Id_Expediente,
                "Id_Tipo_Expediente": 0,
                "Nombre_Tipo_Expediente": "",
                "Nombre_Estado_Expediente": "",
                "Descripcion": "",
                "Id_Proyecto": 0,
                "Id_Proceso": 0,
                "Id_Estado_Expediente": "0",
                "Rut_Revisor": 0,
                "Rut_Empresa": "0"
            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }
        };
     

        return listado(filtro);
    };

    var agregar = function (expediente) {

        return $http.post(ruta, expediente);
    };
  
    var actualizar = function (expediente) {

        return $http.put(ruta, expediente);
    };

    var enviar = function (expediente) {

        return $http.put(ruta + "enviar/", expediente);
    };

    var eliminar = function (expediente) {

        return $http.put(ruta + "eliminar/", expediente);
    };


    return {
        listado: listado,
        porId:porId,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual,
        guardar_filtro_adm: guardar_filtro_adm,
        filtro_actual_adm: filtro_actual_adm,
        agregar: agregar,
        actualizar: actualizar,
        eliminar: eliminar,
        enviar: enviar
    };
}

app.factory("expedienteDocumentoSrv", ["$http", "urlApi",  ExpedienteDocumentoSrv]);

function ExpedienteDocumentoSrv($http, urlApi) {

    var ruta = urlApi + "expedienteDocumento/";

    var filtro = {
        expediente: {
         
                "Id_Expediente": 0,
                "Id_Tipo_Expediente": "0",
                "Nombre_Tipo_Expediente": "",
                "Nombre_Estado_Expediente": "",
                "Descripcion": "",
                "Id_Proyecto": 0,
                "Id_Proceso": 0,
                "Id_Estado_Expediente": "0",
                "Rut_Revisor": "0",
                "Rut_Empresa": "0"
            
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };

    //var filtro = {
    //    expedienteDocumento: {
    //        Expediente: {
    //            "Id_Expediente": 0,
    //            "Id_Tipo_Expediente": "0",
    //            "Nombre_Tipo_Expediente": "",
    //            "Nombre_Estado_Expediente": "",
    //            "Descripcion": "",
    //            "Id_Proyecto": 0,
    //            "Id_Proceso": 0,
    //            "Id_Estado_Expediente": "0",
    //            "Rut_Revisor": "0",
    //            "Rut_Empresa": "0"
    //        }
    //    },
    //    pagina: {
    //        "numero": "1",
    //        "tamacno": "10"
    //    }
    //};
   
    

   

    var guardar_filtro = function (nuevoFiltro) {
        filtro = nuevoFiltro;
    };

    var filtro_actual = function () {
        return filtro;
    };

   

    var listado = function (filtro) {


        return $http.post(ruta + "listado", filtro);
    };
       


    return {
        listado: listado,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual
       
    };
}



app.factory("expediente_tareaSrv", ["$http", "urlApi", ExpedienteTareaSrv]);

function ExpedienteTareaSrv($http, urlApi) {

    var ruta = urlApi + "expedienteTarea/";

    var filtro = {
        expediente_tarea: {
            "Id_Expediente": 0,
            "Id_Estado_Expediente": "0",
            "Vigente":-1
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };


    var filtroAdm = {
        expediente_tarea: {
            "Id_Expediente": 0,
            "Id_Estado_Expediente": "0",
            "Vigente": -1
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };


    var guardar_filtro_adm = function (nuevoFiltroAdm) {
        filtroAdm = nuevoFiltroAdm;
    };

    var guardar_filtro = function (nuevoFiltro) {
        filtro = nuevoFiltro;
    };

    var filtro_actual = function () {
        return filtro;
    };

    var filtro_actual_adm = function () {
        return filtroAdm;
    };

    var listado = function (filtro) {
        return $http.post(ruta + "listado", filtro);
    };

    var historial = function (Id_Expediente) {

        var filtro = {

            expediente_tarea: {

                "Id_Expediente": Id_Expediente,
                "Id_Estado_Expediente": "0",
                "Vigente": -1
            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }
        };


        return listado(filtro);




    }

    var porId = function (Id_Expediente) {

        var filtro = {

            expediente_tarea: {

                "Id_Expediente": Id_Expediente,
                "Id_Estado_Expediente": "0",
                "Vigente": 1
            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }
        };


        return listado(filtro);
    };

    var agregar = function (expediente) {

        return $http.post(ruta, expediente);
    };

    var actualizar = function (expediente) {

        return $http.put(ruta, expediente);
    };

    var enviar = function (expediente) {

        return $http.put(ruta + "enviar/", expediente);
    };

    var observar = function (expediente) {

        return $http.put(ruta + "observar/", expediente);
    };

    var recepcionar = function (expediente) {

        return $http.put(ruta + "recepcionar/", expediente);
    };

    var modificar = function (expediente) {

        return $http.put(ruta + "modificar/", expediente);
    };

    var eliminar = function (expediente) {

        return $http.put(ruta + "eliminar/", expediente);
    };


    return {
        listado: listado,
        porId: porId,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual,
        guardar_filtro_adm: guardar_filtro_adm,
        filtro_actual_adm: filtro_actual_adm,
        agregar: agregar,
        actualizar: actualizar,
        eliminar: eliminar,
        enviar: enviar,
        observar: observar,
        recepcionar: recepcionar,
        modificar: modificar,
        historial: historial
    };
}

function Expediente(json) {

    this.json = json;

    this.string = function () {

        return new FormHorizontal([]).con("Codigo Proyecto", this.json.Codigo_Proyecto)
            .con("Nombre Proyecto", this.json.Nombre_Proyecto)
            .con("Tipo Expediente", this.json.Nombre_Tipo_Expediente)
            .con("Descripcion", this.json.Descripcion).string();
            
    };

    this.como_asunto_correo = function () {
        return "Tramitación expediente " + this.json.Descripcion;
    };
}

function FormHorizontal(campos) {

    this.campos = campos;

    this.con = function (nombreCampo, valorCampo) {

        var campos = this.campos;
        var tupla = { nombre: nombreCampo, valor: valorCampo };

        campos.push(tupla);

        return new FormHorizontal(campos);
    };

    this.string = function () {

        var head = "";
        var fila = "";

        for (var i = 0; i < campos.length; i++) {
            head = head + "<th style=\"background: #68889b; color: #fff; \">" + campos[i].nombre + "</th > ";
            fila = fila + "<td>" + campos[i].valor + "</td> ";
        }
        return "<table border = \"1\" width=\"600px\"><tr>" + head + "</tr>" + "<tr>" + fila + "</tr></table>";
    };
}

app.directive('documentos', [Documentos]);

function Documentos() {
    return {
        restrict: 'E',
        templateUrl: 'Cliente/documentos/vistas/frmDocumentos.html'
    };
};

app.directive('expedienteEditar', [ExpedienteEditar]);

function ExpedienteEditar() {
    return {
        restrict: 'E',
        templateUrl: 'Cliente/expedientes/vistas/frmExpedienteEditar.html'
    };
};

app.directive('tareaHistorial', [TareaHistorial]);

function TareaHistorial() {
    return {
        restrict: 'E',
        templateUrl: 'Cliente/expedientes/vistas/frmTareasHistorial.html'
    };
}
