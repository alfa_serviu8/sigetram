﻿app.controller("tipoSubsidiosCtrl", ["$scope", "tipoSubsidiosSrv", "usuariosSrv", "$location", "$routeParams", TipoSubsidiosCtrl]);

function TipoSubsidiosCtrl($scope, tipoSubsidiosSrv,usuariosSrv,$location,$routeParams) {

    $scope.tipoSubsidios = [];
   
    $scope.filtro = tipoSubsidiosSrv.filtro_actual();
  
    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();
    }
   
    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.filtro = angular.copy(tipoSubsidiosSrv.filtro_actual());

        $scope.listadoPaginado(tipoSubsidiosSrv.filtro_actual());

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }) // fin usuario
  

    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }


    $scope.listadoPaginado = function (filtro_actual) {

        //var filtro = angular.copy(filtro_actual);
        var filtro = filtro_actual;

        tipoSubsidiosSrv.guardar_filtro(filtro);

        tipoSubsidiosSrv.listado(filtro).then(function (respuesta) {

            $scope.tipoSubsidios = respuesta.data.pagina.tiposSubsidio;

                    
            $scope.bigTotalItems = respuesta.data.pagina.total_registros;
        })
    }
}// fin controlador


app.controller("tipoSubsidioAgregarCtrl", ["$scope", "tipoSubsidiosSrv", "$location", "$routeParams",TipoSubsidioAgregarCtrl]);

function TipoSubsidioAgregarCtrl($scope, tipoSubsidiosSrv,$location,$routeParams) {

    $scope.tipoSubsidio = {
        Vigente: "1"
    };

    $scope.guardando_tipo_subsidio = false;

    $scope.guardar = function () {

        tipoSubsidiosSrv.agregar($scope.tipoSubsidio).then(function (respuesta) {

            console.log(respuesta);

            $location.path("/tipoSubsidios");

            //console.log(respuesta);
        })
    }
}


app.controller("tipoSubsidioEditarCtrl", ["$scope", "tipoSubsidiosSrv", "$location", "$routeParams", TipoSubsidioEditarCtrl]);

function TipoSubsidioEditarCtrl($scope, tipoSubsidiosSrv, $location, $routeParams) {

    $scope.tipoSubsidio = {};

    $scope.guardando_tipo_subsidio = false;

    $scope.filtro = angular.copy(tipoSubsidiosSrv.filtro_actual());

    $scope.filtro.tipoSubsidio.Id_Tipo_Subsidio = $routeParams.Id_Tipo_Subsidio;

   

    tipoSubsidiosSrv.listado($scope.filtro).then(function (respuesta) {

   
        $scope.tipoSubsidio = respuesta.data.pagina.tiposSubsidio[0];

        $scope.tipoSubsidio.Vigente = $scope.tipoSubsidio.Vigente.toString();

    });



    $scope.guardar = function () {

        tipoSubsidiosSrv.actualizar($scope.tipoSubsidio).then(function (respuesta) {

            $location.path("/tipoSubsidios");
        });
    }
}