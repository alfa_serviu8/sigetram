﻿
app.factory("rolSrv", ["$http", "urlApi", "$localStorage", RolSrv]);

function RolSrv($http, urlApi, $localStorage) {


    var ruta = urlApi + "Roles/";

    var filtro = {
        rol: {
           
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }

    };

    var guardar_filtro = function (filtroNuevo) {

        filtro = filtroNuevo;
    };

    var filtro_actual = function () {

        return filtro;
    };

    var listado = function (filtro) {

        return $http.post(ruta + "listado", filtro);
    };

    
    

    return {
        listado: listado,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual

    };

}

app.factory("rolUsuarioSrv", ["$http", "urlApi", RolUsuarioSrv]);

function RolUsuarioSrv($http, urlApi) {


    var ruta = urlApi + "RolesUsuario/";

    var filtro = {
        rolUsuario: {
            "Id_Rol": 0,
            "Rut":0
        },
        pagina: {
            "numero": "1",
            "tamacno": "200"
        }

    };


    var guardar_filtro = function (filtroNuevo) {

        filtro = filtroNuevo;
    };

    var filtro_actual = function () {

        return filtro;
    };

    var listado = function (filtro) {

        return $http.post(ruta + "listado", filtro);
    };

    var porId = function (id) {

       


        return $http.get(ruta + id);
    };


    var porRol = function (id_rol) {

        var filtro = {
            rolUsuario: {
                "Id_Rol": id_rol,
                "Rut": 0
            },
            pagina: {
                "numero": "1",
                "tamacno": "2000"
            }

        };

        return listado(filtro);

        
    };

    var porUsuario = function (rut) {

        var filtro = {
            rolUsuario: {
                "Id_Rol": 0,
                "Rut": rut
            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }

        };

        return listado(filtro);
    };


   
    var agregar = function (rolUsuario) {

        return $http.post(ruta, rolUsuario);
    };

    var eliminar = function (rolUsuario) {

        return $http.delete(ruta + rolUsuario.Id);
    };

    return {
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual,
        porId: porId,
        porRol: porRol,
        porUsuario: porUsuario,
        agregar: agregar,
        eliminar: eliminar
    };

}