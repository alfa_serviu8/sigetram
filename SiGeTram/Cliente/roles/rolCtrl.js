﻿
app.controller("rolCtrl", ["$scope", "rolSrv", "usuariosSrv", "sistemaSrv", "$location", RolCtrl]);

function RolCtrl($scope, rolSrv, usuariosSrv, sistemaSrv, $location) {
   
    $scope.rol = {};

    $scope.roles = [];

    $scope.filtro = {};

    $scope.filtro = angular.copy(rolSrv.filtro_actual());

    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        rolSrv.listado($scope.filtro).then(function (response) {

            $scope.roles = response.data.pagina.roles;
                        

        });
    });

    
    $scope.verUsuarios = function (rol) {
        
        $location.path("/roles/" + rol.Id + "/usuarios");

    };




}

// Este controlador muestra los usuarios por rol

app.controller("rolUsuariosCtrl", ["$scope", "rolSrv", "usuariosSrv", "rolUsuarioSrv", "funcionarioSrv", "$routeParams", "$location", "sistemaSrv", RolUsuariosCtrl]);

function RolUsuariosCtrl($scope, rolSrv, usuariosSrv, rolUsuarioSrv, funcionarioSrv, $routeParams, $location, sistemaSrv) {

    // usuarios por rol

    $scope.rolesUsuario = [];

    console.log(usuariosSrv.usuarioActual());


    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        rolUsuarioSrv.porRol($routeParams.id_rol).then(function (response) {

            $scope.rolesUsuario = response.data.pagina.rolesUsuario;

            console.log("Estos son los usuarios por rol");

            console.log($scope.rolesUsuario);

        });

    });



    $scope.agregarUsuarios = function () {

        $location.path("/roles/" + $routeParams.id_rol + "/usuarios/agregar");
    };



    $scope.eliminarRolUsuario = function (rolusuario) {

        rolUsuarioSrv.eliminar(rolusuario).then(function (respuesta) {

            console.log(respuesta);

            rolUsuarioSrv.porRol($routeParams.id_rol).then(function (response) {

                $scope.rolesUsuario = response.data.pagina.rolesUsuario;

            });

        });
    }

    $scope.cancelar = function () {

        $location.path("/roles");


    }


}

app.controller("rolUsuariosAgregarCtrl", ["$scope", "rolSrv", "usuariosSrv", "rolUsuarioSrv", "funcionarioSrv", "$routeParams", "$location", "sistemaSrv", RolUsuariosAgregarCtrl]);

function RolUsuariosAgregarCtrl($scope, rolSrv, usuariosSrv, rolUsuarioSrv, funcionarioSrv, $routeParams, $location, sistemaSrv) {

    // usuarios por rol




    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.bigCurrentPage);

        $scope.listadoPaginado();
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {
        $scope.bigCurrentPage = pageNo;
    };

    $scope.maxSize = 5;
    $scope.bigTotalItems = 0;
    $scope.bigCurrentPage = 1;

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.bigCurrentPage);

        $scope.listadoPaginado();


    }

    $scope.filtro = {};

    $scope.filtro.Tamacno_Pagina = '10';

    $scope.funcionarios = [];

    $scope.filtro.Habilitado = '-1';

    $scope.listadoPaginado = function () {

        $scope.funcionarios = [];
        var filtro = {};
        filtro.Habilitado = 1;
        //filtro.Id_Servicio = usuariosSrv.usuarioActual().Id_Servicio;
        filtro.Id_Servicio = 0;
        filtro.Id_Dependencia = 0;
        filtro.Id_Calidad_Juridica = 0;
        filtro.Id_Estamento = 0;
        if ($scope.filtro.Nombre_Completo == undefined) {
            filtro.Nombre_Completo = 'SinFiltro';
        }
        else {

            filtro.Nombre_Completo = $scope.filtro.Nombre_Completo;
        }


        //filtro.Habilitado = -1;
        //filtro.Habilitado = $scope.filtro.Habilitado;

        filtro.Id_Genero = 0;
        filtro.Id_Estado_Civil = 0;
        filtro.Es_Persona_Natural = 1;
        filtro.Es_Usuario_Interno = -1;
        filtro.Es_Empresa_Provedor = 0;
        filtro.Es_Funcionario_Publico = 1;
        filtro.Pagina_Solicitada = $scope.bigCurrentPage;
        filtro.Tamacno_Pagina = $scope.filtro.Tamacno_Pagina;

        funcionarioSrv.GuardarFiltro(filtro);

        

        funcionarioSrv.ListadoPaginado(funcionarioSrv.filtro()).then(function (respuesta) {

            if (respuesta.data.listado.length > 0) {

                
                for (var i = 0; i < respuesta.data.listado.length; i++) {

                    var funcionario = {
                        Rut: 0,
                        Rut_Con_Dv: "",
                        Nombre_Completo: "",
                        Seleccionado: false
                    }

                    funcionario = respuesta.data.listado[i];

                    // ver si estaba seleccionado

                    for (var j = 0; j < seleccionados.length; j++) {

                        if (funcionario.Rut == seleccionados[j].Rut) {

                            funcionario.Seleccionado = true;

                        }


                    }

                    var esta_en_rol = false;


                    for (var k = 0; k < rolesUsuario.length; k++) {

                        if (funcionario.Rut == rolesUsuario[k].Rut) {



                            esta_en_rol = true;

                        }


                    };



                    if (!esta_en_rol) {
                        $scope.funcionarios.push(funcionario);
                    }

                }// fin for


                $scope.bigTotalItems = respuesta.data.Total_Registros;



            }

        })
    }


    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);



    });


    var rolesUsuario = [];

    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        rolUsuarioSrv.porRol($routeParams.id_rol).then(function (response) {

            rolesUsuario = response.data.pagina.rolesUsuario;

            $scope.listadoPaginado();

        })




    })



    // manejo del datepicker

    $scope.today = function () {
        $scope.funcionario.Fecha_Nacimiento = new Date();
    };


    $scope.clear = function () {
        $scope.funcionario.Fecha_Nacimiento = null;
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    //$scope.setDate = function (year, month, day) {
    //    $scope.funcionario.Fecha_Nacimiento = new Date(year, month, day);
    //};

    //$scope.formats = ['dd/MM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    //$scope.format = $scope.formats[0];

    $scope.popup1 = {
        opened: false
    };



    $scope.cancelar = function () {

        $location.path("/roles");


    }


    var seleccionados = [];

    $scope.seleccionar = function (f, isChecked) {
        if (isChecked == false) {
            for (var i = 0, L = seleccionados.length; i < L; i++) {
                if (seleccionados[i].Rut_Sin_Dv == f.Rut_Sin_Dv) {
                    seleccionados.splice(i, 1);
                    break;
                }
            }

        } else {
            seleccionados.push(f);
        }
    }







    $scope.agregar_funcionarios = function () {



        for (var i = 0; i < seleccionados.length; i++) {

            var rolUsuario = {

                Id: 0,
                Id_Rol: $routeParams.id_rol,
                Id_Sistema: new sistemaSrv().numero(),
                Id_Servicio: usuariosSrv.usuarioActual().Id_Servicio,
                Rut: seleccionados[i].Rut

            }


            rolUsuarioSrv.agregar(rolUsuario);
        }

            $location.path("/roles/" + $routeParams.id_rol + "/usuarios");

        

    }

}


