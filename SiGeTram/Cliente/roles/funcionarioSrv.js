﻿app.factory("funcionarioSrv", ["$http", "urlApiSigeFun", FuncionarioSrv]); 

function FuncionarioSrv($http, urlApiSigeFun) {
        
    var ruta = urlApiSigeFun + "funcionarios/";

    var funcionarios = [];

    var filtro_Actual = null;

    var GuardarFiltro = function (filtro) {

        filtro_Actual = filtro;

    }

    var filtro = function () {


        return filtro_Actual;
    }

    var GuardarListado = function (func) {

        funcionarios = func;
        
    }

    var ListadoActual = function () {

        return funcionarios;

    }

    var ListadoFuncionarios = function () {

        return funcionarios;
    }

    var GuardarFuncionarios = function (fun) {
        funcionarios = fun;
    }
    
    var PorRut = function (Rut) {
        return $http.get(ruta + Rut);
    };

    var ListadoPaginado = function (filtro) {

       
        console.log(filtro);
        
       return $http.get(ruta + filtro.Id_Servicio +"/"+filtro.Id_Dependencia+"/"+filtro.Id_Calidad_Juridica+"/"+filtro.Id_Estamento+"/"+filtro.Nombre_Completo+"/"+filtro.Habilitado+"/"+filtro.Id_Genero+"/"+filtro.Id_Estado_Civil+"/"+filtro.Es_Persona_Natural+"/" +filtro.Es_Empresa_Provedor + "/" + filtro.Es_Funcionario_Publico +"/"+filtro.Es_Usuario_Interno+"/"+filtro.Pagina_Solicitada+"/"+filtro.Tamacno_Pagina);
    };

    var Listado = function (id_servicio) {

        return $http.get(ruta+ "servicios/"+ id_servicio);
    };

    var Agregar = function (funcionario) {

        return $http.post(ruta, funcionario);
    };

    var Actualizar = function (funcionario) {

        return $http.put(ruta, funcionario);
        
    };

    var Establecer_datos = function (Rut) {
        return $http.get(ruta + "servicio/"+ Rut);

    }

    var Eliminar = function (idSolicitud) {

        return $http.delete(ruta + idSolicitud);
    };

   

    return {
        PorRut: PorRut,
        Listado: Listado,
        ListadoPaginado: ListadoPaginado,
        Agregar: Agregar,
        Actualizar: Actualizar,
        Eliminar: Eliminar,
        GuardarListado: GuardarListado,
        ListadoActual: ListadoActual,
        Establecer_datos: Establecer_datos,
        GuardarFuncionarios: GuardarFuncionarios,
        ListadoFuncionarios: ListadoFuncionarios,
        GuardarFiltro:GuardarFiltro,
        filtro:filtro


    };
}


