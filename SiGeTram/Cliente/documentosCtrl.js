﻿


app.controller("documentosCtrl", ["$scope", "usuariosSrv", "documentosSrv", "$routeParams","$location",DocumentosCtrl]);

function DocumentosCtrl($scope, usuariosSrv, documentosSrv, $routeParams,$location) {

    $scope.expediente = {};
    $scope.vista = '';
    $scope.subiendoArchivo = false;

    var legible = new Legible(1000, 100000);

    $scope.$on("Expediente", function (event, expediente) {

        $scope.expediente = expediente;

       
        console.log($scope.expediente);

        $scope.filtro = {
            documento: {
                "Id_Expediente": expediente.Id_Expediente

            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }

        };

       

        $scope.buscar();

    });


         
   
   
       
    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.numero_pagina = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pagina seteada a: ' + $scope.currentPage);
    };


    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();


    };

    
    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;


    };


    $scope.listadoPaginado = function (filtro_actual) {

             
        documentosSrv.listado(filtro_actual).then(function (respuesta) {

            $scope.documentos = respuesta.data.pagina.documentos;

            for (var i = 0; i < respuesta.data.pagina.documentos.length; i++) {

                $scope.documentos[i].Tamacno = legible.valor($scope.documentos[i].Tamacno);

            }

            $scope.$emit("totalDocumentos", $scope.documentos.length);



            $scope.bigTotalItems = respuesta.data.pagina.total_registros;


        });

    };


   



    var formdata;

   

    $scope.getTheFiles = function ($files) {

        formdata = new FormData();
      
        formdata.append("id_expediente", $scope.filtro.documento.Id_Expediente);

        angular.forEach($files, function (value, key) {

            formdata.append(key, value);


        });

    };

    // sube la información es decir losa archivos con la identificación del informe

    $scope.uploadFiles = uploadFiles;

    $scope.subiendoArchivo = false;

    function uploadFiles() {


        $scope.subiendoArchivo = true;

        documentosSrv.uploadFiles(formdata).then(function (response) {

                    
            $scope.subiendoArchivo = false;

            $scope.buscar();


        },
            // funcion en caso de error
            function (response) {

            });

    }
       
       
   

    $scope.eliminar = function (idDocumento) {

        documentosSrv.eliminar(idDocumento).then(function (response) {


            $scope.buscar();


        });



    };

}// fin controlador

