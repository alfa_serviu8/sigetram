﻿'use strict';




var app = angular.module('SiGeTram', ['ngRoute', 'ui.bootstrap', 'gp.rutValidator', 'ngMessages', 'ngStorage']);

app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/empresas', {
            controller: 'empresasCtrl',
            templateUrl: 'Cliente/empresas/vistas/frmEmpresas.html'
        }).when('/empresas/agregar', {
            controller: 'empresaAgregarCtrl',
            templateUrl: 'Cliente/empresas/vistas/frmEmpresaAgregar.html'
        }).when('/empresas/:Rut_Empresa/editar', {
            controller: 'empresaEditarCtrl',
            templateUrl: 'Cliente/empresas/vistas/frmEmpresaEditar.html'
        }).when('/empresas/:Rut_Empresa/resetearClave', {
            controller: 'resetearClaveCtrl',
            templateUrl: 'Cliente/empresas/vistas/frmResetearClave.html'
        }).when('/empresas/:Rut_Empresa/eliminar', {
            controller: 'empresaEliminarCtrl',
            templateUrl: 'Cliente/empresas/vistas/frmEmpresaEliminar.html'
        }).when('/empresas/:Rut_Empresa/existe', {
            controller: 'empresaExisteCtrl',
            templateUrl: 'Cliente/empresas/vistas/frmEmpresaExiste.html'
        }).when('/empresas/:Rut_Empresa/proyectos', {
            controller: 'proyectosAdmCtrl',
            templateUrl: 'Cliente/proyectos/vistas/frmProyectosAdm.html'
        }).when('/', {
            controller: 'expedientesCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedientes.html'
        }).when('/procesos', {
            controller: 'procesosCtrl',
            templateUrl: 'Cliente/procesos/vistas/frmProcesos.html'
        }).when('/procesos/:Id_Proceso/proyectos', {
            controller: 'proyectosAdmCtrl',
            templateUrl: 'Cliente/proyectos/vistas/frmProyectosAdm.html'
        }).when('/procesos/:Id_Proceso/tiposExpediente', {
            controller: 'tipoExpedientesCtrl',
            templateUrl: 'Cliente/tipoExpedientes/vistas/frmTipoExpedientes.html'
        }).when('/tipoTramites', {
            controller: 'tipoTramitesCtrl',
            templateUrl: 'Cliente/tipoTramites/vistas/frmTipoTramites.html'
        }).when('/tipoTramites/:id_tipo_tramite/editar', {
            controller: 'tipoTramiteEditarCtrl',
            templateUrl: 'Cliente/tipoTramites/vistas/frmTipoTramiteEditar.html'
        }).when('/tipoTramites/:id_tipo_tramite/respaldar', {
            controller: 'tipoTramiteRespaldarCtrl',
            templateUrl: 'Cliente/tipoTramites/vistas/frmTipoTramiteRespaldar.html'
        }).when('/tipoTramites/agregar', {
            controller: 'tipoTramiteAgregarCtrl',
            templateUrl: 'Cliente/tipoTramites/vistas/frmTipoTramiteEditar.html'
        }).when('/proyectosAdm', {
            controller: 'proyectosAdmCtrl',
            templateUrl: 'Cliente/proyectos/vistas/frmProyectosAdm.html'
        }).when('/proyectosAdm/:Id_Proyecto/expedientesAdm', {
            controller: 'expedientesAdmCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedientesAdm.html'
        }).when('/proyectosAdm/:Id_Proyecto/editar', {
            controller: 'proyectoEditarAdmCtrl',
            templateUrl: 'Cliente/proyectos/vistas/frmProyectoEditarAdm.html'
        }).when('/proyectos', {
            controller: 'proyectosCtrl',
            templateUrl: 'Cliente/proyectos/vistas/frmProyectos.html'

        }).when('/proyectos/:Id_Proceso/agregar', {
            controller: 'proyectoAgregarCtrl',
            templateUrl: 'Cliente/proyectos/vistas/frmProyectoAgregar.html'

        }).when('/proyectos/:Id_Proyecto/editar', {
            controller: 'proyectoEditarCtrl',
            templateUrl: 'Cliente/proyectos/vistas/frmProyectoEditar.html'

        }).when('/proyectos/:Id_Proyecto/eliminar', {
            controller: 'proyectoEliminarCtrl',
            templateUrl: 'Cliente/proyectos/vistas/frmProyectoEliminar.html'
        }).when('/expedientes', {
            controller: 'expedientesCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedientes.html'
        }).when('/expedientesConsulta', {
            controller: 'expedientesConsultaCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedientesConsulta.html'
        }).when('/proyectos/:id_proyecto/expedientes', {
            controller: 'expedientesCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedientes.html'
        }).when('/proyectos/:id_proyecto/expediente/agregar', {
            controller: 'expedienteEditarCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedienteEditar.html'
        }).when('/proyectos/:id_proyecto/expediente/:id_expediente/editar', {
            controller: 'expedienteEditarCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedienteEditar.html'
        }).when('/expedientes/:id_expediente/ver', {
            controller: 'expedienteVerCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedienteVer.html'
        }).when('/expedientes/:id_expediente/revisar', {
            controller: 'expedienteRevisarCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedienteRevisar.html'
        }).when('/expedientesAdm/:id_expediente/ver', {
            controller: 'expedienteAdmVerCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedienteAdmVer.html'
        }).when('/expedientesAdm/:id_expediente/asignar', {
            controller: 'expedienteAdmAsignarCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedienteAdmAsignar.html'
        }).when('/expedientesAdm/:id_expediente/eliminar', {
            controller: 'expedienteAdmEliminarCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedienteAdmEliminar.html'
        }).when('/expedientesAdm', {
            controller: 'expedientesAdmCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedientesAdm.html'
        }).when('/expedientesDocumento', {
            controller: 'expedienteDocumentoCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedienteDocumento.html'
        }).when('/proyectos/:id_proyecto/expediente/:id_expediente/enviar', {
            controller: 'expedienteEnviarCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedienteEnviar.html'
        }).when('/proyectos/:id_proyecto/expediente/:id_expediente/ver', {
            controller: 'expedienteVerCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedienteVer.html'
        }).when('/proyectos/:id_proyecto/expediente/:id_expediente/documentos', {
            controller: 'documentosCtrl',
            templateUrl: 'Cliente/documentos/vistas/frmDocumentos.html'
        }).when('/proyectos/:id_proyecto/expediente/:id_expediente/documentos/ver', {
            controller: 'documentosCtrl',
            templateUrl: 'Cliente/documentos/vistas/frmDocumentosVer.html'
        }).when('/empresas/:rut_empresa/proyectos', {
            controller: 'proyectosCtrl',
            templateUrl: 'Cliente/proyectos/vistas/frmProyectos.html'
        }).when('/tipoSubsidios', {
            controller: 'tipoSubsidiosCtrl',
            templateUrl: 'Cliente/tipoSubsidios/vistas/frmTipoSubsidios.html'
        }).when('/tipoSubsidios/agregar', {
            controller: 'tipoSubsidioAgregarCtrl',
            templateUrl: 'Cliente/tipoSubsidios/vistas/frmTipoSubsidioAgregar.html'
        }).when('/tipoSubsidios/:Id_Tipo_Subsidio/editar', {
            controller: 'tipoSubsidioEditarCtrl',
            templateUrl: 'Cliente/tipoSubsidios/vistas/frmTipoSubsidioEditar.html'
        }).when('/tipoSubsidios/:Id_Tipo_Subsidio/eliminar', {
            controller: 'tipoSubsidioEliminarCtrl',
            templateUrl: 'Cliente/tipoSubsidios/vistas/frmTipoSubsidioEliminar.html'
        }).when('/tipoExpedientes', {
            controller: 'tipoExpedientesCtrl',
            templateUrl: 'Cliente/tipoExpedientes/vistas/frmTipoExpedientes.html'
        }).when('/tipoExpedientes/:Id_Proceso/agregar', {
            controller: 'tipoExpedienteAgregarCtrl',
            templateUrl: 'Cliente/tipoExpedientes/vistas/frmTipoExpedienteAgregar.html'
        }).when('/tipoExpedientes/:Id_Tipo_Expediente/editar', {
            controller: 'tipoExpedienteEditarCtrl',
            templateUrl: 'Cliente/tipoExpedientes/vistas/frmTipoExpedienteEditar.html'
        }).when('/tipoExpedientes/:Id_Tipo_Expediente/eliminar', {
            controller: 'tipoExpedienteEliminarCtrl',
            templateUrl: 'Cliente/tipoExpedientes/vistas/frmTipoExpedienteEliminar.html'
        }).when('/modalidades', {
            controller: 'modalidadesCtrl',
            templateUrl: 'Cliente/modalidades/vistas/frmModalidades.html'
        }).when('/tipologias', {
            controller: 'tipologiasCtrl',
            templateUrl: 'Cliente/tipologias/vistas/frmTipologias.html'
        }).when('/tipologias/agregar', {
            controller: 'tipologiaAgregarCtrl',
            templateUrl: 'Cliente/tipologias/vistas/frmTipologiaAgregar.html'
        }).when('/tipologias/:Id_Tipologia/editar', {
            controller: 'tipologiaEditarCtrl',
            templateUrl: 'Cliente/tipologias/vistas/frmTipologiaEditar.html'
        }).when('/tipologias/:Id_Tipologia/eliminar', {
            controller: 'tipologiaEliminarCtrl',
            templateUrl: 'Cliente/tipologias/vistas/frmTipologiaEliminar.html'

        }).when('/roles', {
            controller: 'rolCtrl',
            templateUrl: 'Cliente/roles/vistas/frmRoles.html'

        }).when('/roles/:id_rol/usuarios', {
            controller: 'rolUsuariosCtrl',
            templateUrl: 'Cliente/roles/vistas/frmRolUsuarios.html'
        }).when('/roles/:id_rol/usuarios/agregar', {
            controller: 'rolUsuariosAgregarCtrl',
            templateUrl: 'Cliente/roles/vistas/frmRolUsuariosAgregar.html'
        });
}]);

app.controller('navegacionCtrl', ['$scope', 'usuariosSrv', 'rolUsuarioSrv', 'sistemaSrv', '$location', '$localStorage',  NavegacionCtrl]);

function NavegacionCtrl($scope, usuariosSrv, rolUsuarioSrv, sistemaSrv, $location, $localStorage) {

    $scope.usuario = {

    };

    //$scope.tipoTramite = $localStorage.tipoTramite;
    //console.log($localStorage);

    //if ($localStorage.tipoTramite) {
    //    $localStorage.tipoTramite = { "Id_Tipo_Tramite": 0, "Nombre_Tipo_Tramite": "" };
    //}


    //$scope.tramiteSeleccionado = function () {

    //    return $localStorage.tipoTramite.Id_Tipo_Tramite != 0;
    //}

   

    usuariosSrv.porLogin().then(function (response_usuario) {

        usuariosSrv.guardarUsuarioActual(response_usuario.data);

        $scope.usuario = response_usuario.data;

        console.log("Este es el usuario");
        console.log($scope.usuario);

        rolUsuarioSrv.porUsuario($scope.usuario.Rut).then(function (response) {

            $scope.rol = new Roles(response.data.pagina.rolesUsuario);


        });

       

        // xilofagos
        //rolUsuarioSrv.porRol(22,19,24).then(function (response) {

        //    $scope.listado = [];

        //    $scope.listado = response.data;
        //    console.log($scope.listado);
        //    //$scope.rol = new Roles(response.data);

        //    //$scope.rol = rol;
        //});

        //$location.path('/');

    });
}



function Usuario(u) {

    this.rut = u.Rut;

    this.nombres = u.Nombres;

    this.id_servicio = u.Id_Servicio; // o será que tiene un objeto servicio asociado?

    this.logo_servicio = u.Logo_Servicio;

    this.nombre_servicio = u.Nombre_Servicio;


    this.Nombres = function () {

        return this.nombres;
    };

    this.Rut = function () {

        return this.rut;
    };

    this.Id_Servicio = function () {

        return this.id_servicio;
    };


} // fin usuario

function Roles(roles) {

    this.roles = roles;

    this.esAdministrador = function () {

        var es_administrador = false;

        if (this.roles.length > 0) {

            for (var i = 0; i < this.roles.length; i++) {

                if (this.roles[i].Id_Rol === 1) {

                    es_administrador = true;


                }// fin if

            }// fin for


        }
        return es_administrador;
    };

    this.esRevisorSocial = function () {

        var esRevisor = false;

        if (this.roles.length > 0) {

            for (var i = 0; i < this.roles.length; i++) {

                if (this.roles[i].Id_Rol === 27) {

                    esRevisor = true;


                }// fin if

            }// fin for


        }
        return esRevisor;
    };

    this.esOperador = function () {

        var esOperador = false;

        if (this.roles.length === 0) {


            esOperador = true;


        }
        return esOperador;
    };

}

app.factory("sistemaSrv", sistemaSrv);

function sistemaSrv() {

    function Sistema() {

        this.id_sistema = 22;

        this.numero = function () {

            return this.id_sistema;
        }//fin if

    }//fin administrador

    
    return Sistema;
}







////Local
app.constant("urlApi", "http://localhost:61529/api/");
app.constant("urlApiSigeFun", "http://intranet08.minvu.cl/sigefun/api/");
app.constant("urlApiLogin", "http://localhost:61529/api/");



////Desarrollo
//app.constant("urlApi", "http://intranet08.minvu.cl/PlaninEP/api/");
//app.constant("urlApiProceso", "http://10.208.18.100:8080/engine-rest/");
//app.constant("urlApiLogin", "http://intranet08.minvu.cl/PlaninEP/api/");
//app.constant("urlApiSigeFun", "http://intranet08.minvu.cl/SigeFun/Api/");

////Produccion
//app.constant("urlApi", "http://intranet08.minvu.cl/sigetram/api/");
//app.constant("urlApiSigeFun", "http://intranet08.minvu.cl/SigeFun/Api/");
//app.constant("urlApiLogin", "http://intranet08.minvu.cl/webapilogin/api/");



