﻿'use strict';

app.controller("dialogoCtrl", ["$scope", DialogoCtrl]);

function DialogoCtrl($scope, expedientesSrv, funcionarioSrv, empresasSrv, procesosSrv, $location, $routeParams) {

    $scope.$on("Dialogo", function (event, dialogo) {

        $scope.dialogo = dialogo;
      

     });


    $scope.aceptar = function () {

        $scope.$emit("Respuesta", true);

    };

    $scope.cancelar = function () {

        $scope.$emit("Respuesta", false);

    };

}


app.directive('dialogo', [Dialogo]);

function Dialogo() {
    return {
        templateUrl: 'Cliente/frmDialogo.html',
        scope: {
            dialogo: { titulo: "=titulo" }
        }
    };
}
