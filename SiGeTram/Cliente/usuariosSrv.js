﻿
app.factory("usuariosSrv", ["$http", "urlApiLogin" ,UsuariosSrv]);

function UsuariosSrv($http, urlApiLogin) {

    var usuarioAct = {};


    var ruta = urlApiLogin + "Usuarios/";


    var guardarUsuarioActual = function (usuarioActual) {

        usuarioAct = usuarioActual;
    };

    var usuarioActual = function () {

        return usuarioAct;
    };

    var porRut = function (rut) {

        return $http.get(ruta + rut);
    };
       
    var porLogin = function () {

        return $http.get(ruta+"login");
    };


    var actualizar = function (usuario) {


        return $http.put(ruta, usuario);
    };

    var agregar = function (usuario) {

        return $http.post(ruta, usuario);
    };

    var eliminar = function (usuario) {

        return $http.delete(ruta + usuario.Rut);
    };

    var porRol = function (idRol) {

        return $http.get(ruta + "/rol/" + idRol);
    };

    
   
   

    return {
        porRut: porRut,
        agregar: agregar,
        actualizar: actualizar,
        eliminar: eliminar,
        porRol: porRol,
        guardarUsuarioActual: guardarUsuarioActual,
        usuarioActual: usuarioActual,
        porLogin:porLogin
        
    };
}


app.factory("correoSrv",["correosSrv", CorreoSrv]);

function CorreoSrv(correosSrv) {

  

    function Correo(asunto, body, de, alias,Hacia) {

        this.Asunto = asunto;
        this.Body = body;
        this.De = de;
        this.Alias = alias;
        this.Hacia = Hacia;
              
    }

       
    return Correo;
}

app.factory("correosSrv", ["$http", "urlApiSigeFun", CorreosSrv]);

function CorreosSrv($http, urlApiSigeFun) {

    var ruta = urlApiSigeFun + "correos/";

    var enviar = function (c) {

        console.log("este es el correo a enviar");
        console.log(c);
            return $http.post(ruta, c);

        }

    

    return {
        enviar:enviar
    }
}



app.factory("tablaHtmlSrv", TablaHtmlSrv);

function TablaHtmlSrv() {

    function TablaHtml(objeto) {
                
        this.objeto = objeto;
        this.estiloCelda = 'style = \"border: 1px solid black;border-collapse:collapse\"';
        this.estiloEncabezado = 'style = \"background: #68889b; color: #fff; text-align: center;border: 1px solid black;border-collapse:collapse\"';
        this.html = function () {

            var property;
            var head = "";
            var fila = "";
            for (property in this.objeto) {
                if (property != "html") {

                    head = head + "<th style=\"background: #68889b; color: #fff; \">" + property + "</th > ";
                    fila = fila + "<td>" + this.objeto[property] + "</td> "
                }
            }
           
            return "<table border = \"1\" width=\"600px\"><tr>" + head + "</tr>" + "<tr>" + fila + "</tr></table>";
    
        }
    }

    return TablaHtml;
}


app.factory("saludoSrv", SaludoSrv);

function SaludoSrv() {

    function Saludo(persona) {
        
        this.persona = persona;
        
        this.saludo = function () {

            var saludo;

            if (this.persona.Id_Genero == 1) {
                saludo = 'Estimada ' + this.persona.Nombres + ':<br/><br/>'
            } else {

                saludo = 'Estimado ' + this.persona.Nombres + ':<br/><br/>'
            }

            return saludo;
        }

    }

    return Saludo;
}



app.factory("fechaStringSrv", fechaStringSrv);

function fechaStringSrv() {

    function FechaString() {

        this.fecha = function (f) {

            var d = new Date(f);
            var mes = d.getMonth() + 1;

            return d.getDate() + "/" + mes + "/" + d.getFullYear();

        }
    }

    return FechaString;
}



app.factory("usuarioSrv", UsuarioSrv);

function UsuarioSrv() {
    

    function Usuario(json) {

        this.Rut= json.Rut;
        this.Dv= json.Dv;
        this.Nombres = json.Nombres;
        this.Paterno = json.Paterno;
        this.Materno = json.Materno;
        this.Nombre_Completo= json.NombreCompleto;
        this.Genero= json.Id_Genero;
        this.Email_Particular = json.Email_Particular;
        this.Email = json.Email;
        this.Id_Servicio = json.Id_Servicio;
        this.Nombre_Servicio = json.Nombre_Servicio;
        this.Id_Dependencia = json.Id_Dependencia;
        this.Id_Provincia = json.Id_Provincia;
        this.Id_Calidad_Juridica = json.Id_Calidad_Juridica;   
        
    }


    return Usuario;
}