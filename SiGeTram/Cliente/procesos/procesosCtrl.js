﻿app.controller("procesosCtrl", ["$scope", "procesosSrv", "usuariosSrv", "$location", "$routeParams", "rolUsuarioSrv",  ProcesosCtrl]);

function ProcesosCtrl($scope, procesosSrv, usuariosSrv, $location, $routeParams, rolUsuarioSrv) {

    $scope.procesos = [];

    
    


    $scope.usuario = {

    };
   
    $scope.filtro = procesosSrv.filtro_actual();

 
  
    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();
    }
   
    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.usuario = response.data;

        $scope.filtro = angular.copy(procesosSrv.filtro_actual());

        $scope.listadoPaginado(procesosSrv.filtro_actual());

        $scope.maxSize = 5;

        $scope.currentPage = 1;
  }) 
  

    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }

    $scope.proyectos = function (idProceso) {

        $location.path("/procesos/" + idProceso + "/proyectos")

    }


    $scope.tiposExpedientes = function (idProceso) {

        $location.path("/procesos/" + idProceso + "/tiposExpediente")

    }


    $scope.respaldar = function (proceso) {

        $scope.respaldando_tipo_tramite = true;
        alert("El respaldo tomará algunos minutos. Espere por favor....")
        procesosSrv.respaldar(proceso).then(function (respuesta) {

            //$location.path("/tipoTramites");
            //$scope.respaldando_tipo_tramite = false;
            alert("El respaldo finalizó, por favor verifique los datos respaldados.")
        });
    }


    $scope.listadoPaginado = function (filtro_actual) {

        //var filtro = angular.copy(filtro_actual);
        var filtro = filtro_actual;

        procesosSrv.guardar_filtro(filtro);

        procesosSrv.listado(filtro).then(function (respuesta) {

            $scope.procesos = respuesta.data.pagina.procesos;

                    
            $scope.bigTotalItems = respuesta.data.pagina.totalRegistros;
        })
    }
}// fin controlador




app.controller("procesoEditarCtrl", ["$scope", "procesosSrv", "$location", "$routeParams", ProcesoEditarCtrl]);

function ProcesoEditarCtrl($scope, procesosSrv, $location, $routeParams) {

    $scope.proceso = {};

    $scope.guardando_proceso = false;

    $scope.filtro = angular.copy(procesosSrv.filtro_actual());

    $scope.filtro.proceso.Id_Proceso = $routeParams.id_proceso;



    procesosSrv.listado($scope.filtro).then(function (respuesta) {


        $scope.proceso = respuesta.data.pagina.procesos[0];

        $scope.proceso.Vigente = $scope.proceso.Vigente.toString();

    });



    $scope.guardar = function () {

        procesosSrv.actualizar($scope.proceso).then(function (respuesta) {

            $location.path("/procesos");
        });
    }
}

