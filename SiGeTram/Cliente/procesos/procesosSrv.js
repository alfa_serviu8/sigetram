﻿app.factory("procesosSrv", ["$http", "urlApi", "$localStorage", ProcesosSrv]);

function ProcesosSrv($http, urlApi) {

    var ruta = urlApi + "procesos/";

    var filtro = {
        proceso: {
            "Id_Proceso": 0,
            "Vigente": -1
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }

    };


    var guardar_filtro = function (nuevofiltro) {

        filtro = nuevofiltro;
    };

    var filtro_actual = function () {
        return filtro;
    };

    var listado = function (filtro) {
        return $http.post(ruta + "listado", filtro);
    };

    var listadoGeneral = function () {
        var filtro = {
            proceso: {
                "Id_Sistema":22,
                "Id_Proceso": 0,
                "Vigente": -1
            },
            pagina: {
                "numero": "1",
                "tamacno": "10000"
            }

        };


        return listado(filtro);
    };

    var respaldar = function (proceso) {
        return $http.post(ruta + "respaldo", proceso);
    };


    return {
        listado: listado,
        listadoGeneral: listadoGeneral,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual,
        respaldar: respaldar


    }



}