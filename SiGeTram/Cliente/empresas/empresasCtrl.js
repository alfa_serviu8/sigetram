﻿
app.controller("empresasCtrl", ["$scope", "empresasSrv", "usuariosSrv","$location",EmpresasCtrl]);

function EmpresasCtrl($scope, empresasSrv, usuariosSrv, $location) {

    $scope.empresas = [];

    $scope.filtro = {};
    
    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();
    };


    console.log("filtro actual");
    

    console.log(empresasSrv.filtro_actual());


    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.filtro = angular.copy(empresasSrv.filtro_actual());

        $scope.filtro.empresa.Es_Empresa_Patrocinante = 1;
        
        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;


    }); // fin usuario
      

    $scope.buscar = function () {

      
        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }


    $scope.listadoPaginado = function (filtro_actual) {

        var filtro = angular.copy(filtro_actual);

        empresasSrv.guardar_filtro(filtro);

        empresasSrv.listado(filtro).then(function (respuesta) {

            $scope.empresas = respuesta.data.pagina.empresas;

            console.log("empresas");
            console.log($scope.empresas);

            $scope.bigTotalItems = respuesta.data.pagina.totalRegistros;
        })
    }

    $scope.iniciarEliminar = function (rutEmpresa) {

        $location.path("empresas/" + rutEmpresa + "/eliminar");
    };


}// fin controlador


app.controller("empresaAgregarCtrl", ["$scope", "empresasSrv", "$location","$localStorage",EmpresaAgregarCtrl]);

function EmpresaAgregarCtrl($scope, empresasSrv, $location, $localStorage) {

    $scope.empresa = {};

    $scope.guardando_empresa = false;

   
    $scope.closeAlert = function () {
        $scope.registroCivilOk = true;

    };

    $scope.empresaExiste = false;

    $scope.closeAlert2 = function () {

        $scope.empresaExiste = false;

    };


  

    $scope.verificarRut = function () {

        if ($scope.frmEmpresa.rut.$valid) {

           
            var rut = new Rut(angular.copy($scope.empresa.RutConDv));

            $scope.empresa.Dv = rut.dv();

            $scope.empresa.Rut_Empresa = rut.rut();
          
          
            empresasSrv.porRut($scope.empresa.Rut_Empresa).then(function (respuesta) {

                console.log("esta es la empresa buscada");

                console.log(respuesta.data.pagina.empresas[0]);

                
                if (respuesta.data.pagina.empresas.length > 0 ) {

                    $scope.empresa.Razon_Social = respuesta.data.pagina.empresas[0].Razon_Social;
                    $scope.empresa.Nombre_Fantasia = respuesta.data.pagina.empresas[0].Nombre_Fantasia;
                    $scope.empresa.Email_Particular = respuesta.data.pagina.empresas[0].Email_Particular;
                    $scope.empresa.Pass_Sistemas = respuesta.data.pagina.empresas[0].Pass_Sistemas;
                    $scope.empresa.Fono_Fijo = respuesta.data.pagina.empresas[0].Fono_Fijo;
                    $scope.empresa.Celular = respuesta.data.pagina.empresas[0].Celular;
                    $scope.empresa.Es_Empresa_Patrocinante = 1;
                    $scope.empresaExiste = respuesta.data.pagina.empresas.length > 0;
                   
                }

            });

        }

    };

    $scope.closeAlert2 = function () {

        $scope.empresaExiste = false;

    };
   
    $scope.guardar = function () {

        var rut = new Rut(angular.copy($scope.empresa.RutConDv));

        $scope.empresa.Dv = rut.dv();

        $scope.empresa.Rut_Empresa = rut.rut();

        if ($scope.empresaExiste) {

            empresasSrv.actualizar($scope.empresa).then(function (respuesta) {

                $location.path("/empresas");


            });




        } else {




            empresasSrv.agregar($scope.empresa).then(function (respuesta) {

                $location.path("/empresas");


            });
        }
    };

}


app.controller("resetearClaveCtrl", ["$scope", "empresasSrv", "$location","$routeParams", ResetearClaveCtrl]);

function ResetearClaveCtrl($scope, empresasSrv,$location,$routeParams) {

    $scope.empresa = {};

    $scope.completando_tarea = false;

     
    empresasSrv.porRut($routeParams.Rut_Empresa).then(function (respuesta) {

        $scope.empresa = respuesta.data.pagina.empresas[0];

        $scope.empresa.RutConDv = $scope.empresa.Rut_Empresa + "-" + $scope.empresa.Dv;

    });
        
    $scope.guardar = function () {

        empresasSrv.resetearClave($scope.empresa).then(function (respuesta) {
           

           
            alert("Se envió la clave, favor verifique con la empresa si puede acceder al sistema.");
           

                $location.path("/empresas");

           
            
        });
    };
}

app.controller("empresaEditarCtrl", ["$scope", "empresasSrv", "$location", "$routeParams", EmpresaEditarCtrl]);

function EmpresaEditarCtrl($scope, empresasSrv, $location, $routeParams) {

    $scope.empresa = {};

    $scope.guardando_empresa = false;

    console.log("$routeParams.Rut_Empresa");
    console.log($routeParams.Rut_Empresa);

    $scope.filtro = angular.copy(empresasSrv.filtro_actual());

    $scope.filtro.empresa.Rut_Empresa = $routeParams.Rut_Empresa;
    


    empresasSrv.listado($scope.filtro).then(function (respuesta) {
    
        $scope.empresa = respuesta.data.pagina.empresas[0];

        console.log("$scope.empresa");
        console.log($scope.empresa);

        $scope.empresa.RutConDv = $scope.empresa.Rut_Empresa + "-" + $scope.empresa.Dv;

    });


    $scope.guardar = function () {

        empresasSrv.actualizar($scope.empresa).then(function (respuesta) {

            $location.path("/empresas");
        });
    };
}



app.controller("empresaEliminarCtrl", ["$scope", "empresasSrv","proyectosSrv", "$location", "$routeParams", EmpresaEliminarCtrl]);

function EmpresaEliminarCtrl($scope, empresasSrv, proyectosSrv, $location, $routeParams) {

    $scope.empresa = {};
    $scope.tieneProyectos = false;
   
    empresasSrv.porRut($routeParams.Rut_Empresa).then(function (respuesta) {

        $scope.empresa = respuesta.data.pagina.empresas[0];

        $scope.empresa.RutConDv = $scope.empresa.Rut_Empresa + "-" + $scope.empresa.Dv;
    });

    $scope.eliminar = function () {

        proyectosSrv.porEmpresa($scope.empresa.Rut_Empresa).then(function (response) {

            if (response.data.listado.length > 0) {

                $scope.tieneProyectos = true;
            }
            else {

                empresasSrv.eliminar($scope.empresa.Rut_Empresa).then(function (respuesta) {

                    $location.path("/empresas");
                });
            }
        });
    };

    $scope.volver = function () {
        
            $location.path("/empresas");
    };
}

app.controller("empresaExisteCtrl", ["$scope", "empresasSrv", "proyectosSrv", "$location", "$routeParams", EmpresaExisteCtrl]);

function EmpresaExisteCtrl($scope, empresasSrv, proyectosSrv, $location, $routeParams) {

    $scope.empresa = {};
   

    empresasSrv.porRut($routeParams.Rut_Empresa).then(function (respuesta) {

        $scope.empresa = respuesta.data.listado[0];

        $scope.empresa.RutConDv = $scope.empresa.Rut_Empresa + "-" + $scope.empresa.Dv;

    });

    $scope.agregar = function () {

        $scope.empresa.Es_Empresa_Patrocinante = 1;

           empresasSrv.actualizar($scope.empresa).then(function (respuesta) {

                    $location.path("/empresas");
                });
      
    };

    $scope.volver = function () {


        $location.path("/empresas");

    };
}

