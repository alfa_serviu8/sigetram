﻿app.factory("tipologiasSrv", ["$http", "urlApi", TipologiasSrv]);

function TipologiasSrv($http, urlApi) {

    var ruta = urlApi + "tipologias/";

    var filtro = {
        tipologia: {
            "Id_Tipologia": 0,
            "Nombre_Tipologia": "",
            "Id_Tipo_Subsidio": "0"
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }

    };

    var guardar_filtro = function (filtro) {
        filtro = filtro;
    };

    var filtro_actual = function () {
        return filtro;
    };

    var listado = function (filtro) {
        return $http.post(ruta + "listado", filtro);
    };

    var agregar = function (tipologia) {
        return $http.post(ruta, tipologia);
    };
  
    var actualizar = function (tipologia) {
        return $http.put(ruta, tipologia);
    };
       
    var eliminar = function (Id_Tipologia) {

        return $http.delete(ruta, Id_Tipologia);
    };

    return {
        listado: listado,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual,
        agregar: agregar,
        actualizar: actualizar,
        eliminar: eliminar
    };

}



