﻿

app.factory("estadoExpedientesSrv", ["$http", "urlApi", EstadoExpedientesSrv]);

function EstadoExpedientesSrv($http, urlApi) {

    var ruta = urlApi + "estadoExpedientes/";

    var filtro = {
        estadoExpediente: {
            "Id_Estado_Expediente": 0
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };

    var guardar_filtro = function (nuevoFiltro) {


        filtro = nuevoFiltro;
    };

    var filtro_actual = function () {
        return filtro;
    };


    var listado = function () {
              
        return $http.post(ruta + "listado", filtro);
    };

   
    return {
        listado: listado,
        guardar_filtro: guardar_filtro,
        filtro_actual:filtro_actual
    };
}



app.factory("estadoProyectosSrv", ["$http", "urlApi", EstadoProyectosSrv]);

function EstadoProyectosSrv($http, urlApi) {

    var ruta = urlApi + "estadoProyectos/";

    var filtro = {
        estadoProyecto: {
            "Id_Estado_Proyecto": 0,
            "Nombre_Estado_Proyecto":""
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };

    var guardar_filtro = function (nuevoFiltro) {


        filtro = nuevoFiltro;
    };

    var filtro_actual = function () {
        return filtro;
    };


    var listado = function () {

        return $http.post(ruta + "listado", filtro);
    };


    return {
        listado: listado,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual
    };
}





app.factory("revisoresSrv", ["$http", "urlApi", RevisoresSrv]);

function RevisoresSrv($http, urlApi) {

var ruta = urlApi + "revisores/";

var filtro = {
    revisorBuscado: {
        "Rut": 0
    },
    "numero_pagina": "1",
    "tamacno_pagina": "10"

};



var listado = function (filtro) {

    return $http.post(ruta + "listado", filtro);
};

var porRut = function (rut) {

    var filtro = {

        revisorBuscado: {

            "Rut": rut
         },
        "numero_pagina": "1",
        "tamacno_pagina": "10"

    };



    return listado(filtro);


};



return {
    listado: listado,
    porRut: porRut
};
}