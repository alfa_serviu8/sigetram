USE [BD_COMUN_SISTEMAS]
GO
/****** Object:  User [MINVU_NT\jescobars]    Script Date: 20-05-2021 13:08:05 ******/
CREATE USER [MINVU_NT\jescobars] FOR LOGIN [MINVU_NT\jescobars] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [MINVU_NT\snavarrete]    Script Date: 20-05-2021 13:08:05 ******/
CREATE USER [MINVU_NT\snavarrete] FOR LOGIN [MINVU_NT\snavarrete] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [MINVU_NT\snavarretec]    Script Date: 20-05-2021 13:08:05 ******/
CREATE USER [MINVU_NT\snavarretec] FOR LOGIN [MINVU_NT\snavarretec] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [snavarretec]    Script Date: 20-05-2021 13:08:05 ******/
CREATE USER [snavarretec] FOR LOGIN [snavarretec] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [usuario_interno]    Script Date: 20-05-2021 13:08:05 ******/
CREATE USER [usuario_interno] FOR LOGIN [usuario_interno] WITH DEFAULT_SCHEMA=[dbo]
GO
sys.sp_addrolemember @rolename = N'db_owner', @membername = N'MINVU_NT\jescobars'
GO
sys.sp_addrolemember @rolename = N'db_datareader', @membername = N'MINVU_NT\jescobars'
GO
sys.sp_addrolemember @rolename = N'db_datawriter', @membername = N'MINVU_NT\jescobars'
GO
sys.sp_addrolemember @rolename = N'db_owner', @membername = N'MINVU_NT\snavarrete'
GO
sys.sp_addrolemember @rolename = N'db_datareader', @membername = N'MINVU_NT\snavarrete'
GO
sys.sp_addrolemember @rolename = N'db_datawriter', @membername = N'MINVU_NT\snavarrete'
GO
sys.sp_addrolemember @rolename = N'db_owner', @membername = N'MINVU_NT\snavarretec'
GO
sys.sp_addrolemember @rolename = N'db_datareader', @membername = N'MINVU_NT\snavarretec'
GO
sys.sp_addrolemember @rolename = N'db_datawriter', @membername = N'MINVU_NT\snavarretec'
GO
sys.sp_addrolemember @rolename = N'db_owner', @membername = N'usuario_interno'
GO
sys.sp_addrolemember @rolename = N'db_datareader', @membername = N'usuario_interno'
GO
sys.sp_addrolemember @rolename = N'db_datawriter', @membername = N'usuario_interno'
GO
/****** Object:  UserDefinedTableType [dbo].[ItemKeyList]    Script Date: 20-05-2021 13:08:05 ******/
CREATE TYPE [dbo].[ItemKeyList] AS TABLE(
	[ItemId] [varchar](50) NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[Mayuscula_Inicial]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ALberto Saez Peña
-- Create date: 06/08/2018
-- Description:	devuelve un string con mayuscula inicial
-- =============================================
CREATE FUNCTION [dbo].[Mayuscula_Inicial] (
 @string varchar(255)
)  
RETURNS varchar(255) AS


BEGIN 
-- Vigente hasta el 19-02-2018
-- RETURN upper(left(@string, 1)) + right(@string, len(@string) - 1) 

--Modificacion hecha por Ricardo
DECLARE @i INT
SET @string =upper(left(@string, 1)) + lower(right(@string, len(@string) - 1) )
SET @i=1
WHILE @i <= LEN(@string) 
      BEGIN
	      IF SUBSTRING(@string, @i, 1)=' ' set @string = REPLACE(@string,SUBSTRING(@string,@i,2),UPPER(SUBSTRING(@string,@i,2)))
	      SET @i = @i + 1
      END
RETURN @string

END
GO
/****** Object:  UserDefinedFunction [dbo].[ObtenerDigitoVerificador]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  FUNCTION [dbo].[ObtenerDigitoVerificador]
(
	@rut INTEGER
 )
 RETURNS VARCHAR(1)
 
 AS
 BEGIN
 
 DECLARE @dv VARCHAR(1)
 DECLARE @rutAux INTEGER
 DECLARE @Digito INTEGER
 DECLARE @Contador INTEGER
 DECLARE @Multiplo INTEGER
 DECLARE @Acumulador INTEGER
 
 
 SET @Contador = 2;
 SET @Acumulador = 0;
 SET @Multiplo = 0;
 
	WHILE(@rut!=0)
		BEGIN
 
			SET @Multiplo = (@rut % 10) * @Contador;
			SET @Acumulador = @Acumulador + @Multiplo;
			SET @rut = @rut / 10;
			SET @Contador = @Contador + 1;
			if(@Contador = 8)
			BEGIN
				SET @Contador = 2;
			End;
		END;
 
	SET @Digito = 11 - (@Acumulador % 11);
 
	SET @dv = LTRIM(RTRIM(CONVERT(VARCHAR(2),@Digito)));
 
	IF(@Digito = 10)
	BEGIN
		SET @dv = 'K';
	END;
 
	IF(@Digito = 11)
	BEGIN
		SET @dv = '0';
	END;
 
RETURN @dv
 
END

GO
/****** Object:  UserDefinedFunction [dbo].[Ver_Presencia]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ALberto Saez Peña
-- Create date: 06/08/2018
-- Description:	devuelve un string con mayuscula inicial
-- =============================================
CREATE FUNCTION [dbo].[Ver_Presencia] (
 @rut varchar(12)
)  
RETURNS bit AS

BEGIN 
Declare @salida tinyint 
Declare @obj as Int;
Declare @response as varchar(8000);
Declare @sUrl as varchar(100);
set @salida= 1
set @sUrl = 'http://intranet08.minvu.cl/SigeFun/api/asistencia/' + @rut 
exec sp_OACreate 'MSXML2.ServerXMLHttp', @obj OUT
exec sp_OAMethod @obj, 'Open', NULL, 'GET', @sUrl, false
exec sp_OAMethod @obj, 'send'
exec sp_OAGetProperty @obj, 'responseText', @response OUT
exec sp_OADestroy @obj
return @response
END
GO
/****** Object:  Table [dbo].[Actividad]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actividad](
	[Id_Actividad] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Actividad] [varchar](100) NULL,
 CONSTRAINT [PK_Actividad] PRIMARY KEY CLUSTERED 
(
	[Id_Actividad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Afp]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Afp](
	[Id_Afp] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Afp] [varchar](50) NULL,
 CONSTRAINT [PK_Afp] PRIMARY KEY CLUSTERED 
(
	[Id_Afp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ambiente]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ambiente](
	[Id_Ambiente] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Ambiente] [nvarchar](50) NULL,
 CONSTRAINT [PK_Ambiente] PRIMARY KEY CLUSTERED 
(
	[Id_Ambiente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Archivo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Archivo](
	[Id_Archivo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_Archivo] [varchar](100) NOT NULL,
	[Tipo_Contenido] [varchar](50) NOT NULL,
	[Contenido] [varbinary](max) NOT NULL,
	[Tamacno] [int] NOT NULL,
	[Rut] [int] NOT NULL,
	[Tipo_Archivo] [int] NOT NULL,
 CONSTRAINT [PK_Documento] PRIMARY KEY CLUSTERED 
(
	[Id_Archivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Area_Trabajo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Area_Trabajo](
	[Id_Area_Trabajo] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Area_Trabajo] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
	[Habilitado] [int] NULL,
	[Id_Servicio] [int] NULL,
 CONSTRAINT [PK_AreaTrabajo] PRIMARY KEY CLUSTERED 
(
	[Id_Area_Trabajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Calidad_Juridica]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calidad_Juridica](
	[Id_Calidad_Juridica] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Calidad_Juridica] [varchar](50) NULL,
	[Id_Codigo_Presupuestario] [int] NULL,
 CONSTRAINT [PK_CalidadJuridica] PRIMARY KEY CLUSTERED 
(
	[Id_Calidad_Juridica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[Rut] [int] NOT NULL,
	[Dv] [varchar](1) NOT NULL,
	[Id_Dependencia] [int] NOT NULL,
	[Nombres] [varchar](180) NULL,
	[Paterno] [varchar](60) NULL,
	[Materno] [varchar](60) NULL,
	[Razon_Social] [varchar](500) NULL,
	[Nombre_Fantasia] [varchar](500) NULL,
	[Representante_Legal] [varchar](120) NULL,
	[Direccion] [varchar](100) NULL,
	[Fono_Fijo] [int] NULL,
	[Celular] [int] NULL,
	[Email_Particular] [varchar](50) NULL,
	[Fecha_Nacimiento] [datetime] NULL,
	[Id_Comuna] [int] NULL,
	[Id_Estado_Civil] [int] NULL,
	[Id_Genero] [int] NULL,
	[Login_Sistemas] [varchar](30) NULL,
	[Pass_Sistemas] [varchar](25) NULL,
	[Es_Persona_Natural] [int] NULL,
	[Es_Empresa_Provedor] [int] NULL,
	[Es_Funcionario_Publico] [int] NULL,
	[Es_Usuario_Interno] [int] NULL,
	[Es_Empresa_Patrocinante] [int] NULL,
	[Empresa_Rol] [int] NULL,
	[Empresa_Vigente_en_Registro_Tecnico] [varchar](20) NULL,
	[Es_EGR] [int] NULL,
 CONSTRAINT [PK_Cliente_1] PRIMARY KEY CLUSTERED 
(
	[Rut] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cliente_copia]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cliente_copia](
	[Rut] [int] NOT NULL,
	[Dv] [varchar](1) NOT NULL,
	[Id_Dependencia] [int] NOT NULL,
	[Nombres] [varchar](180) NULL,
	[Paterno] [varchar](60) NULL,
	[Materno] [varchar](60) NULL,
	[Razon_Social] [varchar](500) NULL,
	[Nombre_Fantasia] [varchar](500) NULL,
	[Representante_Legal] [varchar](120) NULL,
	[Direccion] [varchar](100) NULL,
	[Fono_Fijo] [int] NULL,
	[Celular] [int] NULL,
	[Email_Particular] [varchar](50) NULL,
	[Fecha_Nacimiento] [datetime] NULL,
	[Id_Comuna] [int] NULL,
	[Id_Estado_Civil] [int] NULL,
	[Id_Genero] [int] NULL,
	[Login_Sistemas] [varchar](30) NULL,
	[Pass_Sistemas] [varchar](25) NULL,
	[Es_Persona_Natural] [int] NULL,
	[Es_Empresa_Provedor] [int] NULL,
	[Es_Funcionario_Publico] [int] NULL,
	[Es_Usuario_Interno] [int] NULL,
	[Es_Empresa_Patrocinante] [int] NULL,
	[Empresa_Rol] [int] NULL,
	[Empresa_Vigente_en_Registro_Tecnico] [varchar](20) NULL,
	[Es_EGR] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente_Estado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente_Estado](
	[Id_Estado] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Estado] [varchar](50) NULL,
 CONSTRAINT [PK_Cliente_Estado] PRIMARY KEY CLUSTERED 
(
	[Id_Estado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente_Tipo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente_Tipo](
	[Id_Tipo_Cliente] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Tipo_Cliente] [varchar](50) NULL,
 CONSTRAINT [PK_Cliente_Tipo] PRIMARY KEY CLUSTERED 
(
	[Id_Tipo_Cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Codigo_Presupuestario]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Codigo_Presupuestario](
	[Id_Codigo_Presupuestario] [int] IDENTITY(1,1) NOT NULL,
	[Codigo_Presupuestario] [varchar](50) NOT NULL,
	[Nombre_Codigo_Presupuestario] [varchar](100) NOT NULL,
	[Acno] [int] NOT NULL,
	[Con_Viatico] [int] NOT NULL,
	[Vigente] [int] NOT NULL,
	[Id_Servicio] [int] NOT NULL,
 CONSTRAINT [PK_Codigo_Presupuestario_1] PRIMARY KEY CLUSTERED 
(
	[Id_Codigo_Presupuestario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comuna]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comuna](
	[Id_Comuna] [int] NOT NULL,
	[Id_Provincia] [int] NOT NULL,
	[Nombre_Comuna] [varchar](100) NOT NULL,
	[Corresponde_Pago_Viatico] [int] NULL,
	[Cod_Comuna] [int] NULL,
 CONSTRAINT [PK_Comuna] PRIMARY KEY CLUSTERED 
(
	[Id_Comuna] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dependencia]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dependencia](
	[Id_Dependencia] [int] IDENTITY(0,1) NOT NULL,
	[Id_Servicio] [int] NOT NULL,
	[Id_Provincia] [int] NOT NULL,
	[DependeDe] [int] NULL,
	[Nombre_Dependencia] [varchar](200) NOT NULL,
	[Habilitada] [int] NOT NULL,
	[Id_Tipo_Dependencia] [int] NOT NULL,
	[Camino] [varchar](250) NULL,
	[Nombre_Dependencia2] [varchar](100) NULL,
	[Es_Departamento_Administracion] [bit] NOT NULL,
	[Es_Departamento_Provincial] [bit] NOT NULL,
 CONSTRAINT [PK_Dependencia] PRIMARY KEY CLUSTERED 
(
	[Id_Dependencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dependencias_Tipo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dependencias_Tipo](
	[Id_Tipo_Dependencia] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Tipo_Dependencia] [varchar](50) NULL,
	[Orden] [int] NULL,
 CONSTRAINT [PK_Dependencias_Tipo] PRIMARY KEY CLUSTERED 
(
	[Id_Tipo_Dependencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Documento]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Documento](
	[Id_Documento] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_Documento] [varchar](255) NULL,
	[Tipo_Contenido] [varchar](50) NULL,
	[Contenido] [varbinary](max) NULL,
	[Tamacno] [int] NULL,
	[Actividad] [varchar](255) NULL,
	[Fecha_Ingreso] [datetime] NULL,
	[Rut_Funcionario] [int] NULL,
	[Fecha_Lectura] [datetime] NULL,
	[Fue_Leido] [int] NULL,
 CONSTRAINT [PK_Documento_1] PRIMARY KEY CLUSTERED 
(
	[Id_Documento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estado_Civil]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estado_Civil](
	[Id_Estado_Civil] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Estado_Civil] [varchar](50) NULL,
 CONSTRAINT [PK_Estado_Civil] PRIMARY KEY CLUSTERED 
(
	[Id_Estado_Civil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estamento]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estamento](
	[Id_Estamento] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Estamento] [varchar](50) NULL,
	[orden] [int] NULL,
 CONSTRAINT [PK_Estamento] PRIMARY KEY CLUSTERED 
(
	[Id_Estamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Excepcion]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Excepcion](
	[Id_Excepcion] [int] IDENTITY(1,1) NOT NULL,
	[Id_Tipo_Excepcion] [int] NOT NULL,
	[Id_Sistema] [int] NOT NULL,
	[Rut_Funcionario] [int] NOT NULL,
	[Rut_Autoriza] [int] NULL,
	[Fecha] [datetime] NOT NULL,
	[Vigente] [bit] NOT NULL,
 CONSTRAINT [PK_Excepcion] PRIMARY KEY CLUSTERED 
(
	[Id_Excepcion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Facultad]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Facultad](
	[Id_Facultad] [int] IDENTITY(1,1) NOT NULL,
	[Rut] [int] NULL,
	[Id_Dependencia] [int] NULL,
	[Descripcion_Facultad] [varchar](300) NULL,
	[Vigente] [int] NULL,
 CONSTRAINT [PK_Facultad] PRIMARY KEY CLUSTERED 
(
	[Id_Facultad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Festivo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Festivo](
	[Id_Festivo] [int] IDENTITY(1,1) NOT NULL,
	[Es_Nacional] [int] NULL,
	[Es_Regional] [int] NULL,
	[Id_Provincia] [int] NULL,
	[Fecha_Festivo] [smalldatetime] NULL,
	[Nombre_Festivo] [varchar](50) NULL,
 CONSTRAINT [PK_Festivo] PRIMARY KEY CLUSTERED 
(
	[Id_Festivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Flujo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Flujo](
	[Id_Simbolo] [int] NOT NULL,
	[Id_Simbolo_Siguiente] [int] NOT NULL,
	[Id_Modelo] [int] NOT NULL,
	[Condicion] [int] NULL,
 CONSTRAINT [PK_Simbolo_Siguiente] PRIMARY KEY CLUSTERED 
(
	[Id_Simbolo] ASC,
	[Id_Simbolo_Siguiente] ASC,
	[Id_Modelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Folio]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folio](
	[Id_Folio] [int] IDENTITY(1,1) NOT NULL,
	[Id_Servicio] [int] NOT NULL,
	[id_Provincia] [int] NULL,
	[Id_Tipo_Documento] [int] NOT NULL,
	[Acno] [int] NOT NULL,
	[Folio_Numero] [int] NULL,
 CONSTRAINT [PK_Folios] PRIMARY KEY CLUSTERED 
(
	[Id_Folio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Funcionario]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Funcionario](
	[Rut] [int] NOT NULL,
	[Id_Calidad_Juridica] [int] NULL,
	[Id_Estamento] [int] NULL,
	[Id_Grado] [int] NULL,
	[Id_Afp] [int] NULL,
	[Id_Isapre] [int] NULL,
	[Id_Sub_Area_Trabajo] [int] NULL,
	[Email] [varchar](50) NULL,
	[Anexo] [int] NULL,
	[Funcion] [varchar](100) NULL,
	[Habilitado] [int] NULL,
	[Id_Servicio] [int] NULL,
	[Mosca] [varchar](10) NULL,
 CONSTRAINT [PK_Funcionario] PRIMARY KEY CLUSTERED 
(
	[Rut] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Funcionario_Profesion]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Funcionario_Profesion](
	[Rut] [int] NOT NULL,
	[Id_Profesion] [int] NOT NULL,
 CONSTRAINT [PK_Funcionario_Profesion] PRIMARY KEY CLUSTERED 
(
	[Rut] ASC,
	[Id_Profesion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Genero]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Genero](
	[Id_Genero] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Genero] [varchar](50) NULL,
 CONSTRAINT [PK_Genero] PRIMARY KEY CLUSTERED 
(
	[Id_Genero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Grado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Grado](
	[Id_Grado] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Grado] [varchar](50) NULL,
 CONSTRAINT [PK_Grado] PRIMARY KEY CLUSTERED 
(
	[Id_Grado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Hora_Inicio]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hora_Inicio](
	[Id_Hora_Inicio] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_Hora_Inicio] [varchar](50) NULL,
 CONSTRAINT [PK_Horario_Inicio] PRIMARY KEY CLUSTERED 
(
	[Id_Hora_Inicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Hora_Termino]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hora_Termino](
	[Id_Hora_Termino] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_Hora_Termino] [varchar](50) NULL,
 CONSTRAINT [PK_Horario_Termino] PRIMARY KEY CLUSTERED 
(
	[Id_Hora_Termino] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Horario_Asistencia]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Horario_Asistencia](
	[Id_Horario_Asistencia] [int] IDENTITY(1,1) NOT NULL,
	[Id_Servicio] [int] NOT NULL,
	[Acno] [int] NOT NULL,
	[Horario_Jornada_Lunes_Jueves] [varchar](5) NOT NULL,
	[Horario_Jornada_Viernes] [varchar](5) NOT NULL,
	[Horario_Entrada_Tope] [varchar](5) NOT NULL,
	[Inconsistencia_Dia_Corte] [int] NOT NULL,
	[Inconsistencia_Dia_Habiles] [int] NOT NULL,
	[Vigente] [int] NOT NULL,
 CONSTRAINT [PK_Horario_Asistencia] PRIMARY KEY CLUSTERED 
(
	[Id_Horario_Asistencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Horario_Cometido]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Horario_Cometido](
	[Id_Horario_Cometido] [int] IDENTITY(1,1) NOT NULL,
	[Id_Servicio] [int] NOT NULL,
	[Acno] [int] NOT NULL,
	[Limite_Pago_Am_Pm] [varchar](5) NOT NULL,
	[Hora_Inicio_Am] [varchar](5) NOT NULL,
	[Hora_Termino_Pm] [varchar](5) NOT NULL,
	[Vigente] [int] NOT NULL,
 CONSTRAINT [PK_Horario_Cometido] PRIMARY KEY CLUSTERED 
(
	[Id_Horario_Cometido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Horario_Parametro]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Horario_Parametro](
	[Id_Servicio] [int] NOT NULL,
	[Acno] [int] NOT NULL,
	[Horario_Jornada_Lunes_Jueves] [varchar](5) NULL,
	[Horario_Jornada_Viernes] [varchar](5) NULL,
	[Horario_Entrada_Tope] [varchar](5) NULL,
	[Cometido_Limite_Pago] [varchar](5) NULL,
	[Cometido_Inicio_Am] [varchar](5) NULL,
	[Cometido_Termino_Pm] [varchar](5) NULL,
	[Inconsistencia_Dia_Corte] [int] NULL,
	[Inconsistencia_Dia_Habiles] [int] NULL,
 CONSTRAINT [PK_Horarios_Servicios] PRIMARY KEY CLUSTERED 
(
	[Id_Servicio] ASC,
	[Acno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Instancia_Proceso]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Instancia_Proceso](
	[Id_Instancia_Proceso] [int] IDENTITY(1,1) NOT NULL,
	[Id_Proceso] [int] NOT NULL,
	[Fecha_Inicio] [datetime] NOT NULL,
	[Fecha_Termino] [datetime] NULL,
	[Estado] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Instancia_Proceso] PRIMARY KEY CLUSTERED 
(
	[Id_Instancia_Proceso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Isapre]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Isapre](
	[Id_Isapre] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Isapre] [varchar](50) NULL,
 CONSTRAINT [PK_Isapre] PRIMARY KEY CLUSTERED 
(
	[Id_Isapre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Jefatura]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jefatura](
	[Id_Jefatura] [int] IDENTITY(1,1) NOT NULL,
	[Rut] [int] NOT NULL,
	[Id_Dependencia] [int] NOT NULL,
	[Id_Calidad_Jefatura] [int] NOT NULL,
	[Habilitado] [int] NOT NULL,
	[Orden] [int] NOT NULL,
	[Fecha_Ingreso] [datetime] NULL,
 CONSTRAINT [PK_Jefatura] PRIMARY KEY CLUSTERED 
(
	[Id_Jefatura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Jefatura_Calidad]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jefatura_Calidad](
	[Id_Calidad_Jefatura] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Calidad_Jefatura] [varchar](50) NULL,
 CONSTRAINT [PK_Jefatura_Calidad] PRIMARY KEY CLUSTERED 
(
	[Id_Calidad_Jefatura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Localidad]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Localidad](
	[Id_Localidad] [bigint] IDENTITY(1,1) NOT NULL,
	[Id_Comuna] [int] NOT NULL,
	[Nombre_Localidad] [varchar](50) NOT NULL,
	[Corresponde_Pago_Viatico_Localidad] [int] NULL,
 CONSTRAINT [PK_Localidad] PRIMARY KEY CLUSTERED 
(
	[Id_Localidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Logo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logo](
	[Id_Logos] [int] IDENTITY(1,1) NOT NULL,
	[Id_Servicio] [int] NOT NULL,
	[Desde] [smalldatetime] NOT NULL,
	[Hasta] [smalldatetime] NOT NULL,
	[Nombre_Archivo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Logos] PRIMARY KEY CLUSTERED 
(
	[Id_Logos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mensaje]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mensaje](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mensaje] [varchar](120) NOT NULL,
 CONSTRAINT [PK_Respuesta] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Modelo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Modelo](
	[Id_Modelo] [int] IDENTITY(1,1) NOT NULL,
	[Id_Proceso] [int] NOT NULL,
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_Modelo] PRIMARY KEY CLUSTERED 
(
	[Id_Modelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Nivel_Aprobacion]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Nivel_Aprobacion](
	[Id_Nivel_Aprobacion] [int] IDENTITY(1,1) NOT NULL,
	[Id_Servicio] [int] NOT NULL,
	[Id_Sistema] [int] NULL,
	[Id_Unidad] [int] NOT NULL,
	[Nivel] [int] NOT NULL,
 CONSTRAINT [PK_Nivel_Aprobacion] PRIMARY KEY CLUSTERED 
(
	[Id_Nivel_Aprobacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notificacion]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notificacion](
	[Id_Notificacion] [int] IDENTITY(1,1) NOT NULL,
	[Rut_Notificado] [int] NOT NULL,
	[Acno] [int] NOT NULL,
	[Rut_Notificador] [int] NOT NULL,
	[Id_Documento] [int] NULL,
	[Id_Grado] [int] NULL,
	[Id_Provincia] [int] NULL,
	[Id_Estamento] [int] NULL,
	[Id_Calidad_Juridica] [int] NULL,
	[Fecha_ingreso] [datetime] NULL,
	[Rut_Funcionario] [int] NULL,
	[Fecha_Lectura] [datetime] NULL,
	[Fue_Leido] [int] NULL,
 CONSTRAINT [PK_Notificacion] PRIMARY KEY CLUSTERED 
(
	[Id_Notificacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Plantilla]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Plantilla](
	[Id_Plantilla] [int] IDENTITY(1,1) NOT NULL,
	[Id_Servicio] [int] NULL,
	[Id_Sistema] [int] NULL,
	[Id_Tipo] [int] NOT NULL,
	[Nombre_Plantilla] [varchar](100) NOT NULL,
	[Desde] [date] NULL,
	[Hasta] [date] NULL,
 CONSTRAINT [PK_Plantillas] PRIMARY KEY CLUSTERED 
(
	[Id_Plantilla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Plantilla_Parrafo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Plantilla_Parrafo](
	[Id_Parrafo] [int] IDENTITY(1,1) NOT NULL,
	[Id_Plantilla] [int] NOT NULL,
	[Descripcion_Parrafo] [varchar](50) NOT NULL,
	[Orden_Parrafo] [int] NULL,
 CONSTRAINT [PK_Plantilla_Parrafo] PRIMARY KEY CLUSTERED 
(
	[Id_Parrafo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Plantilla_Parrafo_Texto]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Plantilla_Parrafo_Texto](
	[Id_Texto] [int] IDENTITY(1,1) NOT NULL,
	[Id_Parrafo] [int] NOT NULL,
	[Texto] [varchar](max) NOT NULL,
	[Flag_Presente] [int] NOT NULL,
	[Orden] [int] NOT NULL,
	[Cierra_Documento] [int] NOT NULL,
	[Id_Provincia] [int] NULL,
 CONSTRAINT [PK_Plantilla_Parrafo_Texto] PRIMARY KEY CLUSTERED 
(
	[Id_Texto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Plantilla_Parrafo_Variables]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Plantilla_Parrafo_Variables](
	[Id_Variable] [int] IDENTITY(1,1) NOT NULL,
	[Id_Texto] [int] NOT NULL,
	[Campo_A_Reemplazar] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Plantilla_Parrafo_Variables] PRIMARY KEY CLUSTERED 
(
	[Id_Variable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Plantilla_Tipo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Plantilla_Tipo](
	[Id_Tipo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_Tipo] [varchar](50) NULL,
 CONSTRAINT [PK_Platilla_Tipo] PRIMARY KEY CLUSTERED 
(
	[Id_Tipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Proceso]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proceso](
	[Id_Proceso] [int] IDENTITY(1,1) NOT NULL,
	[Codigo_Proceso] [varchar](50) NULL,
	[Nombre_Proceso] [varchar](255) NULL,
	[Vigente] [int] NULL,
	[Numero_Resolucion] [int] NULL,
	[Fecha_Resolucion] [datetime] NULL,
	[EsInterno] [int] NULL,
	[Id_Sistema] [int] NULL,
 CONSTRAINT [PK_Proceso] PRIMARY KEY CLUSTERED 
(
	[Id_Proceso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Profesion]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profesion](
	[Id_Profesion] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Profesion] [varchar](100) NULL,
	[normada] [int] NULL,
 CONSTRAINT [PK_Profesion] PRIMARY KEY CLUSTERED 
(
	[Id_Profesion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Provincia]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Provincia](
	[Id_Provincia] [int] NOT NULL,
	[Id_Region] [int] NOT NULL,
	[Nombre_Provincia] [varchar](100) NOT NULL,
	[Ciudad] [varchar](50) NULL,
	[Pie_Firma] [varchar](50) NULL,
 CONSTRAINT [PK_Provincia] PRIMARY KEY CLUSTERED 
(
	[Id_Provincia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Region]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Region](
	[Id_Region] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Region] [varchar](100) NOT NULL,
	[Romano] [varchar](50) NULL,
	[Orden] [int] NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[Id_Region] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol](
	[Id_Rol] [int] NOT NULL,
	[Nombre_Rol] [varchar](100) NULL,
	[Habilitado] [int] NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[Id_Rol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rol_Sistema]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol_Sistema](
	[Id_Rol_Sistema] [int] IDENTITY(1,1) NOT NULL,
	[Id_Sistema] [int] NOT NULL,
	[Id_Rol] [int] NOT NULL,
	[Id_Servicio] [int] NULL,
 CONSTRAINT [PK_Rol_Sistema] PRIMARY KEY CLUSTERED 
(
	[Id_Rol_Sistema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rol_Usuario]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol_Usuario](
	[Id_Rol_Usuario] [int] IDENTITY(1,1) NOT NULL,
	[Id_Servicio] [int] NULL,
	[Id_Sistema] [int] NOT NULL,
	[Id_Rol] [int] NOT NULL,
	[Rut_Usuario] [int] NOT NULL,
 CONSTRAINT [PK_Autorizacion] PRIMARY KEY CLUSTERED 
(
	[Id_Rol_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Servicio]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Servicio](
	[Id_Servicio] [int] IDENTITY(0,1) NOT NULL,
	[Id_Tipo_Servicio] [int] NOT NULL,
	[Id_Region] [int] NOT NULL,
	[Nombre_Servicio] [varchar](100) NOT NULL,
	[Nombre_Largo] [varchar](120) NULL,
	[Direccion] [varchar](120) NULL,
	[Telefono] [varchar](50) NULL,
 CONSTRAINT [PK_Servicio] PRIMARY KEY CLUSTERED 
(
	[Id_Servicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Servicio_Sistema]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Servicio_Sistema](
	[Id_Servicio] [int] NOT NULL,
	[Id_Sistema] [int] NOT NULL,
	[Id_Ambiente] [int] NOT NULL,
	[Habilitado] [int] NOT NULL,
 CONSTRAINT [PK_Servicio_Sistema] PRIMARY KEY CLUSTERED 
(
	[Id_Servicio] ASC,
	[Id_Ambiente] ASC,
	[Id_Sistema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Servicio_Tipo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Servicio_Tipo](
	[Id_Tipo_Servicio] [int] IDENTITY(0,1) NOT NULL,
	[Nombre_Tipo_Servicio] [varchar](50) NULL,
 CONSTRAINT [PK_Servicio_Tipo] PRIMARY KEY CLUSTERED 
(
	[Id_Tipo_Servicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Simbolo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Simbolo](
	[Id_Simbolo] [int] NOT NULL,
	[Id_Modelo] [int] NULL,
	[Nombre_Simbolo] [varchar](255) NULL,
	[Tipo_Simbolo] [varchar](50) NULL,
	[Rut_Asignado] [int] NULL,
	[Rol_Asignado] [int] NULL,
	[Formulario] [varchar](255) NULL,
 CONSTRAINT [PK_Simbolo] PRIMARY KEY CLUSTERED 
(
	[Id_Simbolo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sistema]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sistema](
	[Id_Sistema] [int] NOT NULL,
	[Nombre_Sistema] [varchar](100) NULL,
	[Con_Excepciones] [bit] NOT NULL,
 CONSTRAINT [PK_Sistema] PRIMARY KEY CLUSTERED 
(
	[Id_Sistema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sub_Area_Trabajo]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sub_Area_Trabajo](
	[Id_Sub_Area_Trabajo] [int] IDENTITY(0,1) NOT NULL,
	[Id_Area_Trabajo] [int] NOT NULL,
	[Nombre_Sub_Area_Trabajo] [varchar](50) NULL,
	[Habilitado] [int] NULL,
 CONSTRAINT [PK_Sub_Area_Trabajo] PRIMARY KEY CLUSTERED 
(
	[Id_Sub_Area_Trabajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tabla_prueba]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tabla_prueba](
	[MainId] [int] NULL,
	[KeyId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tarea]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tarea](
	[Id_Tarea] [int] IDENTITY(1,1) NOT NULL,
	[Id_Tipo_Tarea] [int] NULL,
	[Rut_Asignado] [int] NULL,
	[Rol_Asignado] [int] NULL,
	[Fecha_Inicio] [datetime] NULL,
	[Fecha_Termino] [datetime] NULL,
	[Vigente] [int] NULL,
	[Observaciones] [varchar](480) NULL,
	[Formulario] [varchar](255) NULL,
	[Id_Instancia_Proceso] [int] NULL,
	[Condicion] [int] NULL,
 CONSTRAINT [PK_Tarea] PRIMARY KEY CLUSTERED 
(
	[Id_Tarea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tipo_Documento]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipo_Documento](
	[Id_Tipo_Documento] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_Tipo_Documento] [varchar](50) NOT NULL,
	[Por_Provincia] [int] NULL,
 CONSTRAINT [PK_Tipo_Documento] PRIMARY KEY CLUSTERED 
(
	[Id_Tipo_Documento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tipo_Excepcion]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipo_Excepcion](
	[Id_Tipo_Excepcion] [int] NOT NULL,
	[Nombre_Tipo_Excepcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Tipo_Excepcion_1] PRIMARY KEY CLUSTERED 
(
	[Id_Tipo_Excepcion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Valor_Grado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Valor_Grado](
	[Id_Valor_Grado] [int] IDENTITY(1,1) NOT NULL,
	[Id_Servicio] [int] NOT NULL,
	[Acno] [int] NULL,
	[Id_Grado] [int] NOT NULL,
	[Desde] [smalldatetime] NULL,
	[Hasta] [smalldatetime] NULL,
	[Valor_C_Pernoctar] [int] NULL,
	[Valor_S_Pernoctar] [int] NULL,
	[Vigente] [int] NULL,
 CONSTRAINT [PK_Valor_Grado] PRIMARY KEY CLUSTERED 
(
	[Id_Valor_Grado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Cliente] ADD  CONSTRAINT [DF_Cliente_Id_Dependencia]  DEFAULT ((0)) FOR [Id_Dependencia]
GO
ALTER TABLE [dbo].[Cliente] ADD  CONSTRAINT [DF_Cliente_id_Comuna]  DEFAULT ((0)) FOR [Id_Comuna]
GO
ALTER TABLE [dbo].[Cliente] ADD  CONSTRAINT [DF_Cliente_Id_Estado_Civil]  DEFAULT ((0)) FOR [Id_Estado_Civil]
GO
ALTER TABLE [dbo].[Cliente] ADD  CONSTRAINT [DF_Cliente_Id_Genero]  DEFAULT ((0)) FOR [Id_Genero]
GO
ALTER TABLE [dbo].[Cliente] ADD  CONSTRAINT [DF_Cliente_Es_Persona_Natural]  DEFAULT ((0)) FOR [Es_Persona_Natural]
GO
ALTER TABLE [dbo].[Cliente] ADD  CONSTRAINT [DF_Cliente_Es_Empresa_Provedor]  DEFAULT ((0)) FOR [Es_Empresa_Provedor]
GO
ALTER TABLE [dbo].[Cliente] ADD  CONSTRAINT [DF_Cliente_Es_Funcionario_Publico]  DEFAULT ((0)) FOR [Es_Funcionario_Publico]
GO
ALTER TABLE [dbo].[Cliente] ADD  CONSTRAINT [DF_Cliente_Es_Usuario_Interno]  DEFAULT ((0)) FOR [Es_Usuario_Interno]
GO
ALTER TABLE [dbo].[Cliente] ADD  CONSTRAINT [DF_Cliente_Es_Empresa_Patrocinante]  DEFAULT ((0)) FOR [Es_Empresa_Patrocinante]
GO
ALTER TABLE [dbo].[Codigo_Presupuestario] ADD  CONSTRAINT [DF_Codigo_Presupuestario_Vigente]  DEFAULT ((1)) FOR [Vigente]
GO
ALTER TABLE [dbo].[Dependencia] ADD  CONSTRAINT [DF_Dependencia_Id_Servicio]  DEFAULT ((0)) FOR [Id_Servicio]
GO
ALTER TABLE [dbo].[Dependencia] ADD  CONSTRAINT [DF_Dependencia_Id_Provincia]  DEFAULT ((0)) FOR [Id_Provincia]
GO
ALTER TABLE [dbo].[Dependencia] ADD  CONSTRAINT [DF_Dependencia_DependeDe]  DEFAULT ((0)) FOR [DependeDe]
GO
ALTER TABLE [dbo].[Dependencia] ADD  CONSTRAINT [DF_Dependencia_Habilitada]  DEFAULT ((0)) FOR [Habilitada]
GO
ALTER TABLE [dbo].[Dependencia] ADD  CONSTRAINT [DF_Dependencia_Id_Tipo_Dependencia]  DEFAULT ((0)) FOR [Id_Tipo_Dependencia]
GO
ALTER TABLE [dbo].[Dependencia] ADD  CONSTRAINT [DF_Dependencia_Es_Departamento_Administracion]  DEFAULT ((0)) FOR [Es_Departamento_Administracion]
GO
ALTER TABLE [dbo].[Dependencia] ADD  CONSTRAINT [DF_Dependencia_Es_Departamento_Delegacion]  DEFAULT ((0)) FOR [Es_Departamento_Provincial]
GO
ALTER TABLE [dbo].[Documento] ADD  CONSTRAINT [DF_Documento_Fue_Leido]  DEFAULT ((0)) FOR [Fue_Leido]
GO
ALTER TABLE [dbo].[Excepcion] ADD  CONSTRAINT [DF_Excepcion_Vigente]  DEFAULT ((1)) FOR [Vigente]
GO
ALTER TABLE [dbo].[Festivo] ADD  CONSTRAINT [DF_Festivo_Es_Nacional]  DEFAULT ((1)) FOR [Es_Nacional]
GO
ALTER TABLE [dbo].[Festivo] ADD  CONSTRAINT [DF_Festivo_Es_Regional]  DEFAULT ((0)) FOR [Es_Regional]
GO
ALTER TABLE [dbo].[Festivo] ADD  CONSTRAINT [DF_Festivo_Id_Provincia]  DEFAULT ((0)) FOR [Id_Provincia]
GO
ALTER TABLE [dbo].[Folio] ADD  CONSTRAINT [DF_Folios_id_Provincia]  DEFAULT ((0)) FOR [id_Provincia]
GO
ALTER TABLE [dbo].[Funcionario] ADD  CONSTRAINT [DF_Funcionario_Id_Calidad_Juridica]  DEFAULT ((0)) FOR [Id_Calidad_Juridica]
GO
ALTER TABLE [dbo].[Funcionario] ADD  CONSTRAINT [DF_Funcionario_Id_Estamento]  DEFAULT ((0)) FOR [Id_Estamento]
GO
ALTER TABLE [dbo].[Funcionario] ADD  CONSTRAINT [DF_Funcionario_Id_Grado]  DEFAULT ((0)) FOR [Id_Grado]
GO
ALTER TABLE [dbo].[Funcionario] ADD  CONSTRAINT [DF_Funcionario_Id_Afp]  DEFAULT ((0)) FOR [Id_Afp]
GO
ALTER TABLE [dbo].[Funcionario] ADD  CONSTRAINT [DF_Funcionario_Id_Isapre]  DEFAULT ((0)) FOR [Id_Isapre]
GO
ALTER TABLE [dbo].[Funcionario] ADD  CONSTRAINT [DF_Funcionario_Id_Sub_Area_Trabajo]  DEFAULT ((0)) FOR [Id_Sub_Area_Trabajo]
GO
ALTER TABLE [dbo].[Funcionario] ADD  CONSTRAINT [DF_Funcionario_Habilitado]  DEFAULT ((1)) FOR [Habilitado]
GO
ALTER TABLE [dbo].[Horario_Asistencia] ADD  CONSTRAINT [DF_Horario_Asistencia_Vigente]  DEFAULT ((1)) FOR [Vigente]
GO
ALTER TABLE [dbo].[Horario_Cometido] ADD  CONSTRAINT [DF_Horario_Cometido_Vigente]  DEFAULT ((1)) FOR [Vigente]
GO
ALTER TABLE [dbo].[Jefatura] ADD  CONSTRAINT [DF_Jefatura_Rut]  DEFAULT ((0)) FOR [Rut]
GO
ALTER TABLE [dbo].[Jefatura] ADD  CONSTRAINT [DF_Jefatura_Id_Dependencia]  DEFAULT ((0)) FOR [Id_Dependencia]
GO
ALTER TABLE [dbo].[Jefatura] ADD  CONSTRAINT [DF_Jefatura_Id_Jefatura_Calidad]  DEFAULT ((0)) FOR [Id_Calidad_Jefatura]
GO
ALTER TABLE [dbo].[Jefatura] ADD  CONSTRAINT [DF_Jefatura_Habilitado]  DEFAULT ((0)) FOR [Habilitado]
GO
ALTER TABLE [dbo].[Sistema] ADD  CONSTRAINT [DF_Sistema_Con_Excepciones]  DEFAULT ((0)) FOR [Con_Excepciones]
GO
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Comuna] FOREIGN KEY([Id_Comuna])
REFERENCES [dbo].[Comuna] ([Id_Comuna])
GO
ALTER TABLE [dbo].[Cliente] NOCHECK CONSTRAINT [FK_Cliente_Comuna]
GO
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Dependencia] FOREIGN KEY([Id_Dependencia])
REFERENCES [dbo].[Dependencia] ([Id_Dependencia])
GO
ALTER TABLE [dbo].[Cliente] NOCHECK CONSTRAINT [FK_Cliente_Dependencia]
GO
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Estado_Civil] FOREIGN KEY([Id_Estado_Civil])
REFERENCES [dbo].[Estado_Civil] ([Id_Estado_Civil])
GO
ALTER TABLE [dbo].[Cliente] NOCHECK CONSTRAINT [FK_Cliente_Estado_Civil]
GO
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Funcionario] FOREIGN KEY([Rut])
REFERENCES [dbo].[Funcionario] ([Rut])
GO
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Funcionario]
GO
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Genero] FOREIGN KEY([Id_Genero])
REFERENCES [dbo].[Genero] ([Id_Genero])
GO
ALTER TABLE [dbo].[Cliente] NOCHECK CONSTRAINT [FK_Cliente_Genero]
GO
ALTER TABLE [dbo].[Comuna]  WITH CHECK ADD  CONSTRAINT [FK_Comuna_Provincia] FOREIGN KEY([Id_Provincia])
REFERENCES [dbo].[Provincia] ([Id_Provincia])
GO
ALTER TABLE [dbo].[Comuna] CHECK CONSTRAINT [FK_Comuna_Provincia]
GO
ALTER TABLE [dbo].[Dependencia]  WITH CHECK ADD  CONSTRAINT [FK_Dependencia_Dependencias_Tipo] FOREIGN KEY([Id_Tipo_Dependencia])
REFERENCES [dbo].[Dependencias_Tipo] ([Id_Tipo_Dependencia])
GO
ALTER TABLE [dbo].[Dependencia] CHECK CONSTRAINT [FK_Dependencia_Dependencias_Tipo]
GO
ALTER TABLE [dbo].[Excepcion]  WITH CHECK ADD  CONSTRAINT [FK_Excepcion_Funcionario] FOREIGN KEY([Rut_Funcionario])
REFERENCES [dbo].[Funcionario] ([Rut])
GO
ALTER TABLE [dbo].[Excepcion] CHECK CONSTRAINT [FK_Excepcion_Funcionario]
GO
ALTER TABLE [dbo].[Excepcion]  WITH CHECK ADD  CONSTRAINT [FK_Excepcion_Sistema] FOREIGN KEY([Id_Sistema])
REFERENCES [dbo].[Sistema] ([Id_Sistema])
GO
ALTER TABLE [dbo].[Excepcion] CHECK CONSTRAINT [FK_Excepcion_Sistema]
GO
ALTER TABLE [dbo].[Excepcion]  WITH CHECK ADD  CONSTRAINT [FK_Excepcion_Tipo_Excepcion] FOREIGN KEY([Id_Tipo_Excepcion])
REFERENCES [dbo].[Tipo_Excepcion] ([Id_Tipo_Excepcion])
GO
ALTER TABLE [dbo].[Excepcion] CHECK CONSTRAINT [FK_Excepcion_Tipo_Excepcion]
GO
ALTER TABLE [dbo].[Flujo]  WITH CHECK ADD  CONSTRAINT [FK_Flujo_Modelo] FOREIGN KEY([Id_Modelo])
REFERENCES [dbo].[Modelo] ([Id_Modelo])
GO
ALTER TABLE [dbo].[Flujo] CHECK CONSTRAINT [FK_Flujo_Modelo]
GO
ALTER TABLE [dbo].[Flujo]  WITH CHECK ADD  CONSTRAINT [FK_Flujo_Simbolo] FOREIGN KEY([Id_Simbolo])
REFERENCES [dbo].[Simbolo] ([Id_Simbolo])
GO
ALTER TABLE [dbo].[Flujo] CHECK CONSTRAINT [FK_Flujo_Simbolo]
GO
ALTER TABLE [dbo].[Funcionario]  WITH CHECK ADD  CONSTRAINT [FK_Funcionario_Afp] FOREIGN KEY([Id_Afp])
REFERENCES [dbo].[Afp] ([Id_Afp])
GO
ALTER TABLE [dbo].[Funcionario] CHECK CONSTRAINT [FK_Funcionario_Afp]
GO
ALTER TABLE [dbo].[Funcionario]  WITH CHECK ADD  CONSTRAINT [FK_Funcionario_Calidad_Juridica] FOREIGN KEY([Id_Calidad_Juridica])
REFERENCES [dbo].[Calidad_Juridica] ([Id_Calidad_Juridica])
GO
ALTER TABLE [dbo].[Funcionario] CHECK CONSTRAINT [FK_Funcionario_Calidad_Juridica]
GO
ALTER TABLE [dbo].[Funcionario]  WITH CHECK ADD  CONSTRAINT [FK_Funcionario_Estamento] FOREIGN KEY([Id_Estamento])
REFERENCES [dbo].[Estamento] ([Id_Estamento])
GO
ALTER TABLE [dbo].[Funcionario] CHECK CONSTRAINT [FK_Funcionario_Estamento]
GO
ALTER TABLE [dbo].[Funcionario]  WITH CHECK ADD  CONSTRAINT [FK_Funcionario_Grado] FOREIGN KEY([Id_Grado])
REFERENCES [dbo].[Grado] ([Id_Grado])
GO
ALTER TABLE [dbo].[Funcionario] CHECK CONSTRAINT [FK_Funcionario_Grado]
GO
ALTER TABLE [dbo].[Funcionario]  WITH CHECK ADD  CONSTRAINT [FK_Funcionario_Isapre] FOREIGN KEY([Id_Isapre])
REFERENCES [dbo].[Isapre] ([Id_Isapre])
GO
ALTER TABLE [dbo].[Funcionario] CHECK CONSTRAINT [FK_Funcionario_Isapre]
GO
ALTER TABLE [dbo].[Funcionario_Profesion]  WITH CHECK ADD  CONSTRAINT [FK_Funcionario_Profesion_Funcionario] FOREIGN KEY([Rut])
REFERENCES [dbo].[Funcionario] ([Rut])
GO
ALTER TABLE [dbo].[Funcionario_Profesion] CHECK CONSTRAINT [FK_Funcionario_Profesion_Funcionario]
GO
ALTER TABLE [dbo].[Funcionario_Profesion]  WITH CHECK ADD  CONSTRAINT [FK_Funcionario_Profesion_Profesion] FOREIGN KEY([Id_Profesion])
REFERENCES [dbo].[Profesion] ([Id_Profesion])
GO
ALTER TABLE [dbo].[Funcionario_Profesion] CHECK CONSTRAINT [FK_Funcionario_Profesion_Profesion]
GO
ALTER TABLE [dbo].[Modelo]  WITH CHECK ADD  CONSTRAINT [FK_Modelo_Proceso] FOREIGN KEY([Id_Proceso])
REFERENCES [dbo].[Proceso] ([Id_Proceso])
GO
ALTER TABLE [dbo].[Modelo] CHECK CONSTRAINT [FK_Modelo_Proceso]
GO
ALTER TABLE [dbo].[Provincia]  WITH CHECK ADD  CONSTRAINT [FK_Provincia_Region] FOREIGN KEY([Id_Region])
REFERENCES [dbo].[Region] ([Id_Region])
GO
ALTER TABLE [dbo].[Provincia] CHECK CONSTRAINT [FK_Provincia_Region]
GO
ALTER TABLE [dbo].[Rol_Sistema]  WITH CHECK ADD  CONSTRAINT [FK_Rol_Sistema_Sistema] FOREIGN KEY([Id_Sistema])
REFERENCES [dbo].[Sistema] ([Id_Sistema])
GO
ALTER TABLE [dbo].[Rol_Sistema] CHECK CONSTRAINT [FK_Rol_Sistema_Sistema]
GO
ALTER TABLE [dbo].[Rol_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Rol_Usuario_Servicio] FOREIGN KEY([Id_Servicio])
REFERENCES [dbo].[Servicio] ([Id_Servicio])
GO
ALTER TABLE [dbo].[Rol_Usuario] CHECK CONSTRAINT [FK_Rol_Usuario_Servicio]
GO
ALTER TABLE [dbo].[Rol_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Sistema Rol_Rol] FOREIGN KEY([Id_Rol])
REFERENCES [dbo].[Rol] ([Id_Rol])
GO
ALTER TABLE [dbo].[Rol_Usuario] CHECK CONSTRAINT [FK_Sistema Rol_Rol]
GO
ALTER TABLE [dbo].[Rol_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Sistema Rol_Sistema] FOREIGN KEY([Id_Sistema])
REFERENCES [dbo].[Sistema] ([Id_Sistema])
GO
ALTER TABLE [dbo].[Rol_Usuario] CHECK CONSTRAINT [FK_Sistema Rol_Sistema]
GO
ALTER TABLE [dbo].[Servicio]  WITH CHECK ADD  CONSTRAINT [FK_Servicio_Region] FOREIGN KEY([Id_Region])
REFERENCES [dbo].[Region] ([Id_Region])
GO
ALTER TABLE [dbo].[Servicio] CHECK CONSTRAINT [FK_Servicio_Region]
GO
ALTER TABLE [dbo].[Servicio]  WITH CHECK ADD  CONSTRAINT [FK_Servicio_Servicio_Tipo] FOREIGN KEY([Id_Tipo_Servicio])
REFERENCES [dbo].[Servicio_Tipo] ([Id_Tipo_Servicio])
GO
ALTER TABLE [dbo].[Servicio] CHECK CONSTRAINT [FK_Servicio_Servicio_Tipo]
GO
ALTER TABLE [dbo].[Servicio_Sistema]  WITH CHECK ADD  CONSTRAINT [FK_Servicio_Sistema_Ambiente] FOREIGN KEY([Id_Ambiente])
REFERENCES [dbo].[Ambiente] ([Id_Ambiente])
GO
ALTER TABLE [dbo].[Servicio_Sistema] CHECK CONSTRAINT [FK_Servicio_Sistema_Ambiente]
GO
ALTER TABLE [dbo].[Servicio_Sistema]  WITH CHECK ADD  CONSTRAINT [FK_Servicio_Sistema_Servicio] FOREIGN KEY([Id_Servicio])
REFERENCES [dbo].[Servicio] ([Id_Servicio])
GO
ALTER TABLE [dbo].[Servicio_Sistema] CHECK CONSTRAINT [FK_Servicio_Sistema_Servicio]
GO
ALTER TABLE [dbo].[Servicio_Sistema]  WITH CHECK ADD  CONSTRAINT [FK_Servicio_Sistema_Sistema] FOREIGN KEY([Id_Sistema])
REFERENCES [dbo].[Sistema] ([Id_Sistema])
GO
ALTER TABLE [dbo].[Servicio_Sistema] CHECK CONSTRAINT [FK_Servicio_Sistema_Sistema]
GO
ALTER TABLE [dbo].[Simbolo]  WITH CHECK ADD  CONSTRAINT [FK_Simbolo_Modelo] FOREIGN KEY([Id_Modelo])
REFERENCES [dbo].[Modelo] ([Id_Modelo])
GO
ALTER TABLE [dbo].[Simbolo] CHECK CONSTRAINT [FK_Simbolo_Modelo]
GO
ALTER TABLE [dbo].[Sub_Area_Trabajo]  WITH CHECK ADD  CONSTRAINT [FK_Sub_Area_Trabajo_AreaTrabajo] FOREIGN KEY([Id_Area_Trabajo])
REFERENCES [dbo].[Area_Trabajo] ([Id_Area_Trabajo])
GO
ALTER TABLE [dbo].[Sub_Area_Trabajo] CHECK CONSTRAINT [FK_Sub_Area_Trabajo_AreaTrabajo]
GO
ALTER TABLE [dbo].[Tarea]  WITH CHECK ADD  CONSTRAINT [FK_Tarea_Simbolo] FOREIGN KEY([Id_Tipo_Tarea])
REFERENCES [dbo].[Simbolo] ([Id_Simbolo])
GO
ALTER TABLE [dbo].[Tarea] CHECK CONSTRAINT [FK_Tarea_Simbolo]
GO
/****** Object:  StoredProcedure [dbo].[Afp_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Afp_Listado] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Afp
	
END
GO
/****** Object:  StoredProcedure [dbo].[Archivo_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Archivo_Agregar] 
	@Nombre_Archivo  varchar(50),
	@Tipo_Contenido varchar(50),
	@Tamacno int,
	@Contenido varbinary(max),
	@Rut int,
	@Tipo_Archivo int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Insert into Archivo(Nombre_Archivo,Tipo_Contenido,Tamacno,Contenido,rut,tipo_archivo)
	values(@Nombre_archivo,@Tipo_Contenido,@Tamacno,@Contenido,@rut,@tipo_archivo)

	
END
GO
/****** Object:  StoredProcedure [dbo].[Archivo_Eliminar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Archivo_Eliminar] 
	-- Add the parameters for the stored procedure here
	@id_archivo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
Delete Archivo
	where Id_Archivo=@Id_Archivo
END
GO
/****** Object:  StoredProcedure [dbo].[Archivo_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Archivo_PorId] 
	-- Add the parameters for the stored procedure here
	@Id_archivo int 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Archivo 
	where Id_Archivo=@Id_Archivo
END
GO
/****** Object:  StoredProcedure [dbo].[Archivo_PorRut]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Archivo_PorRut] 
	-- Add the parameters for the stored procedure here
	@Rut int,
	@tipo_archivo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from archivo
	where rut=@rut and Tipo_Archivo=@tipo_archivo
END
GO
/****** Object:  StoredProcedure [dbo].[Area_Trabajo_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Area_Trabajo_Actualizar]
	-- Add the parameters for the stored procedure here

	@Id_Area_Trabajo int,
	@Nombre_Area_Trabajo varchar(50),
	@Direccion varchar(50),
	@Habilitado int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @x_error as int

   
begin transaction Actualizar

update Area_Trabajo
set Nombre_Area_Trabajo=@Nombre_Area_Trabajo,Direccion =@Direccion,Habilitado=@Habilitado
where Id_Area_Trabajo=@Id_Area_Trabajo

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction
		
END
GO
/****** Object:  StoredProcedure [dbo].[Area_Trabajo_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Area_Trabajo_Agregar] 
	-- Add the parameters for the stored procedure here
	
	@Nombre_Area_Trabajo varchar(50),
	@Direccion varchar(50),
	@Habilitado int,
	@Id_Servicio int,
	@Id_Area_Trabajo int out

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @x_error as int

   
begin transaction Agregar

insert into Area_Trabajo (Nombre_Area_Trabajo,Direccion,Habilitado,Id_Servicio)
values (@Nombre_Area_Trabajo,@Direccion,@Habilitado,@Id_Servicio)

select @Id_Area_Trabajo = @@IDENTITY

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction
	
END
GO
/****** Object:  StoredProcedure [dbo].[Area_Trabajo_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Area_Trabajo_Listado] 
	-- Add the parameters for the stored procedure here
	
	@Id_Servicio int,
	@Habilitado int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	select at.Id_Area_Trabajo,at.Nombre_Area_Trabajo,at.Direccion,at.Habilitado,at.Id_Servicio,s.Nombre_Servicio
	from Area_Trabajo at 
	inner join BD_COMUN_SISTEMAS.dbo.Servicio s on at.Id_Servicio=s.Id_Servicio
	where at.id_servicio=@Id_Servicio and (@Habilitado = -1 or at.Habilitado = @Habilitado)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Area_Trabajo_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Area_Trabajo_PorId] 
	-- Add the parameters for the stored procedure here
	
	@Id_Area_Trabajo int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	select at.Id_Area_Trabajo,at.Nombre_Area_Trabajo,at.Direccion,at.Habilitado,at.Id_Servicio,s.Nombre_Servicio
	from Area_Trabajo at 
	inner join BD_COMUN_SISTEMAS.dbo.Servicio s on at.Id_Servicio=s.Id_Servicio
	where at.Id_Area_Trabajo=@Id_Area_Trabajo
		
END
GO
/****** Object:  StoredProcedure [dbo].[Calcular_Intervalo_Pagina]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Saez
-- Create date: 03-08-2018
-- Description:	Calcula la fila inicial y final de una 
-- página cosiderando el número de página y el 
-- total de registros obtenido de una consulta
-- =============================================
CREATE PROCEDURE [dbo].[Calcular_Intervalo_Pagina] 
	@numero_Pagina   int, 
	@tamacno_Pagina  int,
	@total_Registros int,
	@fila_inicial  int out,
	@fila_final    int out
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	Set @numero_Pagina = ABS(@numero_Pagina)
	Set @tamacno_Pagina= ABS(@tamacno_Pagina)

	-- Calcula la fila inicial y final de acuerdo al numero de página y tamaño de la página
	-- Si el numero de página es 2 y el tamaño de la pagina es de 10 registros
	-- entonces
	-- filaInicial = ((2-1)*10= 10
	-- filaFinal = 10 + 10 +1 = 21
	-- luego para la página 2 y tamaño 10 el procedimiento entregará los registros de 10 al 21


	-- valida que número de página y el tamaño de la página sea 1

	IF @numero_Pagina < 1 SET @numero_Pagina = 1
    IF @tamacno_Pagina < 1 SET @tamacno_Pagina = 1



    SET @fila_Inicial = ((@numero_Pagina - 1) * @tamacno_Pagina)
    SET @fila_Final = @fila_Inicial + @tamacno_Pagina + 1
    IF @fila_Inicial >= @total_Registros
	BEGIN
      SET @fila_Final = @total_Registros + 1
      SET @fila_Inicial = @fila_Final - (@tamacno_Pagina + 1)
	END
END

GO
/****** Object:  StoredProcedure [dbo].[Calidad_Juridica_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Calidad_Juridica_Actualizar] 
	-- Add the parameters for the stored procedure here

	@Id_Calidad_Juridica int,
	@Nombre_Calidad_Juridica varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @x_error as int

   
begin transaction Actualizar

Update Calidad_Juridica Set Nombre_Calidad_Juridica=@Nombre_Calidad_Juridica Where Id_Calidad_Juridica=@Id_Calidad_Juridica
	

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[Calidad_Juridica_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Calidad_Juridica_Agregar] 
	-- Add the parameters for the stored procedure here

	@Nombre_Calidad_Juridica varchar(50),
	@Id_Calidad_Juridica int out	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @x_error as int

   
begin transaction Agregar

insert into Calidad_Juridica (Nombre_Calidad_Juridica)
values (@Nombre_Calidad_Juridica)

select @Id_Calidad_Juridica = @@IDENTITY

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[Calidad_Juridica_Eliminar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 09/01/2019
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Calidad_Juridica_Eliminar] 
	-- Add the parameters for the stored procedure here
	@Id_Calidad_Juridica int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @x_error as int

   
begin transaction Eliminar

delete from Calidad_Juridica where Id_Calidad_Juridica=@Id_Calidad_Juridica
	

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction	
	
END
GO
/****** Object:  StoredProcedure [dbo].[Calidad_Juridica_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Calidad_Juridica_Listado]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Id_Calidad_Juridica,Nombre_Calidad_Juridica
	from Calidad_Juridica
	
END
GO
/****** Object:  StoredProcedure [dbo].[Calidad_Juridica_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Calidad_Juridica_PorId]
	-- Add the parameters for the stored procedure here

	@Id_Calidad_Juridica int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Id_Calidad_Juridica,Nombre_Calidad_Juridica
	from Calidad_Juridica 
	where Id_Calidad_Juridica = @Id_Calidad_Juridica
	
END
GO
/****** Object:  StoredProcedure [dbo].[Cliente_Estado_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Cliente_Estado_Actualizar]
	-- Add the parameters for the stored procedure here

	@Id_Estado int,
	@Nombre_Estado varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	update Cliente_Estado
	set Nombre_Estado=@Nombre_Estado
	where Id_Estado=@Id_Estado
	
END
GO
/****** Object:  StoredProcedure [dbo].[Cliente_Estado_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Cliente_Estado_Agregar] 
	-- Add the parameters for the stored procedure here

	@Nombre_Estado varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	insert into Cliente_Estado (Nombre_Estado)
	values (@Nombre_Estado)
	
END



GO
/****** Object:  StoredProcedure [dbo].[Cliente_Estado_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Cliente_Estado_Listado] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Id_Estado,Nombre_Estado
	from Cliente_Estado
	
END
GO
/****** Object:  StoredProcedure [dbo].[Cliente_Estado_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Cliente_Estado_PorId]
	-- Add the parameters for the stored procedure here
	
	@Id_Estado int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Id_Estado,Nombre_Estado
	from Cliente_Estado
	where Id_Estado = @Id_Estado
	
END
GO
/****** Object:  StoredProcedure [dbo].[Cliente_Tipo_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Cliente_Tipo_Actualizar] 
	-- Add the parameters for the stored procedure here

	@Id_Tipo_Cliente int,
	@Nombre_Tipo_Cliente varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	update Cliente_Tipo
	set Nombre_Tipo_Cliente=@Nombre_Tipo_Cliente
	where @Id_Tipo_Cliente=@Id_Tipo_Cliente
	
END
GO
/****** Object:  StoredProcedure [dbo].[Cliente_Tipo_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Cliente_Tipo_Agregar] 
	-- Add the parameters for the stored procedure here

	@Nombre_Tipo_Cliente varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	insert into Cliente_Tipo (Nombre_Tipo_Cliente)
	values (@Nombre_Tipo_Cliente)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Cliente_Tipo_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Cliente_Tipo_Listado]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Id_Tipo_Cliente,Nombre_Tipo_Cliente
	from Cliente_Tipo
	
END
GO
/****** Object:  StoredProcedure [dbo].[Cliente_Tipo_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Cliente_Tipo_PorId] 
	-- Add the parameters for the stored procedure here
	
	@Id_Tipo_Cliente int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Id_Tipo_Cliente,Nombre_Tipo_Cliente
	from Cliente_Tipo
	where Id_Tipo_Cliente = @Id_Tipo_Cliente
	
END
GO
/****** Object:  StoredProcedure [dbo].[Codigo_Presupuestario_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 02/01/2019
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Codigo_Presupuestario_Actualizar] 
	-- Add the parameters for the stored procedure here
	
	@Id_Codigo_Presupuestario int,
	@Codigo_Presupuestario varchar(50),
	@Nombre_Codigo_Presupuestario varchar(100),
	@Acno int,
	@Con_Viatico int,
	@Vigente int,
	@Id_Servicio int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @x_error as int

   
begin transaction Actualizar

Update Codigo_Presupuestario 
Set  Codigo_Presupuestario=@Codigo_Presupuestario
	,Nombre_Codigo_Presupuestario=@Nombre_Codigo_Presupuestario
	--,Acno=@Acno
	,Con_Viatico=@Con_Viatico 
	,Vigente=@Vigente
	--,Id_Servicio=@Id_Servicio
Where Id_Codigo_Presupuestario=@Id_Codigo_Presupuestario
	

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction
	
END
GO
/****** Object:  StoredProcedure [dbo].[Codigo_Presupuestario_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 02/01/2019
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Codigo_Presupuestario_Agregar] 
	-- Add the parameters for the stored procedure here
	
	@Id_Servicio int,
	@Acno int, 
	@Codigo_Presupuestario varchar(50),
	@Nombre_Codigo_Presupuestario varchar(100),
	@Con_Viatico int,
	@Vigente int,
	@Id_Codigo_Presupuestario int out
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @x_error as int

   
begin transaction Agregar

Insert into Codigo_Presupuestario (Codigo_Presupuestario,Nombre_Codigo_Presupuestario,Acno,Con_Viatico,Vigente,Id_Servicio)
values (@Codigo_Presupuestario,@Nombre_Codigo_Presupuestario,@Acno,@Con_Viatico,@Vigente,@Id_Servicio)

select @Id_Codigo_Presupuestario = @@IDENTITY

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[Codigo_Presupuestario_Eliminar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 02/01/2019
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Codigo_Presupuestario_Eliminar] 
	-- Add the parameters for the stored procedure here
	
	@Id_Codigo_Presupuestario int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @x_error as int

   
begin transaction Eliminar

delete from Codigo_Presupuestario where Id_Codigo_Presupuestario=@Id_Codigo_Presupuestario
	

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction	
	
END
GO
/****** Object:  StoredProcedure [dbo].[Codigo_Presupuestario_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 02/01/2019
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Codigo_Presupuestario_Listado] 
	-- Add the parameters for the stored procedure here
	@Id_Servicio int, 
	@Acno int,
	@Con_Viatico int,
	@Vigente int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select cp.Id_Codigo_Presupuestario,cp.Id_Servicio,cp.Acno,cp.Codigo_Presupuestario,cp.Nombre_Codigo_Presupuestario,cp.Con_Viatico,s.Id_Servicio,s.Nombre_Servicio
	from Codigo_Presupuestario cp
	inner join Servicio s on cp.Id_Servicio = s.Id_Servicio
	where   (@Id_Servicio = 0 or cp.Id_Servicio = @Id_Servicio) 
		and (@Acno = 0 or cp.Acno = @Acno) 
		and (@Con_Viatico = -1 or cp.Con_Viatico = @Con_Viatico) 
		and (@vigente = -1 or cp.Vigente=@Vigente)
	order by s.Id_Servicio,cp.Acno desc, cp.Codigo_Presupuestario
	
END
GO
/****** Object:  StoredProcedure [dbo].[Codigo_Presupuestario_Listado_Acnos]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 09/01/2019
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Codigo_Presupuestario_Listado_Acnos]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT distinct Acno, STR(Acno) as Nombre_Acno 
	FROM Codigo_Presupuestario
	ORDER BY Acno DESC
	
END
GO
/****** Object:  StoredProcedure [dbo].[Codigo_Presupuestario_Listado_Paginado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/01/2019
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Codigo_Presupuestario_Listado_Paginado] 
	-- Add the parameters for the stored procedure here
	
	@Id_Codigo_presupuestario int,
	@Acno int,
	@Con_Viatico int,
	@Vigente int,
	@Id_Servicio int,
	@Pagina_Solicitada int = 1,
	@tamacno_Pagina int= null,
	@total_Registros int Output 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    

Select @total_Registros = count(*)
from  Codigo_Presupuestario cp
inner join Servicio s on cp.Id_Servicio = s.Id_Servicio
where (@Id_Codigo_Presupuestario = 0 or cp.Id_Codigo_presupuestario = @Id_Codigo_Presupuestario)
  and (@Id_Servicio = 0 or cp.Id_Servicio = @Id_Servicio)
  and (@Acno = 0 or cp.Acno = @Acno) 
  and (@Con_Viatico = -1 or cp.Con_Viatico = @Con_Viatico)
  and (@Vigente = -1 or cp.Vigente=@Vigente)


DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out

SELECT * FROM
(SELECT ROW_NUMBER() OVER(ORDER BY s.Id_Servicio,cp.Acno desc, cp.Codigo_Presupuestario) as RowNum,
 cp.Id_Codigo_Presupuestario
,cp.Codigo_Presupuestario
,cp.Nombre_Codigo_Presupuestario
,cp.Acno
,cp.Con_Viatico
,cp.Vigente
,s.Id_Servicio
,s.Nombre_Servicio

from  Codigo_Presupuestario cp
inner join Servicio s on cp.Id_Servicio = s.Id_Servicio
where (@Id_Codigo_Presupuestario = 0 or cp.Id_Codigo_presupuestario = @Id_Codigo_Presupuestario)
  and (@Id_Servicio = 0 or cp.Id_Servicio = @Id_Servicio)
  and (@Acno = 0 or cp.Acno = @Acno) 
  and (@Con_Viatico = -1 or cp.Con_Viatico = @Con_Viatico)
  and (@Vigente = -1 or cp.Vigente=@Vigente)
	
	) as lista 
WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
    
END
GO
/****** Object:  StoredProcedure [dbo].[Codigo_Presupuestario_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 02/01/2019
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Codigo_Presupuestario_PorId] 
	-- Add the parameters for the stored procedure here
	@Id_Codigo_Presupuestario int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select cp.Id_Codigo_Presupuestario,cp.Id_Servicio,cp.Acno,cp.Codigo_Presupuestario,cp.Nombre_Codigo_Presupuestario,cp.Con_Viatico,s.Nombre_Servicio,cp.Vigente
	from Codigo_Presupuestario cp
	inner join Servicio s on cp.Id_Servicio = s.Id_Servicio
	where cp.Id_Codigo_Presupuestario = @Id_Codigo_Presupuestario
	
END
GO
/****** Object:  StoredProcedure [dbo].[Comuna_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Comuna_Listado] 
	-- Add the parameters for the stored procedure here
	@id_Provincia int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select c.Id_Comuna,c.Id_Provincia,c.Nombre_Comuna,p.Nombre_Provincia
	from Comuna c
	inner join Provincia p on c.Id_Provincia = p.Id_Provincia
	where p.Id_Provincia = @id_Provincia
END
GO
/****** Object:  StoredProcedure [dbo].[Comuna_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Comuna_PorId] 
	-- Add the parameters for the stored procedure here
	
	@Id_Comuna int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select c.Id_Comuna,c.Id_Provincia,c.Nombre_Comuna,p.Nombre_Provincia
	from Comuna c
	inner join Provincia p on c.Id_Provincia = p.Id_Provincia
	where c.Id_Comuna=@Id_Comuna
	
END
GO
/****** Object:  StoredProcedure [dbo].[Control_Folio_Documento]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Control_Folio_Documento]
	@Id_Servicio int,
	@Id_Tipo_Documento int,
	@Id_Provincia int, 
	@Folio int out
AS
BEGIN
Declare @separa_por_provincia int
Declare @esta int


set @separa_por_provincia= isnull((select por_provincia from tipo_documento  where id_tipo_documento=@Id_Tipo_Documento),0)
if (@separa_por_provincia=0)
    set @Id_Provincia=NULL

set @esta= (
select COUNT(*) from Folio where Acno=YEAR(getdate()) and Id_Tipo_Documento=@Id_Tipo_Documento and Id_Servicio=@Id_Servicio and (Id_Provincia=@Id_Provincia or @Id_Provincia is null))

if( @esta=0)
   exec Folio_Agregar  @Id_Servicio, @Id_Tipo_Documento, @Id_Provincia, @Folio out
  else
   exec Folio_Actualizar  @Id_Servicio, @Id_Tipo_Documento, @Id_Provincia, @Folio out

return @Folio


END
GO
/****** Object:  StoredProcedure [dbo].[Dependencia_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Dependencia_Actualizar] 
	-- Add the parameters for the stored procedure here

	@Id_Dependencia int,
	@Id_Servicio int,
	@Id_Provincia int,
	@DependeDe int,
	@Nombre_Dependencia varchar(100),
	@Habilitada int,
	@Id_Tipo_Dependencia int,
	@Es_Departamento_Administracion bit,
	@Es_Departamento_Provincial bit
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	update Dependencia
	set Id_Servicio=@Id_Servicio,
	Id_Provincia=@Id_Provincia,
	DependeDe=@DependeDe,
	Nombre_Dependencia=@Nombre_Dependencia,
	Habilitada=@Habilitada,
	Id_Tipo_Dependencia=@Id_Tipo_Dependencia,
	Es_Departamento_Administracion=@Es_Departamento_Administracion,
	Es_Departamento_Provincial=@Es_Departamento_Provincial
	where Id_Dependencia=@Id_Dependencia
	
END
GO
/****** Object:  StoredProcedure [dbo].[Dependencia_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Dependencia_Agregar] 
	-- Add the parameters for the stored procedure here
	
	@Id_Servicio int,
	@Id_Provincia int,
	@DependeDe int,
	@Nombre_Dependencia varchar(100),
	@Habilitada int,
	@Id_Tipo_Dependencia int,
	@Es_Departamento_Administracion bit,
	@Es_Departamento_Provincial bit,
	@Id_Dependencia   int out
	
AS

declare @x_error as int

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

begin transaction ingreso

	insert into Dependencia (Id_Servicio,
	Id_Provincia,
	DependeDe,
	Nombre_Dependencia,
	Habilitada,
	Id_Tipo_Dependencia,
	Es_Departamento_Administracion,
	Es_Departamento_Provincial)
	values (@Id_Servicio,
	@Id_Provincia,
	@DependeDe,
	@Nombre_Dependencia,
	@Habilitada,
	@Id_Tipo_Dependencia,
	@Es_departamento_Administracion,
	@Es_Departamento_Provincial
	)
	
select @Id_Dependencia = @@IDENTITY

-- Se agrega el camino del padre

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction	
	
END
GO
/****** Object:  StoredProcedure [dbo].[Dependencia_Ant]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Dependencia_Ant]
	-- Add the parameters for the stored procedure here
	@Id_Dependencia int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   With DemoCTE (Id,DependeDe,Nombre)
      As(Select depuno.Id_dependencia ,depuno.DependeDe,depuno.Nombre_Dependencia from Dependencia as depuno
	  where  depuno.Id_Dependencia=@id_dependencia
	  union all
	  Select depdos.Id_Dependencia, depdos.DependeDe ,depdos.Nombre_Dependencia  from dependencia as depdos
	  inner join  demoCte as dc on (depdos.Id_Dependencia=dc.DependeDe)
	  where depdos.DependeDe <> 2)
	 
	
	  SELECT * from DemoCTE 
END
GO
/****** Object:  StoredProcedure [dbo].[Dependencia_Antecesores]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Dependencia_Antecesores]
	@id_Dependencia int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



with Arbol_Jerarquico(id_Padre,padre,hijo)
as
(
select uno.id_dependencia, uno.dependede, uno.nombre_dependencia
 from dependencia uno 
 where uno.id_dependencia = isnull(@id_dependencia,uno.id_dependencia)
union all
select dos.id_dependencia, dos.dependede, dos.nombre_dependencia  from dependencia dos 
 INNER JOIN Arbol_Jerarquico cero ON dos.dependede = cero.id_padre 
)

SELECT AJ.id_Padre, dependencia.nombre_dependencia as nombre_dependencia_padre, AJ.hijo as nombre_dependencia_hija FROM Arbol_Jerarquico AJ inner join dependencia on AJ.padre = id_dependencia ORDER BY padre,AJ.hijo
OPTION(MAXRECURSION 1000)

	 
END
GO
/****** Object:  StoredProcedure [dbo].[Dependencia_Decendientes]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Dependencia_Decendientes]
	@id_Dependencia int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



with Arbol_Jerarquico(id_Padre,padre,hijo)
as
(
select uno.id_dependencia, uno.dependede, uno.nombre_dependencia
 from dependencia uno 
 where uno.id_dependencia = isnull(@id_dependencia,uno.id_dependencia)
union all
select dos.id_dependencia, dos.dependede, dos.nombre_dependencia  from dependencia dos 
 INNER JOIN Arbol_Jerarquico cero ON dos.dependede = cero.id_padre 
)

SELECT AJ.id_Padre, dependencia.nombre_dependencia as nombre_dependencia_padre, AJ.hijo as nombre_dependencia_hija FROM Arbol_Jerarquico AJ inner join dependencia on AJ.padre = id_dependencia ORDER BY padre,AJ.hijo
OPTION(MAXRECURSION 1000)

	 
END
GO
/****** Object:  StoredProcedure [dbo].[Dependencia_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Dependencia_Listado]
	-- Add the parameters for the stored procedure here
	@Id_Servicio int,
	@Id_Provincia int
	
	-- exec Dependencia_Listado 24,83

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select d.Id_Dependencia,
	d.Nombre_Dependencia,
	d.Id_Servicio,
	s.Nombre_Servicio,
	d.Id_Provincia,
	p.Nombre_Provincia,
	d.Id_Tipo_Dependencia,
	isnull(d.DependeDe,0) as DependeDe
	,d2.Nombre_Dependencia as Nombre_DependeDe
	,d.Habilitada,
	d.Es_Departamento_Administracion,
	d.Es_Departamento_Provincial
	from Dependencia d
	inner join Servicio s on d.Id_Servicio = s.Id_Servicio
	inner join Provincia p on d.Id_Provincia = p.Id_Provincia
	inner join Dependencia d2 on d.DependeDe = d2.Id_Dependencia
	where (@Id_Servicio = 0 or d.Id_Servicio=@Id_Servicio) and (@Id_Provincia = 0 or d.Id_Provincia=@Id_Provincia)
	order by s.Id_Servicio, p.Id_Provincia,d.Nombre_Dependencia
	
END
GO
/****** Object:  StoredProcedure [dbo].[Dependencia_Listado_Paginado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Dependencia_Listado_Paginado] 
	-- Add the parameters for the stored procedure here
	@Id_Servicio int,
    @Id_Provincia int,
	@Nombre_Dependencia varchar(200),
    @Pagina_Solicitada int = 1,
	@tamacno_Pagina int= null,
	@total_Registros int Output 
AS
BEGIN

Select @total_Registros = count(*)
from Dependencia d
	inner join Servicio s on d.Id_Servicio = s.Id_Servicio
	inner join Provincia p on d.Id_Provincia = p.Id_Provincia
	inner join Dependencia d2 on d.DependeDe = d2.Id_Dependencia
where (@Id_Servicio = 0 or d.Id_Servicio=@Id_Servicio) and
	  (@Id_Provincia = 0 or d.Id_Provincia=@Id_Provincia) and
	  (@Nombre_Dependencia='SinFiltro' or d.Nombre_Dependencia like '%' + @Nombre_Dependencia + '%')



DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out



	





 SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY d.Id_Dependencia) as RowNum,
	d.Id_Dependencia,
	d.Nombre_Dependencia,
	d.Id_Servicio,
	s.Nombre_Servicio,
	d.Id_Provincia,
	p.Nombre_Provincia,
	d.Id_Tipo_Dependencia,
	isnull(d.DependeDe,0) as DependeDe
	,d2.Nombre_Dependencia as Nombre_DependeDe
	,d.Habilitada,
	d.Es_Departamento_Administracion,
	d.Es_Departamento_Provincial

	FROM  Dependencia d
	inner join Servicio s on d.Id_Servicio = s.Id_Servicio
	inner join Provincia p on d.Id_Provincia = p.Id_Provincia
	inner join Dependencia d2 on d.DependeDe = d2.Id_Dependencia
	where (@Id_Servicio = 0 or d.Id_Servicio=@Id_Servicio) and (@Id_Provincia = 0 or d.Id_Provincia=@Id_Provincia) and
	(@Nombre_Dependencia='SinFiltro' or d.Nombre_Dependencia like '%' + @Nombre_Dependencia + '%')
		
		) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final


END
GO
/****** Object:  StoredProcedure [dbo].[Dependencia_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Dependencia_PorId]
	-- Add the parameters for the stored procedure here

	@Id_Dependencia int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	select d.Id_Dependencia,
	d.Id_Servicio,
	d.Id_Provincia,
	p.Nombre_Provincia,
	d.DependeDe,
	dt.Nombre_Tipo_Dependencia,
	d.Nombre_Dependencia,
	dt.Nombre_Tipo_Dependencia + ' ' + d.Nombre_Dependencia as Nombre_tipo_Dependencia_Dependencia,
	d.Habilitada,
	d.Id_Tipo_Dependencia,
	d.Es_Departamento_Administracion,
	d.Es_Departamento_Provincial
	from Dependencia d
	inner join Provincia p on d.Id_Provincia = p.Id_Provincia
	inner join Dependencias_Tipo dt on d.Id_Tipo_Dependencia = dt.Id_Tipo_Dependencia
	where d.Id_Dependencia = @Id_Dependencia
	
END
GO
/****** Object:  StoredProcedure [dbo].[Dependencia_Suc]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Dependencia_Suc]
	-- Add the parameters for the stored procedure here
	@Id_Dependencia int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   With DemoCTE (paco,pepe,juan)
      As(Select Id_dependencia,DependeDe,Nombre_dependencia from Dependencia)

	SELECT * from DemoCTE
END
GO
/****** Object:  StoredProcedure [dbo].[Dependencia_Tipo_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Dependencia_Tipo_Listado] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select Id_Tipo_Dependencia,Nombre_Tipo_Dependencia,Orden 
	From Dependencias_Tipo
	where Id_Tipo_Dependencia > 0
	
END
GO
/****** Object:  StoredProcedure [dbo].[Dependencia_Tipo_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Dependencia_Tipo_PorId] 
	-- Add the parameters for the stored procedure here

	@Id_Tipo_Dependencia int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Id_Tipo_Dependencia,Nombre_Tipo_Dependencia,Orden
	from Dependencias_Tipo 
	where Id_Tipo_Dependencia = @Id_Tipo_Dependencia
	
END
GO
/****** Object:  StoredProcedure [dbo].[Dependencias_Lista_Departamentos]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ASP
-- Create date: 17/10/2018
-- Description:	Entrega las dependencias que son departamento en el servicio especificado
-- =============================================
CREATE PROCEDURE [dbo].[Dependencias_Lista_Departamentos]
	-- Add the parameters for the stored procedure here
	@Id_Servicio int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select d.Id_Dependencia,
	d.Id_Servicio,
	d.Id_Provincia,
	p.Nombre_Provincia,
	d.DependeDe,
	dt.Nombre_Tipo_Dependencia,
	d.Nombre_Dependencia,
	dt.Nombre_Tipo_Dependencia + ' ' + d.Nombre_Dependencia as Nombre_tipo_Dependencia_Dependencia,
	d.Habilitada,
	d.Id_Tipo_Dependencia,
	d.Es_Departamento_Administracion,
	d.Es_Departamento_Provincial
	from Dependencia d
	inner join Provincia p on d.Id_Provincia = p.Id_Provincia
	inner join Dependencias_Tipo dt on d.Id_Tipo_Dependencia = dt.Id_Tipo_Dependencia
	where d.Id_Servicio = @Id_Servicio and
	d.Id_Tipo_Dependencia=4;
END
GO
/****** Object:  StoredProcedure [dbo].[DependenciasAnidadas]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author: Ricardo Pino>
-- Create date: <09/07/2019>
-- Description:	Genera la Jerarquia de las dependencias, asi mismo permitiria generar la grafica y/o mostrar los funcionarios dependendientes
-- =============================================
CREATE PROCEDURE [dbo].[DependenciasAnidadas]
    @id_servicio int
AS
BEGIN
declare @largo int;
set @largo= (select LEN(id_dependencia)+2 from dependencia where dependede in (select dos.id_dependencia from dependencia dos where DependeDe=-1) and Id_Servicio=@id_servicio);

WITH ConsultaRecursiva AS (
SELECT
 ID_dependencia, dependede, Nombre_Dependencia,
 0 as nivel,
 CAST('/' + CAST(ID_dependencia AS VARCHAR(15)) + '/' AS varchar(900)) AS camino_
 ,id_tipo_dependencia
 
FROM
 Dependencia
WHERE
 (id_servicio=@id_servicio and habilitada=1) 
 

UNION ALL

SELECT 
 hijo.ID_dependencia, hijo.dependede, hijo.Nombre_Dependencia,
 cr.nivel + 1,
 CAST(CR.camino_ + CAST(HIJO.ID_dependencia AS VARCHAR(15)) + '/' AS varchar(900)) AS camino_
 ,hijo.id_tipo_dependencia
FROM
 Dependencia AS HIJO
 INNER JOIN
 ConsultaRecursiva AS CR 
 ON CR.ID_dependencia = HIJO.dependede
 WHERE
 (hijo.id_servicio=@id_servicio and hijo.habilitada=1) 
)

SELECT 
id_dependencia,
dependede,
--(case nivel when 0 then null else dependede end ) as dependede,
nombre_dependencia,
nivel,camino_,
isnull((select isnull(DT.Nombre_Tipo_Dependencia,'') from Dependencias_Tipo DT where DT.Id_Tipo_Dependencia=ConsultaRecursiva.Id_Tipo_Dependencia),'') as Nombre_Tipo_Dependencia
--'new primitives.orgdiagram.ItemConfig({id:' + CAST(id_dependencia as varchar(10))+',parent: ' + CAST(dependede as varchar(10)) +',title: "' + CAST(nivel as varchar(10)) +'0",description: "' + nombre_dependencia + '",image: "CDN/js/images/c.png" }) ' as organigrama,
--'new primitives.orgdiagram.ItemConfig({id:' + CAST(id_dependencia as varchar(10))+',parent: ' + CAST(dependede as varchar(10)) +',title: "' + isnull((select isnull(DT.Nombre_Tipo_Dependencia,'') from Dependencias_Tipo DT where DT.Id_Tipo_Dependencia=ConsultaRecursiva.Id_Tipo_Dependencia),'') +'",description: "' + nombre_dependencia + '",image: "data:image/png;base64,"+' + 'window.btoa(unescape(encodeURIComponent("CDN/js/images/c.png")))' + ' }) ' as organigrama

 FROM ConsultaRecursiva where left(camino_,@largo)='/' + (select top 1 cast(id_dependencia as varchar(15)) from ConsultaRecursiva order by camino_) + '/' 
 ORDER BY camino_


END
GO
/****** Object:  StoredProcedure [dbo].[Destinos_Por_Provincia]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Destinos_Por_Provincia] 
	-- Add the parameters for the stored procedure here
	
	@Id_Provincia int 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 create table #Destinos
	(	Id int,
		Id_Comuna	int,	
		Nombre_Comuna varchar(60),
		Id_Localidad int,
		Nombre_Localidad varchar(60),
		Tipo_Destino int
	)

	Insert into #Destinos (Id,Id_Comuna,Nombre_Comuna,Id_Localidad,Nombre_Localidad,Tipo_Destino)	
		select Id_Comuna,Id_Comuna,Nombre_Comuna,Id_Comuna,'',1
		From Comuna as c
		where c.Id_Provincia=@Id_Provincia
			
	Insert into #Destinos (ID,Id_Comuna,Nombre_Comuna,Id_Localidad,Nombre_Localidad,Tipo_Destino)	
		select l.Id_Localidad,l.Id_Comuna,d.Nombre_Comuna,l.Id_Localidad,'(' + l.Nombre_Localidad + ')',2
		From Localidad as l inner join #Destinos as d on (l.Id_Comuna=d.Id_Comuna)
	
	Select * from  #Destinos
	order by Id_Comuna,id asc
	
END
GO
/****** Object:  StoredProcedure [dbo].[Estado_Civil_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Estado_Civil_Listado]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Id_Estado_Civil,Nombre_Estado_Civil 
	from Estado_Civil
	
END
GO
/****** Object:  StoredProcedure [dbo].[Estamento_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Estamento_Listado] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Id_Estamento,Nombre_Estamento 
	from Estamento
	
END
GO
/****** Object:  StoredProcedure [dbo].[Excepcion_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ASP
-- Create date: 24-10-2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Excepcion_Actualizar] 
	-- Add the parameters for the stored procedure here
	@Id_Excepcion int,
	@vigente int
	
AS
BEGIN

Update Excepcion set vigente = @vigente
where Id_Excepcion=@Id_Excepcion
end
GO
/****** Object:  StoredProcedure [dbo].[Excepcion_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ASP
-- Create date: 24/10/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Excepcion_Agregar] 
	-- Add the parameters for the stored procedure here
	@Rut_Autoriza int,
	@Rut_Funcionario int,
	@Id_Sistema int,
	@Id_Tipo_Excepcion int
	
AS
BEGIN
	SET NOCOUNT ON;
	Insert into Excepcion (Rut_Autoriza,Rut_funcionario,Id_Sistema,Id_Tipo_Excepcion,fecha)
	Values(@Rut_Autoriza,@Rut_Funcionario,@Id_Sistema,@Id_Tipo_Excepcion,GetDate())
END
GO
/****** Object:  StoredProcedure [dbo].[Excepcion_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ASP
-- Create date: 25/10/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Excepcion_Listado] 
	-- Add the parameters for the stored procedure here
	@rut_Funcionario int,
	@id_sistema int,
	@id_excepcion int,
	@vigente int,
	@Pagina_Solicitada int = 1,
	@tamacno_Pagina int= null,
	@total_Registros int Output 
AS
BEGIN



Select @total_Registros = count(*)
FROM       Excepcion as e inner join
		   Tipo_Excepcion as te on (e.Id_tipo_excepcion=te.Id_Tipo_Excepcion) inner join
		   Cliente AS c ON C.Rut = e.Rut_Funcionario 
where 

(@rut_Funcionario=0 or e.Rut_Funcionario = @rut_Funcionario) and
(@id_sistema=0 or e.Id_Sistema = @id_sistema) and
(@id_excepcion=0 or e.Id_excepcion = @id_excepcion) and
(@vigente=-1 or e.Vigente = @vigente) 

DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


 SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY e.Id_Tipo_Excepcion) as RowNum,
	
	C.Dv, 
	C.Paterno + ' ' + C.Materno + ' ' + C.Nombres AS Nombre_Funcionario,
	e.Id_excepcion,
	e.Id_Tipo_Excepcion,
	te.Nombre_Tipo_Excepcion,
	e.Id_Sistema,
	s.Nombre_Sistema,
	e.Rut_Funcionario,
	e.Fecha,
	e.Vigente, 
    e.Rut_Autoriza

	FROM         Excepcion as e inner join
		   Tipo_Excepcion as te on (e.Id_tipo_excepcion=te.Id_Tipo_Excepcion) inner join
		   Sistema as s on (e.Id_Sistema= s.Id_Sistema) inner join
		   Cliente AS c ON C.Rut = e.Rut_Funcionario 
	where 
		(@rut_Funcionario=0 or e.Rut_Funcionario = @rut_Funcionario)  and
		(@id_sistema=0 or e.Id_Sistema = @id_sistema) and
		(@id_excepcion=0 or e.Id_excepcion = @id_excepcion) and
		(@vigente=-1 or e.Vigente = @vigente) 

		) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
end
GO
/****** Object:  StoredProcedure [dbo].[Excepcion_PorFuncionario]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 10-12-2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Excepcion_PorFuncionario]
	-- Add the parameters for the stored procedure here

	@rut_Funcionario int,
	@id_sistema int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @Id_Tipo_Excepcion as int

set @Id_Tipo_Excepcion=isnull((select Id_Tipo_Excepcion from Excepcion where Rut_Funcionario=@rut_Funcionario and Id_Sistema=@id_sistema and Vigente=1),0)

select @Id_Tipo_Excepcion as Id_Tipo_Excepcion

--SELECT isnull(Id_Tipo_Excepcion,0) as Id_Tipo_Excepcion
--FROM Excepcion
--WHERE Rut_Funcionario=@rut_Funcionario and Id_Sistema=@id_sistema and Vigente=1

END
GO
/****** Object:  StoredProcedure [dbo].[Facultad_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Facultad_Listado] 
	-- Add the parameters for the stored procedure here
	@Rut int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT f.Id_Facultad,
	f.Rut,
	f.Id_Dependencia,
	f.Descripcion_Facultad,
	f.Vigente
	From Facultad as f
END
GO
/****** Object:  StoredProcedure [dbo].[Festivo_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Festivo_Actualizar]
	@Id_Festivo int,
	@Fecha_Festivo smalldatetime, 
	@Es_Regional int,
	@Id_Provincia int,
	@Nombre_Festivo varchar(100)
AS
BEGIN
	Update Festivo set 
	Fecha_Festivo = @Fecha_Festivo,
	Es_Regional = @Es_Regional, 
	Id_Provincia = @Id_Provincia,
	Nombre_Festivo = @Nombre_Festivo 
	where Id_Festivo = @Id_Festivo
END
GO
/****** Object:  StoredProcedure [dbo].[Festivo_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Festivo_Agregar]
	
	@Fecha_Festivo smalldatetime, 
	@Es_Regional int,
	@Id_Provincia int,
	@Nombre_Festivo varchar(100)
AS
BEGIN
	Insert Festivo ( Fecha_Festivo, Es_Regional, Id_Provincia, Nombre_Festivo )
	Select @Fecha_Festivo, @Es_Regional, @Id_Provincia, @Nombre_Festivo
END
GO
/****** Object:  StoredProcedure [dbo].[Festivo_Eliminar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Festivo_Eliminar]
	@Id_Festivo int
	
AS
BEGIN
	DELETE Festivo where Id_Festivo = @Id_Festivo
END
GO
/****** Object:  StoredProcedure [dbo].[Festivo_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Festivo_Listado]
	
	@acno int, 
	@id_region int
AS
BEGIN
	Select * from Festivo where YEAR(Fecha_Festivo)=@acno or id_provincia in (select id_provincia from Provincia where Id_Region =@id_region)
END
GO
/****** Object:  StoredProcedure [dbo].[Folio_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Folio_Actualizar]
	@Id_Servicio int,
	@Id_Tipo_Documento int,
	@Id_Provincia int,
	@Folio int output
AS
BEGIN
	
	update Folio set Folio_Numero = Folio_Numero + 1
	where  Id_Servicio = @Id_Servicio and
	Id_Tipo_Documento = @Id_Tipo_Documento and 
	(id_Provincia = @Id_Provincia or @Id_Provincia is null) and
	Acno = Year(Getdate())


	set @Folio = (select Folio_Numero from Folio where Acno=YEAR(getdate()) and Id_Tipo_Documento=@Id_Tipo_Documento and Id_Servicio=@Id_Servicio and (Id_Provincia=@Id_Provincia or @Id_Provincia is null) )

	return @Folio
END
GO
/****** Object:  StoredProcedure [dbo].[Folio_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Folio_Agregar]
		@Id_Servicio int,
	@Id_Tipo_Documento int,
	@Id_Provincia int,
	@Folio int out
AS
BEGIN
	
	INSERT Folio ( Id_Servicio, Id_Tipo_Documento, id_Provincia, Acno, Folio_Numero)
	select @Id_Servicio, @Id_Tipo_Documento, @Id_Provincia, Year(Getdate()), 1
	
	set @Folio = 1
	return @Folio
END
GO
/****** Object:  StoredProcedure [dbo].[Funcionario_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Funcionario_Actualizar] 
	-- Add the parameters for the stored procedure here
	
	-- Cliente
	@Rut int,
	@Id_Dependencia int,
	@Nombres varchar(180),
	@Paterno varchar(60),
	@Materno varchar(60),
	@Direccion varchar(100),
	@Fono_Fijo int,
	@Celular int,
	@Email_Particular varchar(50),
	@Fecha_Nacimiento DateTime,
	@Id_Comuna int,
	@Id_Estado_Civil int,
	@Id_Genero  int,
	@Login_Sistemas varchar(30),
	--@Es_Persona_Natural tinyint,
	--@Es_Empresa_Provedor tinyint,
	--@Es_Funcionario_Publico tinyint,
	@Es_Usuario_Interno int,

	---- Funcionario
	@Id_Calidad_Juridica int,
	@Id_Estamento int,
	@Id_Grado int,
	@Id_Afp  int,
	@Id_Isapre int,
	@Id_Sub_Area_Trabajo int,
 	@Email  varchar(50),
	@Anexo  int,	
	@Funcion varchar(50),
	@Habilitado int
	
	
AS
BEGIN
	SET NOCOUNT ON;

    Update Funcionario
	Set 	
	Id_Calidad_Juridica = @Id_Calidad_Juridica,
	Id_Estamento = @Id_Estamento,
	Id_Grado = @Id_Grado ,
	Id_Afp  = @Id_Afp ,
	Id_Isapre = @Id_Isapre ,
	Id_Sub_Area_Trabajo = @Id_Sub_Area_Trabajo,
 	Email  = @Email, 
	Anexo  =@Anexo  ,	
	Funcion = @Funcion ,
	Habilitado = @Habilitado,
	Mosca=	Upper(left(@Nombres,1)+ Left(@Paterno,1)+ left(@Materno,1))
	Where Rut =@rut


    Update Cliente
	set
	Id_Dependencia = @Id_Dependencia, 
	Nombres = @Nombres ,
	Paterno = @Paterno ,
	Materno = @Materno ,
	Direccion = @Direccion ,
	Fono_Fijo = @Fono_Fijo ,
	Celular = @Celular ,
	Email_Particular = @Email_Particular ,
	Fecha_Nacimiento = @Fecha_Nacimiento ,
	Id_Comuna = @Id_Comuna ,
	Id_Estado_Civil = @Id_Estado_Civil,
	Id_Genero = @Id_Genero,
	Login_Sistemas = @Login_Sistemas,
	--Es_Persona_Natural = @Es_Persona_Natural,
	--Es_Empresa_Provedor = @Es_Empresa_Provedor,
	--Es_Funcionario_Publico = @Es_Funcionario_Publico,
	Es_Usuario_Interno = @Es_Usuario_Interno
	
	where Rut =@Rut

	
END
GO
/****** Object:  StoredProcedure [dbo].[Funcionario_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Funcionario_Agregar]
	-- Add the parameters for the stored procedure here
	@Rut int,
	@Dv varchar(1),
	@Id_Dependencia int,
	@Nombres varchar(180),
	@Paterno varchar(60),
	@Materno varchar(60),
	@Direccion Varchar(100),
	@Fono_Fijo varchar(50),
	@Celular varchar(50),
	@Email_Particular varchar(50),
	@Fecha_Nacimiento DateTime,
	@Id_Comuna int,
	@Id_Estado_Civil int,
	@Id_Genero  int,
	@Login_Sistemas varchar(30),
	-- funcionario
	@Id_Calidad_Juridica int,
	@Id_Estamento int,
	@Id_Grado int,
	@Id_Afp  int,
	@Id_Isapre int,
	@Id_Sub_Area_Trabajo int,
	@Email  varchar(50),
	@Anexo  int ,
	@Funcion varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @Si tinyint
    Set @Si = 1;

    --declare @dv varchar(1)
    
    --set @dv = Right(@rut,1)
    --set @Rut = convert(int,LEFT(@rut,len(@rut)-1))

Insert into Cliente
	(Rut,
	Dv,
	Id_Dependencia,
	Nombres,
	Paterno,
	Materno,
	Direccion,
	Fono_Fijo,
	Celular,
	Email_Particular,
	Fecha_Nacimiento,
	Id_Comuna,
	Id_Estado_Civil,
	Id_Genero,
	Login_Sistemas,
	Es_Persona_Natural ,
	Es_Funcionario_Publico,
	Es_Usuario_Interno

	)
	Values(
	@Rut,
	@Dv,
	@Id_Dependencia,
	@Nombres,
	@Paterno,
	@Materno,
	@Direccion,
	@Fono_Fijo,
	@Celular,
	@Email_Particular,
	@Fecha_Nacimiento,
	@Id_Comuna,
	@Id_estado_Civil,
	@Id_Genero,
	@Login_Sistemas,
	@Si,
	@Si,
	@Si
	
)


 -- Insert statements for procedure here
	Insert into Funcionario
	(Rut,
	Id_Calidad_juridica,
	Id_Estamento,
	Id_Grado,
	Id_Afp,
	Id_Isapre,
	Id_Sub_Area_Trabajo,
	Email,
	Anexo,
	Funcion,
	Habilitado,
	Mosca
	)
	Values(
	@Rut,
	@Id_Calidad_juridica,
	@Id_Estamento,
	@Id_Grado,
	@Id_Afp,
	@Id_Isapre,
	@Id_Sub_Area_Trabajo,
	@Email,
	@Anexo,
	@Funcion,
	@Si,
	Upper(left(@Nombres,1)+ Left(@Paterno,1)+ left(@Materno,1))
)


END
GO
/****** Object:  StoredProcedure [dbo].[Funcionario_Dependientes_Listado_Paginado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rcardo Pino
-- Create date: 15/02/2019
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Funcionario_Dependientes_Listado_Paginado]
	@rut int,
	@dependencias varchar(100),
    @id_calidad_juridica int,
    @id_estamento int,
    @habilitado int,    
    @nombre_completo varchar(200),
	@pagina_solicitada int = 1,
	@tamano_pagina int= null,
	@total_registros int Output 
AS
BEGIN

DECLARE @es_jefatura_de varchar(100)
DECLARE @fila_Inicial int
DECLARE @fila_Final int

IF @dependencias is null
	BEGIN
		SET @es_jefatura_de = (select COALESCE(@es_jefatura_de + ', ', '') +ltrim(rtrim(str(id_dependencia))) FROM Jefatura where Rut=@rut and habilitado=1) 
		select id_dependencia, nombre_dependencia from Dependencia where Id_Dependencia in (@es_jefatura_de) 
	END
ELSE
	BEGIN
			SELECT @total_Registros = count(*) FROM Cliente AS C INNER JOIN
				Funcionario AS F ON C.Rut = F.Rut INNER JOIN
                Calidad_Juridica AS FC ON F.Id_Calidad_Juridica = FC.Id_Calidad_Juridica INNER JOIN
                Estamento AS FE ON F.Id_Estamento = FE.Id_Estamento INNER JOIN
                Dependencia AS D ON C.Id_Dependencia = D.Id_Dependencia 
			WHERE
				ltrim(rtrim(str(D.Id_Dependencia))) in (@dependencias ) and
				(@Id_Calidad_Juridica = 0 or F.Id_Calidad_Juridica=@Id_Calidad_Juridica) and 
				(@Id_Estamento = 0 or F.Id_Estamento=@Id_Estamento) and
                (@Nombre_Completo='SinFiltro' or C.Paterno + ' ' + C.Materno + ' ' + C.Nombres like '%' + @Nombre_Completo + '%') and
                (@Habilitado = -1 or F.Habilitado = @Habilitado)

			EXEC Calcular_intervalo_Pagina @pagina_solicitada,@tamano_pagina,@total_registros,@fila_inicial out , @fila_Final out

			SELECT * FROM (
					SELECT	ROW_NUMBER() OVER(ORDER BY c.rut) as RowNum,
							(convert(varchar(10),C.Rut)+ '-' + C.Dv) as Rut_Con_Dv, 
							C.Dv, 
							C.Rut,
							C.Paterno + ' ' + C.Materno + ' ' + C.Nombres AS Nombre_Completo,
							C.Id_Dependencia,
							D.Nombre_Dependencia,
							F.Id_Calidad_Juridica,
							FC.Nombre_Calidad_Juridica, 
							Grado.Nombre_Grado, 
							F.Id_Estamento,
							FE.Nombre_Estamento, 
							F.Habilitado
							from Cliente AS C 
							INNER JOIN Funcionario AS F ON C.Rut = F.Rut 
							INNER JOIN Calidad_Juridica AS FC ON F.Id_Calidad_Juridica = FC.Id_Calidad_Juridica 
							INNER JOIN Estamento AS FE ON F.Id_Estamento = FE.Id_Estamento 
							INNER JOIN Grado ON F.Id_Grado = Grado.Id_Grado 
							INNER JOIN Dependencia AS D ON C.Id_Dependencia = D.Id_Dependencia 
					WHERE
							
							ltrim(rtrim(str(D.Id_Dependencia))) in (@dependencias ) and
							(@Id_Calidad_Juridica = 0 or F.Id_Calidad_Juridica=@Id_Calidad_Juridica) and 
							(@Id_Estamento = 0 or F.Id_Estamento=@Id_Estamento) and
							(@Nombre_Completo='SinFiltro' or C.Paterno + ' ' + C.Materno + ' ' + C.Nombres like '%' + @Nombre_Completo + '%') and
							(@Habilitado = -1 or F.Habilitado = @Habilitado)
					) AS lista 
			WHERE RowNum>@fila_Inicial and RowNum<@fila_Final

	END
END
GO
/****** Object:  StoredProcedure [dbo].[Funcionario_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Funcionario_Listado]
@id_servicio int
	
AS
BEGIN

SELECT c.Rut,
c.Paterno + ' ' + c.Materno + ' ' + c.Nombres AS Apellido_Nombre ,
f.Email
FROM BD_COMUN_SISTEMAS.dbo.Cliente as c inner join Funcionario as f on (c.rut=f.rut)

WHERE c.Es_Usuario_Interno=1 and (@id_servicio = 0 or f.id_servicio=@id_servicio)
ORDER BY c.Paterno,c.Materno,c.Nombres

end 
GO
/****** Object:  StoredProcedure [dbo].[Funcionario_Listado_Paginado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Funcionario_Listado_Paginado]
	@Id_Servicio int,
    @Id_Dependencia int,
    @Id_Calidad_Juridica int,
    @Id_Estamento int,
    @Habilitado int,
    @Id_Genero int,
    @Id_Estado_Civil int,
    @Nombre_Completo varchar(200),
    @Es_Persona_Natural int,
    @Es_Empresa_Provedor int,
    @Es_Funcionario_Publico int,
    @Es_Usuario_Interno int,
	@Pagina_Solicitada int = 1,
	@tamacno_Pagina int= null,
	@total_Registros int Output 
AS
BEGIN

Select @total_Registros = count(*)
FROM         Cliente AS C INNER JOIN
                      Funcionario AS F ON C.Rut = F.Rut INNER JOIN
                      Calidad_Juridica AS FC ON F.Id_Calidad_Juridica = FC.Id_Calidad_Juridica INNER JOIN
                      Estamento AS FE ON F.Id_Estamento = FE.Id_Estamento INNER JOIN
                      Grado ON F.Id_Grado = Grado.Id_Grado INNER JOIN
                      Dependencia AS D ON C.Id_Dependencia = D.Id_Dependencia INNER JOIN
                      Servicio AS S ON D.Id_Servicio = S.Id_Servicio INNER JOIN
                      Estado_Civil ON C.Id_Estado_Civil = Estado_Civil.Id_Estado_Civil INNER JOIN
                      Genero ON C.Id_Genero = Genero.Id_Genero
where 
(@Id_Servicio = 0 or S.Id_Servicio=@Id_Servicio) and 
(@Id_Dependencia = 0 or D.Id_Dependencia=@Id_Dependencia) and
(@Id_Calidad_Juridica = 0 or F.Id_Calidad_Juridica=@Id_Calidad_Juridica) and 
(@Id_Estamento = 0 or F.Id_Estamento=@Id_Estamento) and
(@Nombre_Completo='SinFiltro' or C.Paterno + ' ' + C.Materno + ' ' + C.Nombres like '%' + @Nombre_Completo + '%') and
(@Habilitado = -1 or F.Habilitado = @Habilitado) and
(@Id_Genero = 0 or C.Id_Genero=@Id_Genero) and 
(@Id_Estado_Civil = 0 or C.Id_Estado_Civil=@Id_Estado_Civil) and
(@Es_Usuario_Interno =-1 or C.Es_Usuario_Interno=@Es_Usuario_Interno) and
(C.Es_Persona_Natural = @Es_Persona_Natural) and
(C.Es_Empresa_Provedor = @Es_Empresa_Provedor) and
(C.Es_Funcionario_Publico = @Es_Funcionario_Publico) 


DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


 SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY c.rut) as RowNum,
	 (convert(varchar(10),C.Rut)+ '-' + C.Dv) as Rut_Con_Dv, 
	C.Dv, 
	C.Rut,
	C.Paterno + ' ' + C.Materno + ' ' + C.Nombres AS Nombre_Completo,
	C.Id_Dependencia,
	D.Nombre_Dependencia,
	F.Id_Calidad_Juridica,
	FC.Nombre_Calidad_Juridica, 
	Grado.Nombre_Grado, 
	C.Id_Genero,
	Genero.Nombre_Genero, 
	ISNULL(F.Anexo, 0) AS Anexo,
	ISNULL(F.Email, '') AS Email,
	ISNULL(C.Celular, 0) AS Celular,
	S.Id_Servicio,
	S.Nombre_Servicio, 
	s.Id_Region,
	F.Id_Estamento,
	FE.Nombre_Estamento, 
	F.Habilitado,
	C.Id_Estado_Civil,
	Estado_Civil.Nombre_Estado_Civil, 
	C.Es_Empresa_Provedor,
	C.Es_Funcionario_Publico,
	C.Es_Persona_Natural,
	C.Es_Usuario_Interno


	FROM         Cliente AS C INNER JOIN
                      Funcionario AS F ON C.Rut = F.Rut INNER JOIN
                      Calidad_Juridica AS FC ON F.Id_Calidad_Juridica = FC.Id_Calidad_Juridica INNER JOIN
                      Estamento AS FE ON F.Id_Estamento = FE.Id_Estamento INNER JOIN
                      Grado ON F.Id_Grado = Grado.Id_Grado INNER JOIN
                      Dependencia AS D ON C.Id_Dependencia = D.Id_Dependencia INNER JOIN
                      Servicio AS S ON D.Id_Servicio = S.Id_Servicio INNER JOIN
                      Estado_Civil ON C.Id_Estado_Civil = Estado_Civil.Id_Estado_Civil INNER JOIN
                      Genero ON C.Id_Genero = Genero.Id_Genero
	where 
		(@Id_Servicio = 0 or S.Id_Servicio=@Id_Servicio) and 
		(@Id_Dependencia = 0 or D.Id_Dependencia=@Id_Dependencia) and
		(@Id_Calidad_Juridica = 0 or F.Id_Calidad_Juridica=@Id_Calidad_Juridica) and 
		(@Id_Estamento = 0 or F.Id_Estamento=@Id_Estamento) and
		(@Nombre_Completo='SinFiltro' or C.Paterno + ' ' + C.Materno + ' ' + C.Nombres like '%' + @Nombre_Completo + '%') and
		(@Habilitado = -1 or F.Habilitado = @Habilitado) and
		(@Id_Genero = 0 or C.Id_Genero=@Id_Genero) and 
		(@Id_Estado_Civil = 0 or C.Id_Estado_Civil=@Id_Estado_Civil) and
		(@Es_Usuario_Interno = -1  or C.Es_Usuario_Interno=@Es_Usuario_Interno) and
		(C.Es_Persona_Natural = @Es_Persona_Natural) and
		(C.Es_Empresa_Provedor = @Es_Empresa_Provedor) and
		(C.Es_Funcionario_Publico = @Es_Funcionario_Publico) 
		
		) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final


END
GO
/****** Object:  StoredProcedure [dbo].[Funcionario_Por_Rut]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Funcionario_Por_Rut] 
	-- Add the parameters for the stored procedure here
	@Rut int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT f.* ,c.*,C.Paterno + ' ' + C.Materno + ' ' + C.Nombres AS Nombre_Completo from Funcionario as f inner join Cliente as c on (f.rut=c.rut)
	
set dateformat ymd	SELECT 

 (convert(varchar(10),C.Rut)+ '-' + C.Dv) as Rut_Con_Dv, 
 C.Nombres + ' ' + C.Paterno + ' ' + C.Materno AS Nombre_Completo,
 C.Rut,
 C.Dv,
 C.Es_Funcionario_Publico,
 C.Es_Empresa_Provedor,
 C.Es_Persona_Natural,
 C.Es_Usuario_Interno,
 isnull(F.Id_Calidad_Juridica,0) as Id_Calidad_Juridica,
 isnull(FC.Nombre_Calidad_Juridica,'Sin Información' ) as Nombre_Calidad_Juridica,
 isnull(C.Id_Dependencia,0) as Id_Dependencia,
 isNull(D.Nombre_Dependencia, 'Sin Información') as Nombre_Dependencia,
 isNull(C.Id_Estado_Civil,0) as Id_Estado_Civil,
 isNull(F.Id_Estamento,0) as Id_Estamento,
 isNull(FE.Nombre_Estamento,'Sin Información') as Nombre_Estamento, 
 isNull(s.Id_Region,0) as Id_Region,
 isnull(C.Id_Comuna,0) as Id_Comuna,
 isnull(Provincia.Id_Provincia,0) as Id_Provincia,
 isnull(S.Id_Servicio,0) as Id_Servicio,
isNull(C.Id_Genero,0) as Id_Genero,
isnull(f.id_grado,0) as Id_Grado,
isnull(Grado.Nombre_Grado,'Sin Información') as Nombre_Grado,
isnull(C.Login_Sistemas,'') as Login_sistemas ,
isnull(C.Email_Particular,'') as Email_Particular ,
isnull(C.Fono_Fijo ,'') as Fono_Fijo,
isnull(C.Celular ,'') as Celular,
isnull(f.Anexo,'') as Anexo, 
isnull(f.Email,'') as Email , 
isnull(C.Nombres,'') as Nombres,
isnull(C.Paterno,'') as Paterno,
isNull(C.Materno,'') as MAterno,
isnull(c.Direccion,'') as Direccion, 
isnull(c.fecha_nacimiento,'') as fecha_nacimiento,
isnull(sat.id_area_trabajo,0) as Id_Area_Trabajo,
isnull(sat.id_sub_area_trabajo,0) as Id_Sub_Area_Trabajo,
isnull(sat.Nombre_Sub_Area_Trabajo,'Sin SubArea de Trabajo') as Nombre_Sub_Area_Trabajo,
isnull(at.Nombre_Area_Trabajo,'Sin Area de Trabajo') as Nombre_Area_Trabajo,
isnull(f.id_isapre,0) as Id_Isapre,
isnull(f.id_afp,0) as Id_AFP,
isnull(f.funcion,'Sin Información') as funcion,
f.Habilitado

FROM         Cliente AS C INNER JOIN
                      Funcionario AS F ON C.Rut = F.Rut left join
					  Sub_Area_Trabajo as sat on sat.Id_Sub_Area_Trabajo=f.Id_Sub_Area_Trabajo inner join
					  Area_Trabajo as at on  at.Id_Area_Trabajo=sat.Id_Area_Trabajo left join
                      Calidad_Juridica AS FC ON F.Id_Calidad_Juridica = FC.Id_Calidad_Juridica left JOIN
                      Estamento AS FE ON F.Id_Estamento = FE.Id_Estamento left JOIN
                      Grado ON F.Id_Grado = Grado.Id_Grado left JOIN
                      Dependencia AS D ON C.Id_Dependencia = D.Id_Dependencia left JOIN
				      Estado_Civil ON C.Id_Estado_Civil = Estado_Civil.Id_Estado_Civil left JOIN
                      Genero ON C.Id_Genero = Genero.Id_Genero left JOIN
                      Comuna ON C.Id_Comuna = Comuna.Id_Comuna left JOIN
                      Provincia ON Comuna.Id_Provincia = Provincia.Id_Provincia left JOIN
					  Servicio AS S ON D.Id_Servicio = S.Id_Servicio
                    
	Where f.Rut=@Rut




END
GO
/****** Object:  StoredProcedure [dbo].[Funcionario_Profesion_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Funcionario_Profesion_Agregar] 
	-- Add the parameters for the stored procedure here
	@Rut int, 
	@Id_Profesion int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	Insert Funcionario_Profesion
	 (Rut,Id_Profesion)
	 Values (@Rut,@Id_Profesion)



END
GO
/****** Object:  StoredProcedure [dbo].[Funcionario_Profesion_Eliminar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Funcionario_Profesion_Eliminar] 
	-- Add the parameters for the stored procedure here
	@Rut int,
	@id_Profesion int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Delete Funcionario_Profesion
	Where Rut=@Rut and
	id_profesion=@id_Profesion
END
GO
/****** Object:  StoredProcedure [dbo].[Funcionario_Profesion_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Funcionario_Profesion_Listado] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * From Funcionario_Profesion
END
GO
/****** Object:  StoredProcedure [dbo].[Genero_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Genero_Listado] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id_Genero,Nombre_Genero 
	From Genero
	
END
GO
/****** Object:  StoredProcedure [dbo].[Grado_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Grado_Listado]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id_Grado,Nombre_Grado
	From Grado
	
END
GO
/****** Object:  StoredProcedure [dbo].[HoraInicio_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/12/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[HoraInicio_Listado]
	-- Add the parameters for the stored procedure here


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id_Hora_Inicio,Nombre_Hora_Inicio from Hora_Inicio
	
END
GO
/****** Object:  StoredProcedure [dbo].[Horario_Cometido_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Horario_Cometido_Listado] 
	-- Add the parameters for the stored procedure here
	@Id_Horario_Cometido int,
	@Id_Servicio int, 
	@Pagina_Solicitada int,
	@Tamacno_Pagina int,
	@total_Registros int Output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

DECLARE @fila_Inicial int
DECLARE @fila_Final int    
    
SELECT @total_Registros = count(*)    
FROM Horario_Cometido as hc
WHERE (@Id_Servicio = 0 or hc.Id_servicio = @id_Servicio)
 and (@Id_Horario_Cometido = 0 or hc.Id_Horario_Cometido=@Id_Horario_Cometido) 
  
    
Exec bd_comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out
    
    
SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY hc.Acno desc) as RowNum,
	  hc.*
	  From Horario_Cometido as hc
	        

--WHERE year(s.Fecha_Inicio) = @Acno and s.Rut = @Rut and (@Id_Estado_Solicitud = 0 or s.Id_Estado_Solicitud = @Id_Estado_Solicitud) 
WHERE (@Id_Servicio = 0 or hc.Id_Servicio = @Id_Servicio)
 and (@Id_Horario_Cometido = 0 or hc.Id_Horario_Cometido=@Id_Horario_Cometido) 
 

) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final

END
GO
/****** Object:  StoredProcedure [dbo].[Horario_Parametro_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Horario_Parametro_Actualizar] 
	@acno int,
	@id_servicio int,
	@Horario_Jornada_Lunes_Jueves varchar(5),
	@Horario_Jornada_Viernes varchar(5),
	@Horario_Entrada_Tope varchar(5),
	@Cometido_Limite_Pago varchar(5),
	@Cometido_Inicio_Am varchar(5),
	@Cometido_Termino_Pm  varchar(5),
	@Inconsistencia_Dia_Corte int,
	@Inconsistencia_Dia_Habiles int
AS
BEGIN

declare @esta int
set @esta = (select ISNULL(count(*),0) from Horario_Parametro where id_servicio=@id_servicio and acno=@acno)
if @esta=0
    exec Horario_Parametro_Agregar @acno, @id_servicio, @Horario_Jornada_Lunes_Jueves, @Horario_Jornada_Viernes, @Horario_Entrada_Tope,	@Cometido_Limite_Pago, @Cometido_Inicio_Am,	@Cometido_Termino_Pm, @Inconsistencia_Dia_Corte, @Inconsistencia_Dia_Habiles
   else
   
Update Horario_Parametro set 
Horario_Jornada_Lunes_Jueves	= @Horario_Jornada_Lunes_Jueves,
Horario_Jornada_Viernes			= @Horario_Jornada_Viernes,	
Horario_Entrada_Tope			= @Horario_Entrada_Tope,
Cometido_Limite_Pago			= @Cometido_Limite_Pago,
Cometido_Inicio_Am				= @Cometido_Inicio_Am,
Cometido_Termino_Pm				= @Cometido_Termino_Pm,
Inconsistencia_Dia_Corte		= @Inconsistencia_Dia_Corte,
Inconsistencia_Dia_Habiles		= @Inconsistencia_Dia_Habiles
where id_servicio=@id_servicio and acno=@acno
END
GO
/****** Object:  StoredProcedure [dbo].[Horario_Parametro_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Horario_Parametro_Agregar] 
	@acno int,
	@id_servicio int,
	@Horario_Jornada_Lunes_Jueves varchar(5),
	@Horario_Jornada_Viernes varchar(5),
	@Horario_Entrada_Tope varchar(5),
	@Cometido_Limite_Pago varchar(5),
	@Cometido_Inicio_Am varchar(5),
	@Cometido_Termino_Pm  varchar(5),
	@Inconsistencia_Dia_Corte	int,
	@Inconsistencia_Dia_Habiles int
AS
BEGIN
INSERT Horario_Parametro (
acno,
id_servicio,
Horario_Jornada_Lunes_Jueves,
Horario_Jornada_Viernes,
Horario_Entrada_Tope,
Cometido_Limite_Pago,
Cometido_Inicio_Am,
Cometido_Termino_Pm,
Inconsistencia_Dia_Corte,
Inconsistencia_Dia_Habiles)
select 
@acno,
@id_servicio,
@Horario_Jornada_Lunes_Jueves,
@Horario_Jornada_Viernes,	
@Horario_Entrada_Tope,
@Cometido_Limite_Pago,
@Cometido_Inicio_Am,
@Cometido_Termino_Pm,
@Inconsistencia_Dia_Corte,
@Inconsistencia_Dia_Habiles

END
GO
/****** Object:  StoredProcedure [dbo].[Horario_Parametro_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Horario_Parametro_Listado] 
	@acno int,
	@id_servicio int
AS
BEGIN
	SELECT Id_Servicio
      ,Acno
      ,Horario_Jornada_Lunes_Jueves
      ,Horario_Jornada_Viernes
      ,Horario_Entrada_Tope
      ,Cometido_Limite_Pago
      ,Cometido_Inicio_Am
      ,Cometido_Termino_Pm
      ,Inconsistencia_Dia_Corte
	  ,Inconsistencia_Dia_Habiles
  FROM dbo.Horario_Parametro
  where id_servicio=@id_servicio and acno=@acno
END
GO
/****** Object:  StoredProcedure [dbo].[HoraTermino_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/12/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[HoraTermino_Listado]
	-- Add the parameters for the stored procedure here


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id_Hora_Termino,Nombre_Hora_Termino from Hora_Termino
	
END
GO
/****** Object:  StoredProcedure [dbo].[Isapre_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Isapre_Actualizar] 
	-- Add the parameters for the stored procedure here
	@id_Isapre int, 
	@Nombre_Isapre Varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Update Isapre
	set Nombre_Isapre=@Nombre_Isapre
	Where Id_Isapre=@id_Isapre
    
END
GO
/****** Object:  StoredProcedure [dbo].[Isapre_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Isapre_Agregar] 
	-- Add the parameters for the stored procedure here
	@Nombre_Isapre varchar(50) 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Insert into Isapre (Nombre_Isapre)
	Values(@Nombre_Isapre)
	    
END
GO
/****** Object:  StoredProcedure [dbo].[Isapre_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Isapre_Listado] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id_Isapre,Nombre_Isapre 
	from Isapre
	
END
GO
/****** Object:  StoredProcedure [dbo].[Isapre_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Isapre_PorId] 
	-- Add the parameters for the stored procedure here
	
	@Id_Isapre int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Id_Isapre,Nombre_Isapre 
	from Isapre
	where Id_Isapre=@Id_Isapre
	
END
GO
/****** Object:  StoredProcedure [dbo].[Jefatura_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Jefatura_Actualizar]
	-- Add the parameters for the stored procedure here
	@Id_Jefatura int,
	@Rut int,
	@Id_Dependencia int, 
	@Id_Calidad_Jefatura int,
	@Habilitado  int,
	@Orden  int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


-- ************************** Modificado por FVI **************************

declare @x_error as int
declare @Correlativo as int

   
begin transaction Actualizar

if @Id_Calidad_Jefatura =1
	begin
		
		update Jefatura set Habilitado=0 where Id_Calidad_Jefatura=1 and Id_Dependencia=@Id_Dependencia
		Update Jefatura Set Habilitado=@Habilitado,Orden=1 Where Id_Jefatura=@Id_Jefatura
		
	end
else
	begin
	
		Update Jefatura Set Habilitado=@Habilitado,Orden=@Orden,Fecha_Ingreso=convert(varchar(19),getdate(),113) Where Id_Jefatura=@Id_Jefatura
	
		IF OBJECT_ID('#Paso_Orden') IS NULL
		 BEGIN
			CREATE TABLE #Paso_Orden 
			(
				Id_Jefatura int,
				Rut int,
				Id_Dependencia int, 
				Id_Calidad_Jefatura int,
				Habilitado  int,
				Orden	int,
				Fecha_Ingreso datetime
			)  
		 END
		else
			DELETE #Paso_Orden


		insert #Paso_Orden(Id_Jefatura,Rut,Id_Dependencia,Id_Calidad_Jefatura,Habilitado,Orden,Fecha_Ingreso) 
		select Id_Jefatura,Rut,Id_Dependencia,Id_Calidad_Jefatura,Habilitado,Orden,Fecha_Ingreso from Jefatura 
		where Habilitado=1 and Id_Calidad_Jefatura=2 and Id_Dependencia=@Id_Dependencia    
		    
		Set @Correlativo=0

		declare xCURSOR cursor for
		select Id_Jefatura from #Paso_Orden order by Orden, Fecha_Ingreso desc
		open xCURSOR

			 fetch next from xCURSOR
			 into @Id_Jefatura
			 while @@fetch_status = 0
			 begin
				Set @Correlativo=@Correlativo + 1
				
				update Jefatura set Orden=@Correlativo where Id_Jefatura=@Id_Jefatura

				fetch next from xCURSOR
				into @Id_Jefatura

			 end

		close xCURSOR
		deallocate xCURSOR	
	
	end
	

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction

-- ************************************************************************  
  

END
GO
/****** Object:  StoredProcedure [dbo].[Jefatura_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Jefatura_Agregar] 
	-- Add the parameters for the stored procedure here
	@Rut int,
	@Id_Dependencia int, 
	@Id_Calidad_Jefatura int,
	@Habilitado  int,
	@Orden	int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


-- ************************** Modificado por FVI **************************

declare @x_error as int
declare @Id_Jefatura as int
declare @Correlativo as int

   
begin transaction Agregar

Insert into Jefatura (rut,id_dependencia,Id_Calidad_Jefatura,Habilitado,Orden,Fecha_Ingreso)
values (@rut,@Id_Dependencia,@Id_Calidad_Jefatura,@Habilitado,@orden,convert(varchar(19),getdate(),113))

if @Id_Calidad_Jefatura =1
	begin
		
		select @Id_Jefatura = @@IDENTITY
		
		update Jefatura set Habilitado=0 where Id_Calidad_Jefatura=1 and Id_Dependencia=@Id_Dependencia
		update Jefatura set Habilitado=1,Orden=1 where Id_Jefatura=@Id_Jefatura
		
	end
else
	begin
	
		IF OBJECT_ID('#Paso_Orden') IS NULL
		 BEGIN
			CREATE TABLE #Paso_Orden 
			(
				Id_Jefatura int,
				Rut int,
				Id_Dependencia int, 
				Id_Calidad_Jefatura int,
				Habilitado  int,
				Orden	int,
				Fecha_Ingreso datetime
			)  
		 END
		else
			DELETE #Paso_Orden


		insert #Paso_Orden(Id_Jefatura,Rut,Id_Dependencia,Id_Calidad_Jefatura,Habilitado,Orden,Fecha_Ingreso) 
		select Id_Jefatura,Rut,Id_Dependencia,Id_Calidad_Jefatura,Habilitado,Orden,Fecha_Ingreso from Jefatura 
		where Habilitado=1 and Id_Calidad_Jefatura=2 and Id_Dependencia=@Id_Dependencia    
		    
		Set @Correlativo=0

		declare xCURSOR cursor for
		select Id_Jefatura from #Paso_Orden order by Orden, Fecha_Ingreso desc
		open xCURSOR

			 fetch next from xCURSOR
			 into @Id_Jefatura
			 while @@fetch_status = 0
			 begin
				Set @Correlativo=@Correlativo + 1
				
				update Jefatura set Orden=@Correlativo where Id_Jefatura=@Id_Jefatura

				fetch next from xCURSOR
				into @Id_Jefatura

			 end

		close xCURSOR
		deallocate xCURSOR	
	
	end
	

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction

-- ************************************************************************    
    
END
GO
/****** Object:  StoredProcedure [dbo].[Jefatura_Calidad_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Jefatura_Calidad_Listado] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Jefatura_Calidad
END
GO
/****** Object:  StoredProcedure [dbo].[Jefatura_Eliminar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 27/12/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Jefatura_Eliminar]
	-- Add the parameters for the stored procedure here

	@Id_Jefatura int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
declare @x_error as int
declare @Correlativo as int
declare @Id_Dependencia as int
declare @Id_Calidad_Jefatura as int

   
begin transaction Eliminar

set @Id_Dependencia=isnull((select Id_Dependencia from Jefatura where Id_Jefatura=@Id_Jefatura),0)
set @Id_Calidad_Jefatura=isnull((select Id_Calidad_Jefatura from Jefatura where Id_Jefatura=@Id_Jefatura),0)


if @Id_Calidad_Jefatura =1
	begin
		
		delete from Jefatura where Id_Jefatura=@Id_Jefatura
		
	end
else
	begin
	
		delete from Jefatura where Id_Jefatura=@Id_Jefatura
		
		IF OBJECT_ID('#Paso_Orden') IS NULL
		 BEGIN
			CREATE TABLE #Paso_Orden 
			(
				Id_Jefatura int,
				Rut int,
				Id_Dependencia int, 
				Id_Calidad_Jefatura int,
				Habilitado  int,
				Orden	int,
				Fecha_Ingreso datetime
			)  
		 END
		else
			DELETE #Paso_Orden


		insert #Paso_Orden(Id_Jefatura,Rut,Id_Dependencia,Id_Calidad_Jefatura,Habilitado,Orden,Fecha_Ingreso) 
		select Id_Jefatura,Rut,Id_Dependencia,Id_Calidad_Jefatura,Habilitado,Orden,Fecha_Ingreso from Jefatura 
		where Habilitado=1 and Id_Calidad_Jefatura=2 and Id_Dependencia=@Id_Dependencia    
		    
		Set @Correlativo=0

		declare xCURSOR cursor for
		select Id_Jefatura from #Paso_Orden order by Orden, Fecha_Ingreso desc
		open xCURSOR

			 fetch next from xCURSOR
			 into @Id_Jefatura
			 while @@fetch_status = 0
			 begin
				Set @Correlativo=@Correlativo + 1
				
				update Jefatura set Orden=@Correlativo where Id_Jefatura=@Id_Jefatura

				fetch next from xCURSOR
				into @Id_Jefatura

			 end

		close xCURSOR
		deallocate xCURSOR	
	
	end
	

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction	
	
END
GO
/****** Object:  StoredProcedure [dbo].[Jefatura_Esta_Presente]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Jefatura_Esta_Presente]
	@rut varchar(12)
AS
BEGIN
	select dbo.Ver_Presencia(@rut)
END
GO
/****** Object:  StoredProcedure [dbo].[Jefatura_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Jefatura_Listado] 
	-- Add the parameters for the stored procedure here
	
	@Id_Dependencia int,
	@id_Provincia int,
	@id_servicio int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT distinct j.id_jefatura,
		 j.Rut,c.Dv,
		j.Id_Dependencia,
		j.Id_Calidad_Jefatura,
		j.Habilitado,
		j.Orden ,
		c.Id_Genero
		,f.Email
		,f.Mosca
		,dbo.Mayuscula_Inicial(lower(c.Nombres)) + ' ' + dbo.Mayuscula_Inicial(lower(c.Paterno)) + ' ' +dbo.Mayuscula_Inicial(lower(c.Materno)) as Nombre_Apellido_Jefatura
		,dbo.Mayuscula_Inicial(lower(c.Paterno)) + ' ' + dbo.Mayuscula_Inicial(lower(c.Materno)) + ' ' +dbo.Mayuscula_Inicial(lower(c.Nombres)) as Apellido_Nombre_Jefatura
		,dbo.Mayuscula_Inicial(lower(c.Nombres)) as Nombres,
		d.Nombre_Dependencia,
		d.Id_Provincia,
		d.Es_Departamento_Administracion,
		d.Es_Departamento_Provincial
		,jc.Nombre_Calidad_Jefatura
	from Jefatura j
	inner join Cliente c on j.Rut=c.Rut
	inner join Funcionario as f on (f.rut=c.rut) 
	inner join Jefatura_Calidad jc on j.Id_Calidad_Jefatura=jc.Id_Calidad_Jefatura
	inner join Dependencia d on j.Id_Dependencia=d.Id_Dependencia
	where (@Id_Dependencia = 0 or j.Id_Dependencia=@Id_Dependencia)   and
		  (@Id_Provincia = 0 or d.Id_Provincia=@Id_Provincia) and 
		  (@id_servicio = 0 or f.id_servicio=@id_servicio)
	order by j.Habilitado desc, j.Id_Calidad_Jefatura,j.Orden
	
END
GO
/****** Object:  StoredProcedure [dbo].[Jefatura_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 14/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Jefatura_PorId] 
	-- Add the parameters for the stored procedure here
	@id_jefatura int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT j.Id_jefatura,j.Rut,c.Dv,j.Id_Dependencia,j.Id_Calidad_Jefatura,j.Habilitado,j.Orden 
		,c.Nombres + ' ' + c.Paterno + ' ' + c.Materno as Nombre_Apellido_Jefatura
		,c.Paterno + ' ' + c.Materno + ' ' + c.Nombres as Apellido_Nombre_Jefatura
		,c.Nombres
		,c.Id_Genero
		,d.Nombre_Dependencia,
		d.Es_Departamento_Administracion,
		d.Es_Departamento_Provincial
		,jc.Nombre_Calidad_Jefatura
		,f.Email
		,f.Mosca
	from Jefatura j
	inner join Cliente c on j.Rut=c.Rut
	inner join Jefatura_Calidad jc on j.Id_Calidad_Jefatura=jc.Id_Calidad_Jefatura
	inner join Dependencia d on j.Id_Dependencia=d.Id_Dependencia inner join
	Funcionario as f on (f.rut=c.rut) 
	where (j.id_jefatura=@id_jefatura)
	order by j.Orden
	
END
GO
/****** Object:  StoredProcedure [dbo].[Jefatura_Presente]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Jefatura_Presente]
	@Id_Dependencia int
AS
BEGIN


IF OBJECT_ID('tempdb..#Jefaturas_Presentes') IS NULL
 BEGIN
    create table #Jefaturas_Presentes
	(
		Id_jefatura int,
		Rut	int,	
		Dv varchar(1),
		Id_Dependencia int,
		Es_Departamento_Administracion bit,
		Es_Departamento_Provincial bit,
		Nombre_Dependencia varchar(300),
		Nombre_Jefatura	varchar(300),
		Calidad_Jefatura	varchar(100),
		Id_Genero int,
		Email varchar(50),
		Mosca varchar(20),
		DependeDe int,
		Nombres	varchar(180),
		Presente	int default 1
	)
 END
else
    DELETE #Jefaturas_Presentes

declare @Existen_jefes int
declare @Dependede int    
--while (select isnull(COUNT(*),0) from #Jefaturas_Presentes)<2
while (select isnull(COUNT(*),0) from #Jefaturas_Presentes)=0
begin
	insert #Jefaturas_Presentes (id_jefatura,Rut,
	 dv,
	 Id_Dependencia,
	 Es_Departamento_Administracion,
	 Es_Departamento_Provincial,
	 Nombre_Dependencia,
	 Nombre_Jefatura,
	 Nombres,
	 Id_Genero,
	 Email,
	 Mosca,
	 Calidad_Jefatura,
	 DependeDe,
	 presente)
	SELECT j.Id_jefatura, j.Rut, c.dv,j.Id_Dependencia, 
	d.Es_Departamento_Administracion,
	d.Es_Departamento_Provincial,
	d.Nombre_Dependencia,
	dbo.Mayuscula_Inicial(lower(c.Nombres)) + ' ' + dbo.Mayuscula_Inicial(lower(c.Paterno)) + ' ' + dbo.Mayuscula_Inicial(lower(c.Materno)) AS Nombre_Apellido_Jefatura,
	dbo.Mayuscula_Inicial(lower(c.Nombres)) as Nombres,
	c.Id_Genero,
	f.Email,
	f.Mosca,
	jc.Nombre_Calidad_Jefatura,
	d.DependeDe,
	dbo.Ver_Presencia(ltrim(rtrim(str(c.Rut)))+'-'+c.dv)
	FROM Jefatura AS j INNER JOIN Cliente AS c ON j.Rut = c.Rut INNER JOIN
	Funcionario as f on (f.rut=c.rut) inner join 
    Jefatura_Calidad AS jc ON j.Id_Calidad_Jefatura = jc.Id_Calidad_Jefatura INNER JOIN
    Dependencia AS d ON j.Id_Dependencia = d.Id_Dependencia 
    WHERE j.Habilitado = 1 and j.Id_Dependencia = @Id_Dependencia ORDER BY j.Id_Calidad_Jefatura, j.Orden
    
delete #Jefaturas_Presentes where Presente=0

    if isnull((select COUNT(*) from #Jefaturas_Presentes),0)=0
       begin
         set @Id_Dependencia =(select dependede from Dependencia where Id_Dependencia = @id_dependencia)
         set @Dependede=(select dependede from Dependencia where Id_Dependencia = @id_dependencia)
       end
      else
       break 
    if @Dependede=@Id_Dependencia
       break   
end

select * from #Jefaturas_Presentes

END
GO
/****** Object:  StoredProcedure [dbo].[Jefatura_Tipo_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Jefatura_Tipo_Listado] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id_Tipo_Jefatura,Nombre_Tipo_Jefatura 
	from Jefatura_Tipo
	
END
GO
/****** Object:  StoredProcedure [dbo].[Localidad_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Localidad_Listado] 
	-- Add the parameters for the stored procedure here
	@id_Comuna int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT l.Id_Localidad, l.Nombre_Localidad, l.Id_Comuna,c.Nombre_Comuna
	FROM Localidad as l inner join Comuna as c  on (c.Id_Comuna=l.Id_Comuna) 
	Where l.id_comuna=@id_comuna

END
GO
/****** Object:  StoredProcedure [dbo].[Mensaje_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Mensaje_Agregar] 
	-- Add the parameters for the stored procedure here
	@mensaje varchar(240)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into mensaje (mensaje)
	values(@mensaje)
END
GO
/****** Object:  StoredProcedure [dbo].[Plantilla_Parrafo_Por_Servicio_Sistema]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Plantilla_Parrafo_Por_Servicio_Sistema]
@Id_Servicio int, @Id_Sistema int, @fecha_resolucion datetime
AS
BEGIN
	SELECT ppt.Texto, ppt.Orden, pp.Descripcion_Parrafo from Plantilla_Parrafo_Texto  as ppt inner join 	Plantilla_Parrafo  as pp on (ppt.Id_Parrafo = pp.Id_Parrafo)
	 inner join Plantilla as p on (pp.Id_Plantilla = p.Id_Plantilla)

	 --Where p.id_plantilla = @id_plantilla
	 where p.Id_Servicio=@Id_Servicio and p.id_sistema=@Id_Sistema and @fecha_resolucion between p.Desde and p.hasta
	 
END
GO
/****** Object:  StoredProcedure [dbo].[Plantilla_Parrafo_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Saez 
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Plantilla_Parrafo_PorId] 
	-- Add the parameters for the stored procedure here
	@id_plantilla int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ppt.Texto, ppt.Orden, pp.Descripcion_Parrafo from Plantilla_Parrafo_Texto  as ppt inner join 	Plantilla_Parrafo  as pp on (ppt.Id_Parrafo = pp.Id_Parrafo)
	 inner join Plantilla as p on (pp.Id_Plantilla = p.Id_Plantilla)

	 Where p.id_plantilla = @id_plantilla
	
END
GO
/****** Object:  StoredProcedure [dbo].[Proceso_Listado_Paginado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Proceso_Listado_Paginado] 
	-- Add the parameters for the stored procedure here
	@Vigente int,
	@Pagina_Solicitada int = 1,
	@tamacno_Pagina int= null,
	@total_Registros int Output 
AS
BEGIN

Select @total_Registros = count(*)
FROM         Proceso AS p 
where 
(@Vigente = -1 or p.Vigente=@Vigente) 

DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


 SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY p.Id_Proceso) as RowNum,
	 p.Codigo_Proceso,
	 p.Id_Proceso,
	 p.Nombre_Proceso,
	 p.Vigente,
	 isNull(p.Numero_Resolucion,0) as Numero_Resolucion,
	 p.Fecha_Resolucion 



	FROM         Proceso AS p 
	where 
		
		(@Vigente = -1 or p.Vigente=@Vigente)  
		
		) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final


END
GO
/****** Object:  StoredProcedure [dbo].[Profesion_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Profesion_Actualizar] 
	-- Add the parameters for the stored procedure here
	@Id_Profesion int, 
	@Nombre_Profesion varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Profesion
	Set Nombre_Profesion=@Nombre_Profesion
	Where id_profesion =@id_profesion
	
END
GO
/****** Object:  StoredProcedure [dbo].[Profesion_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Profesion_Agregar] 
	-- Add the parameters for the stored procedure here
	@Nombre_Profesion varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into Profesion (Nombre_Profesion)
	Values(@Nombre_Profesion)
END
GO
/****** Object:  StoredProcedure [dbo].[Profesion_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Sáez Peña
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Profesion_Listado]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Id_Profesion,Nombre_Profesion,normada 
	from Profesion
	
END
GO
/****** Object:  StoredProcedure [dbo].[Profesion_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Saez
-- Create date: 02/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Profesion_PorId] 
	-- Add the parameters for the stored procedure here
	@Id_Profesion int 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Id_Profesion,Nombre_Profesion,normada 
	from Profesion 
	where id_profesion = @Id_Profesion
	
END
GO
/****** Object:  StoredProcedure [dbo].[Provincia_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Provincia_Listado] 
	-- Add the parameters for the stored procedure here
	
	@Id_Servicio int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select distinct p.Id_Provincia,p.Id_Region,p.Nombre_Provincia,r.Nombre_Region
	from Provincia p
	inner join Region r on p.Id_Region = r.Id_Region
	inner join Servicio s on r.Id_Region = s.Id_Region
	where p.Id_Provincia > 0 and p.Id_Region=s.Id_Region and s.Id_Servicio=@Id_Servicio
	order by p.Id_Region, p.Id_Provincia
	
END
GO
/****** Object:  StoredProcedure [dbo].[Provincia_Por_Region]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Provincia_Por_Region] 
	-- Add the parameters for the stored procedure here
	@id_region int 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Provincia
	where Id_Region=@id_region
END
GO
/****** Object:  StoredProcedure [dbo].[Provincia_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Provincia_PorId]
	-- Add the parameters for the stored procedure here
	
	@Id_Provincia int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select p.Id_Provincia,p.Id_Region,p.Nombre_Provincia,r.Nombre_Region
	from Provincia p
	inner join Region r on p.Id_Region = r.Id_Region
	where p.Id_Provincia=@Id_Provincia
	
END
GO
/****** Object:  StoredProcedure [dbo].[Region_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Region_Listado]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Id_Region,Nombre_Region,Romano,Orden
	from Region
	
END
GO
/****** Object:  StoredProcedure [dbo].[Region_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Region_PorId] 
	-- Add the parameters for the stored procedure here

	@Id_Region int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Id_Region,Nombre_Region,Romano,Orden
	from Region
	where Id_Region = @Id_Region
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Actividad_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Actividad_Agregar] 
	-- Add the parameters for the stored procedure here

	@Id_Rol int, 
	@Id_Actividad int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	insert Rol_Actividad (Id_Rol,Id_Actividad)
	values (@Id_Rol,@Id_Actividad)
	 
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Actividad_Eliminar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Actividad_Eliminar] 
	-- Add the parameters for the stored procedure here

	@Id_Rol int, 
	@Id_Actividad int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	delete Rol_Actividad
	where Id_Rol=@Id_Rol and Id_Actividad=@Id_Actividad
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Actividad_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Actividad_Listado] 
	-- Add the parameters for the stored procedure here
	@id_rol int


	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select	ra.Id_Rol,
			r.Nombre_Rol,
			ra.Id_Actividad,
			a.Nombre_Actividad
						
	from Rol_Actividad ra
	inner join Rol r on ra.Id_Rol = r.Id_Rol
	inner join Actividad a on ra.Id_Actividad = a.Id_Actividad
	where ra.Id_Rol=@id_rol
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Actualizar] 
	-- Add the parameters for the stored procedure here

	@Id_Rol int,
	@Nombre_Rol varchar(100),
	@Habilitado int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	update Rol
	set Nombre_Rol=@Nombre_Rol,Habilitado=@Habilitado
	where Id_Rol=@Id_Rol
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Agregar] 
	-- Add the parameters for the stored procedure here

	@Nombre_Rol varchar(100),
	@Habilitado int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into Rol (Nombre_Rol,Habilitado)
	values (@Nombre_Rol,@Habilitado)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Rol_PorId]
	-- Add the parameters for the stored procedure here

	@Id_Rol int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select Id_Rol,Nombre_Rol,Habilitado
	from Rol
	where Id_Rol = @Id_Rol
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Usuario_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Usuario_Agregar] 
	-- Add the parameters for the stored procedure here

	@Id_Rol int, 
	@Id_Sistema int,
	@Id_Servicio int,
	@Rut_Usuario int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	insert Rol_Usuario (Id_Rol,id_Sistema,id_servicio,Rut_Usuario)
	values (@Id_Rol,@Id_sistema,@id_servicio,@Rut_Usuario)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Rol_Usuario_Eliminar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Rol_Usuario_Eliminar] 
	-- Add the parameters for the stored procedure here
	@Id_Rol_Usuario int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	delete Rol_Usuario
	where Id_Rol_Usuario=@Id_Rol_Usuario
	
END
GO
/****** Object:  StoredProcedure [dbo].[Roles_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Roles_Listado] 
	-- Add the parameters for the stored procedure here
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	r.id_rol,
	r.Nombre_Rol,
	r.Habilitado
	From Rol as r 
	

END
GO
/****** Object:  StoredProcedure [dbo].[Roles_Por_Sistema]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Roles_Por_Sistema]
	-- Add the parameters for the stored procedure here
@id_sistema int,
@id_servicio int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select	r.Id_Rol,
			r.Nombre_Rol,
			r.Habilitado
	From Rol  as r inner join
	Rol_Sistema as rs on (r.Id_Rol=rs.Id_Rol)
	Where rs.Id_Sistema = @id_sistema and
		 rs.Id_servicio =@id_servicio
	


END
GO
/****** Object:  StoredProcedure [dbo].[Roles_Por_Usuario]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Saez Peña
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Roles_Por_Usuario] 
	-- Add the parameters for the stored procedure here
	@rut_usuario int,
	@id_sistema int ,
	@id_servicio int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	ru.id_rol_usuario,
	ru.id_rol ,
	ru.id_sistema,
	ru.id_servicio
	From Rol_Usuario as ru 
	Where	ru.rut_usuario =@rut_usuario and
			ru.id_sistema = @id_sistema  and
			ru.id_servicio =@id_servicio

END
GO
/****** Object:  StoredProcedure [dbo].[Servicio_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Servicio_Actualizar] 
	-- Add the parameters for the stored procedure here

	@Id_Servicio int,
	@Id_Tipo_Servicio int,
	@Id_Region int,	
	@Nombre_Servicio varchar(100)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update Servicio
	set Id_Tipo_Servicio=@Id_Tipo_Servicio,Id_Region=@Id_Region,Nombre_Servicio=@Nombre_Servicio
	where Id_Servicio=@Id_Servicio
	
END
GO
/****** Object:  StoredProcedure [dbo].[Servicio_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Servicio_Agregar]
	-- Add the parameters for the stored procedure here

	@Id_Tipo_Servicio int,
	@Id_Region int,	
	@Nombre_Servicio varchar(100)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	insert into Servicio (Id_Tipo_Servicio,Id_Region,Nombre_Servicio)
	values (@Id_Tipo_Servicio,@Id_Region,@Nombre_Servicio)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Servicio_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Servicio_Listado] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select s.Id_Servicio,s.Id_Tipo_Servicio,s.Id_Region,st.Nombre_Tipo_Servicio,s.Nombre_Servicio,st.Nombre_Tipo_Servicio + ' ' + s.Nombre_Servicio as Nombre_Tipo_Servicio_Servicio
	from Servicio s
	inner join Servicio_Tipo st on s.Id_Tipo_Servicio = st.Id_Tipo_Servicio
	where s.Id_Servicio > 0
	order by s.Id_Servicio
	
END
GO
/****** Object:  StoredProcedure [dbo].[Servicio_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Servicio_PorId] 
	-- Add the parameters for the stored procedure here

	@Id_Servicio int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	select s.Id_Servicio,s.Id_Tipo_Servicio,s.Id_Region,st.Nombre_Tipo_Servicio,s.Nombre_Servicio,st.Nombre_Tipo_Servicio + ' ' + s.Nombre_Servicio as Nombre_Tipo_Servicio_Servicio,
	(select Nombre_archivo from Logo where id_servicio=@Id_Servicio and GETDATE() between desde and hasta) as Nombre_archivo
	
	from Servicio s
	inner join Servicio_Tipo st on s.Id_Tipo_Servicio = st.Id_Tipo_Servicio
	where s.Id_Servicio = @Id_Servicio
	
END
GO
/****** Object:  StoredProcedure [dbo].[Servicio_Sistema_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Servicio_Sistema_Agregar]
	-- Add the parameters for the stored procedure here

	@Id_Servicio int, 
	@Id_Sistema int,
	@Id_Ambiente int,
	@Habilitado int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	insert Servicio_Sistema (Id_Servicio,Id_Sistema,Id_Ambiente,Habilitado)
	values (@Id_Servicio,@Id_Sistema,@Id_Ambiente,@Habilitado)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Servicio_Sistema_Eliminar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Servicio_Sistema_Eliminar] 
	-- Add the parameters for the stored procedure here

	@Id_Servicio int, 
	@Id_Sistema int,
	@Id_Ambiente int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 
 	delete Servicio_Sistema
	where Id_Servicio=@Id_Servicio and Id_Sistema=@Id_Sistema and Id_Ambiente=@Id_Ambiente
	
END
GO
/****** Object:  StoredProcedure [dbo].[Servicio_Sistema_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Servicio_Sistema_Listado]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select ss.Id_Servicio,s.Nombre_Servicio,ss.Id_Sistema,si.Nombre_Sistema,ss.Id_Ambiente,a.Nombre_Ambiente,ss.Habilitado
	from Servicio_Sistema ss
	inner join Servicio s on ss.Id_Servicio = s.Id_Servicio
	inner join Sistema si on ss.Id_Sistema = si.Id_Sistema
	inner join Ambiente a on ss.Id_Ambiente = a.Id_Ambiente
	
END
GO
/****** Object:  StoredProcedure [dbo].[Servicio_Tipo_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Servicio_Tipo_Actualizar]
	-- Add the parameters for the stored procedure here

	@Id_Tipo_Servicio int,
	@Nombre_Tipo_Servicio varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update Servicio_Tipo
	set Nombre_Tipo_Servicio=@Nombre_Tipo_Servicio
	where Id_Tipo_Servicio=@Id_Tipo_Servicio
	
END
GO
/****** Object:  StoredProcedure [dbo].[Servicio_Tipo_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Servicio_Tipo_Agregar] 
	-- Add the parameters for the stored procedure here
	
	@Nombre_Tipo_Servicio varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into Servicio_Tipo (Nombre_Tipo_Servicio)
	values (@Nombre_Tipo_Servicio)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Servicio_Tipo_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Servicio_Tipo_Listado] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Id_Tipo_Servicio,Nombre_Tipo_Servicio
	from Servicio_Tipo
	
END
GO
/****** Object:  StoredProcedure [dbo].[Servicio_Tipo_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Servicio_Tipo_PorId] 
	-- Add the parameters for the stored procedure here
	
	@Id_Tipo_Servicio int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	select Id_Tipo_Servicio,Nombre_Tipo_Servicio
	from Servicio_Tipo
	where Id_Tipo_Servicio = @Id_Tipo_Servicio
	
END
GO
/****** Object:  StoredProcedure [dbo].[Sistema_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Sistema_Actualizar]
	-- Add the parameters for the stored procedure here

	@Id_Sistema int,
	@Nombre_Sistema varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update Sistema
	set Nombre_Sistema=@Nombre_Sistema
	where Id_Sistema=@Id_Sistema
	
END
GO
/****** Object:  StoredProcedure [dbo].[Sistema_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Sistema_Agregar]
	-- Add the parameters for the stored procedure here

	@Nombre_Sistema varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into Sistema (Nombre_Sistema)
	values (@Nombre_Sistema)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Sistema_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Sistema_Listado]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Id_Sistema,Nombre_Sistema,Con_Excepciones
	from Sistema
	
END
GO
/****** Object:  StoredProcedure [dbo].[Sistema_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Sistema_PorId] 
	-- Add the parameters for the stored procedure here

	@Id_Sistema int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select Id_Sistema,Nombre_Sistema
	from Sistema
	where Id_Sistema = @Id_Sistema
	
END
GO
/****** Object:  StoredProcedure [dbo].[Sistema_Rol_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Sistema_Rol_Agregar] 
	-- Add the parameters for the stored procedure here

	@Id_Sistema int,
	@Id_Rol int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	insert Sistema_Rol (Id_Sistema,Id_Rol)
	values (@Id_Sistema,@Id_Rol)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Sistema_Rol_Eliminar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Sistema_Rol_Eliminar] 
	-- Add the parameters for the stored procedure here

	@Id_Sistema int,
	@Id_Rol int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	delete Sistema_Rol
	where Id_Sistema=@Id_Sistema and Id_Rol=@Id_Rol
	
END
GO
/****** Object:  StoredProcedure [dbo].[Sistema_Rol_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Sistema_Rol_Listado]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select sr.Id_Sistema,sr.Id_Rol,r.Nombre_Rol,s.Nombre_Sistema
	from Sistema_Rol sr
	inner join Sistema s on sr.Id_Sistema = s.Id_Sistema
	inner join Rol r on sr.Id_Rol = r.Id_Rol
	
END
GO
/****** Object:  StoredProcedure [dbo].[something_AddKeysToTable]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[something_AddKeysToTable]
		(
		@MainId				BIGINT,
		@KeyList			ItemKeyList readonly)
AS
BEGIN
	SET NOCOUNT ON;
	
		INSERT [Tabla_prueba]
			   ([MainId]
			   ,[KeyId])
		SELECT
			@MainId, ItemId 
		FROM
		    @KeyList
		WHERE
			ItemId 
		NOT IN 
		(
			SELECT [KeyId] FROM  [Tabla_prueba] WHERE  MainId = @MainId
		)
			            
END
GO
/****** Object:  StoredProcedure [dbo].[Sub_Area_Trabajo_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Sub_Area_Trabajo_Actualizar]
	-- Add the parameters for the stored procedure here

	@Id_Sub_Area_Trabajo int,
	@Nombre_Sub_Area_Trabajo varchar(50),
	@Habilitado int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
declare @x_error as int

   
begin transaction Actualizar

update Sub_Area_Trabajo 
set Nombre_Sub_Area_Trabajo=@Nombre_Sub_Area_Trabajo,Habilitado=@Habilitado
where Id_Sub_Area_Trabajo=@Id_Sub_Area_Trabajo	

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction
		
END
GO
/****** Object:  StoredProcedure [dbo].[Sub_Area_Trabajo_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Sub_Area_Trabajo_Agregar] 
	-- Add the parameters for the stored procedure here
	
	@Nombre_Sub_Area_Trabajo varchar(50),
	@Habilitado int,
	@Id_Area_Trabajo int,
	@Id_Sub_Area_Trabajo int out
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


declare @x_error as int

   
begin transaction Agregar

insert into Sub_Area_Trabajo (Id_Area_Trabajo,Nombre_Sub_Area_Trabajo,Habilitado)
values (@Id_Area_Trabajo,@Nombre_Sub_Area_Trabajo,@Habilitado)

select @Id_Sub_Area_Trabajo = @@IDENTITY

set @x_error = @x_error + @@ERROR

if @x_error > 0
	rollback transaction 
else
	commit transaction
	
END
GO
/****** Object:  StoredProcedure [dbo].[Sub_Area_Trabajo_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Sub_Area_Trabajo_Listado] 
	-- Add the parameters for the stored procedure here
	
	@Id_Area_Trabajo int,
	@Habilitado int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	select sat.Id_Sub_Area_Trabajo,sat.Id_Area_Trabajo,sat.Nombre_Sub_Area_Trabajo,sat.Habilitado,at.Nombre_Area_Trabajo
	from Sub_Area_Trabajo sat
	inner join Area_Trabajo at on sat.Id_Area_Trabajo = at.Id_Area_Trabajo
	where at.Id_Area_Trabajo=@Id_Area_Trabajo and (@Habilitado = -1 or sat.Habilitado = @Habilitado)
	
END
GO
/****** Object:  StoredProcedure [dbo].[Sub_Area_Trabajo_PorId]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Francisco Villouta Inzunza
-- Create date: 03/08/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Sub_Area_Trabajo_PorId] 
	-- Add the parameters for the stored procedure here
	
	@Id_Sub_Area_Trabajo int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	select sat.Id_Sub_Area_Trabajo,sat.Id_Area_Trabajo,sat.Nombre_Sub_Area_Trabajo,sat.Habilitado,at.Nombre_Area_Trabajo
	from Sub_Area_Trabajo sat
	inner join Area_Trabajo at on sat.Id_Area_Trabajo = at.Id_Area_Trabajo
	where sat.Id_Sub_Area_Trabajo = @Id_Sub_Area_Trabajo
	
END
GO
/****** Object:  StoredProcedure [dbo].[Tarea_Listado_Paginado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tarea_Listado_Paginado] 
	-- Add the parameters for the stored procedure here
	@Id_Tarea  int,
	@Rut_Asignado int,
	@Rol_Asignado int,
	@Vigente int,
	@Id_Tipo_Tarea int,
	@Id_Instancia_Proceso int,
	@Pagina_Solicitada int = 1,
	@tamacno_Pagina int= null,
	@total_Registros int Output 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


DECLARE @fila_Inicial int
DECLARE @fila_Final int


SELECT @total_Registros = count(*)
from  Tarea as t
			
	where 
	(@Id_Tarea = 0 or t.Id_Tarea = @Id_Tarea) 	and
	(@Rut_Asignado = 0 or t.Rut_Asignado = @Rut_Asignado) 	and
	(@Rol_Asignado = 0 or t.Rol_Asignado = @Rol_Asignado) 	and
	(@Vigente = -1 or t.Vigente = @Vigente) 	and
	(@Id_Tipo_Tarea = 0 or t.Id_Tipo_Tarea = @Id_Tipo_Tarea) and
	(@Id_Instancia_Proceso = 0 or t.Id_Instancia_Proceso = @Id_Instancia_Proceso) 
	
	
	
	 Exec bd_comun_sistemas.dbo.Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out


SELECT * FROM
 (SELECT ROW_NUMBER() OVER(ORDER BY t.Fecha_Inicio ) as RowNum,
 t.Id_Tarea,
 t.Fecha_Inicio,
 s.Nombre_Simbolo as Nombre_Tarea,
 t.Vigente,
 isnull(t.Formulario,'') as Formulario,
 isNull(t.Rut_Asignado,0) as Rut_Asignado,
 isNull(t.Rol_Asignado,0) as Rol_Asignado,
 t.Id_Instancia_Proceso,
 isNull(t.Observaciones,'') as Observaciones,
 t.Id_Tipo_Tarea,
 isnull(condicion,-1) as Condicion,

 isnull(t.Fecha_Termino,'19000101') as Fecha_Termino

 FROM  Tarea as t inner join Simbolo as s on (t.Id_Tipo_Tarea=s.Id_Simbolo)
 	
	where 
	(@Id_Tarea = 0 or t.Id_Tarea = @Id_Tarea) 	and
	(@Rut_Asignado = 0 or t.Rut_Asignado = @Rut_Asignado) 	and
	(@Rol_Asignado = 0 or t.Rol_Asignado = @Rol_Asignado) 	and
	(@Vigente = -1 or t.Vigente = @Vigente) 	and
	(@Id_Tipo_Tarea = 0 or t.Id_Tipo_Tarea = @Id_Tipo_Tarea) and
	(@Id_Instancia_Proceso = 0 or t.Id_Instancia_Proceso = @Id_Instancia_Proceso) 
     ) as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final
    
END




GO
/****** Object:  StoredProcedure [dbo].[Tarea_PorSolicitud]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Tarea_PorSolicitud] 
	-- Add the parameters for the stored procedure here
	@Id_Solicitud int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	t.Id_tarea,
	t.Nombre,
	t.Id_Estado_Tarea,
	t.Id_Estado_Solicitud,
	et.Nombre_Estado_Tarea,
	t.Id_Solicitud,
	t.Rut_asignado,
	bd_comun_sistemas.dbo.Mayuscula_Inicial(lower(c.Paterno)) + ' ' + bd_comun_sistemas.dbo.Mayuscula_Inicial(lower(c.Materno)) + ' ' + bd_comun_sistemas.dbo.Mayuscula_Inicial(lower(c.Nombres)) AS Nombre_Asignado,
	isNull(t.Observaciones,'') as Observaciones,
	isNull(t.Id_Vb_Jefe,0) as Id_Vb_Jefe
	
	

	from  TareaP as t inner join  
          Bd_comun_sistemas.dbo.Cliente AS C ON t.Rut_Asignado = c.rut inner join
		  Estado_Tarea as et on (et.Id_Estado_Tarea=t.Id_Estado_Tarea) 
	Where t.Id_Solicitud = @Id_solicitud
END
GO
/****** Object:  StoredProcedure [dbo].[Tipo_Excepcion_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ASP
-- Create date: 25.10.2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Tipo_Excepcion_Listado] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Tipo_Excepcion
END
GO
/****** Object:  StoredProcedure [dbo].[Usuario_Actualizar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Usuario_Actualizar]
	-- Add the parameters for the stored procedure here
	@Rut int, 
	@Nombres varchar(180),
	@Paterno varchar(60),
	@Materno varchar(60),
	@Email_Particular varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Cliente
	SET dbo.Cliente.Nombres = @Nombres, dbo.Cliente.Paterno = @Paterno, dbo.Cliente.Materno = @Materno, dbo.Cliente.Email_Particular = @Email_Particular
	WHERE dbo.Cliente.Rut = @Rut;
END
GO
/****** Object:  StoredProcedure [dbo].[Usuario_Agregar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Usuario_Agregar] 
	-- Add the parameters for the stored procedure here
	@Rut int, 
	@Dv varchar(1),
	@Id_Dependencia int,
	@Nombres varchar(180),
	@Paterno varchar(60),
	@Materno varchar(60),
	@Email_Particular varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.Cliente (Rut,Dv,Id_Dependencia,Nombres,Paterno,Materno,Email_Particular,Es_Persona_Natural)
	
	VALUES(@Rut,@Dv,@Id_Dependencia,@Nombres,@Paterno,@Materno,@Email_Particular,1);
END
GO
/****** Object:  StoredProcedure [dbo].[Usuario_Eliminar]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Usuario_Eliminar]
	-- Add the parameters for the stored procedure here
	@Rut int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE dbo.Cliente WHERE dbo.Cliente.Rut = @Rut;
END
GO
/****** Object:  StoredProcedure [dbo].[Usuario_Por_Login]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alberto Saez 
-- Create date: 06/08/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Usuario_Por_Login]
	-- Add the parameters for the stored procedure here
	@Login_Sistemas varchar(30)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT  
			f.Habilitado ,
		    c.Rut,
			c.Dv,
			dbo.Mayuscula_Inicial(lower(c.Paterno)) as Paterno,
			dbo.Mayuscula_Inicial(lower(c.Materno)) as Materno,
			dbo.Mayuscula_Inicial(lower(c.Nombres)) as Nombres,
			dbo.Mayuscula_Inicial(lower(c.Nombres)) + ' ' + dbo.Mayuscula_Inicial(lower(c.Paterno)) + ' ' + dbo.Mayuscula_Inicial(lower(c.Materno)) as Nombre_Completo,
			isNull(c.Email_Particular,'') as Email_Particular,
			f.Email,
			f.Mosca,
			c.Id_Genero,
			d.Id_Servicio,
			d.Id_Provincia,
			d.Id_Dependencia,
			s.Nombre_Servicio,
			s.Nombre_Largo,
			s.Direccion,
			s.Telefono,
			s.Id_Region,
			f.Id_Calidad_Juridica,
			(select Nombre_archivo from Logo where id_servicio=d.Id_Servicio and GETDATE() between desde and hasta) as Logo_Servicio,
			isnull(cp.id_codigo_presupuestario,0) as Id_Codigo_Presupuestario
			

	from  Cliente as c 	inner join Funcionario as f on (f.rut=c.rut)
	inner join Dependencia as d on (c.id_dependencia=d.id_dependencia)
	inner join Servicio as s on  (s.Id_servicio=d.id_servicio)
	inner join Calidad_Juridica as cj on (f.Id_Calidad_Juridica=cj.Id_Calidad_Juridica)
	left join Codigo_Presupuestario as cp on (cj.Id_Codigo_Presupuestario=cp.Id_Codigo_Presupuestario)
	Where c.Login_Sistemas=@login_Sistemas
END
GO
/****** Object:  StoredProcedure [dbo].[Usuario_Por_Rut]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Usuario_Por_Rut]
	@Rut  int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT  isnull(f.Habilitado,0) as Habilitado,
		    c.Rut,
			c.Dv,
			isnull(c.Paterno,'') as Paterno,
			isNull(c.Materno,'') as Materno,
			isNull(c.Nombres,'') as Nombres,
			isNull(c.Nombres + ' ' + c.Paterno + ' ' + c.Materno,'') as Nombre_Completo,
			isNull(c.Razon_Social,'') as Nombre_Completo_Empresa,
			isNull(c.Email_Particular,'') as Email_Particular,
			isnull(c.Pass_Sistemas,'') as Clave,
		    isNull(f.Email,'') as Email,
			isNull(f.Mosca,'') as Mosca,
			isNull(c.Id_Genero,0) as Id_Genero,
			isNull(d.Id_Servicio,0) as Id_Servicio,
			isNull(d.Id_Provincia,0) as Id_Provincia,
			isNull(d.Id_Dependencia,0) as Id_Dependencia,
			isNull(s.Nombre_Servicio,0) as Nombre_Servicio,
			isNull(s.Nombre_Largo,'') as Nombre_Largo,
			isNull(s.Direccion,'') as Direccion,
			isNull(s.Telefono,0) as Telefono,
			isNull(s.Id_Region,0) as Id_Region,
			isNull(f.Id_Calidad_Juridica,0) as Id_Calidad_Juridica,
			
			isNull((select Nombre_archivo from Logo where id_servicio=d.Id_Servicio and GETDATE() between desde and hasta),'') as Logo_Servicio
			

	from  Cliente as c 	left join Funcionario as f on (f.rut=c.rut)
	left join Dependencia as d on (c.id_dependencia=d.id_dependencia)
	left join Servicio as s on  (s.Id_servicio=d.id_servicio)
	Where c.Rut=@Rut
END

GO
/****** Object:  StoredProcedure [dbo].[Usuarios_Listado]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Usuarios_Listado] 
	-- Add the parameters for the stored procedure here
	@Paterno varchar(60),
	@Pagina_Solicitada int = 1,
	@tamacno_Pagina int= null,
	@total_Registros int Output 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	Select @total_Registros = count(*)
FROM         Cliente
Where Es_Persona_Natural=1 and
	 (Paterno= ''  or Paterno like '%'+@Paterno+'%')
    -- Insert statements for procedure here

DECLARE

	@fila_Inicial int,
	@fila_Final int

Exec Calcular_intervalo_Pagina @Pagina_solicitada,@tamacno_pagina,@total_registros,@fila_inicial out , @fila_Final out




SELECT * FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY c.rut) as RowNum,

	c.Rut,
	c.Dv,
	c.Nombres, 
	c.Paterno, 
	c.Materno,
	c.Nombres + ' ' + c.Paterno + ' ' + c.Materno as Nombre_Completo,
	c.Email_Particular
	FROM Cliente c
	Where c.Es_Persona_Natural=1 and
	(Paterno= ''  or Paterno like '%'+@Paterno+'%'))
	as lista 
    WHERE RowNum>@fila_Inicial and RowNum<@fila_Final

END
GO
/****** Object:  StoredProcedure [dbo].[Usuarios_Por_Rol]    Script Date: 20-05-2021 13:08:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ASP
-- Create date: 26-11-2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Usuarios_Por_Rol] 
	-- Add the parameters for the stored procedure here
	@id_rol int,
	@id_sistema int,
	@id_servicio int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 
	ru.id_rol_usuario,
	ru.id_rol ,
	ru.Rut_Usuario,
	ru.id_sistema,
	ru.id_servicio,
	dbo.Mayuscula_Inicial(lower(c.Nombres)) + ' ' + dbo.Mayuscula_Inicial(lower(c.Paterno)) + ' ' + dbo.Mayuscula_Inicial(lower(c.Materno)) as Nombre_Completo
	

	From Rol_Usuario as ru inner join Cliente as c on  (ru.rut_usuario=c.rut)

	
	Where	ru.id_rol =@id_rol and
			ru.id_sistema = @id_sistema and
			ru.id_servicio=@id_servicio

END
GO
