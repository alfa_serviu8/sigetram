﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTram.Models.Entidades
{
    public class RolUsuario
    {
        public int Id { set; get; }
        public int Id_Servicio { set; get; }
        public int Id_Sistema { set; get; }
        public int Id_Rol { set; get; }
        public String Nombre_Rol { set; get; }
        public int Rut { set; get; }
        public String Nombre_Completo { set; get; }
        public string Email { set; get; }
        public int Id_Provincia { set; get; }
        public int Id_Dependencia { set; get; }

      
 

        public RolUsuario(int id, int idRol, int rut)
        {
            this.Id = id;
            this.Id_Servicio = new Servicio().ValorId();
            this.Id_Sistema = new Sistema().ValorId();
            this.Id_Rol = idRol;
            this.Rut = rut;

        }

        
        public RolUsuario(SqlDataReader dr)
        {
            this.Id = Convert.ToInt32(dr["Id_Rol_Usuario"]);
            this.Id_Rol = Convert.ToInt32(dr["Id_Rol"]);
            this.Nombre_Rol = dr["Nombre_Rol"].ToString();
            this.Rut= Convert.ToInt32(dr["Rut_Usuario"]);
            this.Nombre_Completo = dr["Nombre_Completo"].ToString();
            this.Email= dr["Email"].ToString();
            this.Id_Dependencia = Convert.ToInt32(dr["Id_Dependencia"]);
       }

        public RolUsuario()
        {

        }

    }

}