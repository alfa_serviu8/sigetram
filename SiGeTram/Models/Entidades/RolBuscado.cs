﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;


namespace SiGeTram.Models.Entidades
{
    public class RolBuscado
    {
        public Rol rol { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public RolBuscado(Rol ru,PaginaImp p)
        {

            this.rol = ru;
            this.pagina = p;
                           
        }

        
        public RolBuscado()
        {

        }

       


    }

}