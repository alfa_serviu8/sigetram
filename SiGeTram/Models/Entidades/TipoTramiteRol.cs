﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTram.Models.Entidades
{
    public class TipoTramiteRol
    {
        public int Id_Tipo_Tramite { set; get; }
        public int Id_Rol { set; get; }
        public int Id_Sistema { set;get; }
        public int Id_Servicio { set; get; }
        public bool Habilitado { set; get; }
        
        public TipoTramiteRol(SqlDataReader dr)
        {
            this.Id_Tipo_Tramite = Convert.ToInt32(dr["Id_Tipo_Tramite"]);
            this.Id_Rol= Convert.ToInt32(dr["Id_Rol"]);
            this.Id_Servicio = new Servicio().ValorId();
            this.Id_Sistema = new Sistema().ValorId();
                 
        }

        public TipoTramiteRol()
        {
            

        }

    }

}