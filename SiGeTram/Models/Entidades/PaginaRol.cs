﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTram.Models.Entidades
{
    public class PaginaRol
    {
        public List<Rol> roles { set; get; }
        public int totalRegistros { set; get; }

        public PaginaRol(List<Rol> r,int totalRegistros)
        {

            this.roles = r;
                        
            this.totalRegistros = totalRegistros;
        }

        public PaginaRol()
        {

        }
    }
        
}