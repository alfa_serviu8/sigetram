﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTram.Models.Entidades
{
    public class Rol
    {
        public int Id { set; get; }
        public string Nombre_Rol { set; get; }
        public int Id_Sistema { set;get; }
        public int Id_Servicio { set; get; }
        public bool Habilitado { set; get; }
        
        public Rol(SqlDataReader dr)
        {
            this.Id = Convert.ToInt32(dr["Id_Rol"]);
            this.Id_Servicio = new Servicio().ValorId();
            this.Id_Sistema = new Sistema().ValorId();
            this.Nombre_Rol = dr["Nombre_Rol"].ToString();
      
           
        }

        public Rol()
        {


        }

    }

}