﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTram.Models.Entidades
{
    public class PaginaRolUsuario
    {
        public List<RolUsuario> rolesUsuario { set; get; }
        public int totalRegistros { set; get; }

        public PaginaRolUsuario(List<RolUsuario> ru,int totalRegistros)
        {

            this.rolesUsuario = ru;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}