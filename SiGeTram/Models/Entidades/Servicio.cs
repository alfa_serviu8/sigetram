﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTram.Models.Entidades
{
    public class Servicio
    {
        int Id { set; get; }
        string Nombre { set; get; }
        

        public Servicio()
        {
            this.Id = 24;

            this.Nombre = "SERVIU Región del Biobío";
      
           
        }

        public int ValorId()
        {

            return Id;

        }

        public string ValorNombre()
        {

            return Nombre;

        }

    }

}