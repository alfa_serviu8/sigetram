﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTram.Models.Entidades
{
    public class Sistema
    {
        int Id { set; get; }
        string Nombre { set; get; }
        

        public Sistema()
        {
            this.Id = 22;

            this.Nombre = "SiGeTram";
      
           
        }

        public int ValorId()
        {

            return Id;

        }

        public string ValorNombre()
        {

            return Nombre;

        }

    }

}