﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;


namespace SiGeTram.Models.Entidades
{
    public class RolUsuarioBuscado
    {
        public RolUsuario rolUsuario { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public RolUsuarioBuscado(RolUsuario ru,PaginaImp p)
        {

            this.rolUsuario = ru;
            this.pagina = p;
                           
        }

        
        public RolUsuarioBuscado()
        {

        }

       


    }

}