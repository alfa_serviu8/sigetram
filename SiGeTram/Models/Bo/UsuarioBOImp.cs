﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTram.Models.Entidades;
using SiGeTram.Models.Dao;

namespace SiGeTram.Models.Bo
{
    public class UsuarioBOImp:UsuarioBO
    {

        UsuarioDao usuario_DAO = new UsuarioDaoImp();
                      
        List<Usuario> listado_Usuario = new List<Usuario>();
                 

        public Usuario PorLogin(string login)
        {
            listado_Usuario = usuario_DAO.PorLogin(login);
                                   
        
            Usuario usuario_Logeado  = new Usuario();

            if (listado_Usuario.Count > 0)
            {
                usuario_Logeado = listado_Usuario[0];

               
               
            }

            return usuario_Logeado;
           
        }
               


    }
    }
