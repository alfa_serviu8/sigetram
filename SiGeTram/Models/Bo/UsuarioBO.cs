﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTram.Models.Entidades;

namespace SiGeTram.Models.Bo
{
    public interface UsuarioBO
    {
        Usuario PorLogin(string login);

    }
}
