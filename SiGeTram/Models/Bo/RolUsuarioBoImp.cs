﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTram.Models.Entidades;
using SiGeTram.Models.Dao;

namespace SiGeTram.Models.Bo
{
    public class RolUsuarioBoImp:RolUsuarioBo
    {

        RolUsuarioDao ruDao= new RolUsuarioDaoImp();
                      
                     

        public PaginaRolUsuario Listado(RolUsuarioBuscado rub)
        {
            

            return ruDao.Listado(rub);
           
        }
               
        public void Agregar(RolUsuario ru)
        {
            
            ruDao.Agregar(ru);
                       

        }

        public void Eliminar(int id)
        {

            ruDao.Eliminar(id);



        }

    }
    }
