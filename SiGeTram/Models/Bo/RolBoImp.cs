﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTram.Models.Entidades;
using SiGeTram.Models.Dao;

namespace SiGeTram.Models.Bo
{
    public class RolBoImp:RolBo
    {

        RolDao rDao= new RolDaoImp();
                      
                     

        public PaginaRol Listado(RolBuscado rb)
        {
            

            return rDao.Listado(rb);
           
        }
               
        public void Agregar(Rol r)
        {
            
            rDao.Agregar(r);

            

        }

        public void Actualizar(Rol r)
        {

            rDao.Actualizar(r);



        }

    }
    }
