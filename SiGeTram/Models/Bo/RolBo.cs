﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTram.Models.Entidades;

namespace SiGeTram.Models.Bo
{
    public interface RolBo
    {
        PaginaRol Listado(RolBuscado rb);

        void Agregar(Rol r);

        void Actualizar(Rol r);


    }
}
