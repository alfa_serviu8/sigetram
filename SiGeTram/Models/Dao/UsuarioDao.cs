﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTram.Models.Entidades;

namespace SiGeTram.Models.Dao
{
    public interface UsuarioDao
    {
        List<Usuario> PorRut(int rut);
        List<Usuario> PorLogin(string login);
            

    }
}
