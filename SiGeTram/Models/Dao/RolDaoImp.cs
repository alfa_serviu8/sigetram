﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTram.Models.Entidades;
using SiGeTramDB;

namespace SiGeTram.Models.Dao
{
    public class RolDaoImp:RolDao
    {

        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();

        public PaginaRol Listado(RolBuscado rb)

        {

            List<Rol> listado = new List<Rol>();

            int totalRegistros = 0;

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Rol_Listado", con);

                    query.CommandType = CommandType.StoredProcedure;


                    query.Parameters.Add(new SqlParameter("@id_sistema", new Sistema().ValorId()));
                    query.Parameters.Add(new SqlParameter("@id_servicio", new Servicio().ValorId()));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", rb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", rb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;

                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            var rol = new Rol(dr);

                            listado.Add(rol);
                        }
                    }

                    totalRegistros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);

                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return new PaginaRol(listado, totalRegistros);
        }



        public void Agregar(Rol rol)
        {


            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Rol_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;


                    query.Parameters.Add(new SqlParameter("@id_sistema", new Sistema().ValorId()));
                    query.Parameters.Add(new SqlParameter("@id_servicio", new Servicio().ValorId()));
                    query.Parameters.Add(new SqlParameter("@Nombre_Rol", rol.Nombre_Rol));
                    query.ExecuteNonQuery();




                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }// fin agregar

        public void Actualizar(Rol rol)
        {


            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Rol_Actualizar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@id_rol", rol.Id));
                    query.Parameters.Add(new SqlParameter("@id_sistema", new Sistema().ValorId()));
                    query.Parameters.Add(new SqlParameter("@id_servicio", new Servicio().ValorId()));
                    query.Parameters.Add(new SqlParameter("@Nombre_Rol", rol.Nombre_Rol));
                    query.ExecuteNonQuery();




                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }// fin agregar



    }
}
