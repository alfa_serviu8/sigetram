﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTram.Models.Entidades;

namespace SiGeTram.Models.Dao
{
    public interface RolUsuarioDao
    {
        PaginaRolUsuario Listado(RolUsuarioBuscado rub);
        void Agregar(RolUsuario rolUsuario);
        void Eliminar(int id_rol_usuario);
        

    }
}
