﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTram.Models.Entidades;
using SiGeTramDB;

namespace SiGeTram.Models.Dao
{
    public class RolUsuarioDaoImp:RolUsuarioDao
    {


        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();

        public PaginaRolUsuario Listado(RolUsuarioBuscado rub)

        {

            List<RolUsuario> listado = new List<RolUsuario>();

            int totalRegistros = 0;

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Rol_Usuario_Listado", con);

                    query.CommandType = CommandType.StoredProcedure;

                  
                    query.Parameters.Add(new SqlParameter("@id_sistema", new Sistema().ValorId()));
                    query.Parameters.Add(new SqlParameter("@id_servicio", new Servicio().ValorId()));
                    query.Parameters.Add(new SqlParameter("@id_rol", rub.rolUsuario.Id_Rol));
                    query.Parameters.Add(new SqlParameter("@rut_usuario", rub.rolUsuario.Rut));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", rub.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", rub.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;

                    using (var dr = query.ExecuteReader())
                    {
                        
                        while (dr.Read())
                        {
                            // Usuario
                            var rol_usuario = new RolUsuario(dr);
                           
                            listado.Add(rol_usuario);
                        }
                    }
                  
                    totalRegistros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);

                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return new PaginaRolUsuario(listado,totalRegistros);
        }

       

        public void  Agregar(RolUsuario rol_Usuario)
        {
           
            
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Rol_Usuario_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;


                    query.Parameters.Add(new SqlParameter("@id_sistema", new Sistema().ValorId()));
                    query.Parameters.Add(new SqlParameter("@id_servicio", new Servicio().ValorId()));
                    query.Parameters.Add(new SqlParameter("@id_Rol", rol_Usuario.Id_Rol));
                    query.Parameters.Add(new SqlParameter("@rut_usuario", rol_Usuario.Rut));
       
                    query.ExecuteNonQuery();

                  
                                                        

                }
            }
            catch (Exception ex)
            {
                throw;
            }
                                   
        }// fin agregar


        public void Eliminar(int id_rol_usuario )
        {
           

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Rol_Usuario_Eliminar", con);

                    query.CommandType = CommandType.StoredProcedure;


                    query.Parameters.Add(new SqlParameter("@id_rol_usuario", id_rol_usuario));
                 
                    query.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                throw;
            }
                    
        }

       
    }
}
