﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTram.Models.Entidades;

namespace SiGeTram.Models.Dao
{
    public interface RolDao
    {

        PaginaRol Listado(RolBuscado rb);

        void Agregar(Rol r);

        void Actualizar(Rol r);


    }
}
