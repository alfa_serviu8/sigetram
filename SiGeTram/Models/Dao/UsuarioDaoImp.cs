﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTram.Models.Entidades;

namespace SiGeTram.Models.Dao
{
    public class UsuarioDaoImp:UsuarioDao
    {
        const int Numero_Sistema = 15;


        List<Usuario> usuarios = new List<Usuario>();

        public List<Usuario> PorRut(int rut)
        {
           

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["comun_sistemas"].ToString()))
                {
                    con.Open();

                    var query = new SqlCommand("usuario_por_rut", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@rut", rut));
               

                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            Usuario usuario = new Usuario
                            {

                                Rut = Convert.ToInt32(dr["Rut"]),
                                Dv = dr["dv"].ToString(),
                                Nombres = dr["Nombres"].ToString(),
                                Paterno = dr["Paterno"].ToString(),
                                Materno = dr["Materno"].ToString(),
                                NombreCompleto = dr["Nombre_Completo"].ToString(),
                                Email_Particular = dr["Email_Particular"].ToString(),
                                Email = dr["Email"].ToString(),
                                Id_Servicio = Convert.ToInt32(dr["Id_Servicio"]),
                                Id_Region = Convert.ToInt32(dr["Id_Region"]),
                                Id_Provincia = Convert.ToInt32(dr["Id_Provincia"]),
                                Nombre_Servicio = (dr["Nombre_Servicio"]).ToString(),
                                Nombre_Largo_Servicio = (dr["Nombre_Largo"]).ToString(),
                                Direccion_Servicio = (dr["Direccion"]).ToString(),
                                Telefono_Servicio = (dr["Telefono"]).ToString(),
                                Id_Dependencia = Convert.ToInt32(dr["Id_Dependencia"]),
                                Id_Genero = Convert.ToInt32(dr["Id_Genero"]),
                                Mosca = dr["Mosca"].ToString()


                            };

                            usuarios.Add(usuario);
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return usuarios;
        }

        public List<Usuario> PorLogin(string login)
        {

        
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["Comun_Sistemas"].ToString()))
                {
                    con.Open();

                    var query = new SqlCommand("usuario_Por_Login", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@login_sistemas", login));

                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            Usuario usuario = new Usuario
                            {

                                Rut = Convert.ToInt32(dr["Rut"]),
                                Dv = dr["dv"].ToString(),
                                Nombres = dr["Nombres"].ToString(),
                                Paterno = dr["Paterno"].ToString(),
                                Materno = dr["Materno"].ToString(),
                                NombreCompleto = dr["Nombre_Completo"].ToString(),
                                Email_Particular = dr["Email_Particular"].ToString(),
                                Email = dr["Email"].ToString(),
                                Id_Servicio = Convert.ToInt32(dr["Id_Servicio"]),
                                Id_Region = Convert.ToInt32(dr["Id_Region"]),
                                Id_Provincia = Convert.ToInt32(dr["Id_Provincia"]),
                                Nombre_Servicio =(dr["Nombre_Servicio"]).ToString(),
                                Nombre_Largo_Servicio = (dr["Nombre_Largo"]).ToString(),
                                Direccion_Servicio = (dr["Direccion"]).ToString(),
                                Telefono_Servicio = (dr["Telefono"]).ToString(),
                                Id_Dependencia= Convert.ToInt32(dr["Id_Dependencia"]),
                                Id_Genero = Convert.ToInt32(dr["Id_Genero"]),
                                Mosca = dr["Mosca"].ToString()


                            };

                            usuarios.Add(usuario);
                            
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return usuarios;
        }
               


    }
    }
