﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTramDB
    {
    public class ConeccionSiGeTram
    {
        public SqlConnection con;
             

        public SqlConnection coneccion() 
        {
            return this.con;
            

        }

        public ConeccionSiGeTram(string conec)
        {
            con= new SqlConnection(ConfigurationManager.ConnectionStrings[conec].ToString());
        }
    }
}