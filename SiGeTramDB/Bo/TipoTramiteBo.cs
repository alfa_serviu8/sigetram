﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public interface TipoTramiteBo
    {
        PaginaTipoTramite Listado(TipoTramiteBuscado ttb);

        int Agregar(TipoTramite tt);

        void AgregarBasico(TipoTramite tt);

        void Actualizar(TipoTramite tt);

        List<Expediente> Respaldar(int idTipoTramite);

    }
}
