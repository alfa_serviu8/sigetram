﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public interface TramiteBo
    {
        PaginaTramite Listado(TramiteBuscado tb);

        int Agregar(Tramite t);

    }
}
