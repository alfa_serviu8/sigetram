﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public class TipoSubsidioBoImp:TipoSubsidioBo
    {
        TipoSubsidioDaoImp dao;

        public TipoSubsidioBoImp()
        {
            this.dao = new TipoSubsidioDaoImp();
        }

       public  PaginaTipoSubsidio Listado(TipoSubsidioBuscado tsb)
        {
            return dao.Listado(tsb);
        }

       public  void Agregar(TipoSubsidio ts) {
            dao.Agregar(ts);
        }

        public void Actualizar(TipoSubsidio ts)
        {
            dao.Actualizar(ts);
        }

        public void Eliminar(int id)
        {
            dao.Eliminar(id);
        }
    }
}
