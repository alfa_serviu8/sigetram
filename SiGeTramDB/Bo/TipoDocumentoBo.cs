﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public interface TipoDocumentoBo
    {
        PaginaTipoDocumento ListadoPaginado(TipoDocumentoBuscado tdbuscado);

        void Agregar(TipoDocumento td);

        void Actualizar(TipoDocumento td);

    }
}
