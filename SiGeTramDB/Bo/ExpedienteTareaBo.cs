﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;


using SiGeProc.Entidades;

namespace SiGeTramDB.Bo
{
    public interface ExpedienteTareaBo
    {
        PaginaExpedienteTarea   Listado(ExpedienteTareaBuscada etb);

        List<Tarea> UltimaTareaVigente(int idExpediente);        
        void Agregar(ExpedienteTarea et);

        void Actualizar(ExpedienteTarea et);

        void Observar(ExpedienteTarea et);

        void Recepcionar(ExpedienteTarea et);

        void Modificar(ExpedienteTarea et);

    }
}
