﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public class EstadoExpedienteBoImp : EstadoExpedienteBo
    {
        EstadoExpedienteDaoImp dao;

        public EstadoExpedienteBoImp()
        {
            this.dao = new EstadoExpedienteDaoImp();
        }

        public PaginaEstadoExpediente Listado(EstadoExpedienteBuscado eeb)
        {
            return dao.Listado(eeb);
        }

       
    }
}
