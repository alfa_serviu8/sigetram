﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public class ElementoReferenciaBoImp : ElementoReferenciaBo
    {
        ElementoReferenciaDaoImp erDao;

        public ElementoReferenciaBoImp()
        {
            this.erDao = new ElementoReferenciaDaoImp();
        }

        public List<ElementoReferencia> Listado(int IdElemento)
        {
            List<ElementoReferencia> listado = erDao.Listado(IdElemento);

            ElementoDao eDao = new ElementoDaoImp();

            foreach (ElementoReferencia er in listado)
            {
               
                er.ElementoSiguiente = eDao.PorId(er.Id_Elemento_Siguiente);


            }


            return listado;
        }



        public void Agregar(ElementoReferencia er)
        {
            erDao.Agregar(er);
        }








    }
}
