﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public class ModalidadBoImp : ModalidadBo
    {
        ModalidadDaoImp dao;

        public ModalidadBoImp(ModalidadDaoImp dao)
        {
            this.dao = dao;
        }

       public  List<Modalidad> Listado()
        {
            return dao.Listado();
        }

       public int TotalRegistros() {
            return 1;
        }
    }
}
