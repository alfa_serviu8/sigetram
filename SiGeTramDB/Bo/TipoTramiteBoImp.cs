﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public class TipoTramiteBoImp : TipoTramiteBo
    {
        TipoTramiteDaoImp dao;

        public TipoTramiteBoImp()
        {
            this.dao = new TipoTramiteDaoImp();
        }

        public PaginaTipoTramite Listado(TipoTramiteBuscado ttb)
        {
            return dao.Listado(ttb);
        }


        public int Agregar(TipoTramite tt)
        {
            return dao.Agregar(tt);
        }

        public void AgregarBasico(TipoTramite tt)
        {


            int idTipoTramite = dao.Agregar(tt);

            ElementoBo eBo = new ElementoBoImp();
           
            ElementoReferenciaBo elementoReferenciaBo = new ElementoReferenciaBoImp();

            int idEventoInicio = eBo.Agregar(EventoInicio(idTipoTramite));
            
            int idTareaRevision = eBo.Agregar(UserTask("Revisar Expediente",idTipoTramite));

            int idTareaModificar = eBo.Agregar(UserTask("Modificar Expediente", idTipoTramite));

            int idEventoFin = eBo.Agregar(EventoFin(idTipoTramite));

            elementoReferenciaBo.Agregar(new ElementoReferencia(idEventoInicio, idTareaRevision,-1));
            
            elementoReferenciaBo.Agregar(new ElementoReferencia(idTareaRevision, idTareaModificar,0));

            elementoReferenciaBo.Agregar(new ElementoReferencia(idTareaModificar, idTareaRevision,-1));

            elementoReferenciaBo.Agregar(new ElementoReferencia(idTareaRevision, idEventoFin));
             

        }

        public Elemento EventoInicio(int idTipoTramite)
        {

            return new Elemento("Inicio", "startEvent", idTipoTramite);


        }

        public Elemento EventoFin(int idTipoTramite)
        {

            return new Elemento("Fin", "endEvent", idTipoTramite);


        }

        public Elemento UserTask(string Nombre,int idTipoTramite)
        {

            return new Elemento(Nombre, "userTask", idTipoTramite);


        }

        public void Actualizar(TipoTramite tt)
        {
            dao.Actualizar(tt);
        }

        public List<Expediente> Respaldar(int idTramite)
        {
            ExpedienteBo eBo = new ExpedienteBoImp();

            List<Expediente> listado = eBo.PorTramite(idTramite);

            List<Expediente> listado2= new List<Expediente>();

            foreach(Expediente e in listado)
            {
               
                e.Razon_Social_Empresa = e.Razon_Social_Empresa.Replace(" ", "_");
                e.Razon_Social_Empresa = e.Razon_Social_Empresa.Replace(".", "");
                e.Razon_Social_Empresa = RemueveCaracteresInvalidos(e.Razon_Social_Empresa);
                e.Nombre_Proyecto = e.Nombre_Proyecto.Replace(".", " ");
                e.Nombre_Proyecto = e.Nombre_Proyecto.Replace(" ", "_");
                e.Nombre_Proyecto = e.Nombre_Proyecto.Replace("\"", "_");
                e.Nombre_Proyecto = RemueveCaracteresInvalidos(e.Nombre_Proyecto);

                string letras = FilterWords(e.Nombre_Tipo_Expediente);



                e.Nombre_Corto_Tipo_Tramite = e.Nombre_Corto_Tipo_Tramite + "/" + e.Razon_Social_Empresa + "/" + e.Nombre_Proyecto + "/" + letras + "/";

                //bool exists = System.IO.Directory.Exists(@"c:\pdf2021\" + e.Nombre_Proyecto);

                //if (!exists)
                //    System.IO.Directory.CreateDirectory(@"c:\pdf2021\" + e.Nombre_Proyecto);

                DocumentoBo dBo = new DocumentoBoImp();

                List<Documento> listadoDoc = dBo.PorExpediente(e.Id_Expediente);

                foreach (Documento doc in listadoDoc)
                {
                    string nombreDoc = doc.Nombre_Documento.Replace(" ", "_");
                    nombreDoc = nombreDoc.Replace(",", "_");
                    nombreDoc = nombreDoc.Replace(".", "_");
                    nombreDoc = nombreDoc.Replace("PDF", ".pdf");
                    nombreDoc = nombreDoc.Replace("pdf", ".pdf");
                    nombreDoc = nombreDoc.Replace("kmz", ".kmz");
                    nombreDoc = nombreDoc.Replace("xlsx", ".xlsx");
                    nombreDoc = doc.Id_Documento.ToString() + "_" + nombreDoc;
                    nombreDoc = RemueveCaracteresInvalidos(nombreDoc);

                    dBo.Respaldar(e.Nombre_Corto_Tipo_Tramite, doc.Id_Documento, nombreDoc);

                }



                listado2.Add(e);

               



                }

            return listado2;
        }

        string RemueveCaracteresInvalidos(string str)
        {
            var invalidChars = Path.GetInvalidFileNameChars();

            string invalidCharsRemoved = new string(str.Where(x => !invalidChars.Contains(x)).ToArray());

            return invalidCharsRemoved;

        }
         int CountNonSpaceChars(string value)
        {
            int result = 0;
            foreach (char c in value)
            {
                if (!char.IsWhiteSpace(c))
                {
                    result++;
                }
            }
            return result;
        }

         string FilterWords(string str)
        {
            return  string.Concat(str.Where(c => c >= 'A' && c <= 'Z'));
        }


        
}
}
