﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public interface ExpedienteBo
    {
        PaginaExpediente Listado(ExpedienteBuscado eb);

        int Agregar(Expediente e);

        Expediente PorId(int id);

        void  Actualizar(Expediente e);

        void Eliminar(int id);

        void Enviar(Expediente e);

        List<Expediente> PorTramite(int idTramite);
    }
}
