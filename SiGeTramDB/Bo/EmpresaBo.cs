﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public interface EmpresaBo
    {
        PaginaEmpresa Listado(EmpresaBuscada eb);


        Empresa PorRut(int rut);

        void Agregar(Empresa e);

        void Autoriza(Empresa e);

        void ResetearClave(Empresa e);

        void Actualizar(Empresa e);

    }
}
