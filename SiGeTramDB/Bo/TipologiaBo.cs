﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public interface TipologiaBo
    {
        PaginaTipologia Listado(TipologiaBuscada tb);

        void Agregar(Tipologia t);

        void Actualizar(Tipologia t);
    }
}
