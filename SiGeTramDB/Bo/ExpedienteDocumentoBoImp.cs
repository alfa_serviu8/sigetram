﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;
using SiGeFunDB;
using SiGeFunDB.Dao;
using SiGeFunDB.Bo;
using SiGeFunDB.Entidades;
using SiGeProc.Bo;
using SiGeProc.Entidades;

namespace SiGeTramDB.Bo
{
    public class ExpedienteDocumentoBoImp : ExpedienteDocumentoBo
    {
        ExpedienteDocumentoDaoImp dao;

        public ExpedienteDocumentoBoImp( )
        {
            this.dao = new ExpedienteDocumentoDaoImp();
        }

       public  PaginaExpedienteDocumento Listado(ExpedienteBuscado edb)
        {
           

            return this.dao.Listado(edb);
        }

       



    }
}
