﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public class ElementoBoImp : ElementoBo
    {
        ElementoDaoImp eDao;

        public ElementoBoImp()
        {
            this.eDao = new ElementoDaoImp();
        }

        public Elemento PorId(int idElemento)
        {
            return eDao.PorId(idElemento);
        }

        public int Agregar(Elemento e)
        {
           return  eDao.Agregar(e);
        }

      

        public void Eliminar(int id)
        {
            eDao.Eliminar(id);
        }


    }
}
