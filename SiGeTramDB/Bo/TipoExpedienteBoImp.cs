﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public class TipoExpedienteBoImp : TipoExpedienteBo
    {
        TipoExpedienteDaoImp dao;

        public TipoExpedienteBoImp()
        {
            this.dao = new TipoExpedienteDaoImp();
        }

        public PaginaTipoExpediente Listado(TipoExpedienteBuscado teb)
        {
            return dao.Listado(teb);
        }


        public List<TipoExpediente> PorProceso(int id_proceso) {

            // recupera los expedientes

            PaginaTipoExpediente pte = Listado(new TipoExpedienteBuscado(new TipoExpediente(id_proceso), new SiGeFunDB.PaginaImp()));

            return pte.tiposExpediente;
        
        
        }
        public void Agregar(TipoExpediente te)
        {
            dao.Agregar(te);
        }

        public void Actualizar(TipoExpediente te)
        {
            dao.Actualizar(te);
        }


    }
}
