﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public class ProyectoBoImp : ProyectoBo
    {
        ProyectoDaoImp dao;
        
        public ProyectoBoImp()
        {
            this.dao = new ProyectoDaoImp();
        }

       public  PaginaProyecto Listado(ProyectoBuscado pb)
        {
            return dao.Listado(pb);
        }

       public void Agregar(Proyecto pr) {

            int idProyecto = dao.Agregar(pr);


            // agrego los expedientes 

            List<TipoExpediente> tiposExpediente = new TipoExpedienteBoImp().PorProceso(pr.Id_Proceso);

            foreach(TipoExpediente te in tiposExpediente)
            {
                new ExpedienteBoImp().Agregar(new Expediente(te.Id_Tipo_Expediente, idProyecto, te.Id_Proceso));


            }
          
            

        }

        public void Actualizar(Proyecto pr)
        {
            dao.Actualizar(pr);
        }
    }
}
