﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public interface TipoSubsidioBo
    {
        PaginaTipoSubsidio Listado(TipoSubsidioBuscado tsb);

        void Agregar(TipoSubsidio ts);

        void Actualizar(TipoSubsidio ts);

        void Eliminar(int id);

    }
}
