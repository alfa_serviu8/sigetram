﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public interface DocumentoBo
    {
        PaginaDocumento Listado(DocumentoBuscado dbuscado);
        byte[] Contenido(int idDocumento);

        Documento PorId(int idDocumento);

        List<Documento> PorExpediente(int idExpediente);

        void Respaldar(string camino, int idDocumento,string nombreDocumento);

        int Agregar(Documento d,out string respuestaCorreo);

        void Actualizar(Documento d);

        void Eliminar(int idDocumento);

    }
}
