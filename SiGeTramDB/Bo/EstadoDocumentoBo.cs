﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public interface EstadoDocumentoBo
    {
        PaginaEstadoDocumento Listado(EstadoDocumentoBuscado edb);

        EstadoDocumento PorId(int idEstadoDocumento);



    }
}
