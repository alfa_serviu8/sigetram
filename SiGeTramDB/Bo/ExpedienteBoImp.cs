﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;
using SiGeFunDB;
using SiGeFunDB.Dao;
using SiGeFunDB.Bo;
using SiGeFunDB.Entidades;
using SiGeProc.Bo;
using SiGeProc.Entidades;

namespace SiGeTramDB.Bo
{
    public class ExpedienteBoImp : ExpedienteBo
    {
        ExpedienteDaoImp dao;

        public ExpedienteBoImp( )
        {
            this.dao = new ExpedienteDaoImp();
        }

       public  PaginaExpediente Listado(ExpedienteBuscado eb)
        {
            ExpedienteTareaBo etBo = new ExpedienteTareaBoImp();

            PaginaExpediente pe = dao.Listado(eb);

            
            if (pe.expedientes.Count > 0)
            {
               foreach(Expediente e in pe.expedientes)
                {
                    List<Tarea> lt = etBo.UltimaTareaVigente(e.Id_Expediente);
                   
                    if (lt.Count > 0) {
                        e.TareaVigente = lt[0];
                      }

                }

            }

            return pe;
        }

       public int Agregar(Expediente e) {
            return dao.Agregar(e);
        }

        public void Actualizar(Expediente e)
        {
             dao.Actualizar(e);
        }

        public void  Eliminar(int id)
        {
            dao.Eliminar(id);
        }

       

       public void Enviar(Expediente e)
        {


            ProcesoBo pBo =new  ProcesoBoImp();

           int idInstanciaProceso = 0;
         
            List<int> listadoIdTareas =  pBo.IniciarProceso(e.Id_Proceso,out idInstanciaProceso);

            e.Id_Instancia_Proceso = idInstanciaProceso;


            foreach (int id in listadoIdTareas)
            {
                ExpedienteTareaBo etBo = new ExpedienteTareaBoImp();

                etBo.Agregar(new ExpedienteTarea(e, id));
            }

            // aqui habria que obtener la instancia de proceso
           
            Actualizar(e);

               

          

       // enviar un correoal revisor

            FuncionarioDao fDao = new FuncionarioDaoImp();

            List<Funcionario> f = fDao.PorRut(e.Rut_Revisor);
          
            string respuestaCorreo = "";

            EmpresaBo eBo = new EmpresaBoImp(new EmpresaDaoImp());

            Empresa emp = eBo.PorRut(e.Rut_Empresa);


            new CorreoBoImp().Enviar(emp.CorreoEnvioExpediente(f[0], e), out respuestaCorreo);

        }

        public Expediente PorId(int id)
        {
            Expediente e = new Expediente(id);
            ExpedienteBuscado eb = new ExpedienteBuscado(e, new PaginaImp());
            return Listado(eb).expedientes[0];

        }

        public List<Expediente> PorTramite(int idTipoTramite)
        {
            Expediente e = new Expediente(0,idTipoTramite);
            ExpedienteBuscado eb = new ExpedienteBuscado(e, new PaginaImp(1,10000));
            return Listado(eb).expedientes;

        }



    }
}
