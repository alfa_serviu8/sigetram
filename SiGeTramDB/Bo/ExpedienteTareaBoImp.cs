﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;
using SiGeTramDB.Dao;
using SiGeFunDB;
using SiGeFunDB.Dao;
using SiGeFunDB.Bo;
using SiGeFunDB.Entidades;
using SiGeProc.Entidades;
using SiGeProc.Bo;

namespace SiGeTramDB.Bo
{
    public class ExpedienteTareaBoImp:ExpedienteTareaBo
    {
        public ExpedienteTareaDao  etDao;

        public ExpedienteTareaBoImp()
        {
            this.etDao = new ExpedienteTareaDaoImp();
                

        }


        public List<Tarea> UltimaTareaVigente(int idExpediente) {

            ExpedienteTareaBuscada etb = new ExpedienteTareaBuscada(new ExpedienteTarea(0,idExpediente,0, 0, "",1), new PaginaImp());

            List<ExpedienteTarea> let = Listado(etb).expedienteTareas;

       
            List<Tarea> lt = new List<Tarea>();

            if (let.Count > 0)
            {
                Tarea t = new Tarea();

                foreach (ExpedienteTarea et in let)
                {
                    t = new TareaBoImp().PorId(et.Id_Tarea);

                    if (t.EsVigente())
                    {
                        lt.Add(t);

                    }
                }


            }
           
            return lt;

        
        }

        public PaginaExpedienteTarea Listado(ExpedienteTareaBuscada etb)
        {

            
            return etDao.Listado(etb);

        }

        
        public void Agregar(ExpedienteTarea et)
        {

            etDao.Agregar(et);


        }


        public void Actualizar(ExpedienteTarea et)
        {


            etDao.Actualizar(et);

        }

       public void Observar(ExpedienteTarea expedienteObservado)
        {

            ProcesoBo pBo = new ProcesoBoImp();

            // aqui hay que pasar latarea actual

            Tarea t = new TareaBoImp().PorId(expedienteObservado.Id_Tarea);

            t.Condicion = 0; // observado

            List<int> listadoIdTareas = pBo.TareasSiguientes(t, 0);


            CambiarAEstadoObservado(expedienteObservado);

            ExpedienteBo eBo = new ExpedienteBoImp();

            Expediente e = eBo.PorId(expedienteObservado.Id_Expediente);


       

            foreach (int id in listadoIdTareas)
            {
                            
                ExpedienteTareaBo etBo = new ExpedienteTareaBoImp();

                etBo.Agregar(new ExpedienteTarea(e, id));


            }
           

       
        
            EnviarCorreoDeObservacion(expedienteObservado);




        }


        public void Recepcionar(ExpedienteTarea expedienteRecepcionado)
        {

            ProcesoBo pBo = new ProcesoBoImp();


            Tarea t = new TareaBoImp().PorId(expedienteRecepcionado.Id_Tarea);

            t.Condicion = 1; // recepcionado

            List<int> listadoIdTareas = pBo.TareasSiguientes(t, 1);


            CambiarAEstadoRecepcionado(expedienteRecepcionado);

            ExpedienteBo eBo = new ExpedienteBoImp();

            Expediente e = eBo.PorId(expedienteRecepcionado.Id_Expediente);


            foreach (int id in listadoIdTareas)
            {


                ExpedienteTareaBo etBo = new ExpedienteTareaBoImp();

                etBo.Agregar(new ExpedienteTarea(e, id));


            }



            EnviarCorreoDeRecepcion(expedienteRecepcionado);

        }

        public void Modificar(ExpedienteTarea expedienteModificado)
        {
           

            ProcesoBo pBo = new ProcesoBoImp();


            Tarea t = new TareaBoImp().PorId(expedienteModificado.Id_Tarea);

            t.Condicion = -1; // recepcionado

            List<int> listadoIdTareas = pBo.TareasSiguientes(t, -1);


            CambiarAEstadoEnviado(expedienteModificado);


            ExpedienteBo eBo = new ExpedienteBoImp();


            Expediente e = eBo.PorId(expedienteModificado.Id_Expediente);
          
        


            foreach (int id in listadoIdTareas)
            {


                ExpedienteTareaBo etBo = new ExpedienteTareaBoImp();

                etBo.Agregar(new ExpedienteTarea(e, id));


            }


         

            

            EnviarCorreoDeModificacion(expedienteModificado);

        }

        private void EnviarCorreoDeObservacion(ExpedienteTarea expedienteObservado)
        {
            CorreoBo cbo = new CorreoBoImp();
            Correo c = new Correo();

            string resultado = "ok";

            string xBody = "";

            ExpedienteBo eBo = new ExpedienteBoImp();

            Expediente e = eBo.PorId(expedienteObservado.Id_Expediente);

            EmpresaBo empBo = new EmpresaBoImp(new EmpresaDaoImp());

            Empresa emp = empBo.PorRut(e.Rut_Empresa);

      
            c.De = "SiGeTram <SiGeTram@minvu.cl>";
            c.Hacia = emp.Email_Particular;
            c.Asunto = "Revisión de Expediente";
                    
            xBody = xBody + "<p>Informamos a Usted que el expediente " + e.Descripcion + " del proyecto " + e.Nombre_Proyecto + " fue observado.</p>";
            xBody = xBody + "<p>Atte,</p>";
            xBody = xBody + "<p>SERVIU Regi&oacute;n del Biob&iacute;o.</p>";

            c.Body = xBody;

            cbo.Enviar(c, out resultado);


        }

        private void EnviarCorreoDeRecepcion(ExpedienteTarea expedienteObservado)
        {
            CorreoBo cbo = new CorreoBoImp();
            Correo c = new Correo();

            string resultado = "ok";

            string xBody = "";

            ExpedienteBo eBo = new ExpedienteBoImp();

            Expediente e = eBo.PorId(expedienteObservado.Id_Expediente);

            EmpresaBo empBo = new EmpresaBoImp(new EmpresaDaoImp());

            Empresa emp = empBo.PorRut(e.Rut_Empresa);


            c.De = "SiGeTram <SiGeTram@minvu.cl>";
            c.Hacia = emp.Email_Particular;
            c.Asunto = "Revisión de Expediente";

            xBody = xBody + "<p>Informamos a Usted que el expediente " + e.Descripcion + " del proyecto " + e.Nombre_Proyecto + " fue recepcionado.</p>";
            xBody = xBody + "<p>Atte,</p>";
            xBody = xBody + "<p>SERVIU Regi&oacute;n del Biob&iacute;o.</p>";

            c.Body = xBody;

            cbo.Enviar(c, out resultado);


        }


        private void EnviarCorreoDeModificacion(ExpedienteTarea expedienteModificado)
        {
                       
            ExpedienteBo eBo = new ExpedienteBoImp();

            Expediente e = eBo.PorId(expedienteModificado.Id_Expediente);

            EmpresaBo empBo = new EmpresaBoImp(new EmpresaDaoImp());

            Empresa emp = empBo.PorRut(e.Rut_Empresa);


           

            FuncionarioDao fDao = new FuncionarioDaoImp();

            List<Funcionario> f = fDao.PorRut(e.Rut_Revisor);
           
            string respuestaCorreo = "";

            new CorreoBoImp().Enviar(emp.CorreoEnvioExpediente(f[0], e), out respuestaCorreo);

        }


        private void CompletarTareaVigente(ExpedienteTarea et)
        {
            TareaBo tBo = new TareaBoImp();

            Tarea t = null;
      
            //t.Vigente = 0;

            //t.Rut_Asignado = et.Rut_Asignado;

            tBo.Actualizar(t);
        }

        private void CambiarAEstadoEnviado(ExpedienteTarea expedienteEnviado)
        {
            const int ESTADO_ENVIADO = 2;

            ExpedienteBo eBo = new ExpedienteBoImp();

            Expediente e = eBo.PorId(expedienteEnviado.Id_Expediente);

            e.Id_Estado_Expediente = ESTADO_ENVIADO;

            eBo.Actualizar(e);

            expedienteEnviado.Id_Estado_Expediente = ESTADO_ENVIADO;

            Actualizar(expedienteEnviado);

        }

        private void CambiarAEstadoRecepcionado(ExpedienteTarea expedienteRecepcionado)
        {
            const int ESTADO_RECEPCIONADO = 4;

            ExpedienteBo eBo = new ExpedienteBoImp();

            Expediente e = eBo.PorId(expedienteRecepcionado.Id_Expediente);

            e.Id_Estado_Expediente = ESTADO_RECEPCIONADO;

            eBo.Actualizar(e);

            expedienteRecepcionado.Id_Estado_Expediente = ESTADO_RECEPCIONADO;

            Actualizar(expedienteRecepcionado);

        }

        private void CambiarAEstadoObservado(ExpedienteTarea expedienteObservado)
        {
            const int ESTADO_OBSERVADO = 3;

            ExpedienteBo eBo = new ExpedienteBoImp();

            Expediente e = eBo.PorId(expedienteObservado.Id_Expediente);

            e.Id_Estado_Expediente = ESTADO_OBSERVADO;

            eBo.Actualizar(e);

            expedienteObservado.Id_Estado_Expediente = ESTADO_OBSERVADO;

            Actualizar(expedienteObservado);

        }

        private void AgregarTareaDeRevision(int idExpediente)
        {
            const int ESTADO_ENVIADO = 2;

            ExpedienteBo eBo = new ExpedienteBoImp();

            Expediente e = eBo.PorId(idExpediente);

            TareaBo tBo = new TareaBoImp();
           
            Tarea tRevisar = null;
           
            //if (e.Id_Tipo_Tramite == 2)
            //{
            //    tRevisar.Id_Tipo_Tarea = 3;
            //}
            //if (e.Id_Tipo_Tramite == 3)
            //{
            //    tRevisar.Id_Tipo_Tarea = 5;
            //}

            //if (e.Id_Tipo_Tramite == 4)
            //{
            //    tRevisar.Id_Tipo_Tarea = 7;
            //}

            //if (e.Id_Tipo_Tramite == 5)
            //{
            //    tRevisar.Id_Tipo_Tarea = 9;
            //}

            //if (e.Id_Tipo_Tramite == 6)
            //{
            //    tRevisar.Id_Tipo_Tarea = 22;
            //}

            //if (e.Id_Tipo_Tramite == 7)
            //{
            //    tRevisar.Id_Tipo_Tarea = 26;
            //}




            int idNuevaTarea = tBo.Agregar(tRevisar);
           
            ExpedienteTarea etNueva = new ExpedienteTarea(0, idExpediente, idNuevaTarea, ESTADO_ENVIADO, "", 1);

            etDao.Agregar(etNueva);

        }

        private void AgregarTareaDeModificacion(int idExpediente)
        {
            const int ESTADO_OBSERVADO = 3;

            ExpedienteBo eBo = new ExpedienteBoImp();

            Expediente e = eBo.PorId(idExpediente);

            TareaBo tBo = new TareaBoImp();

            Tarea tModificar = null;

            //if (e.Id_Tipo_Tramite == 2)
            //{
            //    tModificar.Id_Tipo_Tarea = 4;
            //}
            //if (e.Id_Tipo_Tramite == 3)
            //{
            //    tModificar.Id_Tipo_Tarea = 6;
            //}
            //if (e.Id_Tipo_Tramite == 4)
            //{
            //    tModificar.Id_Tipo_Tarea = 8;
            //}

            //if (e.Id_Tipo_Tramite == 5)
            //{
            //    tModificar.Id_Tipo_Tarea = 10;
            //}

            //if (e.Id_Tipo_Tramite == 6)
            //{
            //    tModificar.Id_Tipo_Tarea = 23;
            //}

            //if (e.Id_Tipo_Tramite == 7)
            //{
            //    tModificar.Id_Tipo_Tarea = 27;
            //}

            int idNuevaTarea = tBo.Agregar(tModificar);

            ExpedienteTarea etNueva = new ExpedienteTarea(0, idExpediente, idNuevaTarea, ESTADO_OBSERVADO, "", 1);

            etDao.Agregar(etNueva);

        }


        //public void EnviarCorreo(int para, string casilla, string asunto, string cuerpo)
        //{

        //    tDao.EnviarCorreo(para, casilla, asunto, cuerpo);

        //}


    }
}
