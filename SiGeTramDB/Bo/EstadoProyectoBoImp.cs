﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public class EstadoProyectoBoImp : EstadoProyectoBo
    {
        EstadoProyectoDaoImp dao;

        public EstadoProyectoBoImp()
        {
            this.dao = new EstadoProyectoDaoImp();
        }

        public PaginaEstadoProyecto Listado(EstadoProyectoBuscado epb)
        {
            return dao.Listado(epb);
        }

        public int TotalRegistros()
        {
            return 1;
        }
    }
}
