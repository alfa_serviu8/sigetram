﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public class TramiteBoImp : TramiteBo
    {
        TramiteDaoImp dao;

        public TramiteBoImp()
        {
            this.dao = new TramiteDaoImp();
        }

        public PaginaTramite Listado(TramiteBuscado tb)
        {
            return dao.Listado(tb);
        }

        public int Agregar(Tramite t)
        {
            return dao.Agregar(t);
        }

    }
}
