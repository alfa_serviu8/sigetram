﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;
using SiGeTramDB.Dao;
using SiGeFunDB;

namespace SiGeTramDB.Bo
{
    public class TipoDocumentoBoImp: TipoDocumentoBo
    {
        TipoDocumentoDao tdDo;

       
       public TipoDocumentoBoImp(TipoDocumentoDao tdDao)


        {
            
            
            this.tdDo = tdDao;

        }
        public PaginaTipoDocumento ListadoPaginado(TipoDocumentoBuscado tdBuscado)
        {
           
            return tdDo.ListadoPaginado(tdBuscado);

        }

        public void Agregar(TipoDocumento td)
        {
           tdDo.Agregar(td);

        }

        public void Actualizar(TipoDocumento td)
        {
             tdDo.Actualizar(td);

        }

    }
    
}
