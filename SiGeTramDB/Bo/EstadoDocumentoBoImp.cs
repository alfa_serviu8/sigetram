﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;
using SiGeTramDB.Dao;

namespace SiGeTramDB.Bo
{
    public class EstadoDocumentoBoImp:EstadoDocumentoBo
    {
        public EstadoDocumentoDao  edDao;

        public EstadoDocumentoBoImp(EstadoDocumentoDao edDao)
        {
            this.edDao = edDao;
                

        }
        public PaginaEstadoDocumento Listado(EstadoDocumentoBuscado edb)
        {
            // DEBO AGREGAR EL DOCUMENTO ASOCIADO A LA TAREA

            return edDao.Listado(edb);
        }

        public EstadoDocumento PorId(int id)
        {
            return edDao.Listado(new EstadoDocumentoBuscado(new EstadoDocumento(id, ""))).estadoDocumentos[0];

        }

    }
    }
