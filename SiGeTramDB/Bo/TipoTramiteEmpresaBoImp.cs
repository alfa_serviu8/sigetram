﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public class TipoTramiteEmpresaBoImp : TipoTramiteEmpresaBo
    {
        TipoTramiteEmpresaDaoImp dao;

        public TipoTramiteEmpresaBoImp()
        {
            this.dao = new TipoTramiteEmpresaDaoImp();
        }

        public PaginaTipoTramiteEmpresa Listado(TipoTramiteEmpresaBuscado tteb)
        {
            return dao.Listado(tteb);
        }

        public bool ContieneEmpresa(int idTipoTramite,int rutEmpresa)
        {
            bool empresaEnTipoTramite = false;
            
            PaginaTipoTramiteEmpresa ptte = Listado(new TipoTramiteEmpresaBuscado(new TipoTramiteEmpresa(idTipoTramite, rutEmpresa)));

           return empresaEnTipoTramite = ptte.tiposTramiteEmpresa.Count > 0;

           
        }

        public void Agregar(TipoTramiteEmpresa tte)
        {
            dao.Agregar(tte);
        }



    }
}
