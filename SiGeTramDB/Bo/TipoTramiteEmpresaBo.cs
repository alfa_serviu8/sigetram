﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public interface TipoTramiteEmpresaBo
    {
        PaginaTipoTramiteEmpresa Listado(TipoTramiteEmpresaBuscado tteb);

        bool ContieneEmpresa(int idTramite, int rutEmpresa);
        void Agregar(TipoTramiteEmpresa tte);
    }
}
