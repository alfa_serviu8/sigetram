﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;
using SiGeTramDB.Dao;
using SiGeFunDB.Bo;
using SiGeFunDB.Dao;
using SiGeFunDB.Entidades;
using SiGeFunDB;




namespace SiGeTramDB.Bo
{
    public class EmpresaBoImp:EmpresaBo
    {
        public EmpresaDao  eDao;
        //public TareaDao tDao;
        

        public EmpresaBoImp(EmpresaDao eDao)
        {
            this.eDao = eDao;
            //this.tDao = tDao;    

        }
        public PaginaEmpresa Listado(EmpresaBuscada eBuscada)
        {
            // DEBO AGREGAR EL DOCUMENTO ASOCIADO A LA TAREA

            return eDao.Listado(eBuscada);
        }

       public Empresa PorRut(int rut)
        {


            Empresa e = new Empresa(rut);

            EmpresaBuscada eb = new EmpresaBuscada(e, new PaginaImp());

            PaginaEmpresa pe = new EmpresaBoImp(new EmpresaDaoImp()).Listado(eb);

            return pe.empresas[0];

        }
        public void Agregar(Empresa e)
        {
            
            eDao.Agregar(e);


        }

        public void Autoriza(Empresa e)
        {
            // agregar correo de aviso a la empresa
            //

            if (true)
            {

                Random rdn = new Random();
                string caracteres = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890%$#@";
                int longitud = caracteres.Length;
                char letra;
                int longitudContrasenia = 10;
                string contraseniaAleatoria = string.Empty;
                for (int i = 0; i < longitudContrasenia; i++)
                {
                    letra = caracteres[rdn.Next(longitud)];
                    contraseniaAleatoria += letra.ToString();
                }

                e.Pass_Sistemas = contraseniaAleatoria;


                CorreoBo cbo = new CorreoBoImp();
                Correo c = new Correo();

                string resultado = "ok";

                string xBody = "";

                //c.De = "VentanillaVirtual@minvu.cl";
                c.De = "Ventanilla Virtual <VentanillaVirtual@minvu.cl>";
                c.Hacia = e.Email_Particular;
                c.Asunto = "Solicitud de Registro";
                //c.Body = "Usted ha sido registrado y su contraseña de ingreso es " + e.Pass_Sistemas;

                xBody = "<p>Bienvenid@:</p>";
                xBody = xBody + "<p>Le informamos que Su solicitud de registro ha sido aceptada, quedando habilitad@ para subir documentaci&oacute;n en nuestra plataforma virtual.</p>";
                xBody = xBody + "<p>Para <a href = 'http://tramites.serviubiobio.cl/SigetramEmp/#!/login' target = '_blank' rel = 'noopener' title = 'Ventanilla Virtual'>ingresar</a>, debe colocar su Rut y la siguiente contrase&ntilde;a de acceso (<strong><span style = 'color: #ff0000;'>" + e.Pass_Sistemas + "</span></strong>), la cual le recomendamos cambiar a la brevedad posible.</p>";
                xBody = xBody + "<p>Atte,</p>";
                xBody = xBody + "<p>SERVIU Regi&oacute;n del Biob&iacute;o.</p>";

                c.Body = xBody;

                cbo.Enviar(c, out resultado);

                            }


            eDao.Agregar(e);


        }

        public void ResetearClave(Empresa e)
        {
            e.EstablecerClaveGenerica();
            
            Actualizar(e);


        }

        public void Actualizar(Empresa e)
        {

                    

            eDao.Actualizar(e);
          

        }

       




    }
    }
