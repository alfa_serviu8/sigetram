﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public class TipologiaBoImp : TipologiaBo
    {
        TipologiaDaoImp dao;

        public TipologiaBoImp()
        {
            this.dao = new TipologiaDaoImp();
        }

       public  PaginaTipologia Listado(TipologiaBuscada tb)
        {
            return dao.Listado(tb);
        }

       public void Agregar(Tipologia t) {
            dao.Agregar(t);
        }

        public void Actualizar(Tipologia t)
        {
            dao.Actualizar(t);
        }
    }
}
