﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;
using SiGeTramDB.Dao;
using SiGeTramDB.Bo;
using SiGeFunDB.Bo;
using SiGeFunDB.Dao;
using SiGeFunDB.Entidades;
using System.IO;

namespace SiGeTramDB.Bo
{
    public class DocumentoBoImp:DocumentoBo
    {
        DocumentoDao dDo;

       
       public  DocumentoBoImp()
        {
            this.dDo = new DocumentoDaoImp();

        }
        public PaginaDocumento Listado(DocumentoBuscado dBuscado)
        {

           

            PaginaDocumento pd = dDo.Listado(dBuscado); 
            
          

            return pd;

        }


        public Documento PorId(int idDocumento)
        {


            Documento doc= dDo.PorId(idDocumento);


            //doc.Contenido = Contenido(idDocumento);

            return doc;

          
        }

        public byte[] Contenido(int idDocumento)
        {
           return dDo.Contenido(idDocumento);

        }

        public int Agregar(Documento d,out string respuestaCorreo)
        {
                                 
            int idDocumento = this.dDo.Agregar(d);
           
            respuestaCorreo = "ok";

           // int rolOficialPartes = 17;
           // int idServicio = 24;
           // int idSistema = 7;

           // RolUsuarioBo ruBo = new RolUsuarioBoImp();

           // RolUsuarioBuscado rub = new RolUsuarioBuscado(new RolUsuario(0, idServicio, idSistema, rolOficialPartes, 0), new SiGeFunDB.PaginaImp());

           // PaginaRolUsuario pagRU = ruBo.Listado(rub);

           // FuncionarioDao fDao = new FuncionarioDaoImp();

           // EmpresaBuscada eb = new EmpresaBuscada(new Empresa(d.Rut_Ingresado_Por, "", "", "", "","","",1,0,0), new SiGeFunDB.PaginaImp());

           // PaginaEmpresa pe = new EmpresaBoImp(new EmpresaDaoImp()).Listado(eb);





           //respuestaCorreo = "OK";


           // foreach (RolUsuario ru in pagRU.rolesUsuario)
           // {
           //     List<Funcionario> f = fDao.PorRut(ru.Rut);

               
           //     new CorreoBoImp().Enviar(pe.empresas[0].CorreoIngresoDocumento(f[0], d), out respuestaCorreo);

           //     f.Clear();

              
           // }

            // crea la nueva tarea

            //TareaBo tBo = new TareaBoImp(new TareaDaoImp());
            //tBo.Agregar(new Tarea(1, idDocumento, 1, 1));


            return idDocumento;

           


            // aqui debo enviar un correo y generar una nueva tarea de recepcion de documentos

        }

        public void Actualizar(Documento d)
        {
             dDo.Actualizar(d);

        }

        public void Eliminar(int idDocumento)
        {
           

            dDo.Eliminar(idDocumento);

        }

        public List<Documento> PorExpediente(int idExpediente)
        {
            PaginaDocumento pd = Listado(new DocumentoBuscado(new Documento(idExpediente, 0)));

            return pd.documentos;


        }

        public void Respaldar(string camino, int idDocumento,string nombreDocumento)
        {

            byte[] contenido = Contenido(idDocumento);


            bool exists = System.IO.Directory.Exists(@"c:\pdf2021\" + camino );

            if (!exists)
                System.IO.Directory.CreateDirectory(@"c:\pdf2021\" + camino );


            //bool exists = System.IO.Directory.Exists(@"\\nasbiobio\sigetram\" + camino);

          

            //if (!exists)
            //    System.IO.Directory.CreateDirectory(@"\\nasbiobio\sigetram\" + camino);

            //string fileName = @"\\nasbiobio\sigetram\" + camino + nombreDocumento;

            string fileName = @"c:\pdf2021\" + camino + nombreDocumento;

            //Check if file already exists. If yes, delete it.
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }





            using (FileStream fs = File.OpenWrite(fileName))
            {


                fs.Write(contenido, 0, contenido.Length);
            }


        }


    }
    
}
