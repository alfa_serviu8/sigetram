﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Bo
{
    public interface TipoExpedienteBo
    {
        PaginaTipoExpediente Listado(TipoExpedienteBuscado teb);

        List<TipoExpediente> PorProceso(int idProceso);

        void Agregar(TipoExpediente te);

        void Actualizar(TipoExpediente te);

     
    }
}
