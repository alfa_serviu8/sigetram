﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class TipoSubsidioDaoImp:TipoSubsidioDao
    {
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
             
      
        public PaginaTipoSubsidio Listado(TipoSubsidioBuscado tsb)
        {
            List<TipoSubsidio> tipos = new List<TipoSubsidio>();

            int total_registros = 0;

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Tipo_Subsidio_Listado_Paginado", con);

                    query.CommandType = CommandType.StoredProcedure;
                   
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", tsb.tipoSubsidio.Id_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@Codigo_Tipo_Subsidio", tsb.tipoSubsidio.Codigo_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@Nombre_Tipo_Subsidio", tsb.tipoSubsidio.Nombre_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@Vigente", tsb.tipoSubsidio.Vigente));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", tsb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", tsb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            TipoSubsidio ts = new TipoSubsidio(Convert.ToInt32(dr["Id_Tipo_Subsidio"]),
                                dr["Codigo_Tipo_Subsidio"].ToString(),
                                dr["Nombre_Tipo_Subsidio"].ToString(),
                               Convert.ToInt32(dr["Vigente"]));

                            tipos.Add(ts);
                        }
                    }// fin using reader

                    total_registros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return new PaginaTipoSubsidio(tipos,total_registros);
        }

        public void Agregar(TipoSubsidio ts)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Tipo_Subsidio_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Codigo_Tipo_Subsidio", ts.Codigo_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@Nombre_Tipo_Subsidio", ts.Nombre_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@Vigente", ts.Vigente));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Actualizar(TipoSubsidio ts)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Tipo_Subsidio_Actualizar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", ts.Id_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@Codigo_Tipo_Subsidio", ts.Codigo_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@Nombre_Tipo_Subsidio", ts.Nombre_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@Vigente", ts.Vigente));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Eliminar(int id)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Tipo_Subsidio_Eliminar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", id));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
