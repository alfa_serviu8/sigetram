﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface TipoDocumentoDao
    {
      PaginaTipoDocumento ListadoPaginado(TipoDocumentoBuscado tdbuscado);
   
        void Agregar(TipoDocumento td);

        void Actualizar(TipoDocumento td);

    }
}
