﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface DocumentoDao
    {
        PaginaDocumento Listado(DocumentoBuscado dbuscado);
        byte[] Contenido(int idDocumento);

        Documento PorId(int idDocumento);

        int Agregar(Documento d);

        void Actualizar(Documento d);

        void Eliminar(int idDocumento);
    }
}
