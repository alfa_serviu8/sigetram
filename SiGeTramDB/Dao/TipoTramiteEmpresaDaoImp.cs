﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class TipoTramiteEmpresaDaoImp : TipoTramiteEmpresaDao
    {
       
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();


        public PaginaTipoTramiteEmpresa Listado(TipoTramiteEmpresaBuscado tteb)
        {
            List<TipoTramiteEmpresa> tipos = new List<TipoTramiteEmpresa>();

            int total_registros = 0;

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Tipo_Tramite_Empresa_Listado_Paginado", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@id_tipo_tramite", tteb.tipoTramiteEmpresa.Id_Tipo_Tramite));
                    query.Parameters.Add(new SqlParameter("@rut_empresa", tteb.tipoTramiteEmpresa.Rut_Empresa));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", tteb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", tteb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            TipoTramiteEmpresa tt = new TipoTramiteEmpresa(dr);

                            tipos.Add(tt);
                        }
                    }// fin using reader

                    total_registros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return new PaginaTipoTramiteEmpresa(tipos, total_registros);
        }


        public void Agregar(TipoTramiteEmpresa tte)
        {
            con = new ConeccionSiGeTram("sigetram").coneccion();
          
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Tipo_Tramite_Empresa_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Tramite", tte.Id_Tipo_Tramite));
                    query.Parameters.Add(new SqlParameter("@Rut_Empresa", tte.Rut_Empresa));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        
    }
}
