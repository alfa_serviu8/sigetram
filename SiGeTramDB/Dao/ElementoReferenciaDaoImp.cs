﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class ElementoReferenciaDaoImp:ElementoReferenciaDao
    {
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
             
      
        public List<ElementoReferencia> Listado(int idElemento)
        {
            List<ElementoReferencia> elementos = new List<ElementoReferencia>();

           

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Elemento_Referencia_Listado", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@id_tipo_tramite", idElemento));
                  
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            ElementoReferencia er = new ElementoReferencia(dr);

                            elementos.Add(er);
                        }
                    }// fin using reader

                    
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return elementos;
        }

        public void Agregar(ElementoReferencia er)
        {
            con = new ConeccionSiGeTram("sigetram").coneccion();

            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Elemento_Referencia_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Elemento", er.Id_Elemento));
                    query.Parameters.Add(new SqlParameter("@Id_Elemento_Siguiente", er.Id_Elemento_Siguiente));
                    query.Parameters.Add(new SqlParameter("@Condicion", er.Condicion));
                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
