﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class TipoTramiteProyectoDaoImp : TipoTramiteProyectoDao
    {
       
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
        
      
       
        public void Agregar(TipoTramiteProyecto ttp)
        {
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Tipo_Tramite_Proyecto_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Tramite", ttp.Id_Tipo_Tramite));
                    query.Parameters.Add(new SqlParameter("@Id_Proyecto", ttp.Id_Proyecto));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        
    }
}
