﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class ExpedienteDocumentoDaoImp:ExpedienteDocumentoDao
    {
        SqlConnection con=new ConeccionSiGeTram("sigetram").coneccion();
        


       
       
        public PaginaExpedienteDocumento Listado(ExpedienteBuscado eb)
        {

            List<ExpedienteDocumento> pagina = new List<ExpedienteDocumento>();

            int total_registros = 0;

            con = new ConeccionSiGeTram("sigetram").coneccion();

            try
            {
                using (con)
                {
                    con.Open();

               
                    SqlCommand query = new SqlCommand("Expediente_Documento_Listado_Paginado");

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Expediente", eb.expediente.Id_Expediente));
                    query.Parameters.Add(new SqlParameter("@Id_Proyecto", eb.expediente.Id_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Id_Proceso", eb.expediente.Id_Proceso));
                    query.Parameters.Add(new SqlParameter("@Rut_Revisor", eb.expediente.Rut_Revisor));
                    query.Parameters.Add(new SqlParameter("@Rut_Empresa", eb.expediente.Rut_Empresa));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Expediente", eb.expediente.Id_Tipo_Expediente));
                    query.Parameters.Add(new SqlParameter("@Id_Estado_Expediente", eb.expediente.Id_Estado_Expediente));
                    query.Parameters.Add(new SqlParameter("@Descripcion", eb.expediente.Descripcion));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", eb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", eb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    query.Connection = con;
                    
                    

                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            ExpedienteDocumento expedienteDoc = new ExpedienteDocumento(dr);

                            pagina.Add(expedienteDoc);
                         

                        }

                    }
                    total_registros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);

                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return new PaginaExpedienteDocumento(pagina, total_registros);

        }

        

        
      
    }
}