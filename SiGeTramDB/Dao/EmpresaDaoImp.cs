﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class EmpresaDaoImp : EmpresaDao
    {
        SqlConnection con;


        public EmpresaDaoImp()
        {

            con = new ConeccionSiGeTram("sigetram").coneccion();

        }

        public PaginaEmpresa Listado(EmpresaBuscada eb)
        {
            List<Empresa> empresas = new List<Empresa>();
           
            int totalRegistros = 0;

        
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Empresa_Listado", con);

                    query.CommandType = CommandType.StoredProcedure;

                          query.Parameters.Add(new SqlParameter("@Rut_Empresa", eb.empresa.Rut_Empresa));

                            query.Parameters.Add(new SqlParameter("@Es_Empresa_Patrocinante", eb.empresa.Es_Empresa_Patrocinante));
                            query.Parameters.Add(new SqlParameter("@Nombre_Empresa", eb.empresa.Nombre_Empresa));
                          query.Parameters.Add(new SqlParameter("@pagina_solicitada", eb.pagina.numero));
                          query.Parameters.Add(new SqlParameter("@tamacno_pagina", eb.pagina.tamacno));
                          query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Empresa e = new Empresa(dr);
                                
                        

                            empresas.Add(e);
                        }
                    }

                            totalRegistros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
                    }
                    catch (Exception ex)
                   {
                       throw;
                    }
                    return new PaginaEmpresa(empresas,totalRegistros);
                }

        public void Agregar(Empresa e)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Empresa_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Rut_Empresa", e.Rut_Empresa));
                    query.Parameters.Add(new SqlParameter("@Dv", e.Dv));
                    query.Parameters.Add(new SqlParameter("@Razon_Social", e.Razon_Social + "."));
                    query.Parameters.Add(new SqlParameter("@Nombre_Fantasia", e.Nombre_Fantasia));
                    query.Parameters.Add(new SqlParameter("@Email_Particular", e.Email_Particular));
                    query.Parameters.Add(new SqlParameter("@fono_fijo", e.Fono_Fijo));
                    query.Parameters.Add(new SqlParameter("@celular", e.Celular));
                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Actualizar(Empresa e)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Empresa_Actualizar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Rut_Empresa", e.Rut_Empresa));
                    query.Parameters.Add(new SqlParameter("@Razon_Social", e.Razon_Social + "."));
                    query.Parameters.Add(new SqlParameter("@Nombre_Fantasia", e.Nombre_Fantasia));
                    query.Parameters.Add(new SqlParameter("@Email_Particular", e.Email_Particular));
                    query.Parameters.Add(new SqlParameter("@Es_Empresa_Patrocinante", e.Es_Empresa_Patrocinante));
                    query.Parameters.Add(new SqlParameter("@pass_sistemas", e.Pass_Sistemas));
                    query.Parameters.Add(new SqlParameter("@fono_fijo", e.Fono_Fijo));
                    query.Parameters.Add(new SqlParameter("@celular", e.Celular));
           
                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }




    }
}
 