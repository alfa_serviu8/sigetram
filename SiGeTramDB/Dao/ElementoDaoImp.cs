﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class ElementoDaoImp:ElementoDao
    {
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
             
      
        public Elemento PorId(int idElemento)
        {
            List<Elemento> elementos = new List<Elemento>();

            con = new ConeccionSiGeTram("sigetram").coneccion();

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Elemento_Listado", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@id_elemento", idElemento));
                  
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            Elemento e = new Elemento(dr);

                            elementos.Add(e);
                        }
                    }// fin using reader

                    
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return elementos[0];
        }

        public int Agregar(Elemento e)
        {
            int idElemento = 0;
            con = new ConeccionSiGeTram("sigetram").coneccion();

            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Elemento_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Elemento", e.Id_Tipo_Elemento));
                    query.Parameters.Add(new SqlParameter("@Nombre_Elemento", e.Nombre_Elemento));
                    query.Parameters.Add(new SqlParameter("@Asignado", e.Asignado));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Tramite", e.Id_Tipo_Tramite));
                    query.Parameters.Add(new SqlParameter("@Formulario", e.Formulario));
                    query.Parameters.Add(new SqlParameter("@Id_Elemento", e.Id_Elemento)).Direction = ParameterDirection.Output;
                    query.ExecuteNonQuery();
                    idElemento = Convert.ToInt32(query.Parameters["@Id_Elemento"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return idElemento;
        }

        public void Eliminar(int id)
        {
            con = new ConeccionSiGeTram("sigetram").coneccion();

            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Elemento_Eliminar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Elemento", id));
                   
                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


    }
}
