﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface ProyectoDao
    {
        PaginaProyecto Listado(ProyectoBuscado pb);

        int Agregar(Proyecto tarea);
        void Actualizar(Proyecto tarea);

        //string Banco(int idLlamado);
    }

    
}
