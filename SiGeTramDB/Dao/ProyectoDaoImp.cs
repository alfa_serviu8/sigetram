﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;

using System.Xml;

namespace SiGeTramDB.Dao
{
    public class ProyectoDaoImp : ProyectoDao
    {
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
        
        public PaginaProyecto Listado(ProyectoBuscado pb)
        {
            List<Proyecto> proyectos = new List<Proyecto>();
            int total_registros = 0;
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Proyecto_Listado_Paginado", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Proyecto", pb.proyecto.Id_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Codigo_Proyecto", pb.proyecto.Codigo_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Nombre_Proyecto", pb.proyecto.Nombre_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Rut_Empresa", pb.proyecto.Rut_Empresa));
                    query.Parameters.Add(new SqlParameter("@Nombre_Empresa", pb.proyecto.Nombre_Empresa));
                    //query.Parameters.Add(new SqlParameter("@Rut_Revisor", pb.proyecto.Rut_Revisor));
                    query.Parameters.Add(new SqlParameter("@Id_Estado_Proyecto", pb.proyecto.Id_Estado_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", pb.proyecto.Id_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@Id_Tipologia", pb.proyecto.Id_Tipologia));
                    query.Parameters.Add(new SqlParameter("@Id_Proceso", pb.proyecto.Id_Proceso));
                    query.Parameters.Add(new SqlParameter("@Id_Comuna", pb.proyecto.Id_Comuna));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", pb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", pb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            Proyecto p = new Proyecto(dr);
                            proyectos.Add(p);
                        }
                    }// fin using reader

                    total_registros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return new PaginaProyecto(proyectos,total_registros);
        }

        public int Agregar(Proyecto pr)
        {
            int idProyecto = 0;
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Proyecto_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Codigo_Proyecto", pr.Codigo_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Nombre_Proyecto", pr.Nombre_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Rut_Empresa", pr.Rut_Empresa));
                    //query.Parameters.Add(new SqlParameter("@Rut_Revisor", pr.Rut_Revisor));
                    query.Parameters.Add(new SqlParameter("@Id_Estado_Proyecto", pr.Id_Estado_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Id_Proceso", pr.Id_Proceso));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", pr.Id_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@Id_Tipologia", pr.Id_Tipologia));
                    query.Parameters.Add(new SqlParameter("@Cantidad_Familias", pr.Cantidad_Familias));
                    query.Parameters.Add(new SqlParameter("@Id_Region", pr.Id_Region));
                    query.Parameters.Add(new SqlParameter("@Id_Provincia", pr.Id_Provincia));
                    query.Parameters.Add(new SqlParameter("@Id_Comuna", pr.Id_Comuna));
                    query.Parameters.Add(new SqlParameter("@id_Proyecto", pr.Id_Proyecto)).Direction = ParameterDirection.Output;

                    query.ExecuteNonQuery();


                    idProyecto = Convert.ToInt32(query.Parameters["@Id_Proyecto"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return idProyecto;
        }

        public void Actualizar(Proyecto pr)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Proyecto_Actualizar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Codigo_Proyecto", pr.Codigo_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Nombre_Proyecto", pr.Nombre_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Rut_Empresa", pr.Rut_Empresa));
                    //query.Parameters.Add(new SqlParameter("@Rut_Revisor", pr.Rut_Revisor));
                    query.Parameters.Add(new SqlParameter("@Id_Estado_Proyecto", pr.Id_Estado_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Id_Proceso", pr.Id_Proceso));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", pr.Id_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@Id_Tipologia", pr.Id_Tipologia));
                    query.Parameters.Add(new SqlParameter("@Cantidad_Familias", pr.Cantidad_Familias));
                    query.Parameters.Add(new SqlParameter("@Id_Region", pr.Id_Region));
                    query.Parameters.Add(new SqlParameter("@Id_Provincia", pr.Id_Provincia));
                    query.Parameters.Add(new SqlParameter("@Id_Comuna", pr.Id_Comuna));
                    query.Parameters.Add(new SqlParameter("@id_Proyecto", pr.Id_Proyecto));
                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }





        //public string Banco(int idLlamado) 
        //{

        //    XmlDocument xmlDoc = new XmlDocument();
        //    SNAT_Minvu.SNATService proyectos = new SNAT_Minvu.SNATService();
        //    SNAT_Minvu.EntradaProyReg entrada = new SNAT_Minvu.EntradaProyReg();

        //    entrada.Lla_Id = idLlamado;
        //    entrada.Reg_Id = 8;

        //    xmlDoc.LoadXml(proyectos.ProyectoLlamadoRegion(entrada));
        //    return xmlDoc.OuterXml.ToString();
        //    //Console.WriteLine(xmlDoc.ToString());

        //}


    }


}
