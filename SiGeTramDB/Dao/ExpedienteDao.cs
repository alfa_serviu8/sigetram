﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface ExpedienteDao
    {
        PaginaExpediente Listado(ExpedienteBuscado eb);

        int Agregar(Expediente e);

        void Actualizar(Expediente e);

        void Eliminar(int id);
    }
}
