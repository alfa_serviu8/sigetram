﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface ElementoReferenciaDao
    {
        List<ElementoReferencia> Listado(int idElemento);

        void Agregar(ElementoReferencia er);



    }
}
