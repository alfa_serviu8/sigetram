﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;
using SiGeFunDB.Entidades;
using SiGeFunDB.Bo;


namespace SiGeTramDB.Dao
{
    public class ExpedienteTareaDaoImp : ExpedienteTareaDao
    {
        SqlConnection con;

        public ExpedienteTareaDaoImp()
        {
            this.con = new SqlConnection(ConfigurationManager.ConnectionStrings["sigetram"].ToString());


        }
        public PaginaExpedienteTarea Listado(ExpedienteTareaBuscada etb)
        {
            this.con = new SqlConnection(ConfigurationManager.ConnectionStrings["sigetram"].ToString());

            List<ExpedienteTarea> expediente_tareas = new List<ExpedienteTarea>();

            ExpedienteTarea et = null;

            int totalRegistros = 0;

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("expediente_tarea_listado_paginado", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@id_expediente", etb.expediente_tarea.Id_Expediente));
                    query.Parameters.Add(new SqlParameter("@vigente", etb.expediente_tarea.Vigente));
                    query.Parameters.Add(new SqlParameter("@pagina_Solicitada", etb.pagina.Numero()));
                    query.Parameters.Add(new SqlParameter("@tamacno_Pagina", etb.pagina.Tamacno()));

                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;


                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            et = new ExpedienteTarea(dr);

                            expediente_tareas.Add(et);

                        }



                        totalRegistros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }


            PaginaExpedienteTarea pt = new PaginaExpedienteTarea(expediente_tareas, totalRegistros);

            return pt;
        }

        public void Agregar(ExpedienteTarea et)
        {
            this.con = new SqlConnection(ConfigurationManager.ConnectionStrings["sigetram"].ToString());

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Expediente_Tarea_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Expediente", et.Id_Expediente));

                    query.Parameters.Add(new SqlParameter("@Id_Tarea", et.Id_Tarea));

                    query.Parameters.Add(new SqlParameter("@Id_Estado_Expediente", et.Id_Estado_Expediente));

                    query.Parameters.Add(new SqlParameter("@Observaciones", et.Observaciones));



                    query.ExecuteNonQuery();

                }

            }

            catch (Exception ex)
            {
                throw;
            }

        }

        public void Actualizar(ExpedienteTarea et)
        {
            this.con = new SqlConnection(ConfigurationManager.ConnectionStrings["sigetram"].ToString());

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Expediente_Tarea_Actualizar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Expediente_Tarea", et.Id_Expediente_Tarea));

                    query.Parameters.Add(new SqlParameter("@Id_Estado_Expediente", et.Id_Estado_Expediente));

                    query.Parameters.Add(new SqlParameter("@Observaciones", et.Observaciones));



                    query.ExecuteNonQuery();

                }

            }

            catch (Exception ex)
            {
                throw;
            }

        }

        //public void EnviarCorreo(int para, string casilla,string asunto, string cuerpo)
        //{
        //    if ((para == 1) || (para == 17))
        //    {
        //        //buscar destino (administradores u oficiales de parte
        //        //using (con)
        //        using (var con2 = new SqlConnection(ConfigurationManager.ConnectionStrings["comun_sistemas"].ToString()))
        //        {
        //            con2.Open();

        //            //agrege esto para poder sacar a quiens enviar, 1 Adminsitradores, 17 Ofical de partes, por eso lo llamo antes de agregarlo en el emmpresaboimp
        //            //ok espera un poco

        //            var query = new SqlCommand("Correo_Roles_Por_Sistema", con2);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@id_servicio", 24));
        //            query.Parameters.Add(new SqlParameter("@id_sistema", 7));
        //            query.Parameters.Add(new SqlParameter("@id_rol", para));
        //            query.Parameters.Add(new SqlParameter("@agrupados", 1));


        //            using (var dr = query.ExecuteReader())
        //            {

        //                while (dr.Read())
        //                {
        //                    // Usuario
        //                    casilla = dr[0].ToString();

        //                }

        //            }

        //        }
        //    }
        //    string resultado = "ok";
        //    CorreoBo cbo = new CorreoBoImp();
        //    Correo c = new Correo();

        //    c.De = "Ventanilla Virtual <VentanillaVirtual@minvu.cl>";
        //    c.Hacia = casilla;
        //    c.Asunto = asunto;
        //    c.Body = cuerpo;

        //    cbo.Enviar(c, out resultado);



        //}


    }
    }
