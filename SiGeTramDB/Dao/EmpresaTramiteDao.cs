﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface EmpresaTramiteDao
    {
        PaginaEmpresaTramite Listado(EmpresaTramiteBuscada etb);

        void Agregar(EmpresaTramite et);

        void Eliminar(int id);




    }
}
