﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class EmpresaTramiteDaoImp : EmpresaTramiteDao
    {
        SqlConnection con;


        public EmpresaTramiteDaoImp()
        {

            con = new ConeccionSiGeTram("sigetram").coneccion();

        }

        public PaginaEmpresaTramite Listado(EmpresaTramiteBuscada etb)
        {
            List<EmpresaTramite> listado = new List<EmpresaTramite>();
          
            int totalRegistros = 0;

        
            try
            {
                using (con)
                {
                    con.Open();


                    var query = new SqlCommand("Empresa_Tramite_Listado");

                    query.CommandType = CommandType.StoredProcedure;
                    query.Parameters.Add(new SqlParameter("@Id_Empresa_Tramite", etb.empresa_tramite.Id_Empresa_Tramite));
                    query.Parameters.Add(new SqlParameter("@Rut_Empresa", etb.empresa_tramite.Rut_Empresa));
                    query.Parameters.Add(new SqlParameter("@Id_Tramite", etb.empresa_tramite.Id_Tramite));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada",etb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", etb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                                      
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            EmpresaTramite et = new EmpresaTramite(dr);
                                
                        

                            listado.Add(et);
                        }
                    }

                            totalRegistros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
                    }
                    catch (Exception ex)
                   {
                       throw;
                    }
                    return new PaginaEmpresaTramite(listado,totalRegistros);
                }

        public void Agregar(EmpresaTramite et)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Empresa_Tramite_Agregar");

                    query.CommandType = CommandType.StoredProcedure;
                    query.Parameters.Add(new SqlParameter("@Id_Empresa_Tramite", et.Id_Empresa_Tramite));
                    query.Parameters.Add(new SqlParameter("@Rut_Empresa", et.Rut_Empresa));
                    query.Parameters.Add(new SqlParameter("@Id_Tramite", et.Id_Tramite));


                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Eliminar(int id)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Empresa_Tramite_Eliminar");

                    query.CommandType = CommandType.StoredProcedure;
                    query.Parameters.Add(new SqlParameter("@Id_Empresa_Tramite", id));


                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }




    }
}
 