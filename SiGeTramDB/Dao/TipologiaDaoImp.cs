﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class TipologiaDaoImp : TipologiaDao
    {
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
        
      
        public PaginaTipologia Listado(TipologiaBuscada tb)
        {
            List<Tipologia> tipologias = new List<Tipologia>();
            int total_registros = 0;

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Tipologia_Listado_Paginado", con);

                    query.CommandType = CommandType.StoredProcedure;
                   
                    query.Parameters.Add(new SqlParameter("@Id_Tipologia", tb.tipologia.Id_Tipologia));
                    query.Parameters.Add(new SqlParameter("@Nombre_Tipologia", tb.tipologia.Nombre_Tipologia));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", tb.tipologia.Id_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", tb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", tb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            Tipologia t = new Tipologia(Convert.ToInt32(dr["Id_Tipologia"]),
                               dr["Nombre_Tipologia"].ToString(),
                               Convert.ToInt32(dr["Id_Tipo_Subsidio"]),
                               dr["Nombre_Tipo_Subsidio"].ToString());

                            tipologias.Add(t);
                        }
                    }// fin using reader

                    total_registros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return new PaginaTipologia(tipologias,total_registros);
        }

        public void Agregar(Tipologia t)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Tipologia_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Nombre_Tipologia", t.Nombre_Tipologia));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", t.Id_Tipo_Subsidio));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Actualizar(Tipologia t)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Tipologia_Actualizar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipologia", t.Id_Tipologia));
                    query.Parameters.Add(new SqlParameter("@Nombre_Tipologia", t.Nombre_Tipologia));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", t.Id_Tipo_Subsidio));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //public void Eliminar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Tipologia_Eliminar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Id_Tipologia", this.Id_Tipologia));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
    }
}
