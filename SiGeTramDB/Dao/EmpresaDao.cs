﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface EmpresaDao
    {
        PaginaEmpresa Listado(EmpresaBuscada eb);

        void Agregar(Empresa e);

        void Actualizar(Empresa e);



    }
}
