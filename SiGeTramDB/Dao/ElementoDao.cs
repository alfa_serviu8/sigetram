﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface ElementoDao
    {
        Elemento PorId(int idElemento);

        int Agregar(Elemento e);


        void Eliminar(int idElemento);



    }
}
