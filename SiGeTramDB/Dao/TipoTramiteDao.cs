﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface TipoTramiteDao
    {
        PaginaTipoTramite Listado(TipoTramiteBuscado ttb);

        int Agregar(TipoTramite tt);

        void Actualizar(TipoTramite tt);

       
                      
    }
}
