﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class EstadoProyectoDaoImp : EstadoProyectoDao
    {
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
       
        public PaginaEstadoProyecto Listado(EstadoProyectoBuscado epb)
        {
            List<EstadoProyecto> estados = new List<EstadoProyecto>();
            int total_registros = 0;

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Estado_Proyecto_Listado_Paginado", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Estado_Proyecto", epb.estadoProyecto.Id_Estado_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Nombre_Estado_Proyecto", epb.estadoProyecto.Nombre_Estado_Proyecto));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", epb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", epb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            EstadoProyecto ep = new EstadoProyecto(Convert.ToInt32(dr["Id_Estado_Proyecto"]),
                               dr["Nombre_Estado_Proyecto"].ToString());

                            estados.Add(ep);
                        }
                    }// fin using reader

                    total_registros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return new PaginaEstadoProyecto(estados,total_registros);
        }

      
    }
 }
