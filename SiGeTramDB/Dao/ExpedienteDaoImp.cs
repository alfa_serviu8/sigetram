﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public class ExpedienteDaoImp:ExpedienteDao
    {
        SqlConnection con=new ConeccionSiGeTram("sigetram").coneccion();
        
               
        public PaginaExpediente Listado(ExpedienteBuscado eb)
        {
            List<Expediente> pagina = new List<Expediente>();
          
            int total_registros = 0;

            con = new ConeccionSiGeTram("sigetram").coneccion();

            try
            {
                using (con)
                {
                    con.Open();
               
                    SqlCommand query = new SqlCommand("Expediente_Listado_Paginado");

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Expediente", eb.expediente.Id_Expediente));
                    query.Parameters.Add(new SqlParameter("@Id_Proyecto", eb.expediente.Id_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Id_Proceso", eb.expediente.Id_Proceso));
                    query.Parameters.Add(new SqlParameter("@Rut_Revisor", eb.expediente.Rut_Revisor));
                    query.Parameters.Add(new SqlParameter("@Rut_Empresa", eb.expediente.Rut_Empresa));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Expediente", eb.expediente.Id_Tipo_Expediente));
                    query.Parameters.Add(new SqlParameter("@Id_Estado_Expediente", eb.expediente.Id_Estado_Expediente));
                    query.Parameters.Add(new SqlParameter("@Descripcion", eb.expediente.Descripcion));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", eb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", eb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    query.Connection = con;

                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Expediente expediente = new Expediente(dr);
                                
                            pagina.Add(expediente);
                        }
                    }
                    total_registros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return new PaginaExpediente(pagina,total_registros);
        }


        public int Agregar(Expediente e)
        {

            int id_Expediente = 0;

            con = new ConeccionSiGeTram("sigetram").coneccion();

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Expediente_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_proyecto", e.Id_Proyecto));
                    query.Parameters.Add(new SqlParameter("@Id_proceso", e.Id_Proceso));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Expediente", e.Id_Tipo_Expediente));
                    query.Parameters.Add(new SqlParameter("@Descripcion", e.Descripcion));
                    query.Parameters.Add(new SqlParameter("@Id_Expediente", e.Id_Expediente)).Direction = ParameterDirection.Output;

                    query.ExecuteNonQuery();

                    id_Expediente = Convert.ToInt32(query.Parameters["@Id_Expediente"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return id_Expediente;
        }

        public void Actualizar(Expediente e)
        {
            int respuesta = 0;

            con = new ConeccionSiGeTram("sigetram").coneccion();

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Expediente_Actualizar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Expediente", e.Id_Expediente));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Expediente", e.Id_Tipo_Expediente));
                    query.Parameters.Add(new SqlParameter("@Descripcion", e.Descripcion));
                    query.Parameters.Add(new SqlParameter("@Id_Proceso", e.Id_Proceso));
                    query.Parameters.Add(new SqlParameter("@Rut_Revisor", e.Rut_Revisor));
                    query.Parameters.Add(new SqlParameter("@Id_Instancia_Proceso", e.Id_Instancia_Proceso));
                    query.Parameters.Add(new SqlParameter("@Id_Estado_Expediente", e.Id_Estado_Expediente));
        
                    respuesta = query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Eliminar(int id)
        {
            int respuesta = 0;

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Expediente_Eliminar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Expediente", id));

                    respuesta = query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}