﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public class DocumentoDaoImp : DocumentoDao
    {

        SqlConnection con;
       
        public DocumentoDaoImp()
        {

            this.con = new ConeccionSiGeTram("sigetram").coneccion();


        }
        
        public PaginaDocumento Listado(DocumentoBuscado db)
        {

            List<Documento> listado = new List<Documento>();
            int totalRegistros = 0;
            con = new ConeccionSiGeTram("sigetram").coneccion();
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("documento_listado", con);

                    

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@id_expediente", db.documento.Id_Expediente));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", db.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", db.pagina.tamacno));


                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;

                    Documento doc ;

                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                             doc = new Documento(dr);                         

                            listado.Add(doc);
                        }

                        
                    }

                    totalRegistros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
               
                throw;
            }



            return new PaginaDocumento(listado,totalRegistros);
        }

        public Documento PorId(int idDocumento)
        {
                      
            Documento doc= null;

            con= new ConeccionSiGeTram("sigetram").coneccion();


            var query = new SqlCommand("documento_porId", con);

            query.CommandType = CommandType.StoredProcedure;

            query.Parameters.Add(new SqlParameter("@id_Documento", idDocumento));



            try
            {
                using (con)
                {
                    con.Open();

                   
                    query.Connection = con;


                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            doc = new Documento(dr);                           

                          
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return doc;
        }



        public byte[] Contenido(int idDocumento)
        {
                byte[] contenido = null;

            con = new ConeccionSiGeTram("sigetram").coneccion();

            try
                {

                    using (con)
                    {
                        con.Open();
                                        
                        var query = new SqlCommand("documento_porId", con);

                        query.CommandType = CommandType.StoredProcedure;
                           

                        query.Parameters.Add(new SqlParameter("@Id_Documento", idDocumento));

              
                        using (var dr = query.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                contenido = (byte[])dr.GetValue(dr.GetOrdinal("contenido"));
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw;
                }

            return contenido;
        }

        public int Agregar(Documento d)
        {
            int idDocumento = 0;


            con = new SqlConnection(ConfigurationManager.ConnectionStrings["sigetram"].ToString());

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("documento_agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Nombre_Documento", d.Nombre_Documento));
                    query.Parameters.Add(new SqlParameter("@Tipo_Contenido", d.Tipo_Contenido));
                    query.Parameters.Add(new SqlParameter("@Tamacno", d.Tamacno));
                    query.Parameters.Add(new SqlParameter("@Contenido", d.Contenido));
                    query.Parameters.Add(new SqlParameter("@id_expediente", d.Id_Expediente));
                    query.Parameters.Add("@id_documento", SqlDbType.Int).Direction = ParameterDirection.Output;

                    query.ExecuteNonQuery();

                    idDocumento = Convert.ToInt32(query.Parameters["@Id_Documento"].Value);
                }

                return idDocumento;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Actualizar(Documento d)
        {
            con = new ConeccionSiGeTram("sigetram").coneccion();
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("documento_actualizar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Documento", d.Id_Documento));

                    query.ExecuteNonQuery();

                }

             
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Eliminar(int idDocumento)
        {

            con = new ConeccionSiGeTram("sigetram").coneccion();
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("documento_eliminar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Documento", idDocumento));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
