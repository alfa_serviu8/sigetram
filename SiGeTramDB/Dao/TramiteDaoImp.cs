﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class TramiteDaoImp : TramiteDao
    {
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
        
      
        public PaginaTramite Listado(TramiteBuscado tb)
        {
            List<Tramite> tramites = new List<Tramite>();
            int total_registros = 0;

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Tramite_Listado_Paginado", con);

                    query.CommandType = CommandType.StoredProcedure;
                   
                    query.Parameters.Add(new SqlParameter("@Id_Tramite", tb.tramite.Id_Tramite));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", tb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", tb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            Tramite t = new Tramite(dr);

                            tramites.Add(t);
                        }
                    }// fin using reader

                    total_registros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return new PaginaTramite(tramites,total_registros);
        }

        public int Agregar(Tramite t)
        {

            int id_tramite = 0;
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Tramite_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Tramite", t.Id_Tipo_Tramite));
                    query.Parameters.Add(new SqlParameter("@Rut_Iniciado_Por", t.Rut_Iniciado_Por));
                    query.Parameters.Add(new SqlParameter("@Id_Tramite", t.Id_Tramite)).Direction = ParameterDirection.Output;

                    query.ExecuteNonQuery();

                    id_tramite = Convert.ToInt32(query.Parameters["@Id_tramite"].Value);

                   
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return id_tramite;
        }

       

        //public void Eliminar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Tipologia_Eliminar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Id_Tipologia", this.Id_Tipologia));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
    }
}
