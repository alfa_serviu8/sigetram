﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class TipoTramiteDaoImp:TipoTramiteDao
    {
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
             
      
        public PaginaTipoTramite Listado(TipoTramiteBuscado ttb)
        {
            List<TipoTramite> tipos = new List<TipoTramite>();

            int total_registros = 0;

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Tipo_Tramite_Listado_Paginado", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@id_tipo_tramite", ttb.tipoTramite.Id_Tipo_Tramite));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", ttb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", ttb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            TipoTramite tt = new TipoTramite(dr);

                            tipos.Add(tt);
                        }
                    }// fin using reader

                    total_registros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return new PaginaTipoTramite(tipos,total_registros);
        }

        public int Agregar(TipoTramite tt)
        {
            int idTipoTramite = 0;

            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Tipo_Tramite_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                
                    query.Parameters.Add(new SqlParameter("@Nombre_Tipo_Tramite", tt.Nombre_Tipo_Tramite));
                    query.Parameters.Add(new SqlParameter("@Vigente", tt.Vigente));
                    query.Parameters.Add(new SqlParameter("@Descripcion", tt.Descripcion));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Tramite", tt.Id_Tipo_Tramite)).Direction = ParameterDirection.Output;
                    query.ExecuteNonQuery();
                    idTipoTramite = Convert.ToInt32(query.Parameters["@Id_Tipo_Tramite"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return idTipoTramite;
        }

        public void Actualizar(TipoTramite tt)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Tipo_Tramite_Actualizar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Tramite", tt.Id_Tipo_Tramite));
                    query.Parameters.Add(new SqlParameter("@Nombre_Tipo_Tramite", tt.Nombre_Tipo_Tramite));
                    query.Parameters.Add(new SqlParameter("@Descripcion", tt.Descripcion));
                    query.Parameters.Add(new SqlParameter("@Vigente", tt.Vigente));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }





    }
}
