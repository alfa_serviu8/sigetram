﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class EstadoDocumentoDaoImp : EstadoDocumentoDao
    {
        SqlConnection con;


        public EstadoDocumentoDaoImp()
        {

            con = new ConeccionSiGeTram("sigetram").coneccion();

        }

        public PaginaEstadoDocumento Listado(EstadoDocumentoBuscado edb)
        {
            List<EstadoDocumento> estadoDocumentos = new List<EstadoDocumento>();
            int totalRegistros = 0;


            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Estado_Documento_Listado", con);

                    query.CommandType = CommandType.StoredProcedure;

                          query.Parameters.Add(new SqlParameter("@Id_Estado_Documento", edb.estadoDocumento.Id_Estado_Documento));
                          query.Parameters.Add(new SqlParameter("@pagina_solicitada", edb.pagina.numero));
                          query.Parameters.Add(new SqlParameter("@tamacno_pagina", edb.pagina.tamacno));
                          query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            EstadoDocumento ed = new EstadoDocumento(dr);

                            estadoDocumentos.Add(ed);
                        }
                    }

                            totalRegistros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
                    }
                    catch (Exception ex)
                   {
                       throw;
                    }
                    return new PaginaEstadoDocumento(estadoDocumentos,totalRegistros);
                }

       
    }
}
 