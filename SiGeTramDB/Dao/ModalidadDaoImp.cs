﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class ModalidadDaoImp : ModalidadDao
    {
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
        public Modalidad modalidadBuscada { set; get; }
        public int numero_pagina { set; get; }
        public int tamacno_pagina { set; get; }
        int total_registros;
      
        public List<Modalidad> Listado()
        {
            List<Modalidad> modalidades = new List<Modalidad>();

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Modalidad_Listado_Paginado", con);

                    query.CommandType = CommandType.StoredProcedure;
                   
                    query.Parameters.Add(new SqlParameter("@Id_Modalidad", modalidadBuscada.Id_Modalidad));
                    query.Parameters.Add(new SqlParameter("@Nombre_Modalidad", modalidadBuscada.Nombre_Modalidad));
                    query.Parameters.Add(new SqlParameter("@Vigente", modalidadBuscada.Vigente));
                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", modalidadBuscada.Id_Tipo_Subsidio));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", this.numero_pagina));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", this.tamacno_pagina));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            Modalidad m = new Modalidad(Convert.ToInt32(dr["Id_Modalidad"]),
                               dr["Nombre_Modalidad"].ToString(),
                               Convert.ToInt32(dr["Vigente"]),
                               Convert.ToInt32(dr["Id_Tipo_Subsidio"]));

                            modalidades.Add(m);
                        }
                    }// fin using reader

                    total_registros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return modalidades;
        }

        public int TotalRegistros()
        {
            return total_registros;
        }
    }
 }
