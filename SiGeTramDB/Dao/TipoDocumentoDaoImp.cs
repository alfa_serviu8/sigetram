﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public class TipoDocumentoDaoImp : TipoDocumentoDao
    {

        SqlConnection con;
       
        public TipoDocumentoDaoImp()
        {

            this.con = new ConeccionSiGeTram("sigetram").coneccion();

        }
        
        public PaginaTipoDocumento ListadoPaginado(TipoDocumentoBuscado tdb)
        {

            List<TipoDocumento> listado = new List<TipoDocumento>();
            int totalRegistros = 0;
            
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("tipo_documento_listado", con);
                    

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Documento", tdb.tipoDocto.Id_Tipo_Documento));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", tdb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", tdb.pagina.tamacno));


                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;

                    TipoDocumento tdoc ;

                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            
                             tdoc = new TipoDocumento(dr);                         

                            listado.Add(tdoc);
                        }

                       
                    }

                    totalRegistros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
               
                throw;
            }

            return new PaginaTipoDocumento(listado,totalRegistros);
        }


        public void Agregar(TipoDocumento td)
        {
            //int Id_Tipo_Documento = 0;


            this.con = new SqlConnection(ConfigurationManager.ConnectionStrings["sigedoc"].ToString());

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("tipo_documento_agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Nombre_Tipo_Documento", td.Nombre_Tipo_Documento));
                    query.Parameters.Add(new SqlParameter("@Vigente", td.Vigente));
                    //query.Parameters.Add("@Id_Tipo_Documento", SqlDbType.Int).Direction = ParameterDirection.Output;

                    query.ExecuteNonQuery();

                    //Id_Tipo_Documento = Convert.ToInt32(query.Parameters["@Id_Tipo_Documento"].Value);
                }

                //return Id_Tipo_Documento;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Actualizar(TipoDocumento td)
        {
        
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("tipo_documento_actualizar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Documento", td.Id_Tipo_Documento));
                    query.Parameters.Add(new SqlParameter("@Nombre_Tipo_Documento", td.Nombre_Tipo_Documento));
                    query.Parameters.Add(new SqlParameter("@Vigente", td.Vigente));

                    query.ExecuteNonQuery();

                }

             
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
