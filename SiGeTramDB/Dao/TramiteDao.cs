﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface TramiteDao
    {
        PaginaTramite Listado(TramiteBuscado tb);

        int Agregar(Tramite t);

       
    }
}
