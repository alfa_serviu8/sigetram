﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramDB.Entidades;


namespace SiGeTramDB.Dao
{
    public class EstadoExpedienteDaoImp : EstadoExpedienteDao
    {
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
       
        

        public PaginaEstadoExpediente Listado(EstadoExpedienteBuscado eeb)
        {
            List<EstadoExpediente> estados = new List<EstadoExpediente>();

            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Estado_Expediente_Listado", con);

                    query.CommandType = CommandType.StoredProcedure;

                                     
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            EstadoExpediente ee = new EstadoExpediente(Convert.ToInt32(dr["Id_Estado_Expediente"]),
                                dr["Nombre_Estado_Expediente"].ToString());

                            estados.Add(ee);
                        }
                    }// fin using reader

                    
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return new PaginaEstadoExpediente(estados,0);
        }



    }
}
