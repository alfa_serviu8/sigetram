﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface ExpedienteTareaDao
    {
        PaginaExpedienteTarea Listado(ExpedienteTareaBuscada etb);
        void Agregar(ExpedienteTarea et);

        void Actualizar(ExpedienteTarea et);


    }
}
