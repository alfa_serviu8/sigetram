﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

using SiGeTramDB.Entidades;
using SiGeTramDB;

namespace SiGeTramDB.Dao
{
    public class TipoExpedienteDaoImp : TipoExpedienteDao
    {
        SqlConnection con = new ConeccionSiGeTram("sigetram").coneccion();
       

        public PaginaTipoExpediente Listado(TipoExpedienteBuscado teb)
        {
            List<TipoExpediente> tipos = new List<TipoExpediente>();
            int total_registros = 0;
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("Tipo_Expediente_Listado_Paginado", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Expediente", teb.tipoExpediente.Id_Tipo_Expediente));
                    query.Parameters.Add(new SqlParameter("@Nombre_Tipo_Expediente", teb.tipoExpediente.Nombre_Tipo_Expediente));
                    query.Parameters.Add(new SqlParameter("@Vigente", teb.tipoExpediente.Vigente));
                    query.Parameters.Add(new SqlParameter("@Id_Proceso", teb.tipoExpediente.Id_Proceso));
                    query.Parameters.Add(new SqlParameter("@pagina_solicitada", teb.pagina.numero));
                    query.Parameters.Add(new SqlParameter("@tamacno_pagina", teb.pagina.tamacno));
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;
                    using (var dr = query.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            // Usuario
                            TipoExpediente ts = new TipoExpediente(dr);

                            tipos.Add(ts);
                        }
                    }// fin using reader

                    total_registros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return new PaginaTipoExpediente(tipos,total_registros);
        }

        public void Agregar(TipoExpediente te)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Tipo_Expediente_Agregar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Nombre_Tipo_Expediente", te.Nombre_Tipo_Expediente));
                    query.Parameters.Add(new SqlParameter("@Id_Proceso", te.Id_Proceso));
                    query.Parameters.Add(new SqlParameter("@Vigente", te.Vigente));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Actualizar(TipoExpediente te)
        {
            try
            {
                using (this.con)
                {
                    this.con.Open();

                    var query = new SqlCommand("Tipo_Expediente_Actualizar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tipo_Expediente", te.Id_Tipo_Expediente));
                    query.Parameters.Add(new SqlParameter("@Nombre_Tipo_Expediente", te.Nombre_Tipo_Expediente));
                    query.Parameters.Add(new SqlParameter("@Vigente", te.Vigente));
                    query.Parameters.Add(new SqlParameter("@Id_Proceso", te.Id_Proceso));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //public void Eliminar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Tipo_Expediente_Eliminar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Id_Tipo_Expediente", this.Id_Tipo_Expediente));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}


    }
}
