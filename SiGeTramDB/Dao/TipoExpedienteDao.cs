﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramDB.Entidades;

namespace SiGeTramDB.Dao
{
    public interface TipoExpedienteDao
    {
        PaginaTipoExpediente Listado(TipoExpedienteBuscado teb);

        void Agregar(TipoExpediente te);

        void Actualizar(TipoExpediente te);

       
    }
}
