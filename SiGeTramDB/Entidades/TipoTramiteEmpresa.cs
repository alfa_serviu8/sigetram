﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class TipoTramiteEmpresa  
        {
        public int Id_Tipo_Tramite { set; get; }

        public int Rut_Empresa { set; get; }

       
        public TipoTramiteEmpresa(int idTipoTramite,int rutEmpresa)
        {
            this.Id_Tipo_Tramite = idTipoTramite;
            
            this.Rut_Empresa=rutEmpresa;
        }

        public TipoTramiteEmpresa(SqlDataReader dr)
        {

            this.Id_Tipo_Tramite = Convert.ToInt32(dr["Id_Tipo_Tramite"]);
            this.Rut_Empresa = Convert.ToInt32(dr["Rut_Empresa"]);
           
        }



        public TipoTramiteEmpresa()
        {


           

        }

      


    }

}