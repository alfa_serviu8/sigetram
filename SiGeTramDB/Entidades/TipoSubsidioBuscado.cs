﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class TipoSubsidioBuscado
    {
        public TipoSubsidio tipoSubsidio { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public TipoSubsidioBuscado(TipoSubsidio ts,PaginaImp p)
        {

            this.tipoSubsidio = ts;
            this.pagina = p;
                           
        }

        
        public TipoSubsidioBuscado()
        {

        }


    }

}