﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class ExpedienteTarea 
    {
        public int Id_Expediente_Tarea{ set; get; }
        public int Id_Tarea { set; get; }
        public int Id_Tipo_Tarea { set; get; }
        public string Nombre_Tipo_Tarea { set; get; }
        public int Rut_Asignado { set; get; }
        public int Rut_Revisor { set; get; }
        public DateTime Fecha_Inicio { set; get; }
        public DateTime Fecha_Termino { set; get; }
        public int Vigente { set; get; }
        public int Id_Expediente { set; get; }
        public int Id_Tipo_Expediente { set; get; }
        public string Nombre_Tipo_Expediente { set; get; }
        public int Id_Estado_Expediente { set; get; }
        public string Descripcion { set; get; }
        public string Nombre_Estado_Expediente { set; get; }
        public string Observaciones { set; get; }

        public int Id_Tipo_Tramite { set; get; }
        
        

        public ExpedienteTarea(int IdExpedienteTarea,int idExpendiente,int idTarea,int idEstadoExpediente,string Observaciones,int Vigente)
        {


            this.Id_Expediente_Tarea = IdExpedienteTarea;
            this.Id_Expediente = idExpendiente;
            this.Id_Tarea = idTarea;
            this.Id_Estado_Expediente = idEstadoExpediente;
            this.Observaciones = Observaciones;
            this.Vigente = Vigente;


        }

        public ExpedienteTarea(Expediente e, int idTarea)
        {


            this.Id_Expediente_Tarea = 0;
            this.Id_Expediente = e.Id_Expediente;
            this.Id_Estado_Expediente = e.Id_Estado_Expediente;
            this.Id_Tarea = idTarea;
            this.Observaciones = "";
           


        }
        public ExpedienteTarea(SqlDataReader dr)
        {
                this.Id_Expediente_Tarea = Convert.ToInt32(dr["Id_Expediente_Tarea"]);
                this.Id_Expediente = Convert.ToInt32(dr["Id_Expediente"]);
                this.Descripcion = dr["Descripcion"].ToString();
                this.Id_Tipo_Expediente = Convert.ToInt32(dr["Id_Tipo_Expediente"]);
                this.Nombre_Tipo_Expediente = dr["Nombre_Tipo_Expediente"].ToString();
                this.Id_Estado_Expediente = Convert.ToInt32(dr["Id_Estado_Expediente"]);
                this.Nombre_Estado_Expediente = dr["Nombre_Estado_Expediente"].ToString();
                this.Observaciones = dr["Observaciones"].ToString();
                this.Id_Tarea = Convert.ToInt32(dr["Id_Tarea"]);
                this.Id_Tipo_Tarea = Convert.ToInt32(dr["Id_Tipo_Tarea"]);
                this.Nombre_Tipo_Tarea = dr["Nombre_Tipo_Tarea"].ToString();
                this.Rut_Asignado = Convert.ToInt32(dr["Rut_Asignado"]);
                this.Rut_Revisor = Convert.ToInt32(dr["Rut_Revisor"]);
                this.Fecha_Inicio = Convert.ToDateTime(dr["Fecha_Inicio"]);
                this.Fecha_Termino = Convert.ToDateTime(dr["Fecha_Termino"]);
                this.Vigente = Convert.ToInt32(dr["Vigente"]);
           




        }




        public ExpedienteTarea()
        {


           

        }

        


    }

}