﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTramDB.Entidades
{
    public class EstadoExpediente
    {
        public int Id_Estado_Expediente { set; get; }
        public string Nombre_Estado_Expediente { set; get; }
    

        public EstadoExpediente(int Id_Estado_Expediente, string Nombre_Estado_Expediente)
        {
            this.Id_Estado_Expediente = Id_Estado_Expediente;
            this.Nombre_Estado_Expediente = Nombre_Estado_Expediente;
        }
    }
}