﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class EntidadBuscada:IEntidadBuscada
    {
        public IEntidad e { set; get; }
        public PaginaImp pagina { set; get; }
                 
        
        public EntidadBuscada(IEntidad e,PaginaImp pag)
        {

            this.e = e;
            this.pagina = pag;
                           
        }

        public EntidadBuscada()
        {

           
        }



    }

}