﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace SiGeTramDB.Entidades
{
    public class Modalidad
    {
        public int Id_Modalidad { set; get; }
        public string Nombre_Modalidad { set; get; }
        public int Vigente { set; get; }
        public int Id_Tipo_Subsidio { set; get; }
  

        public Modalidad(int Id_Modalidad, string Nombre_Modalidad, int Vigente, int Id_Tipo_Subsidio)
        {
            this.Id_Modalidad = Id_Modalidad;
            this.Nombre_Modalidad = Nombre_Modalidad;
            this.Vigente = Vigente;
            this.Id_Tipo_Subsidio = Id_Tipo_Subsidio;
        }

        //public void Agregar()
        //{
        //    try
        //    {
        //        using (this.con)  {
        //            this.con.Open();

        //            var query = new SqlCommand("Modalidad_Agregar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Nombre_Modalidad", this.Nombre_Modalidad));
        //            query.Parameters.Add(new SqlParameter("@Vigente", this.Vigente));
        //            query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", this.Id_Tipo_Subsidio));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public void Actualizar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Modalidad_Actualizar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Id_Modalidad", this.Id_Modalidad));
        //            query.Parameters.Add(new SqlParameter("@Nombre_Modalidad", this.Nombre_Modalidad));
        //            query.Parameters.Add(new SqlParameter("@Vigente", this.Vigente));
        //            query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", this.Id_Tipo_Subsidio));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public void Eliminar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Modalidad_Eliminar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Id_Modalidad", this.Id_Modalidad));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
    }
}