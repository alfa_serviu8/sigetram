﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaExpediente
    {
        public List<Expediente> expedientes { set; get; }
        public int totalRegistros { set; get; }

        public PaginaExpediente(List<Expediente> expedientes,int totalRegistros)
        {

            this.expedientes = expedientes;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}