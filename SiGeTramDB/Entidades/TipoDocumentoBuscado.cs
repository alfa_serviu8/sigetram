﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class TipoDocumentoBuscado
    {
        public TipoDocumento tipoDocto { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public TipoDocumentoBuscado(TipoDocumento td,PaginaImp p)
        {

            this.tipoDocto = td;
            this.pagina = p;
                           
        }

       
        public TipoDocumentoBuscado()
        {

        }


    }

}