﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class Tramite
    {
        public int Id_Tramite { set; get; }
        public int Id_Tipo_Tramite { set; get; }
        public string Nombre_Tipo_Tramite { set; get; }
        public int Rut_Iniciado_Por { set; get; }
        public DateTime Fecha_Inicio { set; get; }
        public DateTime Fecha_Termino { set; get; }
        public int Vigente { set; get; }
       

        public Tramite(int idTipoTramite,int rutIniciadoPor)
        {


            
            this.Id_Tipo_Tramite = idTipoTramite;
            this.Rut_Iniciado_Por = rutIniciadoPor;

        }
            public Tramite(SqlDataReader dr)
        {
           
                this.Id_Tramite = Convert.ToInt32(dr["Id_Tramite"]);
                this.Id_Tipo_Tramite = Convert.ToInt32(dr["Id_Tipo_Tramite"]);
                this.Nombre_Tipo_Tramite = (dr["Nombre_Tipo_Tramite"]).ToString();
                this.Rut_Iniciado_Por = Convert.ToInt32(dr["Rut_Iniciado_Por"]);
                this.Fecha_Inicio = Convert.ToDateTime(dr["Fecha_Inicio"]);
                this.Fecha_Termino = Convert.ToDateTime(dr["Fecha_Termino"]);
              

            
        }


        public Tramite()
        {

           

        }

        
    }

}