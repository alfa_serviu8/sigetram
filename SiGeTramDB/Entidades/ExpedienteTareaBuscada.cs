﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class ExpedienteTareaBuscada
    {
        public ExpedienteTarea expediente_tarea { set; get; }
        public PaginaImp pagina { set; get; }
                 
        
        public ExpedienteTareaBuscada(ExpedienteTarea t,PaginaImp pag)
        {

            this.expediente_tarea = t;
            this.pagina = pag;
                           
        }

        public ExpedienteTareaBuscada()
        {

           
        }



    }

}