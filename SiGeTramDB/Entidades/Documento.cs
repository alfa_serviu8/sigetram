﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.IO.Font;
using iText.Kernel.Font;
using iText.Kernel.Events;
using iText.IO.Image;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Borders;
using iText.Kernel.Colors;

namespace SiGeTramDB.Entidades
{
    public class Documento
    {
        public int Id_Documento { set; get; }
        public string Nombre_Documento { set; get; }
        public string Tipo_Contenido { set; get; }
        public byte[] Contenido { set; get; }
        public long Tamacno { set; get; }
        public int Id_Expediente { set; get; }
           
     

       


        public Documento(int Id, string Nombre, string Tipo_Contenido, byte[] contenido, long Tamacno,int id_expediente)
        {
            this.Id_Documento = Id;
            this.Nombre_Documento = Nombre.Replace(" ","_");
            this.Tipo_Contenido = Tipo_Contenido;
            this.Tamacno = Tamacno;
            this.Contenido = contenido;
            this.Id_Expediente = id_expediente;
         
          
        }

        public Documento(int id_Expediente,int id)
        {
            this.Id_Expediente = id_Expediente;
            this.Id_Documento = id;



        }


        public Documento(SqlDataReader dr)
        {
            this.Id_Documento = Convert.ToInt32(dr["Id_Documento"]);
            this.Nombre_Documento = dr["Nombre_Documento"].ToString();
            this.Tipo_Contenido = dr["Tipo_Contenido"].ToString();
            this.Tamacno = Convert.ToInt32(dr["Tamacno"]);
                 

        }

        public Documento(int Id)
        {
            this.Id_Documento = Id;

        }

        public Documento()
        {
         
           

        }


        public string urlRecepcion()
        {

            return "http://intranet08.minvu.cl/SiTram/#!documentos";

        }

        public byte[] Pdf()
        {

            using (var mem = new MemoryStream())
            {

                //Variables del Documento
                var wri = new PdfWriter(mem);
                var doc = new PdfDocument(wri);

                Document documentopdf = new Document(doc, PageSize.LETTER);
                documentopdf.SetMargins(30, 70, 30, 80);


                FontProgram Negrita = FontProgramFactory.CreateFont(HttpContext.Current.Server.MapPath("~/Content/fonts/gobcl_bold-webfont.ttf"));
                FontProgram Normal = FontProgramFactory.CreateFont(HttpContext.Current.Server.MapPath("~/Content/fonts/gobcl_regular-webfont.ttf"));
                //FontProgram Negrita = FontProgramFactory.CreateFont();
                //FontProgram Normal = FontProgramFactory.CreateFont();

                PdfFont Fuente_Negrita = PdfFontFactory.CreateFont(Negrita, PdfEncodings.WINANSI, true);
                PdfFont Fuente_Normal = PdfFontFactory.CreateFont(Normal, PdfEncodings.WINANSI, true);

                ////Ver como traer el nombre del servicio activo
                //doc.AddEventHandler(PdfDocumentEvent.START_PAGE, new Header("Serviu Región del Bio Bio / Departamento de Administración"));

                //PaginaXdeY ev = new PaginaXdeY(doc);

                //doc.AddEventHandler(PdfDocumentEvent.END_PAGE, ev);

                //Ver como traer el logo del servicio activo
                Image img = new Image(ImageDataFactory.Create(HttpContext.Current.Server.MapPath("~/Content/images/logo_serviu.jpg")));
                img.SetHeight(100);
                img.SetWidth(100);
                img.SetFixedPosition(80, PageSize.LETTER.GetHeight() - 130);

                documentopdf.Add(img);

                //acceso a los datos sql
                //Resolucion resolucion = ResolucionPorId(Id_Resolucion);
                Table tabla_encabezado = new Table(new float[] { 1 });

                tabla_encabezado.SetMarginTop(110);
                tabla_encabezado.SetWidth(UnitValue.CreatePercentValue(100));
                tabla_encabezado.SetFontSize(15);
                tabla_encabezado.SetFont(Fuente_Negrita);
                tabla_encabezado.SetHorizontalAlignment(HorizontalAlignment.CENTER);

                tabla_encabezado.AddCell(new Cell().Add(new Paragraph("COMPROBANTE DE INGRESO DE DOCUMENTOS N°" + this.Id_Documento)).SetBorder(Border.NO_BORDER).SetTextAlignment(TextAlignment.CENTER));



                documentopdf.Add(tabla_encabezado);


                //Table tabla_destino = new Table(new float[] { 1, 1 });

                //tabla_destino.SetMarginTop(15);
                //tabla_destino.SetWidth(UnitValue.CreatePercentValue(100));
                //tabla_destino.SetFontSize(12);
                //tabla_destino.SetFont(Fuente_Normal);
                //tabla_destino.SetHorizontalAlignment(HorizontalAlignment.LEFT);

                //tabla_destino.AddCell(new Cell().Add(new Paragraph("A:")).SetBorder(Border.NO_BORDER));
                //tabla_destino.AddCell(new Cell().Add(new Paragraph("SR/A. " + f.Nombre_Completo)).SetBorder(Border.NO_BORDER));
                //tabla_destino.AddCell(new Cell().Add(new Paragraph("  ")).SetBorder(Border.NO_BORDER));
                //tabla_destino.AddCell(new Cell().Add(new Paragraph("Rut " + f.Rut + "-" + f.Dv)).SetBorder(Border.NO_BORDER).SetPaddingTop(5));

                //documentopdf.Add(tabla_destino);


                Table tabla_escabezado_parrafo = new Table(new float[] { 1 });

               

                Table tabla_cuerpo = new Table(new float[] { 1 });

                tabla_cuerpo.SetMarginTop(15);
                tabla_cuerpo.SetWidth(UnitValue.CreatePercentValue(100));
                tabla_cuerpo.SetFontSize(12);
                tabla_cuerpo.SetFont(Fuente_Normal);
                tabla_cuerpo.SetHorizontalAlignment(HorizontalAlignment.LEFT);

                //string cuerpo = "Estimados " + this.Nombre_Ingresado_Por + " Informamos a Ud. que con fecha " + this.Fecha_Ingreso + " Se ha ingresado el documento " + this.Nombre_Documento  + " al SERVIU Región del Biobío.";

                //tabla_cuerpo.AddCell(new Cell().Add(new Paragraph(cuerpo).SetFirstLineIndent(50).SetFixedLeading(20)).SetBorder(Border.NO_BORDER).SetTextAlignment(TextAlignment.JUSTIFIED));
                //tabla_cuerpo.AddCell(new Cell().Add(new Paragraph("Saluda atentamente a usted,").SetFirstLineIndent(50)).SetBorder(Border.NO_BORDER));


                documentopdf.Add(tabla_cuerpo);


                //Table tabla_firma = new Table(new float[] { 1 });

                //tabla_firma.SetMarginTop(15);
                //tabla_firma.SetWidth(UnitValue.CreatePercentValue(100));
                //tabla_firma.SetFontSize(12);
                //tabla_firma.SetFont(Fuente_Normal);
                //tabla_firma.SetHorizontalAlignment(HorizontalAlignment.LEFT);

                //Image img_firma = new Image(ImageDataFactory.Create(HttpContext.Current.Server.MapPath("~/Content/images/" + "Firma_Jefa_Departamento.png")));
                //img_firma.SetHeight(140);
                //img_firma.SetWidth(310);
                //img_firma.SetBorder(Border.NO_BORDER);
                //img_firma.SetHorizontalAlignment(HorizontalAlignment.CENTER);

                //tabla_firma.AddCell(new Cell().Add(img_firma).SetBorder(Border.NO_BORDER).SetTextAlignment(TextAlignment.CENTER).SetHorizontalAlignment(HorizontalAlignment.CENTER));



                //documentopdf.Add(tabla_firma);



                //Table tabla_pie = new Table(new float[] { 1 });

                //tabla_pie.SetMarginTop(15);
                //tabla_pie.SetWidth(UnitValue.CreatePercentValue(100));
                //tabla_pie.SetFontSize(12);
                //tabla_pie.SetFont(Fuente_Normal);
                //tabla_pie.SetHorizontalAlignment(HorizontalAlignment.LEFT);


                //tabla_pie.AddCell(new Cell().Add(new Paragraph("RMR/YBG")).SetBorder(Border.NO_BORDER));
                //tabla_pie.AddCell(new Cell().Add(new Paragraph("Concepción, noviembre de 2019.-")).SetBorder(Border.NO_BORDER));



                //documentopdf.Add(tabla_pie);



                doc.Close();

                return mem.ToArray();



            }

        }

    }
}