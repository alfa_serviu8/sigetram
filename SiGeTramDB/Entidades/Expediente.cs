﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;

namespace SiGeTramDB.Entidades
{
    public class Expediente
    {
        public int Id_Expediente { set; get; }
        public int Id_Tipo_Expediente { set; get; }
        public string Nombre_Tipo_Expediente { set; get; }

        public string Nombre_Tipo_Tramite { set; get; }
        public int Id_Proceso { set; get; }
        public int Id_Instancia_Proceso { set; get; }
        public string Nombre_Proceso { set; get; }

        public string Nombre_Corto_Tipo_Tramite { set; get; }
        public string Nombre_Estado_Expediente { set; get; }
        public string Nombre_Tipo_Tarea { set; get; }
        public string Id_Tarea { set; get; }
        public string Descripcion { set; get; }
        public int Id_Proyecto { set; get; }
        public string Codigo_Proyecto { set; get; }
        public string Nombre_Proyecto { set; get; }
        public int Id_Estado_Expediente { set; get; }
        public int Rut_Operador { set; get; }
        public int Rut_Empresa { set; get; }
        public string Nombre_Empresa { set; get; }

        public string Razon_Social_Empresa { set; get; }
        public int Rut_Revisor { set; get; }
        public string Nombre_Revisor { set; get; }
        public int Total_Documentos { set; get; }
        public DateTime Fecha_Envio { set; get; }

        public Tarea TareaVigente { set; get; }





   public Expediente()
   {
            this.Id_Expediente = 0;
            this.Id_Proyecto = 0;
            this.Rut_Revisor = 0;
            this.Rut_Empresa = 0;
            this.Id_Tipo_Expediente = 0;
            this.Id_Estado_Expediente = 0;
            this.Descripcion = "";
        }
        public Expediente(int idExpediente)
        {
            this.Id_Expediente = idExpediente;
            this.Id_Proyecto = 0;
            this.Rut_Revisor = 0;
            this.Rut_Empresa = 0;
            this.Id_Tipo_Expediente = 0;
            this.Id_Estado_Expediente = 0;
            this.Descripcion = "";

        }

        public Expediente(int idExpediente,int idProceso)
        {
            this.Id_Expediente = idExpediente;
            this.Id_Proceso = idProceso;
            this.Id_Proyecto = 0;
            this.Rut_Revisor = 0;
            this.Rut_Empresa = 0;
            this.Id_Tipo_Expediente = 0;
            this.Id_Estado_Expediente = 0;
            this.Descripcion = "";

        }

        public Expediente(int Id_Expediente, int Id_Tipo_Expediente, string Descripcion, int Id_Proyecto,string Codigo_Proyecto,string Nombre_Proyecto,int Id_Estado_Expediente, string Nombre_Estado_Expediente, string Nombre_Tipo_Expediente, int Total_Documentos, string Id_Tarea, string Nombre_Tipo_Tarea, int Rut_Empresa,string Nombre_Empresa,DateTime fEnv)
    {
        this.Id_Expediente = Id_Expediente;
        this.Id_Tipo_Expediente = Id_Tipo_Expediente;
        this.Descripcion = Descripcion;
        this.Id_Proyecto = Id_Proyecto;
        this.Codigo_Proyecto = Codigo_Proyecto;
        this.Nombre_Proyecto = Nombre_Proyecto;
        this.Id_Estado_Expediente = Id_Estado_Expediente;
        this.Nombre_Estado_Expediente = Nombre_Estado_Expediente;
        this.Nombre_Tipo_Expediente = Nombre_Tipo_Expediente;
        this.Rut_Revisor = Rut_Revisor;
        this.Nombre_Revisor = Nombre_Revisor;
        this.Total_Documentos = Total_Documentos;
        this.Id_Tarea = Id_Tarea;
        this.Nombre_Tipo_Tarea = Nombre_Tipo_Tarea;
        this.Rut_Empresa = Rut_Empresa;
        this.Nombre_Empresa = Nombre_Empresa;
        this.Fecha_Envio = fEnv;

    }
        public Expediente(int idTipoExpediente, int Id_Proyecto,int idProceso)
        {
           
            this.Id_Tipo_Expediente = idTipoExpediente;
            this.Descripcion = "";
            this.Id_Proyecto = Id_Proyecto;
            this.Id_Proceso = idProceso;

        }
        public Expediente(SqlDataReader dr)
        {
                     
                                 
            this.Id_Expediente = Convert.ToInt32(dr["Id_Expediente"]);
            this.Id_Tipo_Expediente = Convert.ToInt32(dr["Id_Tipo_Expediente"]);
            this.Id_Proceso = Convert.ToInt32(dr["Id_Proceso"]);
            this.Id_Instancia_Proceso = Convert.ToInt32(dr["Id_Instancia_Proceso"]);
            this.Nombre_Proceso = dr["Nombre_Proceso"].ToString();
            this.Nombre_Corto_Tipo_Tramite = dr["Nombre_Corto"].ToString();
            this.Descripcion = dr["Descripcion"].ToString();
            this.Id_Proyecto = Convert.ToInt32(dr["Id_Proyecto"]);
            this.Codigo_Proyecto = dr["Codigo_Proyecto"].ToString();
            this.Nombre_Proyecto = dr["Nombre_Proyecto"].ToString();
            this.Id_Estado_Expediente = Convert.ToInt32(dr["Id_Estado_Expediente"]);
            this.Nombre_Estado_Expediente = dr["Nombre_Estado_Expediente"].ToString();
            this.Nombre_Tipo_Expediente = dr["Nombre_Tipo_Expediente"].ToString();
            this.Rut_Revisor = Convert.ToInt32(dr["Rut_Revisor"]);
            this.Nombre_Revisor = dr["Nombre_Revisor"].ToString();
            this.Total_Documentos = Convert.ToInt32(dr["Total_Documentos"]);
            this.Rut_Empresa = Convert.ToInt32(dr["Rut_Empresa"]);
            this.Nombre_Empresa = dr["Nombre_Empresa"].ToString();
            this.Razon_Social_Empresa = dr["Razon_Social_Empresa"].ToString();
            this.Fecha_Envio = Convert.ToDateTime(dr["Fecha_Envio"]);


        }

        public string urlRevision()
        {

            return "http://intranet08.minvu.cl/SiGeTram/#!expedientes";

        }



        //    public void Eliminar(out bool expedienteEliminado, out string mensajeResultado)
        //    {
        //        const int estadoEliminado = 5;
        //        expedienteEliminado = true;
        //        mensajeResultado = "El expediente  se eliminó con éxito";

        //        string sConsulta = "Update Expediente " +
        //                               "Set id_estado_expediente=@estadoEliminado " +
        //                               "Where id_expediente=@idExpediente";

        //        try
        //        {
        //            using (con)
        //            {
        //                con.Open();

        //                var query = new SqlCommand(sConsulta, con);


        //                query.CommandType = CommandType.Text;


        //                query.Parameters.Add(new SqlParameter("@idExpediente", this.Id_Expediente));
        //                query.Parameters.Add(new SqlParameter("@estadoEliminado", estadoEliminado));


        //                query.ExecuteNonQuery();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            mensajeResultado = "Hubo un problema en la base de datos : " + ex.Message;
        //            expedienteEliminado = false;
        //            throw;
        //        }





        //    }



    }
}
 
