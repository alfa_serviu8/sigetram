﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTramDB.Entidades
{
    public class Proyecto
    {
        public int Id_Proyecto { set; get; }
        public string Codigo_Proyecto { set; get; }
        public string Nombre_Proyecto { set; get; }
        public int Rut_Empresa { set; get; }
        public string Nombre_Empresa { set; get; }
        public int Rut_Revisor { set; get; }
        public string Nombre_Revisor { set; get; }
        public int Id_Estado_Proyecto { set; get; }
        public string Nombre_Estado_Proyecto { set; get; }
        public int Id_Tipo_Subsidio { set; get; }
        public string Nombre_Tipo_Subsidio { set; get; }
        public int Id_Tipologia { set; get; }
        public string Nombre_Tipo_Tramite { set; get; }
        public int Id_Proceso { set; get; }

        public string Nombre_Proceso { set; get; }
        public string Nombre_Tipologia { set; get; }
        public int Cantidad_Familias { set; get; }
        public int Id_Comuna { set; get; }
        public string Nombre_Comuna { set; get; }
        public int Id_Provincia { set; get; }
        public string Nombre_Provincia { set; get; }
        public int Id_Region { set; get; }
        public string Nombre_Region { set; get; }

         


        //public Proyecto(int Id_Proyecto, string Codigo_Proyecto, string Nombre_Proyecto, int Rut_Empresa, int Rut_Revisor_Social, int Id_Estado_Proyecto, int Id_Tipo_Subsidio, int Cantidad_Familias, int Id_Comuna, int Id_Modalidad)
        public Proyecto(int Id_Proyecto,
            string Codigo_Proyecto,
            string Nombre_Proyecto,
            int Rut_Empresa,
            string Nombre_Empresa,
            int Rut_Revisor,
            string Nombre_Revisor,
            int Id_Estado_Proyecto,
            string Nombre_Estado_Proyecto,
            int Id_Tipo_Subsidio,
            string Nombre_Tipo_Subsidio,
            int Id_Tipologia,
            string Nombre_Tipologia,
            int Cantidad_Familias,
            int Id_Comuna,
            string Nombre_Comuna,
            int Id_Provincia,
            string Nombre_Provincia,
            int Id_Region,
            string Nombre_Region
            
            )
        {
            this.Id_Proyecto = Id_Proyecto;
            this.Codigo_Proyecto = Codigo_Proyecto;
            this.Nombre_Proyecto = Nombre_Proyecto;
            this.Rut_Empresa = Rut_Empresa;
            this.Nombre_Empresa = Nombre_Empresa;
            this.Rut_Revisor = Rut_Revisor;
            this.Nombre_Revisor= Nombre_Revisor;
            this.Id_Estado_Proyecto = Id_Estado_Proyecto;
            this.Nombre_Estado_Proyecto = Nombre_Estado_Proyecto;
            this.Id_Tipo_Subsidio = Id_Tipo_Subsidio;
            this.Nombre_Tipo_Subsidio = Nombre_Tipo_Subsidio;
            this.Id_Tipologia = Id_Tipologia;
            this.Nombre_Tipologia = Nombre_Tipologia;
            this.Cantidad_Familias = Cantidad_Familias;
            this.Id_Comuna = Id_Comuna;
            this.Nombre_Comuna = Nombre_Comuna;
            this.Id_Provincia = Id_Provincia;
            this.Nombre_Provincia = Nombre_Provincia;
            this.Id_Region = Id_Region;
            this.Nombre_Region = Nombre_Region;
            
        }


        public Proyecto(SqlDataReader dr)
        {
                   
            this.Id_Proyecto = Convert.ToInt32(dr["Id_Proyecto"]);
            this.Codigo_Proyecto = dr["Codigo_Proyecto"].ToString();
            this.Nombre_Proyecto = dr["Nombre_Proyecto"].ToString();
            this.Rut_Empresa = Convert.ToInt32(dr["Rut_Empresa"]);
            this.Nombre_Empresa = dr["Nombre_Empresa"].ToString();
            //this.Rut_Revisor = Convert.ToInt32(dr["Rut_Revisor"]);
            //this.Nombre_Revisor = dr["Nombre_Revisor"].ToString();
            this.Id_Estado_Proyecto = Convert.ToInt32(dr["Id_Estado_Proyecto"]);
            this.Nombre_Estado_Proyecto = dr["Nombre_Estado_Proyecto"].ToString();
            this.Id_Proceso = Convert.ToInt32(dr["Id_Proceso"]);
            this.Nombre_Proceso = dr["Nombre_Proceso"].ToString();
            this.Id_Tipo_Subsidio = Convert.ToInt32(dr["Id_Tipo_Subsidio"]);
            this.Nombre_Tipo_Subsidio = dr["Nombre_Tipo_Subsidio"].ToString();
            this.Id_Tipologia = Convert.ToInt32(dr["Id_Tipologia"]);
            this.Nombre_Tipologia = dr["Nombre_Tipologia"].ToString();
            this.Cantidad_Familias = Convert.ToInt32(dr["Cantidad_Familias"]);
            this.Id_Comuna = Convert.ToInt32(dr["Id_Comuna"]);
            this.Nombre_Comuna = dr["Nombre_Comuna"].ToString();
            this.Id_Provincia = Convert.ToInt32(dr["Id_Provincia"]);
            this.Nombre_Provincia = dr["Nombre_Provincia"].ToString();
            this.Id_Region = Convert.ToInt32(dr["Id_Region"]);
            this.Nombre_Region = dr["Nombre_Region"].ToString();
           

        }

        public Proyecto()
        {


        }

        //public void Agregar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Proyecto_Agregar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Codigo_Proyecto", this.Codigo_Proyecto));
        //            query.Parameters.Add(new SqlParameter("@Nombre_Proyecto", this.Nombre_Proyecto));
        //            query.Parameters.Add(new SqlParameter("@Rut_Empresa", this.Rut_Empresa));
        //            query.Parameters.Add(new SqlParameter("@Rut_Revisor_Social", this.Rut_Revisor_Social));
        //            query.Parameters.Add(new SqlParameter("@Id_Estado_Proyecto", this.Id_Estado_Proyecto));
        //            query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", this.Id_Tipo_Subsidio));
        //            query.Parameters.Add(new SqlParameter("@Id_Tipologia", this.Id_Tipologia));
        //            query.Parameters.Add(new SqlParameter("@Cantidad_Familias", this.Cantidad_Familias));
        //            query.Parameters.Add(new SqlParameter("@Id_Region", this.Id_Region));
        //            query.Parameters.Add(new SqlParameter("@Id_Provincia", this.Id_Provincia));
        //            query.Parameters.Add(new SqlParameter("@Id_Comuna", this.Id_Comuna));
        //            query.Parameters.Add(new SqlParameter("@Resolucion_Seleccion", this.Resolucion_Seleccion));
        //            query.Parameters.Add(new SqlParameter("@id_Proyecto", this.Id_Proyecto)).Direction = ParameterDirection.Output;

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public void Actualizar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Proyecto_Actualizar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Id_Proyecto", this.Id_Proyecto));
        //            query.Parameters.Add(new SqlParameter("@Codigo_Proyecto", this.Codigo_Proyecto));
        //            query.Parameters.Add(new SqlParameter("@Nombre_Proyecto", this.Nombre_Proyecto));
        //            query.Parameters.Add(new SqlParameter("@Rut_Empresa", this.Rut_Empresa));
        //            query.Parameters.Add(new SqlParameter("@Rut_Revisor_Social", this.Rut_Revisor_Social));
        //            query.Parameters.Add(new SqlParameter("@Id_Estado_Proyecto", this.Id_Estado_Proyecto));
        //            query.Parameters.Add(new SqlParameter("@Id_Tipo_Subsidio", this.Id_Tipo_Subsidio));
        //            query.Parameters.Add(new SqlParameter("@Id_Tipologia", this.Id_Tipologia));
        //            query.Parameters.Add(new SqlParameter("@Cantidad_Familias", this.Cantidad_Familias));
        //            query.Parameters.Add(new SqlParameter("@Id_Region", this.Id_Region));
        //            query.Parameters.Add(new SqlParameter("@Id_Provincia", this.Id_Provincia));
        //            query.Parameters.Add(new SqlParameter("@Id_Comuna", this.Id_Comuna));
        //            query.Parameters.Add(new SqlParameter("@Resolucion_Seleccion", this.Resolucion_Seleccion));
        //            query.Parameters.Add(new SqlParameter("@Id_Proyecto_Camunda", this.Id_Proyecto_Camunda));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public void Eliminar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Proyecto_Eliminar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Id_Proyecto", this.Id_Proyecto));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
    }
}