﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaTipoExpediente
    {
        public List<TipoExpediente> tiposExpediente { set; get; }
        public int totalRegistros { set; get; }

        public PaginaTipoExpediente(List<TipoExpediente> tiposExpediente,int totalRegistros)
        {

            this.tiposExpediente = tiposExpediente;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}