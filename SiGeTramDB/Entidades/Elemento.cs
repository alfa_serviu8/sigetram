﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class Elemento 
    {
        public int Id_Elemento { set; get; }
        public string Nombre_Elemento { set; get; }
        public string Id_Tipo_Elemento { set; get; }
        public int Asignado { set; get; }
        public int Id_Tipo_Tramite { set; get; }

        public string Formulario { set; get; }


        public Elemento(string nombre,string idTipo,int idTipoTramite)
        {
            this.Id_Elemento = 0;
            this.Nombre_Elemento = nombre;
            this.Id_Tipo_Elemento = idTipo;
            this.Id_Tipo_Tramite = idTipoTramite;
            this.Asignado = -1;
            this.Formulario = "";



        }


        public Elemento()
        {


           
        }
            public Elemento(SqlDataReader dr)
        {
           
                this.Id_Elemento = Convert.ToInt32(dr["Id_Elemento"]);
                this.Id_Tipo_Elemento = dr["Id_Tipo_Elemento"].ToString();
                this.Nombre_Elemento = dr["Nombre_Elemento"].ToString();
                this.Asignado = Convert.ToInt32(dr["Asignado"]);
                this.Id_Tipo_Tramite = Convert.ToInt32(dr["Id_Tipo_Tramite"]);
                this.Formulario = dr["Formulario"].ToString();


        }

              

       

    }

}