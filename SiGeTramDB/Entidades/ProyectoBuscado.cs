﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class ProyectoBuscado
    {
        public Proyecto proyecto { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public ProyectoBuscado(Proyecto pr,PaginaImp p)
        {

            this.proyecto = pr;
            this.pagina = p;
                           
        }

        
        public ProyectoBuscado()
        {

        }


    }

}