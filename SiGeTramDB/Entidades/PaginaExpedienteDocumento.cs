﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaExpedienteDocumento
    {
        public List<ExpedienteDocumento> expedientesDocumento { set; get; }
        public int totalRegistros { set; get; }

        public PaginaExpedienteDocumento(List<ExpedienteDocumento> expedientesDocumento,int totalRegistros)
        {

            this.expedientesDocumento = expedientesDocumento;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}