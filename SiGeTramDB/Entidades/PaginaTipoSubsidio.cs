﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaTipoSubsidio
    {
        public List<TipoSubsidio> tiposSubsidio { set; get; }
        public int totalRegistros { set; get; }

        public PaginaTipoSubsidio(List<TipoSubsidio> tiposSubsidio,int totalRegistros)
        {

            this.tiposSubsidio = tiposSubsidio;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}