﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaTipologia
    {
        public List<Tipologia> tipologias { set; get; }
        public int totalRegistros { set; get; }

        public PaginaTipologia(List<Tipologia> tipologias,int totalRegistros)
        {

            this.tipologias= tipologias;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}