﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaTipoDocumento
    {
        public List<TipoDocumento> tipoDocumento { set; get; }
        public int totalRegistros { set; get; }

        public PaginaTipoDocumento(List<TipoDocumento> tipoDocumento,int totalRegistros)
        {

            this.tipoDocumento = tipoDocumento;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}