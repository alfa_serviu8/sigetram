﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaProyecto
    {
        public List<Proyecto> proyectos { set; get; }
        public int totalRegistros { set; get; }

        public PaginaProyecto(List<Proyecto> proyectos,int totalRegistros)
        {

            this.proyectos = proyectos;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}