﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaTipoTramiteEmpresa
    {
        public List<TipoTramiteEmpresa> tiposTramiteEmpresa { set; get; }
        public int totalRegistros { set; get; }

        public PaginaTipoTramiteEmpresa(List<TipoTramiteEmpresa> tiposTramiteEmpresa,int totalRegistros)
        {

            this.tiposTramiteEmpresa = tiposTramiteEmpresa;
                        
            this.totalRegistros = totalRegistros;
        }

        public PaginaTipoTramiteEmpresa()
        {

        }
    }
        
}