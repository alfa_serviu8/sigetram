﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaDocumento
    {
        public List<Documento> documentos { set; get; }
        public int totalRegistros { set; get; }

        public PaginaDocumento(List<Documento> documentos,int totalRegistros)
        {

            this.documentos = documentos;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}