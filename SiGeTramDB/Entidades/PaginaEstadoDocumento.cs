﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaEstadoDocumento
    {
        public List<EstadoDocumento> estadoDocumentos{ set; get; }
        public int totalRegistros { set; get; }

        public PaginaEstadoDocumento(List<EstadoDocumento> estadoDocumentos,int totalRegistros)
        {

            this.estadoDocumentos = estadoDocumentos;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}