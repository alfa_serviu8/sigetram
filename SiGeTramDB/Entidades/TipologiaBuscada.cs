﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class TipologiaBuscada
    {
        public Tipologia tipologia { set; get; }
        public PaginaImp pagina { set; get; }
                 
        
        public TipologiaBuscada(Tipologia t,PaginaImp pag)
        {

            this.tipologia= t;
            this.pagina = pag;
                           
        }

        public TipologiaBuscada()
        {

           
        }



    }

}