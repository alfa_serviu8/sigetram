﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class TipoTramiteBuscado
    {
        public TipoTramite tipoTramite { set; get; }
        public PaginaImp pagina { set; get; }
                 
        
        public TipoTramiteBuscado(TipoTramite tt,PaginaImp pag)
        {

            this.tipoTramite= tt;
            this.pagina = pag;
                           
        }

        public TipoTramiteBuscado()
        {

           
        }



    }

}