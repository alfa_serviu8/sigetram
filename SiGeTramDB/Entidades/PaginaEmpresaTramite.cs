﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaEmpresaTramite
    {
        public List<EmpresaTramite> empresas_tramite { set; get; }
        public int totalRegistros { set; get; }

        public PaginaEmpresaTramite(List<EmpresaTramite> empresas_tramite,int totalRegistros)
        {

            this.empresas_tramite = empresas_tramite;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}