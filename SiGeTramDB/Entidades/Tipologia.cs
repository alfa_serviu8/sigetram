﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace SiGeTramDB.Entidades
{
    public class Tipologia
    {
        public int Id_Tipologia { set; get; }
        public string Nombre_Tipologia { set; get; }
        public int Id_Tipo_Subsidio { set; get; }
        public string Nombre_Tipo_Subsidio { set; get; }
        

        public Tipologia(int Id_Tipologia, string Nombre_Tipologia, int Id_Tipo_Subsidio, string Nombre_Tipo_Subsidio)
        {
            this.Id_Tipologia = Id_Tipologia;
            this.Nombre_Tipologia = Nombre_Tipologia;
            this.Id_Tipo_Subsidio = Id_Tipo_Subsidio;
            this.Nombre_Tipo_Subsidio = Nombre_Tipo_Subsidio;
        }

        
    }
}