﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class TipoElemento 
    {
        public string Id_Tipo_Elemento { set; get; }

        public int Vigente { set; get; }

               
        public TipoElemento(string idTipoElemento,int vigente)
        {


            this.Id_Tipo_Elemento = idTipoElemento;
            this.Vigente = vigente;
           
            

        }
            public TipoElemento(SqlDataReader dr)
        {
           
                this.Id_Tipo_Elemento = dr["Id_Tipo_Elemento"].ToString();
                this.Vigente = Convert.ToInt32(dr["Vigente"].ToString());
               

        }

        public bool EsUserTask()
        {
          return  this.Id_Tipo_Elemento == "userTask";

        }

        public bool EsStartEvent()
        {
            return this.Id_Tipo_Elemento == "startEvent";

        }

        public bool EsEndEvent()
        {
            return this.Id_Tipo_Elemento == "endEvent";

        }

        public TipoElemento()
        {


           

        }

        public SqlCommand Listado()
        {
            var query = new SqlCommand("Empresa_Tramite_Listado");

            query.CommandType = CommandType.StoredProcedure;
            //query.Parameters.Add(new SqlParameter("@Id_Empresa_Tramite", this.Id_Empresa_Tramite));
            //query.Parameters.Add(new SqlParameter("@Rut_Empresa", this.Rut_Empresa));
            //query.Parameters.Add(new SqlParameter("@Id_Tramite", this.Id_Tramite));


            return query;
        }



    }

}