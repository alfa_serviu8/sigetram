﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class EmpresaTramiteBuscada
    {
        public EmpresaTramite empresa_tramite { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public EmpresaTramiteBuscada(EmpresaTramite et,PaginaImp p)
        {

            this.empresa_tramite = et;
            this.pagina = p;
                           
        }

        
        public EmpresaTramiteBuscada()
        {

        }


    }

}