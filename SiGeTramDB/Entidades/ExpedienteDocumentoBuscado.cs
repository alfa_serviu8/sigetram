﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class ExpedienteDocumentoBuscado
    {
        public ExpedienteDocumento expedienteDocumento { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public ExpedienteDocumentoBuscado(ExpedienteDocumento ed,PaginaImp p)
        {

            this.expedienteDocumento = ed;
            this.pagina = p;
                           
        }

        
        public ExpedienteDocumentoBuscado()
        {

        }


    }

}