﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class DocumentoBuscado
    {
        public Documento documento { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public DocumentoBuscado(Documento d,PaginaImp pag)
        {

            this.documento = d;
            this.pagina = pag;
                           
        }

        public DocumentoBuscado(Documento d)
        {

            this.documento = d;
            this.pagina = new PaginaImp(1,10000);

        }
        public DocumentoBuscado()
        {

        }


    }

}