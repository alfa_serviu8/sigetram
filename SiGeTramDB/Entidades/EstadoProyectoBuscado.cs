﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class EstadoProyectoBuscado
    {
        public EstadoProyecto estadoProyecto { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public EstadoProyectoBuscado(EstadoProyecto ep)
        {

            this.estadoProyecto = ep;
            this.pagina = new PaginaImp(); 
                           
        }

        public EstadoProyectoBuscado(EstadoProyecto ep,PaginaImp pagina)
        {

            this.estadoProyecto = ep;
            this.pagina = pagina;

        }

        public EstadoProyectoBuscado()
        {

        }


    }

}