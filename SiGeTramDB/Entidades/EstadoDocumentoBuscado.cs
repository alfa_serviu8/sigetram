﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class EstadoDocumentoBuscado
    {
        public EstadoDocumento estadoDocumento { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public EstadoDocumentoBuscado(EstadoDocumento ed)
        {

            this.estadoDocumento = ed;
            this.pagina = new PaginaImp(); 
                           
        }

        public EstadoDocumentoBuscado(EstadoDocumento ed,PaginaImp pagina)
        {

            this.estadoDocumento = ed;
            this.pagina = pagina;

        }

        public EstadoDocumentoBuscado()
        {

        }


    }

}