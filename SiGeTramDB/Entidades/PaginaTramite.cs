﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaTramite
    {
        public List<Tramite> tramites { set; get; }
        public int totalRegistros { set; get; }

        public PaginaTramite(List<Tramite> tramites,int totalRegistros)
        {

            this.tramites= tramites;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}