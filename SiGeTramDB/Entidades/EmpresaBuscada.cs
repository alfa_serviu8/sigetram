﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class EmpresaBuscada
    {
        public Empresa empresa { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public EmpresaBuscada(Empresa e,PaginaImp p)
        {

            this.empresa = e;
            this.pagina = p;
                           
        }

        
        public EmpresaBuscada()
        {

        }


    }

}