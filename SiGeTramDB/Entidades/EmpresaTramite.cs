﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class EmpresaTramite
    {
        public int Id_Empresa_Tramite { set; get; }
        public int Id_Tramite { set; get; }
        public int Rut_Empresa { set; get; }
        


        public EmpresaTramite(int Id_Empresa_Tramite, int Id_Tramite,int Rut_Empresa)
        {
            this.Rut_Empresa = Rut_Empresa;
            this.Id_Empresa_Tramite = Id_Empresa_Tramite;
            this.Id_Tramite = Id_Tramite;
            
        }

        public EmpresaTramite(SqlDataReader dr)
        {
            this.Rut_Empresa = Convert.ToInt32(dr["Rut_Empresa"]);
            this.Id_Empresa_Tramite = Convert.ToInt32(dr["Id_Empresa_Tramite"]); 
            this.Id_Tramite = Convert.ToInt32(dr["Id_Tramite"]);
           
        }

        public EmpresaTramite()
        {
            
        }
                 
       


             


        //public void Agregar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Empresa_Patrocinante_Agregar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Rut_Empresa", this.Rut_Empresa));
        //            query.Parameters.Add(new SqlParameter("@Dv", this.Dv));
        //            query.Parameters.Add(new SqlParameter("@Razon_Social", this.Razon_Social));
        //            query.Parameters.Add(new SqlParameter("@Nombre_Fantasia", this.Nombre_Fantasia));
        //            query.Parameters.Add(new SqlParameter("@Email_Particular", this.Email_Particular));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public void Actualizar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Empresa_Patrocinante_Actualizar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Rut_Empresa", this.Rut_Empresa));
        //            query.Parameters.Add(new SqlParameter("@Razon_Social", this.Razon_Social));
        //            query.Parameters.Add(new SqlParameter("@Nombre_Fantasia", this.Nombre_Fantasia));
        //            query.Parameters.Add(new SqlParameter("@Email_Particular", this.Email_Particular));
        //            query.Parameters.Add(new SqlParameter("@Es_Empresa_Patrocinante", this.Es_Empresa_Patrocinante));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}


        //public void ResetearClave(out bool claveReseteada,out string mensajeResultado)
        //{
        //    SqlConnection con = new ConeccionComun().coneccion();
        //    claveReseteada = true;
        //    mensajeResultado = "La clave se reseteó con éxito";

        //    string sConsulta = "Update Cliente " +
        //                           "Set pass_sistemas=@nueva_clave " +
        //                           "Where Rut=@Rut_Empresa";

        //    try
        //    {
        //        using (con)
        //        {
        //            con.Open();

        //            var query = new SqlCommand(sConsulta,con);


        //            query.CommandType = CommandType.Text;


        //            query.Parameters.Add(new SqlParameter("@Rut_Empresa", this.Rut_Empresa));
        //            query.Parameters.Add(new SqlParameter("@nueva_clave", this.ClaveGenerica()));


        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mensajeResultado = "Hubo un problema en la base de datos descrito por: " + ex.Message;
        //        claveReseteada = false;
        //        throw;
        //    }





        //}

        //public string ClaveGenerica()
        //{

        //    string rutString = this.Rut_Empresa.ToString();
        //    int primerDigito = 0;
        //    int segundoDigito = 1;
        //    int tercerDigito = rutString.Length - 4;
        //    int cuartoDigito = rutString.Length - 3;
        //    int quintoDigito = rutString.Length - 2;
        //    int sextoDigito = rutString.Length - 1;

        //    string clave =  rutString[primerDigito].ToString()  +
        //                    rutString[segundoDigito].ToString() +
        //                    rutString[tercerDigito].ToString()  +
        //                    rutString[cuartoDigito].ToString()  +
        //                    rutString[quintoDigito].ToString()  +
        //                    rutString[sextoDigito].ToString();


        //    return clave;

        //}


        //public void Eliminar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Empresa_Patrocinante_Eliminar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Rut_Empresa", this.Rut_Empresa));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

    }
}