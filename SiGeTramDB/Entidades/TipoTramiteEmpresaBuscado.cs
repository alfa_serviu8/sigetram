﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class TipoTramiteEmpresaBuscado
    {
        public TipoTramiteEmpresa tipoTramiteEmpresa { set; get; }
        public PaginaImp pagina { set; get; }
                 
        
        public TipoTramiteEmpresaBuscado(TipoTramiteEmpresa tte,PaginaImp pag)
        {

            this.tipoTramiteEmpresa= tte;
            this.pagina = pag;
                           
        }

        public TipoTramiteEmpresaBuscado(TipoTramiteEmpresa tte)
        {

            this.tipoTramiteEmpresa = tte;
            this.pagina = new PaginaImp(1, 10);

        }

        public TipoTramiteEmpresaBuscado()
        {

           
        }



    }

}