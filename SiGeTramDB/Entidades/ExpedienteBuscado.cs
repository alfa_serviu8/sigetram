﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class ExpedienteBuscado
    {
        public Expediente expediente { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public ExpedienteBuscado(Expediente e,PaginaImp p)
        {

            this.expediente = e;
            this.pagina = p;
                           
        }

        
        public ExpedienteBuscado()
        {

        }


    }

}