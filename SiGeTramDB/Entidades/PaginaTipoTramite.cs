﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaTipoTramite
    {
        public List<TipoTramite> tiposTramite { set; get; }
        public int totalRegistros { set; get; }

        public PaginaTipoTramite(List<TipoTramite> tiposTramite,int totalRegistros)
        {

            this.tiposTramite = tiposTramite;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}