﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTramDB.Entidades
{
    public class EstadoProyecto
    {
        public int Id_Estado_Proyecto { set; get; }
        public string Nombre_Estado_Proyecto { set; get; }
 

        public EstadoProyecto(int Id_Estado_Proyecto, string Nombre_Estado_Proyecto)
        {
            this.Id_Estado_Proyecto = Id_Estado_Proyecto;
            this.Nombre_Estado_Proyecto = Nombre_Estado_Proyecto;
        }
    }
}