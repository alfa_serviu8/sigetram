﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class TipoExpedienteBuscado
    {
        public TipoExpediente tipoExpediente { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public TipoExpedienteBuscado(TipoExpediente te,PaginaImp p)
        {

            this.tipoExpediente = te;
            this.pagina = p;
                           
        }

       
        public TipoExpedienteBuscado()
        {

        }


    }

}