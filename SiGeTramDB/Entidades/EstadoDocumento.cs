﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.IO.Font;
using iText.Kernel.Font;
using iText.Kernel.Events;
using iText.IO.Image;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Borders;
using iText.Kernel.Colors;

namespace SiGeTramDB.Entidades
{
    public class EstadoDocumento
    {
        public int Id_Estado_Documento { set; get; }
        public string Nombre_Estado_Documento { set; get; }
        
        
        public EstadoDocumento(int Id, string Nombre)
        {
            this.Id_Estado_Documento = Id;
            this.Nombre_Estado_Documento = Nombre;
            
        }


        public EstadoDocumento(SqlDataReader dr)
        {
            this.Id_Estado_Documento = Convert.ToInt32(dr["Id_Estado_Documento"]);
            this.Nombre_Estado_Documento = dr["Nombre_Estado_Documento"].ToString();
            
        }

       
        public EstadoDocumento()
        {
         
           

        }
                    

       

    }
}