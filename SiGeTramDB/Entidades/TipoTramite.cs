﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class TipoTramite  
        {
        public int Id_Tipo_Tramite { set; get; }

        public string Nombre_Tipo_Tramite { set; get; }

        public string Descripcion { set; get; }

        public int Vigente { set; get; }

        public TipoTramite(int idTipoTramite,string Nombre_Tipo_Tramite,string Descripcion,int Vigente)
        {


            this.Id_Tipo_Tramite = idTipoTramite;
            
            this.Nombre_Tipo_Tramite = Nombre_Tipo_Tramite;

            this.Descripcion = Descripcion;


        }
            public TipoTramite(SqlDataReader dr)
        {
           
                this.Id_Tipo_Tramite = Convert.ToInt32(dr["Id_Tipo_Tramite"]);
                this.Nombre_Tipo_Tramite = dr["Nombre_Tipo_Tramite"].ToString();
                this.Descripcion= dr["Descripcion"].ToString();
                this.Vigente = Convert.ToInt32(dr["Vigente"]);

        }


        public TipoTramite()
        {


           

        }

      


    }

}