﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaEntidad:IPaginaEntidad
    {
        public List<IEntidad> listado { set; get; }
        public int totalRegistros { set; get; }

        public PaginaEntidad(List<IEntidad> listado,int totalRegistros)
        {

            this.listado = listado;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}