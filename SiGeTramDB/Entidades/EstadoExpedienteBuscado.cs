﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class EstadoExpedienteBuscado
    {
        public EstadoExpediente estadoExpediente { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public EstadoExpedienteBuscado(EstadoExpediente ee)
        {

            this.estadoExpediente = ee;
            this.pagina = new PaginaImp(); 
                           
        }

        public EstadoExpedienteBuscado(EstadoExpediente ee,PaginaImp pagina)
        {

            this.estadoExpediente = ee;
            this.pagina = pagina;

        }

        public EstadoExpedienteBuscado()
        {

        }


    }

}