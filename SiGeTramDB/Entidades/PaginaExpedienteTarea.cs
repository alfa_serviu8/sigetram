﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaExpedienteTarea
    {
        public List<ExpedienteTarea> expedienteTareas { set; get; }
        public int totalRegistros { set; get; }

        public PaginaExpedienteTarea(List<ExpedienteTarea> et,int totalRegistros)
        {

            this.expedienteTareas = et;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}