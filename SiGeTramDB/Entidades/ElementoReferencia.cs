﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class ElementoReferencia 
    {
        public int Id_Elemento { set; get; }

        public int Id_Elemento_Siguiente { set; get; }
        
        public Elemento ElementoSiguiente { set; get; }

        public int Condicion { set; get; }



        public ElementoReferencia()
        {


           
        }

        public ElementoReferencia(int idElemento,int idElementoSiguiente,int condicion)
        {
            this.Id_Elemento = idElemento;
            this.Id_Elemento_Siguiente = idElementoSiguiente;
            this.Condicion = condicion;



        }

        public ElementoReferencia(int idElemento, int idElementoSiguiente)
        {
            this.Id_Elemento = idElemento;
            this.Id_Elemento_Siguiente = idElementoSiguiente;
            this.Condicion = -1;
            
        }


        public ElementoReferencia(SqlDataReader dr)
        {
           
                this.Id_Elemento = Convert.ToInt32(dr["Id_Elemento"]);
               
                this.Id_Elemento_Siguiente = Convert.ToInt32(dr["Id_Elemento_Siguiente"]);
             
                this.Condicion = Convert.ToInt32(dr["Condicion"]);

          

        }

              

       

    }

}