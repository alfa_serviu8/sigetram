﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace SiGeTramDB.Entidades
{
    public class TipoSubsidio
    {
        public int Id_Tipo_Subsidio { set; get; }
        public string Codigo_Tipo_Subsidio { set; get; }
        public string Nombre_Tipo_Subsidio { set; get; }
        public int Vigente { set; get; }
       

        public TipoSubsidio(int Id_Tipo_Subsidio, string Codigo_Tipo_Subsidio, string Nombre_Tipo_Subsidio, int Vigente)
        {
            this.Id_Tipo_Subsidio = Id_Tipo_Subsidio;
            this.Codigo_Tipo_Subsidio = Codigo_Tipo_Subsidio;
            this.Nombre_Tipo_Subsidio = Nombre_Tipo_Subsidio;
            this.Vigente = Vigente;
        }

        //public void Agregar( )
        //{
        //    try
        //    {
        //        using (this.con)  {
        //            this.con.Open();

        //            var query = new SqlCommand("Tipo_Subsidio_Agregar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Codigo_Tipo_Subsidio", this.Codigo_Tipo_Subsidio));
        //            query.Parameters.Add(new SqlParameter("@Nombre_Tipo_Subsidio", this.Nombre_Tipo_Subsidio));
        //            query.Parameters.Add(new SqlParameter("@Vigente", this.Vigente));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

       
    }
}