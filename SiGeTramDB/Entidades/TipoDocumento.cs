﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTramDB.Entidades
{
    public class TipoDocumento
    {
        public int Id_Tipo_Documento { set; get; }
        public string Nombre_Tipo_Documento { set; get; }
        public int Vigente { set; get; }

        public TipoDocumento(int Id_Tipo_Documento, string Nombre_Tipo_Documento, int Vigente)
        {
            this.Id_Tipo_Documento = Id_Tipo_Documento;
            this.Nombre_Tipo_Documento = Nombre_Tipo_Documento;
            this.Vigente = Vigente;
        }


        public TipoDocumento(SqlDataReader dr)
        {
            this.Id_Tipo_Documento = Convert.ToInt32(dr["Id_Tipo_Documento"]);
            this.Nombre_Tipo_Documento = dr["Nombre_Tipo_Documento"].ToString();
            this.Vigente = Convert.ToInt32(dr["Vigente"]);
        }

       
        public TipoDocumento()
        {
         
           
        }

    }
}