﻿ 
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTramDB.Entidades
{
    public class TipoExpediente
    {
        public int Id_Tipo_Expediente { set; get; }

        public int Id_Proceso { set; get; }
        public string Nombre_Tipo_Expediente { set; get; }

        public string Nombre_Proceso { set; get; }
        public int Vigente { set; get; }
    

        public TipoExpediente(int Id_Tipo_Expediente, string Nombre_Tipo_Expediente, int Vigente,int Id_Proceso)
        {
            this.Id_Tipo_Expediente = Id_Tipo_Expediente;
            this.Nombre_Tipo_Expediente = Nombre_Tipo_Expediente;
            this.Vigente = Vigente;
            this.Id_Proceso = Id_Proceso;
        }

        public TipoExpediente(int Id_Proceso)
        {
            this.Id_Tipo_Expediente = 0;
            this.Nombre_Tipo_Expediente = "";
            this.Vigente = 1;
            this.Id_Proceso = Id_Proceso;
        }

        public TipoExpediente(SqlDataReader dr)
        {
            this.Id_Tipo_Expediente = Convert.ToInt32(dr["Id_Tipo_Expediente"]);
            this.Id_Proceso = Convert.ToInt32(dr["Id_Proceso"]);
            this.Nombre_Tipo_Expediente = dr["Nombre_Tipo_Expediente"].ToString();
            this.Nombre_Proceso = dr["Nombre_Proceso"].ToString();
            this.Vigente = Convert.ToInt32(dr["Vigente"]);
        }

        public TipoExpediente()
        {

        }
    }
}