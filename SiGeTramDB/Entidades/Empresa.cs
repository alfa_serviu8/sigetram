﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class Empresa
    {
        public int Rut_Empresa { set; get; }

        public int Es_Empresa_Patrocinante { set; get; }
        public string Dv { set; get; }
        public string Razon_Social { set; get; }
        public string Nombre_Fantasia { set; get; }
        public string Nombre_Empresa { set; get; }
        public string Email_Particular { set; get; }

    
        public int Fono_Fijo { set; get; }

        public int Celular { set; get; }

        public string Pass_Sistemas { set; get; }


        public Empresa(int Rut_Empresa, string Dv, string Razon_Social, string Nombre_Fantasia, string Nombre_Empresa, string Email_Particular, string Pass_Sistemas, int Fono_Fijo,int Celular)
        {
            this.Rut_Empresa = Rut_Empresa;
            this.Dv = Dv;
            this.Razon_Social = Razon_Social;
            this.Nombre_Fantasia = Nombre_Fantasia;
            this.Nombre_Empresa = Nombre_Empresa;
            this.Email_Particular = Email_Particular;
            this.Pass_Sistemas = Pass_Sistemas;
            this.Fono_Fijo = Fono_Fijo;
            this.Celular = Celular;
           
        }
        public Empresa(int rut)
        {
            this.Rut_Empresa = rut;
            this.Nombre_Empresa = "";
            this.Es_Empresa_Patrocinante = -1;
        

        }

        public Empresa(SqlDataReader dr)
        {
            this.Rut_Empresa = Convert.ToInt32(dr["Rut_Empresa"]);
            this.Es_Empresa_Patrocinante = Convert.ToInt32(dr["Es_Empresa_Patrocinante"]);
            this.Dv = dr["Dv"].ToString();
            this.Razon_Social = dr["Razon_Social"].ToString();
            this.Nombre_Fantasia = dr["Nombre_Fantasia"].ToString();
            this.Nombre_Empresa = dr["Nombre_Empresa"].ToString();
            this.Email_Particular = dr["Email_Particular"].ToString(); ;
            this.Pass_Sistemas = dr["Clave"].ToString();
            this.Fono_Fijo= Convert.ToInt32(dr["Fono_Fijo"]);
            this.Celular = Convert.ToInt32(dr["Celular"]);


        }

        public Empresa()
        {
            
        }

        
        public Correo CorreoEnvioExpediente(Funcionario f ,Expediente e)
        {
            string correoSistema = "SiGeTram@minvu.cl";
            string alias = "SiGeTram";
            string asunto = "SERVIU BioBio";
            string body = "<br/><br/><br/> " + f.SaludoEnHtml()+ " La empresa: " + this.Nombre_Empresa + " envió un expediente, para revisarlo debe hacer click en " +
                e.urlRevision() + "<br/><br/><br/>";

            Correo c = new Correo(correoSistema, alias, f.Email, body, asunto);

            return c;




        }







        //public void Agregar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Empresa_Patrocinante_Agregar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Rut_Empresa", this.Rut_Empresa));
        //            query.Parameters.Add(new SqlParameter("@Dv", this.Dv));
        //            query.Parameters.Add(new SqlParameter("@Razon_Social", this.Razon_Social));
        //            query.Parameters.Add(new SqlParameter("@Nombre_Fantasia", this.Nombre_Fantasia));
        //            query.Parameters.Add(new SqlParameter("@Email_Particular", this.Email_Particular));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public void Actualizar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Empresa_Patrocinante_Actualizar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Rut_Empresa", this.Rut_Empresa));
        //            query.Parameters.Add(new SqlParameter("@Razon_Social", this.Razon_Social));
        //            query.Parameters.Add(new SqlParameter("@Nombre_Fantasia", this.Nombre_Fantasia));
        //            query.Parameters.Add(new SqlParameter("@Email_Particular", this.Email_Particular));
        //            query.Parameters.Add(new SqlParameter("@Es_Empresa_Patrocinante", this.Es_Empresa_Patrocinante));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}


        //public void ResetearClave(out bool claveReseteada,out string mensajeResultado)
        //{
        //    SqlConnection con = new ConeccionComun().coneccion();
        //    claveReseteada = true;
        //    mensajeResultado = "La clave se reseteó con éxito";

        //    string sConsulta = "Update Cliente " +
        //                           "Set pass_sistemas=@nueva_clave " +
        //                           "Where Rut=@Rut_Empresa";

        //    try
        //    {
        //        using (con)
        //        {
        //            con.Open();

        //            var query = new SqlCommand(sConsulta,con);


        //            query.CommandType = CommandType.Text;


        //            query.Parameters.Add(new SqlParameter("@Rut_Empresa", this.Rut_Empresa));
        //            query.Parameters.Add(new SqlParameter("@nueva_clave", this.ClaveGenerica()));


        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mensajeResultado = "Hubo un problema en la base de datos descrito por: " + ex.Message;
        //        claveReseteada = false;
        //        throw;
        //    }





        //}



        public void EstablecerClaveGenerica()
        {

            string rutString = this.Rut_Empresa.ToString();
            int primerDigito = 0;
            int segundoDigito = 1;
            int tercerDigito = rutString.Length - 4;
            int cuartoDigito = rutString.Length - 3;
            int quintoDigito = rutString.Length - 2;
            int sextoDigito = rutString.Length - 1;

            string clave = rutString[primerDigito].ToString() +
                            rutString[segundoDigito].ToString() +
                            rutString[tercerDigito].ToString() +
                            rutString[cuartoDigito].ToString() +
                            rutString[quintoDigito].ToString() +
                            rutString[sextoDigito].ToString();

            this.Pass_Sistemas = clave;

          

        }


        //public void Eliminar()
        //{
        //    try
        //    {
        //        using (this.con)
        //        {
        //            this.con.Open();

        //            var query = new SqlCommand("Empresa_Patrocinante_Eliminar", con);

        //            query.CommandType = CommandType.StoredProcedure;

        //            query.Parameters.Add(new SqlParameter("@Rut_Empresa", this.Rut_Empresa));

        //            query.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

    }
}