﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaEstadoProyecto
    {
        public List<EstadoProyecto> estadosProyecto { set; get; }
        public int totalRegistros { set; get; }

        public PaginaEstadoProyecto(List<EstadoProyecto> ep,int totalRegistros)
        {

            this.estadosProyecto = ep;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}