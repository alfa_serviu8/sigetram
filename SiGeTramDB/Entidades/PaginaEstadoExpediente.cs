﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaEstadoExpediente
    {
        public List<EstadoExpediente> estadosExpediente { set; get; }
        public int totalRegistros { set; get; }

        public PaginaEstadoExpediente(List<EstadoExpediente> estadosExpediente,int totalRegistros)
        {

            this.estadosExpediente = estadosExpediente;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}