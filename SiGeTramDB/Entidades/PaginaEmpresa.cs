﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class PaginaEmpresa
    {
        public List<Empresa> empresas { set; get; }
        public int totalRegistros { set; get; }

        public PaginaEmpresa(List<Empresa> empresas,int totalRegistros)
        {

            this.empresas = empresas;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}