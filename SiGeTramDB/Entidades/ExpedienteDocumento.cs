﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTramDB.Entidades
{
    public class ExpedienteDocumento
    {
        public int Id { set; get; }
        public int Id_Expediente { set; get; }
        public int Id_Documento { set; get; }
        public string Nombre_Documento { set; get; }

        public Expediente Expediente { set; get; }

        public Documento Documento { set; get; }
   

        public ExpedienteDocumento(int Id, int Id_Expediente, int Id_Documento, string Nombre_Documento )
        {
            this.Id = Id;
            this.Id_Expediente = Id_Expediente;
            this.Id_Documento = Id_Documento;
            this.Nombre_Documento = Nombre_Documento;
            
        }

        public ExpedienteDocumento(int Id_Expediente, int Id_Documento)
        {
            
            this.Id_Expediente = Id_Expediente;
            this.Id_Documento = Id_Documento;
            

        }

        public ExpedienteDocumento(SqlDataReader dr)
        {


            this.Id_Expediente = Convert.ToInt32(dr["Id_Expediente"]);
            this.Expediente = new Expediente(dr);
            this.Id_Documento = Convert.ToInt32(dr["Id_Documento"]);
            this.Nombre_Documento = dr["Nombre_Documento"].ToString();



        }


    }
}
 
            
                     

