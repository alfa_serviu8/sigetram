﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeFunDB;
using SiGeFunDB.Entidades;

namespace SiGeTramDB.Entidades
{
    public class TramiteBuscado
    {
        public Tramite tramite { set; get; }

          
        public PaginaImp pagina { set; get; }
                 
        
        public TramiteBuscado(Tramite t,PaginaImp p)
        {

            this.tramite = t;
            this.pagina = p;
                           
        }

        
        public TramiteBuscado()
        {

        }


    }

}